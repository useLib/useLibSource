/*****
 *
 *		Automatic library for software ergonomic interaction optimization:
 *		@ usability for complex working systems: autoformating selection controls, 
 *		@ several meta controls with formated input (3-month calendar, time, IBAN,
 *		@ Adress, units, countries etc.), automatic supplement (postalcode, BIC 
 *		@ etc.), cursor steering, expert level and layout steering etc., even for
 *		@ old application systems. Adaptability via configuration file.
 *		@ © Dr. Dirk Fischer, use-Optimierung, Köln/Cologne, 2021.
 *		@ Possibly other copyrights reported at the place. 
 * 
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *****/
/* CHANGES
	1.07: add Annex definition (char or string behind number or float) in data-ul 
	1.08: 'Ladies and Gentlemen' to first position in person (default if not required)
	1.08: added	on all intern loadJSONCSS callback functions "if ( D.contains( e ))",
	because e could be deleted by re-initialization of content.
	1.08: add/change some intern types to create correct Annex definitions i. e. for 'unit' type
	1.08: set correct check of type on in intern form submit from i. e. singeleSelect 
	1.08: resetElement wrong access to htmlFor
	1.08: modal forms may have dynamic width on window width and position
	1.08: modal forms layer where not counted
	1.08: add '&lg=[userLanguage]' to useLib submit
	1.09: _afterResponse := not checked if a JSON is given back
	1.09: __getInput := take rows as attribute from size correct
	1.09: _initLabel := no reset element on doubleclick checkbox and radio
	1.09: initTooltips := better seach for hover
	1.09: singleSelect.getContent etc. := width calc with scale if sized GUI
	1.09: toggleNavigation := correct id for ownText in useEditor
	1.09: _getCharList := take correct charRegexp
	1.09: metaReaction.period.doReaction.onblur := set days correct 
	1.09: metaReaction.list.doReaction.onchange := set ∑ foundAmount > showAmount
	1.10: clearForm := clear select-multi also
	1.10: dynList.action := no failure if no block is selected
	1.10: metaReaction.singleSelect->_getValue := no line break in values
	1.11: preload of sounds in initUseLib => sound(s) is just .play()...
	1.11: setJSONValues->__shrink => join value order from JSONexpands definition
	1.11: setJSONValues->__expand => split value order from JSONexpands definition
	1.11: new element type: 'title' instead 'person'. 'person to' to ensemble
	1.11: escapeUnicode := escape also charcodes over 127
	1.11: doOwnInput := get definition of selectable extends from lang.ownInputExtend
			('vat'-steps and '.title')
	1.11: setOwnInput := as function can be called from setEleValue()
	1.11: setEleValue := a bit of optimization and control parameter ALL
	1.11: setRequest->_getExtend := easier initialisation and better dynamic behavior
	1.11: pattern2Char->_unmask := better cleaning of pattern before char analyze
	1.11: delete database2JSON() was old, I overlooked it...
	1.11: openPopup := complete with popUp hint
	1.11: normCal := case differentiation ical to analyse or intern calendar format
	1.11: $P := short'ner for repeats of .parentNode... 
	1.11: taskstep := full depending on tasksteps definition
	1.11: shortnChars := pattern where correct, but chrome throws errors => double char
	1.11: ownInput := timeout workaround for chrome/edge caused on click on mousedown
	1.11: metaReaction.password := if not useLib.security (SHA3-512) use of crypto.subtle
			(SHA-512) with conf.hashPassword as salt
	1.11: consistent use of data-ul
	1.11: metaReaction.multiselect->doKeys := SPACE inverts active element (forgotten :-[
	1.11: selfgrouper->initDynamicExtends := eliminate it and dynamicExtends
	1.11: selfgrouper->_updateDynamicSelect() := optimized
	1.11: checkWikiLink->_open := conf.useLibPath instead 'https://www.use-optimierung.de'
	1.11: metaReaction.time->getcontent := no className in fieldset
	1.11: getFirst := eliminated and integrated in setFocus()
	1.12: pattern2Char->_getCharString := caused on	shortnChars correct decription
*/
if (!navigator.language)
	navigator.language = navigator.userLanguage;
var useLib = (function( W, D, U ){	// window, document, undefined
//@ KONFIGURATION:: there may be changed some defaults, to customize...
if ( !window.useLibConf ) {
	console.warn(
		"useLib: configuration problem: No useLib configuration defined => aborted!"
	);
	return;
}
var
	ULC = W.useLibConf
	, develop = 'designed and developed by'
	, copyright = "© Dr. Dirk Fischer, Cologne"
	, homepage = "https://www.use-optimierung.de"
	, useLibVersion = '1.12'
	, conf = ULC.conf
	, wikiSigns = ULC.wikiSigns
	, wikiEnsemble = ULC.wikiEnsemble
	, cssNS = ULC.cssNS
	, htmlSupport = ULC.htmlSupport
	, JSONexpands = ULC.JSONexpands
	, langConf = ULC.langConf
	;
	conf.designRight = "useLib, Cologne";
//@ useLib is done to admit the interaction and to make it easier for the user
//@ therefore it is necessary to react on changes of different Attributes of
//@ elements. Also you should no, that a lot of experimental interaction and as
//@ result a lot of case differentiation is nessesary. Initialisations are done step
//@ by step and not via class definitions etc. The influences of the different
//@ browsers are quite difficult to control, also to find a 'standard' solution
//@ with no browser specific programming. Well, those touch devices...
//@ Also it is tried to isolate all methods so that other or old scripts (almost)
//@ not influenced. So are i.e. prototypes avoided, but for example the isolaion
//@ of the keyboard stearing fails, because it must be integrative and standardize
//@ different browser behaviors. So if you unhappy with some non informatic like
//@ programm parts, they are pragmatic.
//@ REMARK 1: onchange events e.g. only fire on user interaction and not if the
//@ value is changed by programm code. The mutationobserver only reacts on DOM
//@ changes via setAttribut or simular. To catch all types of changes some kind
//@ like the Object.watch method is the only solution, that is not supported by
//@ any browser. So the following polyfill is needed (Thanx!). It also makes the
//@ code easier. object.watch is deprecated, but needed to control some changes
//@ on elements, because they don't fire events especially if modified on js code!
//@ REMARK 2: It was nessesary to expand Eli's approach from 2012-04-03
//@ (http://eligrey.com) by already existing getters and setter because i.e
//@ element.value didn't react on keyboard input... At least watch is used here
//@ only on oldGetters and oldSetters!

Object.defineProperty( Object.prototype, "watch", {
	  enumerable: false
	, configurable: true
	, writable: false
	, value: function ( prop, handler ) {
		var
		  oldval = this[ prop ]
		, newval = oldval
		, hasIt = this.__lookupGetter__
		, oldGetter = hasIt ? this.__lookupGetter__( prop ) : false
		, oldSetter = hasIt ? this.__lookupSetter__( prop ) : false
		, getter = function () {
			return oldGetter ? oldGetter.call(this) : newval;
		}
		, setter = function( val ) {
			oldval = newval;
			newval = handler.call( this, prop, oldval, val );
			return oldSetter ? oldSetter.call( this, newval ) : newval;
		}
		;
		if ( delete this[ prop ] ) { // can't watch constants
			Object.defineProperty( this, prop, {
				  get: getter
				, set: setter
				, enumerable: true
				, configurable: true
			});
		}
	}
});

(function() {
	for ( var l in langConf ) {
		_split( langConf[ l ][ 'dynActions'], 0 );
		_split( langConf[ l ][ 'dynActions'], 1 );
	}
	function _split( o, _n ) {
		if ( o[ _n ].split )
			o[ _n ] = o[ _n ].split( ',' );
	}
})();

var shortnChars = (function(){
	var base = {
		//@ for short definition of allowed chars for a easier internationalisation
		//@ : just write Gg, Ff, Ss, Ww, Ee after a-z
		//@ REMARK: the first correct solution was a leading '-', but chrome throws errors
		g : "\\u00C4\\u00D6\\u00DC\\u00E4\\u00F6\\u00FC\\u00DF" // german: ÄÖÜäöüß
		, f: "\\u00C0\\u00C2\\u00C6-\\u00CB\\u00CE\\u00CF\\u00D4\\u00D9\\u00DB"
		+ "\\u00E0\\u00E2\\u00E6-\\u00EB\\u00EE\\u00EF\\u00F4\\u00F9\\u00FB"
		+ "\\u0152\\u0153"	// french
		, s: "\\u00A1\\u00AA\\u00BF\\u00C1-\\u00C3\\u00C7-\\u00CA\\u00CC\\u00CD"
		+ "\\u00D1-\\u00D5\\u00DA\\u00E1-\\u00E3\\u00E7\\u00E9\\u00EA\\u00EC\\u00ED-"
		+ "\\u00EF\\u00F1\\u00F3-\\u00F5\\u00FA\\u00FC"	// spain, italy, portugal
		}
		;
		return {
			'gg' : base.g
			, 'ff' : base.f
			, 'ss' : base.s
			, 'ww' : base.g + base.f + base.s						// western european
			, 'ee' : "\\u00C0-\\u017F"									// most european
			, 'uu' : "-A-Za-z0-9\\/\\)\\(\\+._!~:?#$&'*,;="		// url chars
			, 'mm' : "\\.A-Za-z0-9!#$%&'*+\\/=?^_`{|}~\\-"		// mail chars
		};
	})()
	, typeCheck = function( t, o ) {
		return Object.prototype.toString.call( o ) == '[object ' + t + ']';
	}
	, str2obj = function( t, s, num ) {
	//@ PARAM stringDefinedKeys, separatorBetweenKeys := [' '], numberationStart
	//@ [undefined] for easier for loops and definition ask in configuration
		if ( !t.split )
			return t;
		s = '{"' + t.split( s||' ' ).join( '":1,"' ) + '":1}';
		if ( typeof num !== "undefined" ) {
			num = $I( num );
			s = s.replace( /:1/g, function( _ ) { return ':' + num++ });
		}
		return JSON.parse( s );
	}
	, DOM = (function() {
	var
		isTouch = ( 'ontouchstart' in W ) || W.DocumentTouch && D instanceof DocumentTouch
		, getCoord = function( ev, r ) {
			ev = ev.touches ? ev.touches[ 0 ] : ev;
			r = r || {};
			r.x = ev.pageX;
			r.y = ev.pageY;
			return r;
		}
		, scroll = function( d ) {
			return $I( W[ 'page' + d + 'Offset' ] || _s( D.documentElement ) || _s( D.body ));
			
			function _s( e ) { return !!e ? e[ 'scroll' + ( d == 'Y' ? 'Top' : 'Left' )] : 0; }
		}
		, sizeDrag = -1                 // store for active dragging ans sizing
		, dragStop = true
		, measure = null
		;
		function textWidth( t, cN ) {
			if( D.body ) {
				if ( !measure ) {
					measure = D.createElement( 'div' );
					$A( measure, 'style', 'position:absolute;left:-9999px;visibility:hidden;display:block;' );
					D.body.appendChild( measure );
				}
				measure.className = cN || '';
				measure.innerHTML = t;
			}
			return measure.offsetWidth || 0;
		}

		function getRect( e ) {
		//@ gets structure {x ,y ,w, h} of pixel coordinates and dimensions of element
		var
			r = { x: 0, y: 0, w: $I( e.offsetWidth ), h: $I( e.offsetHeight )}
			;
			for (; e != null;
				r.x += $I( e.offsetLeft ), r.y += $I( e.offsetTop ), e = e.offsetParent || null
			);
			r.left = r.x; r.top = r.y; r.right = r.x + r.w; r.bottom = r.y + r.h;
			return r;
		}

		function setPos( e, x, y, FIX ) {
			$S( e ).position = !!FIX ? 'fixed' :'absolute';
			if ( x !== false )
				$S( e ).left = $I( x ) + 'px';
			if ( y !== false )
				$S( e ).top  = $I( y )  + 'px';
		}

		function setControledPos( o, e ) {
		//@ calculates and sets position of object (o) near mouse or other object (e)
		//@ or centered, if e is not given
		var
			dX = 25     // X / Y offset of object position
			, dY = 19
			, fix = false
			, x
			, y
			,xx=0,yy=0
			;
			o.style.transform = 'none';
			o = typeCheck( 'String', o ) ? $( o ) : o;
			if ( !e || !e.getBoundingClientRect ) {
				fix = true;
				x = max( 0, $I(( W.innerWidth - o.offsetWidth ) / 2 ));
				y = max( 0, $I( W.innerHeight / 20 ));
				if ( $CN.contains( o, cssNS.form ))
					o = o.firstChild;
			}
			else {
				e = e.target ? { left: e.pageX , top: e.pageY, right: e.pageX + dX
				, bottom: e.pageY + dY } : getRect( e );
				_calcPos( o, e );
				// correction of position, if outside of window
				x = min( max( 0, x ), $I( W.innerWidth ) - e.w );
				y = min( max( 0, y ), $I( W.innerHeight ) - e.h );
			}
			setPos( o, x, y, fix );

			function _calcPos( o, e ) {
				if ( e.left > W.innerWidth / 2 )	// right off center => o left off e
					x = e.left - dX - o.offsetWidth;
				else
					x = e.right + dX;
				if ( e.top > W.innerHeight / 2 ) // below center => o above e
					y = e.top - dY - o.offsetHeight;
				else
					y = e.bottom + dY;
			}
		}

		function doSizeDrag( ev, e, dir, wS, hS, wMin, hMin, wMax, hMax, wOff, hOff ) {
		//@ PARAM event, sizingObject, direction: 0 or 3 := all, 1 := horizontal, 2 :=
		//@ PARAM  vertical,widthString, heightString, widthMin, heightMin, widthMax,
		//@ heightMax, widthOff, heightOff := for additional offset i. e. tooltipp
			sizeDrag = {
				e: e			// just in case there is no given element
				, w: $I( $GS( e, wS ))
				, h: $I( $GS( e, hS ))
				, dir: dir || 3
				, wS: wS
				, hS: hS
				, wMin: wMin || 200
				, hMin: hMin || 200
				, wMax: wMax || $I( scroll( 'X' ) + W.innerWidth - 20 ) - e.offsetLeft
				, hMax: hMax || $I( scroll( 'Y' ) + W.innerHeight - 20 ) - e.offsetTop
				, wOff: hOff || 0
				, hOff: hOff || 0
			}
			;
			sizeDrag = getCoord( ev, sizeDrag ); // store x, y mouse/touch-coordinates also
			$E( D, "MU", function _startDrag() {
				dragStop = true;
				$E( D, "MM", _doSizeDrag, 1 );
			});
			$E( D, "MM", _doSizeDrag );

			function _doSizeDrag( ev ) {
			//@ calculate new size of sizeElement depending of mouseMove
			var
				c = getCoord( ev )
				, d = sizeDrag
				;
				if ( d.dir & 1 )
					$S( d.e )[ d.wS ] = max( d.wMin, min( d.wOff + d.w - d.x + c.x, d.wMax )) + 'px';
				if ( d.dir & 2 )
					$S( d.e )[ d.hS ] = max( d.hMin, min( d.hOff + d.h - d.y + c.y, d.hMax )) + 'px';
			}
		}
		
		function sizeStart( ev, e, dir, wMin, hMin, wMax, hMax ) {
		//@ sizing via user interaction
			dragStop = false; 
			doSizeDrag( ev, e || ev.currentTarget.parentNode, dir, 'width', 'height', wMin, hMin );
		}
		function dragStart( ev, e, dir, wMin, hMin, wMax, hMax, wOff, hOff ) {
		//@ dragging via user interaction
			e = e || ev.currentTarget.parentNode;
			$S( e ).transform = 'none';
			dragStop = false;
			doSizeDrag( ev, e, dir, 'left', 'top'
				, wMin ||( 50 - e.offsetWidth ), hMin || -10
				, wMax, hMax, wOff, hOff );
		}

		function scrollIntoView( e, delay, pos ) {
		//@ PARAM: elementToView, delayOfSecondsToStart [0], position 
		//@ [nearestIfNeeded, -1 == center, 1 == start, 'top', 'bottom' ] 
		//@ scrolls elements into view. Options not working in safari and IE =>
		//@ have to figger out if bottom or top (center == top).
		//@ REMARK: in some cases center is better i. e. for fastFeeder
			setTimeout( function() {
			var
				ops = /MSIE|Trident|(^((?!chrome|android|crios|fxios).)*safari)/i.test(
					navigator.userAgent )
				//, r = !!e ? e.getBoundingClientRect() : 0	// outside screen?
				;
				if ( e == D.body ) {
					pos = pos == 'top' ? 0 : pos == 'bottom' ? D.body.scrollHeight
					: $I( pos );
					if ( ops )
						D.body.scrollTo( 0, pos );
					else
						D.body.scrollTo({ top: pos, left: 0, behavior: 'smooth' });
				}
				else if ( !!e ) { //&& r.top < 0 || r.bottom > W.innerHeight ) {
					e.scrollIntoView( ops ? pos != 'bottom' || !!pos || r.top < 0
						: { behavior: "smooth", block: pos == 1 || pos == 'top'
						? 'start' : pos == 'bottom' ? 'end' : pos == -1
						? "center" : "nearest" }
					);
				}
			}
			, $I(( delay || 0 ) * 1000 ));
		}
		
		function noScroll() { W.scrollTo( 0, 0 ); }
		function scrollStop( stop ) { $E( W, 'scroll', noScroll, !!!stop ); }

		function coordInRect( ev, e, m ) {
		//@ PARAM: event, elementOfRect, marginOnRect (optional)
		var
			c = getCoord( ev )
			, r = e.getBoundingClientRect()
			;
			m = m || 0;
			return c.x > r.left - m && c.x < r.right + m
			&& c.y > r.top - m && c.y < r.bottom + m;
		}

		return {
			isTouch : isTouch
			, textWidth: textWidth
			, getRect: getRect
			, getCoord: getCoord
			, coordInRect: coordInRect
			, setPos: setPos
			, setControledPos: setControledPos
			, sizeStart: sizeStart
			, dragStart: dragStart
			, dragStop: dragStop
			, scrollIntoView: scrollIntoView
			, scrollStop: scrollStop
			, getScrollbarWidth: function() {
				textWidth( 'a' );	// init measure...
				measure.style.overflow = 'scroll';
				var s = measure.offsetWidth - measure.clientWidth;
				measure.style.overflow = 'auto';
      		return s;
			}
			, redraw: function( e ) {
			//@ this is a redraw which runs expressions also, instead of nice and very fast:
			//@ function( e ) { e.parentNode.replaceChild( e, e ); }  
			var
				d = e.style.display
				;
				e.style.display = "none";
				if ( !!d )
					e.style.display = d;
				else
					e.style.removeProperty( 'display' );
			}
			, insertBefore: function( e, newNode ) {
				if ( e && newNode )
					newNode = e.parentNode.insertBefore( newNode, e );
				return newNode;
			}
			, insertAfter: function( e, newNode ) {
				if ( e && newNode ) {
					if ( e.nextSibling )
						newNode = e.parentNode.insertBefore( newNode, e.nextSibling );
					else
						newNode = e.parentNode.appendChild( newNode );
				}
				return newNode;
			}
			, removeNode: function( e ) {
				if ( !!e && e.parentNode ) {
				var
					ee = e.cloneNode( false )
					;
					// first delete possible formDefs...
					for ( var i = 0, eF = $Q( e, '[name]'); i < eF.length; i++ )
						if ( eF[ i ].form && formDef[ eF[ i ].form.name ])
							delete formDef[ eF[ i ].form.name ][ eF[ i ].name ];
					// ... then delete childNnodes for ensurence
					e.parentNode.replaceChild( ee, e );
					ee.parentNode.removeChild( ee );
					e = null;     // to prevent memoy leaks...
				}
			}
			, replaceNode: function( e, newNode ) {
				if ( !!e && e.parentNode && !!newNode )
					newNode = e.parentNode.replaceChild( newNode, e );
				return newNode;
			}
		};
	})()
	//@ some short'n local functions incl. checks against failures etc.
	//@ REMARK: it could also be helpful to create a polyfill...
	/*
	$  = getElementById
	$T = getElementsByTagName
	$Q = querySelector(All)
	$I = parseInt
	$D	= getDate and other methods
	$A = manage Attributes := -1 = remove
	$P = get the n-th parentNode ( comes often with 3 and is much shorter)
	$E = manage EventListener := -1 = remove (with short eventType, also for touch)
	$GS = getComputedStyle
	$LS = localStorage := -1 = remove	(with fake, if not supported)
	$CN = classList
	$CE = createElements (special from struct)
	//@ this may be used extern also. In closures it can be easy declared like:
	var u=useLib,$=u.$,$T=u.$T,$Q=u.$Q,$I=u.$I,$D=u.$D,$A=u.$A,$E=u.$E,$GS=u.$GS,$LS=u.$LS,$CN=u.$CN,$CE=u.$CE,repeat=u.repeat,compare=u.compare;
	*/
	, $ = function( s ) {						// instead of getElementById
		return typeCheck( 'String', s ) && s.trim().length ? D.getElementById( s ) : false;
	}
	, $S = function( e ) {						// instead of e.style=...
		return( !!e && !!e.style ? e.style : {});
	}
	, $T = function( e, s, f ) {	         // instead of getElementsByTagName
		e = typeCheck( 'String', e ) ? $( e ) : e;
		e = !!e && !!e.getElementsByTagName ? e.getElementsByTagName( s ) : [];
		return f && e.length ? e[ 0 ] : !!f ? null : e;
	}
	, $Q = function( e, s, t ) {           // instead of querySelector(All)
		e = typeCheck( 'String', e ) ? $( e ) : e;
		// t ? only one return || null
		return !!e && !!e.querySelector && s && s.length
		? e[ 'querySelector' + ( !!t ? '' : 'All' ) ]( s ) : !!t ? null : [];
	}
	// setAttribute :: including remove
	, $A = function( e, n, v ) {
	//@ PARAM element, attributeName, attributeValue := if -1 remove else if empty get
		n = attrList[ n ] || n;
		if (!!e && !!n && !!e.getAttribute ) {
			if ( n == "innerHTML" )
				n = e.innerHTML = v;
			else if ( v === -1 )
				n = e.removeAttribute( n );
			else if ( typeof v == 'string' )
				n = e.setAttribute( n, v );
			else {
				e = e.getAttribute( n ); // returns null or '' if empty or not defined
				n = typeof e == 'string' ? ( /value|data/.test( n ) || e.length
				? e : /selected|checked|disabled/.test( n ) ? n : false )
				: false;
			}
			return n;
		}
		return null;
	}
	, $P = function( e, c ) {
		c = c && c > 0 ? c : 1;
		while( c-- && e )
			e = e.parentNode;
		return e;
	}
	, $I = function( v ) { return parseInt( v, 10 ) || 0; }// make sure it's a integer
	, $D = ( function() {
	var
		D = function( d ) {
		//@ returns a date with all time values set to zero, so there may be added
		//@ timeValues and calculation results would be correct, on strings more are cutted
		//@ ATTENTION: counting of given month starts at 1 (not 0!) 
		//@ REMARK: timezoneoffset is not set, so all calculations are including it 
			if ( !!d && ( typeCheck( 'Date', d ) || !isNaN( d )))
				return new Date( d );
			else if ( !d || !d.length ) {
					d = new Date();
					d.setHours( 0 );
					d.setMinutes( 0 );
					d.setSeconds( 0 );
					return d;
			}
			else if ( typeCheck( 'String', d )) {
				d = d.match( /\d+/g );
				if ( d && d.length > 2 ) {
					if ( d[ 2 ].length == 4 )		// german dateformat? standardize it!
						d.reverse();
					else if ( d[ 0 ].length == 2 ) {
						d[ 0 ] = $I( d[ 0 ]) + 2000;
						d[ 0 ] -= d[ 0 ] > D.y() + conf.smallYearFuture ? 100 : 0;
					}			
				}
				else {
					console.warn(
						"useLib: wrong Date string " + d.join( '-' ) + " (set to today)!"
					);
					return D();
				}
			}
			if ( typeCheck( 'Array', d ))
				return new Date( $I( d[ 0 ]), $I( d[ 1 ]) - 1, $I( d[ 2 ]), 0, 0, 0 );
		}
		;
		D.y = function( d ){ return D( d ).getFullYear(); };	// returns year of date
		D.m = function( d ){ return D( d ).getMonth() + 1; };	// returns month of date start at 1
		D.d = function( d ){ return D( d ).getDate(); };  // returns day of date start at 1
		D.add = function ( d, t, add ) {
		//@ add or substract year, month or day(s) form date
			d = D( d ); 
			d.setHours( 0 );
			d.setMinutes( 0 );
			d.setSeconds( 1 );                       // important for correct calculation!!
			t = t == 'y' ? 'FullYear' : t == 'm' ? 'Month' : 'Date';
			d = d[ 'set' + t ]( d[ 'get' + t ]() + add ) - 1000;
			return D( d );
		};
		D.toTime = function( d, SEC ) {
			d = D( d );
			return zeroInt( d.getHours(), 2 ) +':'
			+ zeroInt( d.getMinutes(), 2 )
			+ ( SEC ? ':'+ zeroInt( d.getSeconds(), 2 ) : '' );
		};
		D.toISO = function( d ) {
		//@ UTC causes timezoneOffset problems => not: d.toISOString().substring(0, 10);
			if ( typeCheck( 'String', d ))
				d = d.split( /\.|\//g ).reverse().join( '-' );
			else {
				d = D( d );
				d = D.y( d ) + '-' + zeroInt( D.m( d ), 2 ) + '-' + zeroInt( D.d( d ), 2 );
			}
			return d;
		};
		D.timestamp = function( d ) {
		var
			s = lang.timestamp
			;
   		d = D( d );
   		return s.replace( "YYYY", d.getFullYear()
   		).replace( "MM", zeroInt( d.getMonth() +1, 2 )
			).replace( "DD", zeroInt( d.getDate(), 2 )
			).replace( "HH", zeroInt( d.getHours(), 2 )
			).replace( "mm", zeroInt( d.getMinutes(), 2 )
			).replace( "SS", zeroInt( d.getSeconds(), 2 ));
		};
		D.dayName = function( d ) {
   		d = D( d ).getDay();
			return isNaN( d ) ? '' :
			Object.keys( lang.dayNames )[ d ? d : 7 ];	// Sunday is on index 7...
		};
		D.monthName = function( d ) {
   		d = D( d );
			return d ? lang.months[ D.m( d ) - 1 ] : '';	// January is on index 0...
		};
	   D.checkLeapYear = function ( y ) {
	      return new Date( $I( y ), 1, 29 ).getMonth() === 1;
	   };
      D.getWeekday = function( d, wD ) {
         d = D( d );
         return D.add( d, 'd', wD - d.getDay());     // day from 0=sunday, 1=monday...!!
      };   
      //@ weeks standard ISO 8601 (1988) or EN 28601 (1992) or DIN EN 28601 (1993)
      D.checkWeek53 = function( y ) {
      //@ checks if given year has 53 weeks referred to ISO 8601
      //@ years with 53 weeks : 1976, 1981, 1987, 1992, 1998, 2004, 2009, 2015, 2020
      var
         d = new Date( $I( y ), 0, 1 ).getDay()
         ;
         return d == 4 || ( d == 3 && D.checkLeapYear( y ));
      };
      function getThursday( d ) {
         d = D( d );
         d.setTime( d.getTime() + ( 3 - (( d.getDay() + 6 ) % 7 )) * 86400000 );
         return d;
      }
      D.getWeekDate = function( w, y ) {
      //@ returns the start date of a given week and year or false
         w = $I( w );
      	return w < 1 || w > 53 || ( w == 53 && !D.checkWeek53( y )) ? false
         	: D.add( D.getWeekday( getThursday([ $I( y ), 1, 4 ]), 1 ), 'd', 7 * ( w - 1 ));
      };
      D.getWeekNo = function( d ) {
      //@ calculate calendar week: Thanks! to http://www.salesianer.de/util/kalwoch.html
      var
         thurDate = getThursday( D( d ))
         , thurWeek1 = getThursday([ D.y( thurDate ), 1, 4 ])
         ;
         return Math.floor( 1.5 + ( thurDate.getTime() - thurWeek1.getTime()) / 86400000 / 7 );
      };
		D.daysInMonth = function( d ) {
		//@ count of days in the given month
		//@ REMARK: nice trick day zero of next month
			d = D( d );
			return new Date( D.y( d ), D.m( d ), 0, 0, 0, 0 ).getDate();
		};
   	return D;
	})()
	, $E = function( e, t, f, r ) {
	//@ PARAM element, eventType, function, removeFlag
	//@ REMARK: it's tricky: to realize a simple relative touchmove, it is nessesary to stop
	//@ all scroll/dragg operations on touchscreens, else always the hole content is moved
	//@ and and the relative movement is not some kind of random (difficult to figure out). 
	//@ ATTENTION: it is necessary to remove the touchmove listener directly after use!!! 
		t = attrList[ t ] ? attrList[ t ].slice( 2 ) : t;
		if ( t == 'touchmove' )
			D.ontouchstart = !!r ? null : function( ev ){ ev.preventDefault(); };
		if ( e && t && f )
			e[ ( !!r ? 'remove' : 'add' ) + 'EventListener' ]( t, f, false );
	}
	, $GS = function( e, a ) { // some browsers return floats on px, we need int then
	var
		v = !!e && getComputedStyle( e, null )
		? getComputedStyle( e, null ).getPropertyValue( a ) : 0
		, unit = ( v + '' ).slice( -2 )
		;
		return unit == 'px' || unit == 'pt' ? Math.round( parseFloat( v )) + unit : v;
	}
	, $LS = ( function() {
	var
		t = +new Date
		, r
		;
		_fake.fake = true;
		try {
			localStorage.setItem( t, t );
			r = localStorage.getItem( t ) == t;
			localStorage.removeItem( t );
			return r ? function( n, v, session ) {	// local- or sessionStorage if session
			//@ PARAM keyName, keyValue :=  if -1 remove else if empty get
			var
				storage = !!session ? sessionStorage : localStorage
				;
			if ( v === undefined || v === null ) {
				v = storage.getItem( n );
				return !!v && v.length ? JSON.parse( v ) : false;
			}
			else if ( v === -1 )
				storage.removeItem( n );
			else
				try {
					return storage.setItem( n, JSON.stringify( v ));
				}
				catch(_) {
					dialog.setMsg( 'error', msgReplace( 'localStorage', n ), 0, 3500 );
					return false;
				}
			}
			: _fake;
		}
		catch (exception) {
			return _fake;
		}

		function _fake ( n, v ) {
		//@ this is a temporal store trick if localStorage fails: store the data in
		//@ window.name. Its not changed while the window is opened
		var
			s = W.name.length ? JSON.parse( decodeURIComponent( W.name )) : {}
			;
			if ( v === undefined ) {
				return s[ n ] || false;
			}
			else {
				if ( v === -1 )
					delete ( s[ n ]);
				else
					s[ n ] = v;
				W.name = encodeURIComponent( JSON.stringify( s ));
			}
		}
	})()
	, $CN = {
	//@ className :: there's a lot to do with className
		contains: function( e, n ) {   // check if className is set in element e
			return !e || !n || !e.classList ? false : e.classList.contains( n );
		}
		, add: function( e, n ) {     // adds className, if not already there
			return !e || !n || !e.classList ? false : e.classList.add( n );
		}
		, remove: function( e, n ) {  // removes className from element e
			return !e || !n || !e.classList ? false : e.classList.remove( n );
		}
		, toggle: function( e, n ) { // flips className from element e
			return !e || !n || !e.classList ? false : e.classList.toggle( n );
		}
		, replace: function( e, o, n ) { // replaces className o with n in element e
			return !e || !n || !e.classList ? false : e.classList.replace( o, n );
		}
	}
	, $CE = function( a ) {
	//@ createElements ::  array structure, that is expand to a html node structure
	/* EXPLANATION OF array structure a:

		[tagName-0, {AttributeStruct-0}			// ever tagName and struct, optional...
			[tagName-1, {AttributeStruct-1}		// firstChild
			],
			textString-1,								// or textNode
			[tagName-2, {AttributeStruct-2},		// n-Child
				[tagName-3, {AttributeStruct-3}	// 1. childNode of n-child
				],
				[tagName-4, {AttributeStruct-4}	// 2. childNode of n-child
				]
			],
			textString-n								// may be appended by another textNode
		]
	*/
	var
		aL = a.length
		, e = null
		;
		if ( aL >= 1 ) {
			e = _CE( a[ 0 ], a[ 1 ]);
			for ( var i = 2; i < aL; i++ ) {
				if ( typeCheck( 'String', a[ i ]))
					e.appendChild( D.createTextNode( a[ i ] ));
				else if ( a[ i ] && a[ i ].length )		// could be an empty array ;-)
					e.appendChild( $CE( a[ i ]));
			}
		}
		return e;

		// create new element or one is already given
		function _CE( tagName, attributes ) {
		var
			e = ( typeCheck( 'String', tagName ) ? D.createElement( tagName )
			: tagName ) || null
			, v
			;
			if ( e && !!attributes )
				for ( var name in attributes ) {
					v = attributes[ name ];
					if ( v !== false && v !== null && v !== undefined )
						$A( e, name, v +'' );
				}
			return e;
		}
	}
	// a list to short'n attribute definitions used in $CE arrays
	// in some cases there is to decide between computer and smart phone (touchscreen)
	, attrList = {
		IH: 'innerHTML'
		, CN: 'class'
		, S:  'style'
		, T:  'title'
		, LD: 'longdesc'
		, MC: 'onclick'
		, M2: 'ondblclick'
		, MD: DOM.isTouch ? 'ontouchstart' : 'onmousedown'
		, MU: DOM.isTouch ? 'ontouchend' : 'onmouseup'
		, MM: DOM.isTouch ? 'ontouchmove' : 'onmousemove'
		, MO: 'onmouseover'
		, MT: 'onmouseout'
		//, KP: 'onkeypress'
		, KD: 'onkeydown'
		, KU: 'onkeyup'
		, OF: 'onfocus'
		, OB: 'onblur'
		, OC: 'onchange'
		, classname: 'class'    // avoid failures in html strings ;-)
	}
	, extern = {}
	, checkDesign = function( e, callBack, json ) {
	var
		waitCheck = -1
		;
		if( !( e || e.sheet ))
        	callBack( false );
      else 
			_waitForRules();

      function _waitForRules() {
         try {
            if ( e.sheet.cssRules )
               callBack( _checkDesign( e.sheet.cssRules ));
         }
         catch (_) {
				if( waitCheck++ < 25 )
					setTimeout( _waitForRules, 125 );
				else
					callBack( false );
         }
      }

		function _checkDesign( rules ) {
		var
			reg = /@import\s*(url\()?\s*("|')([^\2]*?)\2\s*\)?;/gi
			, checkText = ''
			, cssText = ''
			, msg = false
			, js = false
			;
			for ( var i = 0, s; i < rules.length; i++ ) {
				cssText += rules[ i ].cssText;
				// clean from import string etc., because of different installations 
				if ( s = rules[ i ].style )
				//@ browser add different attributes => just a differing selection
					checkText += rules[ i ].selectorText
					+ ( s.content || '' )
					+ ( s.backgroundImage || '' )
					+ __add( 'marginLeft', 1 )
					+ __add( 'marginRight', 1 )
					+ __add( 'marginTop', 1 )
					+ __add( 'marginBottom', 1 )
					+ __add( 'paddingLeft', 1 )
					+ __add( 'paddingRight', 1 )
					+ __add( 'paddingTop', 1 )
					+ __add( 'paddingBottom', 1 )
					+ __add( 'top', 1 )
					+ __add( 'left', 1 )
					+ __add( 'height', 1 )
					+ __add( 'width', 1 )
					+ __add( 'borderRadius', 1 )
					+ __add( 'position', 0 )
					+ __add( 'fontSize', 0 )
					+ __add( 'color', 0 )
					+ __add( 'borderColor', 0 )
					+ __add( 'backgroundColor', 0 )
					+ '\n';
			}
			if ( !!json )
				DOM.removeNode( e );
			checkText = checkText.replace(/(#useLibDesigner::?before")(\/\/[a-zA-Z0-9-_]+)"/
			, function( _, _s, _js ) {
				js = JSON.parse( atob( _js.slice( 2 )));
				return '';
			});
			// calc gets also a different order in browsers
			checkText = checkText.replace( /\s*calc\([^\)]+?\)\s*/g, 'calc()' );
			if ( !!js ) {
				if ( js.version != conf.designVersion )
					msg = 'hint';
				else if ( __checkCSSinjection( cssText ))
					msg = 'error';
				else if ( !!json && json != -1 ) {
					json.checksum = getCheckSum( checkText );
					return json;
				}
				else if (( !!js.domain && !!js.domain.length
				&& location.hostname.indexOf( js.domain ) == -1 )
				|| ( $I( js.checksum ) != getCheckSum( checkText )))
					msg = 'error';
			}
			if ( msg ) {
				dialog.setMsg( msg, msg == 'hint' ? 'designVersion' : 'designAuth'
				, 0, 6500 );
				return json == -1 ? js : false;
			}
			return js;
			
			function __add( t, v ) {
				return !s[ t ] ? '' : t + ( !!v ? s[ t ] : '' );
			}
			function __checkCSSinjection( s ) {
			// check against css attacs
			var	// no value selector is allowed and url( "data:...") only
				c = /\[\s*value\s*(|^|\$|~|\*|\|)=/g.test( s )
				;
				if ( !c )
					s.replace( reg, function( _, __, ___, p ) {
	 					if ( !/useLib.css$/i.test( p )	// only useLib.css is allowed
	 					&& !/^data:/i.test( p ))			// or data: urls
	 						c = true;
						return _;
					});
				return c; 
			}
		}
	}
	, setDesign = function( e, v ) {
		checkDesign( e, function( d ) {
			if( !!d )
				_setDesign( d );
			else
				_killDesign();
		}, false );

		function _killDesign() {
			if ( e == $( "uLstandSheet" ))
				e.innerHTML = v;
			else if( v === false ) {
				useLib.call.setDesign( 0 );
				_killDragg();
			}
			else if ( v != 0 )
				useLib.call.setDesign( v );
		}
		function _setDesign( d ) {
		var
			cR = $( cssNS.copyright )
			;
			cR.firstChild.href = 'mailto:' + d.email + '?subject=Your Design: '
			+ d.en;
			cR.firstChild.innerHTML = d.copyright == conf.designRight ? develop
			: '© desidned by ' + d.copyright;
			if ( e == activeDesignLink ) {
				useLibData.design = $( cssNS.designer ).selectedIndex;
				store();
				_killDragg();
			}
		}
		function _killDragg() {
			if ( activeDesignLink.sheet.disabled === true ) {
				DOM.removeNode( $( 'uLdraggedDesign' ));
				activeDesignLink.sheet.disabled = false;
			}
		}
	}
	, getCheckSum = function( s ) {
	//@ creates a fletcher16 checksum of the given string s
	//@ see: https://de.wikipedia.org/wiki/Fletcher%E2%80%99s_Checksum
	   for ( var i = 0, l = s.length, sum1 = 0, sum2 = 0; i < l; ) {
	      do {
	         sum1 += s.charCodeAt( i++ );
	         sum2 += sum1;
	      } while( i < l && i % 20 );
	      /* first reduction */
	      sum1 = ( sum1 & 0xff ) + ( sum1 >> 8 );
	      sum2 = ( sum2 & 0xff ) + ( sum2 >> 8 );
	   }
	   /* second reduction */
	   sum1 = ( sum1 & 0xff ) + ( sum1 >> 8 );
	   sum2 = ( sum2 & 0xff ) + ( sum2 >> 8 );
	   return sum2 << 8 | sum1;
	}
	, hasNet = location.hostname === "localhost" || location.hostname === "127.0.0.1"
	|| navigator.onLine
	, msgReplace = function( msgId, txt, i, level ) {
	var
		m = lang[ msgId ].slice( 0 )
		;
		m[ $I( i )] = m[ $I( i )].replace( /%%/, txt );
		return [ 'span', {}].concat( getStruct.header( m, level || 1 ));
	}
	, filename2Msg = function( msgId, url ) {
	var
		m = url.indexOf( '?' )
		;
		if ( m > 0 )
			url = url.slice( 0, m );
		return msgReplace( msgId, url.slice( url.lastIndexOf( '/' ) + 1
		).replace( /\.txt\.css$/i, '.txt' ));
	}
	, decodeJson = function( json ) {
		json = useLib.security && json.slice( 0, 2 ) != "//"
		? useLib.security.decodeJson( json ) : _parseJSON( atob( json.slice( 2 )));
 		return json;

 		function _parseJSON( j ) {
         try { return JSON.parse( j ); }
         catch ( e ) { return { code: 400, msg: "accessError" }; } 			
 		}
	}
	, getJsonCss = function( e, cB ) {
		for ( var r = e.sheet ? e.sheet.cssRules : [], i = 0; i < r.length; i++ ) {
			if ( new RegExp( '#' + cB + '::?before', 'i' ).test( r[ i ].selectorText || '' ))
    			return decodeJson( r[ i ].style.content.slice( 1, -1 ));
//    		else if (/^#hier/.test( r[ i ].selectorText ))
    	}
		return r.length ? -1 : -2;

	}
	, getHeadElement = function( doc, type, id, path, content, APPEND=true ) {
	var
		o = doc.getElementById( id )
		;
		DOM.removeNode( o );
		o = doc.createElement( type );
		$A( o, 'id', id );
		if ( type != 'meta' ) { // setAttribute it is needed!?
			$A( o, 'type', "text/" + ( type == 'script' ? 'javascript' : 'css' ));
			$A( o, 'charset', "utf-8" );
   		$A( o, 'crossorigin', 'anonymous' );
		}
		if ( type == 'meta' ) {
			o.name = path;
			o.content = content;
		}
		else {
			if ( type == 'link' || type == 'style' ) {
	  			$A( o, 'rel', 'stylesheet' );
				if ( !!path )
   				$A( o, 'href', path );
			}
			else if ( !!path )
				$A( o, 'src', path );
			if ( !!content )
				o.innerHTML = content;
		}
		if ( APPEND )
			doc.head.appendChild( o );
   	return o;
	}
	, loadJSONCSS = (function() {
	//@ loads extern data and modules via jsonCSS. Status of extern[ url ] :=
	//@ -1 = loading, -2 = not found, -3 = no net, -4 try again 
	//@ REMARK: its realised via on php path, the php or other script may be
	//@ adjusted for security resons and access denies to special data 
	var
		callStore = {}
		, check = "uLaccess.json.css"
		, callOnload = function( url, result ) {
			if ( !!callStore[ url ] && extern[ url ] != -2 ) {
				for ( var i = 0; i < callStore[ url ].length; i++ )
					callStore[ url ][ i ]( result );
				delete callStore[ url ];
			}
		}
		, setScript = function( url, timeout, GETRETURN ) {
		var
			isJs = /\.js$/.test( url )	// is javaScript?
			, path = ( url.indexOf( '/' ) < 0 ? conf.cssPath : '' ) + url
			, msgId = false
			, s
			;
   		if ( !isJs && url !== check )
				msgId = dialog.setMsg( 'hint', useLib.security
				&& url.indexOf( useLib.security.apiPath ) > -1 ? 'encryptedMsg'
				: 'loadMsg', 0, timeout || 3000 );
   		s = getHeadElement( D, !!isJs ? 'script' : 'link', url, path );
   		s.onload = function() {
   			if ( !!isJs )
   				extern[ url ] = true;
     			else {
     			var
     				js = extern[ url ] = getJsonCss( this, 'useLibJson' )
     				;
					if ( !GETRETURN && ( !js || js.code || js.msg )) {
						if ( js.msg )
							dialog.setMsg( /^20\d/.test( js.code ) ? 'success'
							: 'error', lang[ js.msg ] || js.msg, 0, 4500 );
						else if ( /^40(0|1)/.test( js.code )) {
							dialog.setMsg( 'error', filename2Msg( 'fileMsg', url )
							, 0, 3500 );
							extern[ url ] = -2;
						}
					}
					DOM.removeNode( this );
   			}			
   			if ( msgId )
   				dialog.closeBox( $( msgId ));
   			callOnload( url, extern[ url ]);
   		};
   		s.onerror = _stop;
			setTimeout( function() {		// wait for loading...
				if ( extern[ url ] == -1 ) {
					if ( url == check ) {
						hasNet = false;
						_stop();
					}
					else if ( path !== false )	// try ones again?
						dialog.confirm( filename2Msg( 'serverMsg', url ), function( v ) {
							DOM.removeNode( s );
							if ( v === true ) {
								extern[ url ] = -4;
								setScript( url, timeout, GETRETURN );
							}
						}, -8 );
				}
				else if ( extern[ url ] == -4 )
					_stop();
			}, timeout || 6500 );
   		return s;

			function _stop() {
				DOM.removeNode( s );
				if ( url == check )
					extern[ url ] = -3;
				else { 
					extern[ url ] = -5;
					dialog.setMsg( 'error', filename2Msg( 'fileMsg', url ), 0, 3500 );
				}
			}
		}
		;
		setScript( check ); // check if online
		return function( url, callBack, timeout, GETRETURN ) {
		//@ REMARK there may be calls of the same url from different elements 
		//@ not to lose those calls, they are stored and called on loading.
			if ( !!url && hasNet && ( !extern[ url ] || extern[ url ] == -1 )) {
				if ( !!callBack ) {
					if ( !callStore[ url ])
						callStore[ url ] = [];
					callStore[ url ].push( callBack );
				}
				if ( !extern[ url ]) {
					extern[ url ] = -1;		// loading
					setScript( url, timeout, GETRETURN );
				}
			}
			else if ( !hasNet )
				extern[ url ] = -3;
			if ( extern[ url ] == -5 )
				dialog.setMsg( 'hint', '? ' + url + ' ?', D.activeElement, 2500 );
			return extern[ url ] < 0 ? false : ( extern[ url ] || false );
		};
	})()
	, ajax = function( url, fn, type, time, sync, timeFn ) {
	//@ gets content of url and hands it to function fn
	//@ url of file, function which gets responseText, synchon := true
	var
		h = new XMLHttpRequest()
		, l = String( location.href )
		, method = "GET"
		, param = null
		; 
		if ( l.slice( 0, 5 ) == 'file:' ) {
		// a normal error handling with try...catch is not working, because
		// of cross origin problems that happen on submit
			dialog.setMsg( 'error', filename2Msg( 'fileMsg', l ), 0, 3500 );
		}
		else if ( h ) {
			if ( /txt/i.test( type ) && url.indexOf( '/' ) == -1 )
				url = conf.wikiPath + url; // complete text file without path
			try {
				if ( /method=[^&]+/.test( url )) {
					if ( conf.submitType.toLowerCase() == 'post' ) {
						method = url.indexOf( '?' );
						param = url.slice( method +1 );
						url = url.slice( 0, method );
					}
					method = conf.submitType;
				}
				h.open( method, encodeURI( url ), !sync );
				h.onreadystatechange = function() {
					if ( h.readyState == 4 ) {
						// there could be a redirektion on unknown files, that gives
						// a JSONCSS for UNAUTHORIZED...
						if ( h.status > 400 || ( h.status == 200
						&& /#useLibJson:before\s*\{/.test( h.responseText ))) {
							dialog.setMsg( 'error', filename2Msg( 'fileMsg', url ), 0, 3500 );
							_timeout();
						}
						else if ( h.status == 200 ) {
							if ( !!time )
								clearTimeout( h._timeout );
							fn( type == 'XML' ? h.responseXML : type == 'JSON'
							? decodeJson( h.responseText ) : h.responseText );
						}
					}
				};
				if ( param )
					h.setRequestHeader( 'Content-type'
					, 'application/x-www-form-urlencoded' );
				h.send( param );
				if ( !sync ) {
					h.timeout = time;
					h.ontimeout = _timeout;
				}
				else
					h._timeout = setTimeout( _timeout, time );
			}
			catch( e ) {}
			if ( !!time ) {
				timeFn = timeFn || function() {
					dialog.confirm( filename2Msg( 'serverMsg', url ), function( v ) {
						if ( v === true )
							ajax.call( arguments );
					});
				};
			}

			function _timeout(){
				h.abort();
				h = null;
				if ( timeFn )
					timeFn( url );
			}
		}
	}
	, dispatchEvent = function( e, t ) {
	// well, not full supported... but its coming
	var
		ev = new Event( t )
		;
		if ( ev )
			e.dispatchEvent( ev );
		else
			e.fireEvent( "on" + t );
	}
   , escapeUnicode = function( s ) {
   //@ escape all unescaped unicodes signs as on JSON_UNESCAPED_UNICODE
   	return s.replace(/[^\x00-\xff]|[\x80-\xff]/g, function( c ) {
   		return "\\u" + ( "000" + c.charCodeAt( 0 ).toString( 16 )).substr( -4 );
		});
	}
   , unescapeUnicode = function( s ) {
   //@ unescape all escaped unicodes, so that they may also be used in HTML
   //@ REMARK: don't forget charset=utf-8 then!
      return s.replace(/\\(u|x)([0-9A-Za-z]{4})/g, function( _, __, h ){
         return String.fromCharCode( '0x' + h )
      }); 
   }
   , uLeval = function( f, a1, a2, a3, a4 ) {
		f = f.replace( /\([^\)]*\);?/, '' ).split( '.' );
		for ( var j = 0, fn = W; j < f.length && fn[ f[ j ]]; j++ )
			fn = fn[ f[ j ]];
		if ( fn && fn.call )
			fn.call( this, a1, a2, a3, a4 );
   }
	, repeat = function( s, c ) {	// instead of long polyfill
	//@PARAM StringToRepeat, countToRepeat
		return Array( $I( c ) + 1 ).join( s );
	}
	, compare = (function(){
	//@ compare two values also for arrays, but in alphabethic order of special chars
	//@ REMARK: a correct german sorting is with i. e. AE for Ä, but it's difficult
	//@ to find an editor, that sorts json stuctures correctly... For the sorting
	//@ and finding of towns in a list all special german chars should follow
	//@ each other, i. e. 'Köln' should follow after 'Kölln-Reisiek'
	var
		rList = {		// don't know if complete...
			"Â|À|Å|Ã|Á": "A"
			, "â|à|å|ã|á": "a"
			, "Ä": "Ae"
			, "ä": "ae"
			, "Ç": "C"
			, "ç|ć|č": "c"
			, "đ": "d"
			, "É|Ê|È|Ë": "E"
			, "é|ê|è|ë": "e"
			, "Ó|Ô|Ò|Õ|Ø": "O"
			, "ó|ô|ò|õ": "o"
			, "Ö": "Oe"
			, "ö": "oe"
			, "Š": "S"
			, "š": "s"
			, "ß": "ss"
			, "Ú|Û|Ù": "U"
			, "ú|û|ù": "u"
			, "Ü": "Ue"
			, "ü": "ue"
			, "Ý|Ÿ": "Y"
			, "ý|ÿ": "y"
			, "Ž": "Z"
			, "ž": "z"
		}
		, reg = '('
		, cL = []
		, repl = function( s ) {
			return String( s ).replace( reg, function( c ) {
			   return cL[ Array.prototype.slice.call( arguments, 1 ).indexOf( c )];
			});
		}
		;
		for ( var r in rList ) {
			reg += r + ')|(';
			cL.push( rList[ r ]);
		}
		reg = new RegExp( reg.slice( 0, -2 ), 'g' );

		return function ( a, b ) {
			a = repl( a );
			b = repl( b );
			return a < b ? -1 : a > b ? 1 : 0 ;
		}
	})()
	, zeroInt = function( v, count, fix, s ) {
	//@ PARAM value, countOfDecimalPlaces, true := append signs, signsToAppend ['0']
		count -= String( $I( v )).length - 1;
		count = new Array( Math.max( 1, count )).join( s || '0' );
		return ( fix ? '' : count ) + $I( v ) + ( fix ? count : '' );
	/*
	var
		n = String( $I( v )).length - count
		;
		return ( n < 0 ? '00000000000000000000000'.slice( n ) : '' ) + $I( v );
	*/
	}
	, delayRemove = function( e, cL, t ) {
	//@ PARAM: Element, classNamesFormTo, timeInSeconds
	//@ adds a className for animation and removes it after the animation has finished.
		if ( !typeCheck( 'Array', cL ))
			cL = [ cL, 0 ];
		$CN.add( e, cL[ 0 ]);
		t = $I( !!t ? t : $I( $GS( e, 'animation-period'))) * 1000;
		setTimeout(( function( _e, _cL ){ // create closure, beause may be called more often
			return function(){
				$CN.remove( _e, _cL[ 0 ] );
				$CN.remove( _e, _cL[ 1 ] );
			}
		})( e, cL ), t );
	}
	, hasFocus = false
	, firstAct = function() { hasFocus = true; $E( W, 'focus', firstAct, 1 ); }
	, sound = function( a ) {
		if ( hasNet && a && a.play && !$CN.contains( $( cssNS.sound ), cssNS.locked )) {
		//@ well, CORS sounds on local use may be blocked...!! It was changed again...
			a.play();
		// var
		// 	a = new Audio( s +'?v='+ Math.random())	// just to please chrome...
		// 	;
		// 	a.crossOrigin = 'anonymous';
		// 	a.muted = true;
		// 	a.oncanplaythrough = () => { var p = a.play()	// prevent console msg...
		// 	; if( p ) { p.then( _ => {}).catch(e => {})}};
		}
	}
	, setOpenClose = function( e, open, t ) {
	//@ PARAM: Element, openFlag, timeInSeconds
	//@ to handle opening and closing of GUI elements. It is done via this special concept
	//@ because any animation shall be possible. The problem is, to implement a status
	//@ of display:none, if the element is closed. It is used:
	//@ 1. class show with display: block i. E.
	//@ 2. class open with the open animation part
	//@ 3. class close with the close animation part
	//@ The animation parts are removed after the animation time.
		if ( !!open ) {
			$CN.add( e, cssNS.show );
			delayRemove( e, cssNS.open, t || 2 );
		}
		else {
			$CN.add( e, cssNS.close );
			delayRemove(  e, [ cssNS.close, cssNS.show ], t || 2 );
		}
	}
	, call = {
		uLinit: function( name, fn ) {
			this[ name ] = fn;
		}
	}
	/*****
	 *
	 *		Start of the formular administration section
	 *
	 *		@ to administrate the formular content and to steer it via dynamic actions
	 *		@ standardizing structure is defined. It contains all necessary information
	 *		@ and can also be used to extend the functionality of existing formulars.
	 *		@ The JSON-structure may be defined in the HTML file containing the 
	 *		@ form via useLib.extend.formDef = { ... } or the form is analyzed
	 *		@ automatically onload by observing the useLib conf.htmlSupport
	 *		@ definitions and the found HTML structs and tags around the form elements.
	 *		@ At least it may be loaded as JSON string via ajax or JSONCSS without
	 *		@ touching the calling file itself.
	 *		@ Elements for selection such as select, checkbox or radio may be mixed
	 *		@ under one element name. All other elements need an unique name for
	 *		@ identification.
	'formName': {
		, 'msg': {	// if 'success' and no elementName all elements are cleared
			type: 'success' or 'error' or 'hint' or 'status'
			, header: 'success', 'error', 'server' or header WIKI-text, for other texts
			, list: { 'elementName': error or hint WIKI-text }
		}
		// more than one value are separated by comma and enclosed by squared bracket
		'elementName': {	// also fields as fieldName[] for select, radio or checkbox
			type := toggle, radio, checkbox, select-one, select-multiple
				, file, date, enddate, color, time, number, range
				, email, password, adress, fon, url, currency, iban, search, ownformat
				, text, textarea
				, group
			, value := actual value :: on fields separated values
			, resetValue := value on reset of element content
			// The fields may be marked as:
			, searcher := find the amount of fitting data sets
			, completer := complete the belonging data of this part of data set i.e.
				banking data on account number
			// different status attributes for...
			// ...interaction: disabled, focused, changed and error are managed directly.
			// ... process flow
			, required	:= must be selected or filled in
			, estimated	:= marked as not sure now, needs an end controll
			, technical	:= modified by system or other person on purpose
			, verified	:= investigated by system or other person and unblocked
			, mnemonic := mnemonics list for fastFeeder (see below)
			, min := minimal value or amount of signs (depending on context)
			, max := maximal value or amount of signs (depending on context)
			, data-ul := depending on eType
				* format RegExp
				* stepsize on number, range...
				* password quality in percent
				* currency sign
				* ...
		}
		'nextElement': {
			...
		}
	}
	, 'nextForm': {
		...
	}
	 *
	 *		@ ATTENTION: if no element id is defined, useLib will create one!
	 *		@ 				 as well a FormName is created, if its missing
	 * 
	 *****/
	, checkElement = function ( e, covered ) {
	var
		noEleType = ( 'button submit reset image hidden' ).split( ' ' )
		;
		return e.tagName != 'FIELDSET' && e.tagName != 'BUTTON'
		&& noEleType.indexOf( e.type ) == -1
		&& (( !!covered && e.offsetParent !== null )
		|| !$CN.contains( e, 'uL_ignore' ))
		&& ( !!covered ? !$CN.contains( e, cssNS.covered )
		: !$CN.contains( $P( e ), cssNS.isCover ));
	}
	, filterElements = function ( eL, covered ) {
	//@ PARAM: elementList [form], coveredElements [noCovered]
	//@ gets all user input and selection elements of a parentNode i.e. fieldset
	//@ or form Therefore especially the input list has to be filtered to
	//@ eliminate noEleType
	//@ REMARK: a complex NOT: selector was discarded.
	var
		l = []
		;
		eL = eL.tagName == 'FORM' ? eL.elements : eL;
		for (var i = 0; i < eL.length; i++ )
			if ( checkElement( eL[ i ], covered ))
				l.push( eL[ i ]);							// ...concat to list
		return l;
	}
	, getLandmark = function( t ) {
		return $Q( D.body, htmlSupport.landmarks[ t ].selector, 1 );
	}
	, getResetEle = function( fD ) {
	var
		v = fD.value.toString() || ''
		;
		return v == fD.resetValue.toString() && fD.storeValue ? fD.storeValue
		: v != fD.resetValue.toString() ? fD.resetValue : fD.value;
	}
	, resetElements = function( e ) {
	//@ resets elements on double click on label or legend of fieldset (parentNode than...)
		if ( e && e.currentTarget ) {
	      e.stopPropagation();
	      e.preventDefault();
	      e = e.currentTarget;
		}
		if ( e )
			e = e.htmlFor ? [ $( e.htmlFor )]
			: filterElements( $Q( e.tagName == 'legend' ? $P( e ) : e
			, 'input, select, textarea' ));
		if ( e && e.length )
			for (var i = 0, fD = formDef[ e[ 0 ].form.name ]; i < e.length; i++ ) {
				dialog.removeError( e[ i ], null );
				setEleValue( e[ i ], getResetEle( fD[ e[ i ].name ]), true );
			}
	}
	, optionExtRegEx = /#(\w+)$/	// to cut, find option extention 
	, getSelectionAttr = function( e ) {
	//@ analyzes elements for selection such as select, checkbox or radio, that
	//@ may be arrays of values.
	//@ REMARK: well, the behavior of select is not carefully thought out: if an
	//@ complex application may re- or overload some data, those of select may
	//@ not appear even if the selected attribute is set correct, the last
	//@ selction of the user is shown (there is a doubled logic). Its possible
	//@ to use the value attribute, on select-multi it clears all other selected
	//@ options etc. Therefore a decision had tobe done: select-one fields are
	//@ handled via the value attribute with all easy onchange events etc. The
	//@ multiselect is treated special by setting both selected possiblties of
	//@ options (attribute and key). changes are monitored via selectedIndex and
	//@ the in some cases fires onchange event.  
		return e.tagName == 'SELECT' ? 'selected'
		: e.type == 'checkbox' || e.type == 'radio' ? 'checked'
		: 'value'
		;
	}
	, getEleValue = function( e, onlyE=true ) {
	//@ PARAMETER element, onlyElementFlag true := only one ElementType (checked 
	//@ or selected) gets the actual value of a element or array of elements, if
	//@ all have the same name this is especially useful if there is a mix of 
	//@ select, radio and checkbox
	var
		v = []
		, a
		;
		if ( e && e.type ) {
			a = getSelectionAttr( e );
			if ( a == 'value' )
				v.push( e.type == 'file' ? _getFilename() : e.value );
			else {
				e = $Q( e.form, ( onlyE ? ( a == 'checked' ? 'input' : 'select' ) : '' )
				+ '[name="' + e.name + '"]' );
				for ( var i = 0; i < e.length; i++ )
					_get( e[ i ]);
			}
		}
		return v; 
		
		function _get( _e ) {
			if ( _e.tagName == 'SELECT' ) {
				_e = _e.options;
				for ( var j = 0; j < _e.length; j++ ) {
					if ( _e[ j ].selected )
						v.push( _e[ j ].value );
				}
			}
			else if ( _e.checked === true )
				v.push( _e.value );
			else if ( _e.value == 'true' )	// in some cases toggle true/false
				v.push( 'false' );
		}

		function _getFilename() {
		//@ values of input type=file are different (even on different browsers), they
		//@ may contain a fakepath or be empty even if a actual value is given in files
		var
			p = e.files[ 0 ] ? e.files[ 0 ].name : $A( e, 'data-ul' )
			;
			return p && p.length ? p.replace( /.*?([^\\\/]+)$/, '$1' ) : '';
		}
	}
   , setEleValue = function( e, v, ALL=false ) {
	//@ PARAM element, value, ALL := true for check more than given element 
	//@ sets the actual value of a element returns value, if not possible (FastFeeder)
	//@ REMARK it's a bit tricky, because on selection tasks could be defined a mixture
	//@ of Selectlists, radiobuttons and checkboxes for one name. And on top there
	//@ could be defined ownInput fields also. A value that is not found in the
	//@ predefined value is assigned to ownInput.
      if ( !!e && e.type ) {
      var
         a = getSelectionAttr( e )
         ;
         v = !typeCheck( 'Array', v ) ? [ v || '' ] : !v[ 0 ] ? [ '' ] : v;
         if ( a == 'value' ) {
            if ( e.type != 'file' ) {  // set file is not allowed for security reasons
               $A( e, a, v[ 0 ] || '' );
               e.value = v[ 0 ] || '';
				}
            else if ( metaReaction.file )
               metaReaction.file.setValue( e, v[ 0 ]);
            onchange( e );
         }
         else {
            e = ALL ? $Q( e.form, '[name="'+ e.name +'"]' ) : [ e ];
            v = v.slice( 0 );
            for ( var i = 0, own = []; e && i < e.length; i++ ) {
               if (( a = getSelectionAttr( e[ i ])) == 'selected' ) {
                  for ( var j = 0, o = e[ i ].options; j < o.length; j++ ) {
                  	if ( $CN.contains( o[ j ], 'ownInput' ))
								own.push( o[ j ]);
                     _chooseValue( o[ j ]);
                  }
               }
               else {
               	if ( $CN.contains( e[ i ], 'ownInput' ))
							own.push( e[ i ]);
                  _chooseValue( e[ i ]);
                  if ( e[ i ].type == 'radio' && !!e[ i ][ a ])
                     getFormDef( e[ i ]).activeRadio = e[ i ];
               }
               onchange( e[ i ]);
            }
				if ( own.length && v.length )
					for ( var i = 0; v[ i ] && i < own.length; i++ )
						ownInput.set( own[ i ], v[ i ]);
         }
         return v;
      }

      function _chooseValue( _e ) {
      var
      	p = v.indexOf( _e.value )
      	;
	      if ( p > -1 )
	         v.splice( p, 1 );
         $A( _e, a, p > -1 ? a : -1 );
         _e[ a ] = p > -1;
      }
   }
   , getAllValues = function( eName, formName, doc ) { // elementName, formName
   doc = doc || D;	// its also possible to get values of a opened window
   var
   	fD = formDef[ formName ]
		, nL = eName
		, done = {}
		, v = []
		;
		if ( !Array.isArray( nL )) {
			eName = '[name'+ ( !!eName && eName.length ? '^="'
			+ ( eName.slice( -1 ) == ']' ? eName.replace( /\d+\]/, '' ) : eName )
			+ '"'	: '' ) + ']';
			nL = $Q( doc.forms[ formName ] || doc.body
			, 'input'+ eName +',select'+ eName +',textarea'+ eName );
		}
		for ( var i = 0, n, _v, ex = false; i < nL.length; i++ ) {
			n = nL[ i ].name;
			if ( !$CN.contains( nL[ i ], 'uL_ignore' )
			&& !!fD && !!fD[ n ] && !fD[ n ].ignore
			&& !fD[ n ].hasEmpty	) {
				_v = _getValue();
				if ( _v.length ) {
					//@ The special grouper type is between all chairs. It's an array
					//@ of non array widgets, normally they are single values. So they
					//@ are marked special and their name ist numbered except the
					//@ first all following have to be pushes to the first...
					n = n.replace( /\[\d+\]$/, '[]' ); 
					if ( isNaN( done[ n ])) {
						done[ n ] = v.length;
						v.push({	name: n, value: _v, task: fD[ n ].task || false, e: nL[ i ]
						, ex: JSONexpands[ fD[ n ].type ] || null, uLis: true });
					}
					else
						v[ done[ n ]].value = [].concat( v[ done[ n ]].value, _v )
				}
			}
		}
		for ( var i = 0, _v; i < v.length; i++ ) {
			_v = v[ i ].value;
			if ( Array.isArray( _v )) {
				_v = _v.filter( function( e, p ) { return _v.indexOf( e ) == p; });
				if ( !/\[\]$/.test( v[ i ].name ))
					_v = _v[ 0 ];
			}
			else {
				if ( /\[\]$/.test( v[ i ].name ))
					_v = [ _v ];
			}
			v[ i ].value = _v;
		}
		return v;

		function _getValue() {
			_v = nL[ i ].value;
			// special case checkbox and radio between 'true' and 'false'...
			// ...and e.g. hidden elements
			// REMARK: the default value ( if none is given) of those is 'on' 
			return ( nL[ i ].type == 'checkbox' || nL[ i ].type == 'radio' ) ?
			( _v == 'true' && !nL[ i ].checked ? 'false' : nL[ i ].checked ? _v : '' )
			: nL[ i ].type == 'hidden' || nL[ i ].type == 'password'	? _v
			: nL[ i ].tagName == 'SELECT' ? getEleValue( nL[ i ]) : _v;
		}
	}
   , getJSONValues = function( eName, formName, doc ) { // elementName, formName
   //@ if no elementName is given it gets the hole form, if no formName given
   //@ it gets the first form of main landmark
   //@ REMARK: if eName is array (on intern use), getAllValues has been 
   //@ already called (x.uLis == true) or its a list of selected nodes...
   doc = doc || D;	// its also possible to get values of a opened window
   formName = !!formName ? formName
	: $Q( doc, htmlSupport.landmarks.main.selector + ' form', 1 ).name;
	var
 		json = {}
 		, arrStore= {}
 		, dynStore = {}
 		, v
		;
		if ( !!doc.forms[ formName ]) {
	  		v = Array.isArray( eName ) && eName[ 0 ].uLis ? eName
	  		: getAllValues( eName, formName, doc );
			for ( var i = 0, tasks = {}, n, nL; i < v.length; i++ ) {
				_setPath( v[ i ].name.split( '.' ),  v[ i ].value, v[ i ].e
				, v[ i ].ex );
				if ( !!v[ i ].task )				// if found task put to task list
					tasks[ v[ i ].name ] = v[ i ].task;
			}
			json.formName = formName;			 
			json.tasks = tasks;
		}
		return json;

		function _setPath( nL, v, e, ex ) {
			if ( nL.length > 1 )
				__checkDynlist( e );
			for ( var j = 0, js = json, dyn, is, n, m; j < nL.length; j++ ) {
				is = false;
				n = nL[ j ].replace( /\[(\d*)\]$/, function( _s, _is ) {
					m = nL.slice( 0, j +1 );
					m = m.join( '.' );
					m = m.replace( /\[\d*\](?!.*\[\d*\])/, '[]' );
					is = _is;
					return '[]';
				});
				if ( j == nL.length -1 ) {
					if ( ex && ex.active ) {
						js[ n ] = {};
						if ( Array.isArray( v ))
							v = v[ 0 ];
						if (( v = v.match( new RegExp( ex.reg ))))
							for ( var k = 0; k < ex.names.length; k++ )
								js[ n ][ ex.names[ k ]] = v[ k +1 ];
					}
					else
						js[ n ] = v;
				}
				else {
					if ( dyn = dynStore[ n ]) {
						if ( !dynStore[ n ])
							dynStore[ n ] = dyn;
						if ( !js[ dyn ])
							js[ dyn ] = {};
						js = js[ dyn ];
					}
					if ( is !== false ) {
						if ( !arrStore[ m ]) {
							arrStore[ m ] = [];
							js[ n ] = [];
						}
						dyn = is;
						if (( is = arrStore[ m ].indexOf( is )) < 0 ) {
							is = arrStore[ m ].length;
							arrStore[ m ].push( dyn );
							js[ n ].push({});
						}
						js = js[ n ][ is ];
					}
					else {
						if ( !js[ n ])
							js[ n ] = {};
						js = js[ n ];
					}
				}
			}

			function __checkDynlist( e ) {
				if (( e = $P( e ) ) && ( e = $P( e ) )
				&& ( e = $A( e.firstChild.firstChild, 'data-ul-dynlist' ))) {
					e = e.split( ':' );
					e[ 1 ] = e[ 1 ].split( '.' ).pop();
					if ( !!wikiEnsemble[ e[ 0 ]] && nL.indexOf( e[ 1 ]) > -1 ) {
						e[ 1 ] = e[ 1 ].replace( /\[\d*\]$/, '[]' );
						if ( !dynStore[ e[ 1 ]])
							dynStore[ e[ 1 ]] = e[ 0 ];
					}
				}
			}
		}
   }
   , setJSONValues = function( json, form ) {
   //@ REMARK: to set values of a opened window choose the right address of form
   form = !!form ? ( form.name ? form : D.forms[ form ])
   : ( json.formName ? ( D.forms[ json.formName ] || false )
   : D.forms.length ? D.forms[ 0 ]: false );
   var
   	found = false
   	;
   	if ( form !== false ) {
   		metaReaction.grouper.deleteAllGrouper( form );
   		if ( !!json.cloud ) {
   			$CN.add( form, cssNS.cloud );
   			delete json.cloud;
   		}
	   	delete json.formName;
	   	reInitValues = true;
	   	internChange = true;
	   	$S( form ).visibility = 'hidden';// prevent from rerendering to speed up
	   	for ( var n in json) {
	   		if ( n == 'tasks' ) {	// set tasks
	   			if ( conf.taskSupport )
						for ( var t in json[ n ])
							call.taskstep( labels[ $Q( form, '[name="' + t + '"]', 1 ).id ]
								, json[ n ][ t ]
							);
	   		}
	   		else if ( n == 'attrDef' )
	   			_setAttr( json[ n ]);
	   		else
	   			_setObject( n, json[ n ], false, false );
	   	}
	   	$S( form ).visibility = 'visible';
	   	reInitValues = false;
	   	internChange = false;
		   initForm( form );
	   	if ( found ) {
		   	D.activeElement.blur();
		   	DOM.scrollIntoView( getLandmark( 'navigation' ), 0, 1 );
		   }
	   }
		return found;

		function _setAttr( js ) {
			for ( var t in js ) {
			var
				e = D.getElementsByName( js[ t ])
				;
				if ( !!e )
					for ( var a in js[ t ])
						$A( e[ 0 ], a, js[ t ][ a ]);
			}
		}
		function _setObject( name, _json, isElement ) {
		//@ recursive method to initialize hierachical dynamic lists (and arrays)
		//@ EXPLAIN: first all existing nodes except one are removed to avoid empty
		//@	or unused nodes. The remaining is used as template for the new created 
		//@	and removed at the end.
		//@ REMARK: to identify dynamic lists on their exType. The exType must be
		//@ used as JSON key followed by the list struct (named list of expands or
		//@ an array of those)
		//@ EXPAMPLE: extendTypeName1: { listName: { exName1: value1, exName2: value2
		//@ , ..., extendTypeName2: { arrayName[]: [{ exName1: value1, ...  }
		//@ , { ... }]}}}
		var
			fD = formDef[ form.name ] ? formDef[ form.name ][ name ] : false
      	, exType = ( name.match( /[^\.]+$/ ) || [ '' ])[ 0 ]
      	, newName = name.slice( 0, -exType.length )
			;
	      if ( !!!isElement && _json === Object( _json )) {
	      var
	      	template = !call.dynList ? []
	      	: $Q( form, '[data-ul-dynlist^="' + exType + ':' + newName + '"]' )
	      	;
		      if ( template.length ) {
		      	if ( $A( template[ 0 ], 'data-ul-dynlist' ).indexOf( 'uLmaster' )
	      		== -1 ) {
		      		template = call.dynList.removeNodes( template );
	      			if ( !newName.length )	// just clean all on first master...
	      				__cleanFormDef( formDef[ form.name ], exType );
		      	}
		      	else	// take template from already copied super template
		      		template = template[ 0 ];
		      	name = newName;
		      	for ( var _n in _json ) {
	      			newName = name + _n;
						if ( Array.isArray( _json[ _n ])) {
							for ( var i = 0; i < _json[ _n ].length; i++ ) {
	      					newName = newName.replace( /\[\d*\]$/
	      					, '[' + zeroInt( i, 5 ) + ']' );
	      					call.dynList.insertNode( template, exType, newName, 0 );
		      				_setObject( newName, _json[ _n ][ i ]);
							}
						}
	      			else {
	      				call.dynList.insertNode( template, exType, newName, 0 );
		      			_setObject( newName, _json[ _n ]);
	      			}
		      	}
					newName = $P( template );	// short store...
		      	DOM.removeNode( template );
		      	if ( newName )
		      		call.dynList.action( newName, exType, 'init' );
		      } 
		      else if ( !__shrink()) {
		      	for ( var _n in _json )
						_setObject( name + '.' + _n, _json[ _n ]);
				}
	      }
			else if ( !__expand()) {
	         for ( var i = 0, e = $Q( form, '[name="'+ name +'"]' )
	         ; !!e && i < e.length; i++ )
					__setValue( _json, e[ i ], i === 0 );
	         found = true;
	      }

			function __setValue( js, _e, INIT ) {
				js = Array.isArray( js ) ? js : [( js || '' ) +'' ];
	         if ( $A( _e, 'data-ul-grp' ) == 'isGrouper' ) {
					metaReaction.grouper.setGrouper( _e, js
					, fD ? fD.type : $A( _e, 'type' ));}
				else {
					if ( INIT && fD )
	         		fD.value = js;
		      	setEleValue( _e, js, true );
		      }
			}

			function __cleanFormDef( fD ) {
			// there may be too much formDefs of elements of dynamicList: delete them
				for ( var n in fD )
					if ( new RegExp( '^'+ exType +'\[\d+\]\.' ).test( n ))
						delete fD[ n ];
			}
			function __expand() {
			var
				ex = JSONexpands[ exType ]
				;
				if ( !!ex && ex.active && typeCheck( 'String', _json )
				&& ( v = _json.match( ex.reg ))) {
					v = v.slice( 1 );
					for ( var j = 0; j < ex.names.length; j++ )
						setEleValue( $Q( form, '[name="'+ newName + ex.names[ j ] +'"]', 1 )
						, [ v[ j ]]);
					return true;
				}
	      	return false;
	      }
			function __shrink() {
			var
				e = $Q( form, '[name="'+ name +'"]', 1 )
				, v = ''
				, r
				;
				if ( !!e ) {
					if ( ex = JSONexpands[ fD ? fD.type : $A( e, 'type' )]) {
						// extract separator list from JSONexpands regexp
						if ( ex.active ) {
							r = ex.reg.replace( /\([^\)]+\)/g, '\u0600' ).split( '\u0600');
							r.shift();					// cut empty first
							for ( var j = 0; j < ex.names.length; j++ )
							// append json subvalues via separators
								v += _json[ ex.names[ j ]] + r.shift();
						}
						__setValue([ v ], e, true );
					}
					else
						_setObject( name, _json, true );
					return true;
				}
	      	return false;
	      }
		}
   }
   , focusTime = false
	, setFocus = function ( e, t ) {
	//@ in some cases e.focus() is not working, a timeout is needed... :-(
		if ( !!e ) {
			e = $CN.contains( e, cssNS.covered ) ? $( getFormDef( e ).first ) : e;
			clearTimeout( focusTime );
			focusTime = setTimeout( function(){ lastActive = e; e.focus(); }, t || 20 );
		}
	}
	, getLabelText = function( e, s ) {
	//@ trys to get the label text content. if s is defined it replaces content
	//@ in given string against %%. Usefull for error messages i. e. 
	var
		t = '' 
		;
		e = !!e && labels[e.id] ? labels[e.id].childNodes : false;
		for ( var i = 0; !!e && i < e.length; i++ )
			if ( !e[ i ].tagName || e[ i ].tagName.toLowerCase() != conf.mnemonicsMarker )
				t += e[ i ].textContent;
		t = t.trim();
		if ( s )
			s = JSON.parse( JSON.stringify( s ).replace(
				/(in|of|von )?%%/g, t.length ? '$1 \\"' + t + '\\"' : ''
			));
		return !!s ? s : t;
	}			
   , analyseFilter = function( s, SORT ) {
   //@ gets the compare method from a filter field used in dynamic lists and single-
   //@ and multiSelect.
   var
      c = s.match( /^(>=|<=|>|<)/ )
      ;
      if ( !!SORT ) {
      	if ( _getValue( s ) !== false )
      		return function( v1, v2 ) {
      	 		return _compare( _getValue( v1 ), _getValue( v2 ));
				};
      	else
      		return compare;
      }
      else if ( !!c ) {
         s = s.slice( c[ 0 ].length ).trim();
         c = c[ 0 ].trim();
         if ( s = _getValue( s ))
         	return function( v ) {
         		v = _compare( _getValue( v ) || 0, s );
         		return v == -1 && c[ 0 ] == '<'
         		|| ( v === 0 && c[ 1 ])
         		|| v == 1 && c[ 0 ] == '>';
         	};
         else
         	return function( v ) { return compare( s, v ); };
      }
      else {
      	if ( s[ 0 ] == '"' && s.slice( -1 )  == '"' )
      		s = '^'+ s.slice( 1, -1 ) +'$';
			try {
	         s = new RegExp( s.replace( /(\w)(,|\.|-)(\w)/g, '$1[\\.,-]$3' ), 'i' );
	         return function( v ) { return s.test( v ) ? 1 : 0; };
			}
			catch(_) {
	         return function( v ) { return s.indexOf( v ) > -1; };
			}
      }

      function _getValue( s ) {
      var
         v = false
         ;
         if ( v = s.match( /^\d{1,4}\D\d\d?\D\d{1,4}/ )) { // analyze if date...
         	v = v.split( /[T\/.:,_-]/ );
            v = v.splice( 0, 3 ); 
            if ( v[ 2 ].length == 4 )
               v.reverse();
            v = v.join( '-' );
            if ( s.length )
               v += 'T'+ s.join( ':' );
            v = new Date( v );
            v = !isNaN( v.getTime()) ? v : false;
         }
         else if ( v = s.match(/^\d+([,\.]\d*)?$/ )) { // analyze if number...
	         if ( isNaN( v = parseFloat( s )))
	            if ( isNaN( v = parseInt( s )))
	            	v = false;
         }
         return v;
      }
      function _compare( a, b ) { return a < b ? -1 : a > b ? 1 : 0; }
   }
   /*****
	 *
	 *	Some functions to deliver struct standards, that may be used to create
	 *	bigger structs converted to HTML via $CE or getStruct.createNodes()
	 * 
	 *****/
	, getStruct = {
		createNodes: $CE
		, button: function( bT, bC, cN ) {
		//@ PARAM buttonText ( may be expanded by an third arrayValue := href)
		//@ returns struture to generate button, some span for possibility of sliding
		//@ door graphical button scaling.
			return [ !!bT[ 3 ] ? 'a' : 'button', {
				CN: cssNS.button + ( !!cN ? ' ' + cN : '' )
				, type: bT == lang.reset ? 'reset'
				: bT == lang.store || bT == lang.submit ? 'submit' : 'button'
				, value: !!bT[ 3 ] || !!cN ? null : bT[ 0 ]
				, href: !!bT[ 3 ] ? bT[ 3 ] : null
				, MC: bC
				}
				, [ 'span', {}
				, [ 'span', {}
				, [ 'span', {}
					, [ 'span', {}
						, [ 'span', {}
							, [ 'span', {}, bT[ 0 ]
							]
						]
					]
				]
				]
				]
			];
		}
		, buttonRow: function( sT, rT, cT, sC, rC, cC ) {
		//@ PARAM specialText: [lang.store] and [lang.reset], else other like lang.submit
			return [ 'div', { CN: cssNS.buttonRow }
				, this.button( cT || lang.cancel, cC || 'useLib.call.closeForm(this.form)' )
				, this.button( rT || lang.reset, rC || null )
				, this.button( sT || lang.store, sC || null )
			];
		}
		, doubleButton: function( bT, fN ) {
		//@ PARAM buttonText, functionName
			return [ 'div', { CN: cssNS.doublebutton }
				, this.button( bT, '', cssNS.top )
				, this.button( [[ 'b', {}, '-' ]], fN + '(-1)', cssNS.left )
				, this.button( [[ 'b', {}, '+' ]], fN + '(1)', cssNS.right )
			];
		}
		, radioCheck: function( checkbox, cN, label, id, name, value, checked
			, disabled, shortcut, MC ) {
			return [ 'div', { CN: cN || null }
				, [ 'input', {
					id: id
					, type: checkbox ? "checkbox" : "radio"
					, name: name || null
					, value: value || null	// this causes 'on' as default if checked
					, checked: checked ? 'checked' : null
					, disabled: disabled ? 'disabled' : null
					, MC: MC || null
				}]
				, [ 'label', { 'for': id, IH: ( shortcut ? '<tt>'+ shortcut +'</tt>'
				: '' ) + useLib.WIKI.toHTML( label ).slice( 3, -4 )}
				]
			];
		}
		, getText: function ( t, cN ) {
			return !!t && t.length ? [ !!useLib.WIKI.open || /\n\n/.test( t )
			? 'div' : 'span', { CN: cN || null, IH: useLib.WIKI.toHTML( t ) }]
			: null;
		}
		, input: function( id, text, params, width ) {
			params.id = id;
			return [ 'div', !!width ? { style: '--ulwidth:'+ width } : {}
				, [ 'label', { 'for': id,  CN: text[ 1 ] ? cssNS.hasInform : null }, text[ 0 ]]
				, this.getText( text[ 1 ], cssNS.inform )
				, this.getText( text[ 2 ], cssNS.explain )
				, [ params.type == 'textarea' || params.type == 'select'
				? params.type : 'input', params ]
			];
		}
		, header: function( text, level, cN, noExplain ) {
			return [
				[ 'h' + ( level || 1 ), { CN: cN || null
					, IH: useLib.WIKI.toHTML( text[ 0 ]).replace( /<p>([\s\S]+)<\/p>/, '$1' ) }
				]
				, this.getText( text[ 1 ], noExplain ? null : cssNS.inform )
				, this.getText( text[ 2 ], noExplain ? null : cssNS.explain )
			];

		}
		, container: function( lm, c, NOWIKI ) { // landmark, wikiContent
		var
			_lm = htmlSupport.landmarks[ lm ]
			;
			IH = [ 'div', { CN: cssNS.landmark + ' '
			+ ( !!_lm ? _lm.selector.slice( 1 ) : lm )
			, IH: !!NOWIKI ? c : useLib.WIKI.toHTML( c ) }
			// append also the design layer of landmark frame
			, [ 'div', { CN: cssNS.design
				, IH: '<span><span><span><span><span></span></span></span></span></span>'
			}]];
			return $CE( IH ); 
		}
	}
	/*****
	 *
	 *		functions to initializise the useLib corner controls:
	 *		selector (sizer, explainer, designer), jumper, fastfeeder
	 *		and helpTxt to decribe basics of the use of the useLib
	 * 
	 *****/
	, minimizer = function( type ) {
		return [ 'button', { 
			CN: cssNS.minimizer
			, MC: 'useLib.call.setMinimizer(this)'
			, T: type +' '+ lang.minimizer[ 0 ]
			}
		];
	}
	, controls = {
		init: function() {
		var
			cDS = str2obj( conf.userSupport )
			, w
			;
			if ( !!!this.done ) {
				this.helpTxt = [];
				if (( cDS.sizer || cDS.explainer || cDS.designer ))
					this.selector( cDS );
				if ( cDS.security && useLib.security )
					this.security();
				if ( cDS.hamburger ) 
					this.hamburger();
				if ( cDS.jumper )
					this.jumper();
				if ( cDS.fastfeeder )
					this.fastfeeder();
				this.section( lang.interaction, 2 );
				this.section( lang.help, 1, this.helpTxt );
				this.done = true;
				if ( $Q( D.body, '.' + cssNS.layout, 1 )) {
					w = DOM.getScrollbarWidth();
					$S( $( cssNS.selector )).right = w + 'px';
					$S( $( cssNS.jumper )).right = w + 'px';
				}
				if ( cDS.showhelp && useLibData.show == 1 ) {
					setTimeout( function() { $( 'uLhelp' ).click(); }, 1000 );
					useLibData.show = 0;
					store();
				}
			}
		}
		, selector: function( cDs ) {
		//@ PARAM conf.userSupport
		var
			designs = Object.keys( conf.designDefs )
			, sizer = []
			, explainer = []
			, designer = []
			;
			if ( !$( cssNS.selector )) { // not allready done?
				if ( $LS.fake ) {
					this.section( msgReplace( 'localStorage'
					, lang.options[ 0 ], 0, 3 ), 2 );
				}
				if ( cDs.sizer ) {
					sizer = [ 'div', { id: cssNS.sizer, T: lang.sizer[ 1 ] }
						, [ 'span', { MC: "useLib.call.resizeFont(-1)" }, '\u25C4' ]
						, [ 'span', { MC: "useLib.call.resizeFont(1)"
							, CN: cssNS.sizer }
							, lang.sizer[ 0 ] + ( useLibData.size + 1 )]
						, [ 'span', { MC: "useLib.call.resizeFont(1)" }, '\u25BA' ]
						];
					this.section( lang.sizer, 3 );
				}
				if ( cDs.explainer ) {
					explainer = [ 'div', { T: lang.explainer[ 1 ] }
						, [ 'label', { 'for': cssNS.explainer }, lang.explainer[ 0 ]]
						, [ 'select', { 'id': cssNS.explainer, OC: "useLib.call.setExplain()"}
							, [ 'option', { value: cssNS.explainAll }, lang.explainAll[ 0 ]]
							, [ 'option', { value: cssNS.explainLess }, lang.explainLess[ 0 ]]
							, [ 'option', { value: cssNS.explainNone }, lang.explainNone[ 0 ]]
							]
						];
					this.section( lang.explainer, 3 );
				}
				if ( cDs.designer && hasNet ) {
				var
					e = $( 'uLstandSheet' )
					;
					if ( e.tagName == 'STYLE' ) {	// load from net
					//@ if there is a list of designs and the standard is given by
					//@ a style tag, that tag has to be exchanged to a link tag
					//@ to be able to change the href attribute
						DOM.insertBefore( e
							, $CE([ 'link', { title: conf.designDefault
							, type: 'text/css', rel: 'stylesheet'
							, href: conf.designDefs[ conf.designDefault ].path }])
						);
						activeDesignLink = !!e ? e.previousSibling : null;
						DOM.removeNode( e );
						$A( activeDesignLink, 'id', 'uLstandSheet' );
					}
					else
						activeDesignLink = e;
					setDesign( activeDesignLink, useLibData.design );
					if ( activeDesignLink && designs.length > 1 && hasNet ) {
						designs = conf.designDefs;
						designer = [ 'select', { id: cssNS.designer, OC: "useLib.call.setDesign()" }];
						for (var n in designs ) {
							designer.push([ 'option'    // find design wording & designer
								, { value: n, title: designs[ n ].designer || null }
								, designs[ n ][ lang.uLlang ] || designs[ n ][ 'en' ] || n
							]);
						}
						designer = [ 'div', { T: lang.designer[ 1 ] }
							, [ 'label', { 'for': cssNS.designer }, lang.designer[ 0 ]]
							, designer
							];
							this.section( lang.designer, 3 );
					}
				}
				this.section( lang.options, 2, this.helpTxt ); 
				this.section( lang.minimizer, 2 );
				D.body.appendChild( $CE(
					[ 'div', { id: cssNS.selector, CN: cssNS.noUserSelect }
						, [ 'div', {}
							, [ 'h1', {}, lang.options[ 0 ]]					
							, sizer
							, explainer
							, designer
							, [ 'div', { CN: cssNS.help }
								, [ 'button', { id: 'uLhelp', CN: cssNS.help, T: lang.help[ 0 ] }]
								, [ 'button', { id: 'uLmsghold', CN: cssNS.msghold
									+ ( useLibData.msghold ? ' ' + cssNS.locked : '')
									, T: lang.msghold[ 0 ] }]
								, [ 'button', { id: 'uLsound', CN: cssNS.sound
									+ ( useLibData.sound ? ' ' + cssNS.locked : '')
									, T: lang.sound[ 0 ] }]
								, [ 'button', { id: 'uLseminar', CN: cssNS.seminar
									+ ( useLibData.seminar ? ' ' + cssNS.locked : '')
									, T: lang.seminar[ 0 ] }]
								// special treatment: FF don't dragg buttons! :]
								, cDs.security ? [ 'span', { id: cssNS.secDragg
									, CN: cssNS.locked 
									, T: lang.security[ 0 ] }] : null
								// may be more in later versions...
							]
							, [ 'div', { id: cssNS.copyright }	// set copyright
								, [ 'a', {
									  href: homepage
									, target: '_blank'
									}
									, develop
								]
								, [ 'a', {
									  href: homepage
									, target: '_blank'
									}
									, copyright
								]
							]
						]
						, minimizer( lang.options[ 0 ])
					]
				));

				function _toggle( ev ) {
					ev = ev.currentTarget;
					useLibData[ ev.id ] = $CN.toggle( ev, cssNS.locked ) ? 1 : 0;
					store();
					return false;
				}
				$E( $( 'uLhelp' ), 'MC', function _showHelp(){ controls.showHelpTxt(); });
				$E( $( 'uLmsghold' ), 'MC', _toggle );
				$E( $( 'uLsound' ), 'MC', _toggle );
				$E( $( 'uLseminar' ), 'MC', function _showSeninar( ev ){
					_toggle( ev );
					controls.showSeminar( $CN.contains( ev.currentTarget, cssNS.locked ));
				});
			}
			call.uLinit( 'resizeFont', function( index ) {
			var
				size = D.body.style.fontSize
				, e = $( cssNS.sizer ).firstChild.nextSibling
				;
				index += useLibData.size;
				index = index < 0 ? sizeList.length -1
				: index >= sizeList.length ? 0 : index;
				useLibData.size = index;
				e.innerHTML = e.innerHTML.replace( /\d$/
					, ( useLibData.size + 1 ));
				D.body.style.fontSize = sizeList[ useLibData.size ];
				store();
				DOM.redraw( D.body ); // prevent wrong rendering
				e = D.activeElement;
				if ( e )
					DOM.scrollIntoView( e );
			});
			call.uLinit( 'setExplain', function( index ) {
			var
				e = $( cssNS.explainer )
				;
				if ( e ) {
					$CN.remove( D.body, e.options[ useLibData.explain ].value );
					useLibData.explain = e.selectedIndex = !!index ? index
					: e.selectedIndex;
					setTimeout( function() { // some kind of behavior of FireFox...
						if ( e.selectedIndex != useLibData.explain )
							useLib.call.setExplain( e.selectedIndex );
					}, 1000 );
					$CN.add( D.body, e.options[ e.selectedIndex ].value );
					store();
					DOM.redraw( D.body ); // prevent wrong rendering
					if ( useLibData.explain === 0 )
						DOM.scrollIntoView( D.body, 0, 0 );
					else
						setFocus( lastActive );
				}
			});
			call.uLinit( 'setDesign', function( index ) {
			var
				e = $( cssNS.designer )
				;
				if ( e ) {
					e.selectedIndex = isNaN( index )	? e.selectedIndex : index;
					activeDesignLink.href = conf.designDefs[ e.options[
					e.selectedIndex ].value ].path;
					setDesign( activeDesignLink, useLibData.design );
					// necessary because of wrong rendering (especially for IE)
					DOM.redraw( D.body );
					setFocus( lastActive );
				}
			});
			call.drop.init( $( cssNS.selector ));
			useLib.call.resizeFont( 0 );
			useLib.call.setExplain( useLibData.explain );
			useLib.call.setDesign( useLibData.design );
			controls.showSeminar( useLibData.seminar );
		}
		, hamburger: function() {
		var
			e = $Q( D.body, '.' + cssNS.layout, 1 )
			;
			DOM.insertBefore( e, $CE([ 'button'
				, { id: cssNS.hamburger, T: lang.hamburger[ 0 ]}]
			));
			e = $( cssNS.hamburger );
			$E( e, 'MC', function _doHamburger() {
				useLibData.hamburger = $CN.toggle( D.body, cssNS.hiddenNavi ) ? 1 : 0;
				store();
				return false;
			});
			if ( !!e && useLibData.hamburger )
				e.click();
		}
		, showSeminar: function( off ) {
		var
			lastKey = ''
			, lastTime = -1
			, keyCount = 0
			;
			$E( W, 'MM', _headlight, off );
			$E( W, 'KD', _keyStroke, off );
			_doNode( cssNS.headLight );
			_doNode( cssNS.keyStroke );
			$E( $( cssNS.keyStroke ), 'MD', function _dragStart( ev ){
				DOM.dragStart( ev, this, 3, 0, 0, W.innerWidth -50, W.innerHeight -50 );
			});
			if ( lastTime < 0 )
				_catchStopPropagation();

			function _doNode( t ) {
				if ( off )
					DOM.removeNode( $( t ));
				else
					D.body.appendChild( $CE([ 'div', {	id: t }, [ 'div', {}]] ));			
			}

			function _headlight( ev ) {
			var
				e = $( cssNS.headLight )
				;
				if ( e ) {
					ev = DOM.getCoord( ev );
					DOM.setPos( e.firstChild, ev.x - W.pageXOffset -80
					, ev.y - W.pageYOffset -80 );
				}
			}

			function _keyStroke( ev, from ) {
			var
				signs = {
					// BS, TAB, ENTER, PAUSE,
					8: '\u232B', 9: '\u21E5' , 13: '\u23CE', 19: '\u23F8'
					// ESC, SPACE, PAGEUP, PAGEDOWN, 
					, 27: '\u238B', 32: '\u2423', 33: '\u21DE', 34: '\u21DF'
					// END, HOME, INSERT, DELETE
					, 35: '\u21D8', 36: '\u21D6', 45: '\u21AA', 46: '\u2326'
					// arrowup, arrowdown, arrowleft, arrowright
					, 38: '\u25B2', 40: '\u25BC', 37: '\u25C4', 39: '\u25BA'
				}
				, c = ev.keyCode
				, shift = ev.shiftKey && c != 16		// SHIFT, but not alone
				&& ( ev.key.length > 1 || ev.altKey || ev.ctrlKey || ev.metaKey )
				, meta = ev.metaKey && c != 224		// OSKEY, but not alone
				, ctrl = ev.ctrlKey && c != 17 		// CTRL, but not alone
				, alt = ev.altKey && c != 18	 		// ALT, but not alone
				&& ( ev.key.length > 1 )
				, e = $( cssNS.keyStroke )
				;
				c = signs[ ev.keyCode ] || ( ev.key.length < 3 ? ev.key : '' );
				if ( !!e && c.length ) {
					c = ( shift ? '^' : '' )
					+ ( alt ? '\u2325' : ''	)
					+ ( ctrl ? '\u2388' : '' )
					+ ( meta ? '\u2318' : '' )
					+ c
					+ ( ev.key.length > 2 || ev.keyCode == 32 ? '<div>'
						+ ( shift ? 'Shift ' : '' )
						+ ( alt ? 'Alt ' : ''	)
						+ ( ctrl ? 'Ctrl ' : '' )
						+ ( meta ? 'Cmd ' : '' )
						+ ( ev.keyCode == 32 ? 'Space' : ev.key ) + '</div>' : ''
					);
					if ( lastKey == c )
						keyCount++;
					else
						keyCount = 0;
					lastKey = c;
					e.innerHTML = ( D.activeElement && D.activeElement.type == 'password'
					? '\u2022' : c ) + ( keyCount < 2 ? '' : '<span>'+ keyCount +'</span>' );
					clearTimeout( lastTime );
					lastTime = setTimeout( function() {
						setOpenClose( e, false, 1.7 );
						lastKey = '';
					}, 1500 );
					$CN.add( e, cssNS.show );
				}
			}

			function _catchStopPropagation() {
			//@ calls of stopPropagation() prevent the event bubbling up to window
			//@ so the key would not no be displayed.
			//@ Replaces the prototype of it to analize the event and calles the
			//@ original after calling _keyStroke() - bad boy ;-)
			var
				original = Event.prototype.stopPropagation
				;
				Event.prototype.stopPropagation = function() {"native code";
					if ( this.currentTarget != W && this.type == 'keydown'
					&& !useLibData.seminar )
						_keyStroke( this, 'from' );
					return original.apply( this );
				};
			}
		}
		, security: function() {
			useLib.security.dragsecure( $( cssNS.secDragg ));
		}
		, jumper: function() {
			if ( !$( cssNS.jumper )) { // not already done?
				call.uLinit( 'setTop', function( delay ) {
					DOM.scrollIntoView( lastLinkPosition || getLandmark( 'main' )
					, delay || 0, 1 );
					lastLinkPosition = false;
				});
				this.section( lang.jumper, 2);
				D.body.appendChild( $CE(
					[ 'div', {
						id: cssNS.jumper, CN: cssNS.noUserSelect
						, MC: 'useLib.call.setTop();'
						, T: lang.jumper[ 0 ]
						}
						, [ 'button', { CN: cssNS.jumper }]
					]
				));
			}
		}
		, fastfeeder: function() {
			if ( $( cssNS.fastfeeder )) // already done?
				return;
			call.uLinit( 'pasteFastfeeder', function( ev ) {// event happens before value is in
			//@ replace linefeed marker on pasted content for better reading and processing
				setTimeout( function() {							// ...so we use a timeout
					ev.currentTarget.value = ev.currentTarget.value.trim().replace( /\\n */gm, '\n' );
				}, 0 );
			});
			call.uLinit( 'doFastfeeder', function( ev ) {
			//@ react on user special keyboard input
			//@ RETURN [code 13] = send input to form at mnemonic element
			//@ ESC [code 27] = get value from mnemonic element in form
			//@ CTRL [code 17] = replace input to \c
			//@ ALT [code 18] = replace input to \a
			//@ TAB [code 9] = replace input to \t
			//@ ATTENTION: its needed to ask for ctrl- and altKey in combination with a 
			//@ different KeyCode, else it throws the key-sign plus unknown or a key
			//@ combination sign as input like 'Ω' or something...
			//@ REMARK: further codes may be defined as refund for old automatized interaction 
			//@ for testing: <7 4000,12 \t \t 321fedcbA \t\t DE73500694550000021792
			var
				e = ev.currentTarget
				, r = false			// inverted logic for easy return value
				;
				if ( r = ev.keyCode == 13 )							// ...cut enter
					e.value = parseFastfeeder( e.value, false, false, e );
				else if ( r = ev.keyCode == 27 )						// ESC 
					e.value = parseFastfeeder( e.value, true, ev.shiftKey, e );
				else if ( r = ev.ctrlKey && ev.keyCode != 17 )	// CTRL + char, but not CTRL
					e.value = _setOnCaret( e, "\\c" + String.fromCharCode( ev.keyCode ) + ' ' );
				else if ( r = ev.altKey  && ev.keyCode != 18 )	// ALT + char, but not ALT
					e.value = _setOnCaret( e, "\\a" + String.fromCharCode( ev.keyCode ) + ' ' );
				else if ( r = ev.keyCode == 9 )						// TAB
					e.value = _setOnCaret( e, ev.shiftKey ? " \\ST " : " \\T " );
				//if( !r && e.preventDefault )
					//e.preventDefault();
				return !r;
				
				function _setOnCaret( e, t ){
					return e.value.slice( 0, e.selectionStart )
						+ t + e.value.slice( e.selectionEnd );
				}
			});
			function parseFastfeeder( v, ESC, SHIFT, tA ) {
			//@ PARAM valueWithMnemonic, escKey, shiftKey, textAreaOfFastFeeder
			var
				fList = false
				, nBefore = '#1'
				, codeType = false
				, r = ''
				;
				v = v.replace( /\\/g, '\\'			// mask backslash
				).replace( /:\[[^\]]+\]/g, ''		// clean from appended related label
				).replace( /\*s(\r\n|[\n\v\f\r\x85\u2028\u2029])/gm, '\n'
				).replace( /\s*(\\s?t)/gi, '$1'		// clean spaces leading TABs
				).trim().split( /\n+/gm );
				for ( var i = 0; i < v.length; i++ )
					r += '\n' + _analyseValues( v[ i ]);
				if ( !codeType )					// clean double spaces an empty lines
					r = r.replace(/\s+/gm, ' ' ).replace( /\n(\s*\n)+/gm, '\n' );
				else
					setTimeout( function(){ tA.focus();tA.select(); }, 100 );
				r = r.trim();
				cover.keyMsg( 
					$( 'doFastfeeder' )
					, !codeType && !ESC && r.length || r == v ? 'Error' : 'Success'
				);
				return r;

				function _analyseValues( _v ) {
				var
					codewords = {
						message: __toggle 
						, sound: __toggle
						, help: __toggle
						, seminar: __toggle
						, submit: __steer
						, reset: __steer 
						, clear: __steer 
						, close: __steer 
						, dump:	__dump
						, export: __save
						, wikibase: __wiki
					}
					, mn = _v.length ? _v.match( /[^\s]+/ )[ 0 ] : false
					, _r = ''
					, ident
					, e
					;
					if ( mn && codewords[( e = mn.toLowerCase())]) {
						codeType = true;
						_r = codewords[ e ]( e );
					}
					else if ( mn ) {
						if (( ident = __analyseTarget()) && ( e = __findId( ident ))) {
							_v = _v.slice( mn.length ).trim();
							if ( ESC || _v.length ) {
								ident = __expand( e, ident, _v );
								for ( var _i = 0; _i < ident.length; _i++ ) {
									_r = __analyzeV( ident[ _i ].e, ident[ _i ].v );
									if ( ESC || _r.length ) {
										e = ident[ _i ].add;
										_r = ident[ _i ].ident
										+ ( !e ? '' : e < 0 ? e : '+' + e ) 
										+ '\t' + _r + '\n'; // use tab! (may copied into excel)
									}
								}
							}
							else // find first cover element or focus element
								setFocus( e );
						}
						else
							_r = _v;
					}
					return _r;

					function __toggle( code ) {
						$( code == 'message' ? 'uLmsghold' : 'uL' + code ).click();
						return '';
					}

					function __steer( code ) {
						__findId({ ident: '#1', add: 0 });
						if ( fList.length ) {
							if ( code == 'clear' || !dialog.layer.length )
								useLib.call[ code + 'Form' ]( fList[ 0 ].form );
							else if ( code == 'close' )
								useLib.call.modalClose( fList[ 0 ].form );
							else if (( code = $Q( fList[ 0 ].form, '[type=' +  code + ']' ))
							&& code.length )
								code[ 0 ].click();
							return '';
						}
						return code;
					}

					function __save() {
					var
						n = location.pathname.match( /([^\/]+)\.html/ )
						;
						call.saveAs( n ? n[ 1 ] + '.json' : ''
							, JSON.stringify( getJSONValues(), null, '\t' ));
						return '';
					}

					function __dump() {
					var
						r = ''
						;
						ESC = true;
						__findId({ ident: '#1', add: 0 });
						for ( var i = 0, c; i < fList.length; i++ ) {
							c = Object.values( mnemonics ).indexOf( fList[ i ].id );
							r += _analyseValues( c > -1 ? Object.keys( mnemonics )[ c ]
							: '#' + ( i + 1 ));
						}
						return r;
					}

					function __wiki() {
					__findId({ ident: '#1', add: 0 });
					var
						wS = wikiSigns
						, __r = wS.block + 'form' + ___get( fList[ 0 ].form, 'name', 'action' )
						, last = ''
						;
						for ( var i = 0; i < fList.length; i++ )
							__r += ___analyze( fList[ i ]);
						return __r  + '\n' + wS.blockEnd;

						function ___get( __e, _s ) {
						var
							__s = ''
							;
							for ( var j = 2, v; j < arguments.length; j++ ) {
								v = $A( __e, arguments[ j ]);
								if ( v != _s )
									__s += ' ' + arguments[ j ] + '="' + v + '"';
							}
							return !__s.length ? '\n'
							: '\t' + wS.interface + __s + ' ' + wS.interface + '\n';
						}

						function ___analyze( _e ) {
						var
							a = getSelectionAttr( _e )
							, fD = getFormDef( _e )
							, s = getLabelText( _e )
							, ___r = ''
							;
							if ( a == 'value' ) {
								___r += wS.header + ' ' + s + ' ' + ___get( _e, s, 'name' )
								+ wS.define + ( fD.type || 'text' ) + '\n\n';
							}
							else if ( a == 'checked' ) {
								a = _e.type == 'radio' ? wS.bullet : wS.number;
								___r += a + ' ' + s + ' ' + ___get( _e, s, 'name', 'value' );
							}
							else {
								___r += wS.header + ' ' + s	+ ___get( _e, 'name' );
								_e = $T( _e, 'option' );
								a = fD.type == 'multiSelect' ? wS.table : wS.indent;
								for ( var k = 0; k < _e.length; k++ )
									___r += a + ' ' + _e[ k ].text
									+ ___get( _e[ k ], _e[ k ].text, 'value' );
							}
							if ( a != last )
								___r = '\n' + ___r;
							last = a; 
							return ___r;
						}
					}

					function __analyseTarget() {
					//@ It may be a defined or a secret mnemonic. A secret mnemonic is at
					//@ least for key users. It matches the element with the after a # sign
					//@ given number of the active form. And the n-th element after or before
					//@ if a number (also negative) follows directly. This also makes forms
					//@ without any mnemonic definiton a bit usable by the fastfeeder...
					var
						reg = /\\(s?t)/gi		// find all tabs
						, l = false
						;
						if ( !( l = ___checkMn( mn, false ))
						&& !( l = ___checkMn( mn.replace( reg, '' ), mn )))
							l = ___countTabs( mn );
						return l;

						function ___checkMn( _mn, add ) {
							if ( mnemonics[ _mn ]) {
								before = _mn;
								return add ? ___countTabs( add ) : { ident: _mn, add: 0 };
							}
							return false;
						}	

						function ___countTabs( _v ) {
						var
							c = 0
							;
							_v = _v.replace( /^\+/, nBefore + '+' ).replace( reg
								, function( _, t ) { return t == 'st' ? '-1' : '+1'  }
							).replace( /(\+|-)\d+/g		// sum all addition
								, function( __v ) { c += $I( __v ); return ''; }
							);
							return { ident: _v, add: c };
						}
					}

					function __expand( _e, _ident, _v ) {
					//@ expand list of numbers etc.
					var
						t = getSelectionAttr( _e )
						, iL = []
						;
						_ident.e = e;
						if ( t == 'value' ) {
							_ident.v = _v;
							return [ _ident ];
						}
						else {
							_v = _v.replace( /(-?\d+)?\s*(>|<)\s*(-?\d+)/g
							, function( _, b, s, e ) {
							var
								r = ( s == '<' ? '-' : '+' ) + ( b || '' ) // value if no value
								;
								s = s == '<' ? ' -' : ' ';
								b = Math.abs( $I( b ));		// tolerance to negativ numbers
								e = Math.abs( $I( e ));
								if ( b > e )
									b = [e, e = b][0];		// swap values
								for ( var _i = b; _i < e; r += s + ( ++_i ));
								return r;
							}).split( /\s+/ );
							for ( var _i = 0; _i < _v.length; _i++ )
								_v[ _i ] = $I( _v[ _i ]) || _v[ _i ];
							if ( t == 'selected' ) {
								_ident.v = _v;
								return [ _ident ];
							}
							else {
								for ( var _i = 0; _i < _v.length; _i++ ) {
									iL.push({
										ident: _ident.ident
										, add: _ident.add	// users don't count from zero!
										+ max( 0, Math.abs( $I( _v[ _i ])) - 1 )
									});
									iL[ _i ].e = __findId( iL[ _i ]);
									iL[ _i ].v = _v[ _i ] < 0 || _v[ _i ] == '-' ? '-' : '+';
									if ( !iL[ _i ].e	// clean list of idents
									|| getSelectionAttr( iL[ _i ].e ) != 'checked' )
										iL[ _i ].pop();
								}
								return iL;
							}
						}
					}

					function __findId( iL ) {
					var
						_e = $( mnemonics[ iL.ident ])
						;
						if ( !_e || iL.add ) {
							if ( !fList ) {
								fList = dialog.layer.length ? dialog.layer.slice( -1 )
								: false;
								fList = fList ? $T( $( fList[ 0 ]), 'form', 1 ) : D.forms[ 0 ];
								fList = fList
								? Array.prototype.slice.call( filterElements( fList ))
								: false;
							}
							_e = ( !!_e ? fList.indexOf( _e )
							: $I( iL.ident.slice( 1 )) -1 ) + iL.add;
							return !fList || _e < 0 || _e >= fList.length ? false
							: fList[ _e ];
						}
						else
							return _e;
					}

					function __analyzeV( e, v ) {
					var
						a = getSelectionAttr( e )
						, fD = getFormDef( e )
						, __r = ''
						, o
						;
						DOM.scrollIntoView( $P( e ), 0, -1 );
						if ( ESC ) {
							v = SHIFT ? getResetEle( fD ) : fD.value;
							if ( a == 'value' ) {
								__r += ( $CN.contains( e, cssNS.covered )
								? cover.getLocal( fD.type, v[ 0 ]) 
								: v[ 0 ]) + ___getRelatedLabel( e );
							}
							else if ( a == 'checked' ) {
								__r += ( v.indexOf( e.value ) > -1 ? '+' : '-' )
								+ ___getRelatedLabel( e );
							}
							else {
								e = $T( e, 'option' );
								for ( var i = 0; i < e.length; i++ )
									if ( v.indexOf( e[ i ].value ) > -1 )
									// users don't count from zero...
										__r += ( i + 1 ) + ___getRelatedLabel( e[ i ]);
							}
						}
						else if ( !e.disabled ) {
						// don't write if disabled, clear if minus sign else set value
		 					if ( !v.length )
								setFocus( e );
							else {
								delayRemove( $P( e ), cssNS.focused, 3 );
								if ( a == 'value' ) {
									if ( v == '-' )
										e[ a ] = '';
									else {
										if ( $CN.contains( e, cssNS.covered )) {
											// special treatment for country keys in front because of default
											if ( fD.type != 'country'
											&& Object.keys( fD.subs )[ 0 ] == 'COUNTRY'
											&& $( fD.first ).value.length == 2
											&& !/^[a-z]{2,2}/i.test( v ))
												v = $( fD.first ).value + v;
											// jump the focus to insure updating onblur
											$( fD.first ).focus();
											doEvent.setInput( $( fD.first ), e.id, v, true );
											$( 'doFastfeeder' ).focus();
											if ( cover.errorList.length )
												__r = v;
										}
										else if ( !fD.regexp || fD.regexp.test( v ))
											e[ a ] = v;
										else
											__r = v;
									}
								}
								else if ( a == 'checked' ) {
									e[ a ] = v != '-';
								}
								else {
									o = $T( e, 'option' );
									v = v.filter( function( _v, _i ) { 
										v[ _i ] = $I( _v );
										return v[ _i ];}
									).sort( function( a, b ) { return a - b; });
									for ( var j = 0, vi; j < v.length; j++ ) {
									// users don't count from zero!
										vi = Math.abs( v[ j ]) - 1;
										if ( !!o[ vi ] && !!o[ vi ].text
										&& !o[ vi ].disabled ) {
											o[ vi ][ a ] = v[ j ] > 0;
											$A( o[ vi ], a, v[ j ] > 0 ? a : -1 );
											v[ j ] = '';
										}
									}
									__r = v.join( ' ' ).trim();
									onchange( e, null );
								}
								setTimeout( function(){ $( 'doFastfeeder' ).focus();}, 500 );
							}
						}
						else
							__r = v;
						return __r;

						function ___getRelatedLabel( e ) {
						var
							t = ' '
							;
							if ( $( cssNS.fastLabel ).checked )
								t = ' :['+ ( e.tagName == 'OPTION' ? e.text : getLabelText( e )) + '] ';
							return t;
						}
					}
				}
			}
		var
			head = getStruct.input( 'doFastfeeder', lang.fastfeeder, {})
			;
			head[ 2 ][ 1 ].IH = '<kbd>F2</kbd> ';		// add shortcut view
			head[ 3 ][ 1 ].CN += ' ' + cssNS.bottom;	// set bottom class for tooltipp
			head.splice( -2 );							// delete explain and input item 
			call.uLinit( 'setFastfeeder', function() {
			var
				active = !$CN.contains( $( cssNS.fastfeeder ), cssNS.minimized )
				;
				active = active ? 1 : 0;
			 	active += $( cssNS.fastLabel ).checked ? 2 : 0;
				useLibData.feeder = active;
				store();
			});
			D.body.appendChild( $CE(
				[ 'div', { id: cssNS.fastfeeder, CN: useLibData.feeder ? null : cssNS.minimized }
					, head
					, getStruct.radioCheck( true, cssNS.fastLabel, lang.fastLabel[ 0 ]
						, cssNS.fastLabel, null, null, useLibData.feeder > 1, null, null
						, 'useLib.call.setFastfeeder()' )
					, [ 'div', {}
						, [ 'textarea', {
							id: 'doFastfeeder'
							, KD: 'return useLib.call.doFastfeeder( event )'
							, onpaste: 'useLib.call.pasteFastfeeder( event )'
							, ondrop: 'useLib.call.pasteFastfeeder( event )'
							, rows: conf.fastFeederRows
							, 'aria-label': lang.fastLabel[ 1 ]
							}
						]
					]
					, minimizer( lang.fastfeeder[ 0 ])
				]
			));
			this.section( lang.fastfeeder, 2 );
		}	/* end of fastFeeder */
		, showHelpTxt: function() {
		var
			id = 'helpTxt' + 'uL__modal__'
			;
			if ( !$( id )) {
				dialog.setModal( 'helpTxt', 'hint', 'draggable closable sizable'
					, [ 'div', { id: cssNS.helpTxt, CN: cssNS.content }].concat( this.helpTxt )
				);
				initFold( $( id ));
				( $T( $( id ), 'h2', 1 )).click(); // close first accordeon
			}
		}
		, section: function( text, level, hlpTxt ) {
		//@ appends a section struct to helpText based on a useLib definition text
		//@ REMARK: the right sequence of calling is important to create the hierarchy.
		//@ 			It's a bit complicated to work with minimized wiki and full
		var
			add = !!text[ 2 ] ? 2 : 1
			, head = text[ 0 ] == 'span' ? text.slice( 2 )
			: getStruct.header( text, level, level == 2 ? cssNS.fold : null, true )
			;
			if (hlpTxt)
				head[ add ] = head[ add ].concat( hlpTxt );
			head = [ head[ 0 ], [ 'div', {}, head[ 1 ], head[ 2 ]]];
			this.helpTxt = hlpTxt ? head : this.helpTxt.concat( head );
		}
		, helpTxt: []
	}
	//******* end of corner controls **************************//
	, initId = function() {
	// iDs have to be unique, there may be some already given iDs i. e. of WIKI,
	// so its nessesary to start new iDs on the higest given number
	// REMARK: this has to be called first on useLib.init()
		D.body.innerHTML.replace( /\sid="uL_(\d+)/g, function( _, d ) {
			d = $I( d );
			if ( d > counter )
				counter = d;
		});
		counter++;
	}
	, ensureId = function( e ) { if ( !!!e.id ) e.id = 'uL_' + counter++; return e.id; }
	, counter = 5
	, min = Math.min
	, max = Math.max
	, sizeList = "37.5% 50% 62.5% 75% 87.5% 100% 112.5%".split( ' ' ) // sizing cycle
	, useLibData = $LS( 'useLibData' )
	, store = function () {	$LS( 'useLibData', useLibData ); }
	, activeDesignLink = false
	, getHTMLTag = function( t, attr, content, path=false ) {
	// short version for some internal calls
		return '<'+ t +' '+ ( attr || '' ) + ( t != 'link' && t != 'script'
		? '>' : ' type="text/'+ ( t == 'script' ? 'javascript"'
		: 'css" charset="utf-8" ' ) + ( !path ? '>' : ( t == 'link'
		? ' rel="Stylesheet" href="' : ' src="' ) + path + '">' )) + ( content || '' )
		+ ( t != 'meta' && t != 'link' ? '</'+ t +'>' : '' );
	}
	, openPopup = function( h, b, wA, cN ) {
	//@PARAMETER headerHTMLContent, bodyHTMLContent, windowAttributes, className
	var
		w = W.open( "", "_blank"
		, wA || 'dependent=yes,scrollbars=yes,resizable=yes' )
		;
		if ( w ) {			// there could be a popup blocking
			dialog.setMsg( 'hint', 'popUp', 0, 6500 );
			w.document.write(	'<!DOCTYPE HTML>'  
			+ getHTMLTag( 'html', 'lang="'+ conf.lang +'"'
				, getHTMLTag( 'head', ''
					, getHTMLTag( 'meta', 'language="'+ conf.lang +'"' )
					+ getHTMLTag( 'meta', _c( 'type', 'text/html; charset=utf-8' ))
					+ getHTMLTag( 'meta', 'name="viewport" content="width='
					+ 'device-width,initial-scale=1.0,user-scalable=no"' )
					+ h
				)
				+ getHTMLTag( 'body', cN ? 'class="'+ cN +'"' : '', b )
			));
			w.document.close();
		}
		return w;

		function _c( equiv, cont ) {
			return 'http-equiv="content-' + equiv + '" content="' + cont
			+'"';
		}
	}
	, analyseICAL = function ( cal={}, ss='' ) {
		ss.replace( /BEGIN:VEVENT([\s\S]+?)END:VEVENT/img, function( _, s ) {
		var
			hasTime = '', b, e
			;
			s.replace( /DT(START|END).*?:(\d{4,4})(\d\d)(\d\d)T(\d\d)(\d\d)/img
			, function( _, p, yyyy, mm, dd, h, m  ) {
				if (( $I( h ) + $I( m )) > 0 )
					hasTime += ( hasTime.length ? '-' : '' )+ h +':'+ m;
				dd = $D( yyyy +'-'+ mm +'-'+ dd +'T00:00:00Z' );
				if ( p == 'START' )
					b = dd;
				else
					e = dd;
				return '';
			})
			;
			s = s.match( /SUMMARY:([^\n\r]*)/i );
			if ( s && ( s = s[ 1 ] ) && s.length ) {
				if ( hasTime.length )
					s += ' ('+ hasTime +')';
				cal[ s ] = { begin: b, end: e };
			}
			return '';
		});
		return cal;
	}
	;
	/*****
	 *
	 *		Start of the minimized wiki to html parser section
	 *
	 *		@ this is just a very simple wiki version, it may be replaced for complex 
	 *		@ wiki definition if the modul is reachable on the given path.
	 *		@ 
	 *		@ Here are paragraphs and attributes processed only:
	 *		@ 		++ // -- __ '' ,, %% §§ &&, the last with predefined classes (kbd
	 *		@		tt quote hint taste telex zitat tipp) and user defined classnames
	 * 
	 *****/
	this.WIKI = (function() {
	var wS = wikiSigns
		, mask = function ( t ) {
			return t.replace( /([\*\+\-\|\/\\\(\)\{\}\[\]])/g, "\\$1" );
		}
		, attrReg = function ( sign, ext, endSign ) {
		//@ don't use ([\s\S]*?) instead of (.*?) to delimit it on single paragraph
			sign = mask( sign );
			return new RegExp( sign + ( ext || '' ) + '(.*?[^\\\\])'
			+ ( !!endSign ? endSign : sign ), 'g' );
		}
		, getAttrSpan = function( _, c, s ) {
			return '<span class="' + cssNS[ c ] + '">' + s + '</span>'; 
		}
		, getLink = function( _, t ) {
		var
			e = t.match( /^extern / )
			, a = '<a '
			;
			if ( e ) {
				t = t.slice( e[ 0 ].length );
				a += 'class="' + cssNS.extern + '" target="_blank" '
			}
			e = t.indexOf( ' ' );
			e = e == -1 ? t.length : e;
			return a + 'href="' + t.slice( 0, e ) + '">' + t.slice( e ) + '</a>';
		}
		, checkSec = new RegExp( '\n[' + mask([ wS.engine, wS.header, wS.number, wS.indent
			, wS.define, wS.table, wS.block, wS.explain, wS.inform ].join( '' ))+ ']', 'm' )
		, attr = [
		[ attrReg( wS.engine, '', wS.engineEnd ), '' ]			// clean from engine
		, [ attrReg( wS.bold ), '<strong>$1</strong>' ]
		, [ attrReg( wS.italic ), '<em>$1</em>' ]
		, [ attrReg( wS.under ), '<u>$1</u>' ]
		, [ attrReg( wS.strike ), '<span class="' + cssNS.strike + '">$1</span>' ]
		, [ attrReg( wS.quote ), '<q>$1</q>' ]
		, [ attrReg( wS.sup ), '<sup>$1</sup>' ]
		, [ attrReg( wS.sub ), '<sub>$1</sub>' ]
		, [ attrReg( wS.kbd ), '<kbd>$1</kbd>' ]
		, [ attrReg( wS.typ, '([^\s]+)' ), getAttrSpan ]
		, [ new RegExp( mask( wS.link ) + '(.*?)' + mask( wS.linkEnd ), 'g' ), getLink ]
		, [ new RegExp( '(^|\n)' + mask( wS.bullet), 'g' ), '\u2022' ]
		]
		;
		return {
			mask: mask
			, regs: { findEngine: function() {	// lb is for markup \u2003
				return new RegExp( mask( wS.engine )
				+ '\\s*([^\\s=\\}\\n]+)\\s*(=)?\\s*([\\s\\S]*?)'
				+ mask( wS.engineEnd ), 'img' );
			}}
			, clean: function( s ) {
				return s.replace( /\r/gm, '' ).replace( /\/\*[\s\S]*?\*\//gm, '' );
			}
			, toHTML: function( s ) {
				if ( checkSec.test( s ) && !useLib.WIKI.open )
					console.warn( "useLib hint: to parse a given string it would be "
					+ 'nessesary to load the WIKI-Part!\n"' + s + '"' );
				for ( var i = 0; i < attr.length; i++ )
					s = s.replace( attr[ i ][ 0 ], attr[ i ][ 1 ]);
				s = s.split( /\n\n*/ );
				return s.length > 1 ? '<p>' + s.join( '</p><p>') + '</p>' : s[ 0 ];
			}
		}
	})();
	DOM.delayRemove = delayRemove;
	useLibData = useLibData || { size: 2, explain: 0, design: 0, feeder: 0
		, show: 1, msghold: 0, sound: 0, seminar: 1, hambuger: DOM.isTouch ? 1 : 0
	}
	;

	call.uLinit( 'setMinimizer', function( e ) {
		$CN.toggle( $P( e ), cssNS.minimized );
		if ( $P( e ).id == cssNS.fastfeeder )
			useLib.call.setFastfeeder();
	});

	call.uLinit( 'saveAs', function( fname, s ) {
		if ( s.length ) {
		var
	      d = D.createElement( 'a' );
			;
			d.href = s.slice( 0, 5 ) == "data:" ? s
			: ( "data:text/plain;charset=utf-8,"+ encodeURIComponent( s ));
			d.download = fname;
			D.body.appendChild( d );
			d.click();
			setTimeout( function() { DOM.removeNode( d ); }, 0);
		}
		return false;
	});

	call.uLinit( 'drop', {
		init: function( e ) {
			if ( W.File && W.FileReader && W.FileList && W.Blob ) {
	         $A( e, 'ondragover', 'useLib.call.drop.ondragover(event)' );
				$A( e, 'ondrop', 'useLib.call.drop.ondrop(event)' );
	         //$E( e, 'dragover', this.ondragover );
				//$E( e, 'drop', this.ondrop );
				// prevent form loading some thing if target is missed
	         $E( D.body, 'dragover', this.onprevent );
				$E( D.body, 'drop', this.onprevent );
			}
		}
		, onprevent:  function( ev ) { ev.preventDefault(); return false; }
		, ondragover: function( ev ) {
         ev.stopPropagation();
         ev.preventDefault();
         ev.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
         return false;
      }
		, ondrop: function( ev, files ) {
		//@ may be called from file onchange also, then files is dataTransfer ...
			ev.stopPropagation();
			ev.preventDefault();
			if ( files || ev.dataTransfer.files.length ) {
			var
				f = files || ev.dataTransfer.files
				, e = ev.currentTarget		// event owning object
				, accept = $A( e, 'accept' )
				, done = false
				;
				accept = !accept ? false
				: new RegExp( accept.replace( /\*/g, '' ).replace( /,/g, '|' ));
				if ( f.length && e.type == 'file' )
					delayRemove( $P( e ), cssNS.success, 2 );
				for ( var i = 0, r = [], type; f && i < f.length; i++ ) {
					if ( accept && !accept.test( f[ i ].type )) {
						dialog.setMsg( 'error', filename2Msg( 'fileFormat', f[ i ].name )
						, 0, 4000 );
						continue;
					}
					type = f[ i ].type.match( /^(image|text|audio|video)\/(.+)/i )
					|| f[ i ].type;
					if ( Array.isArray( type ))
						type = type[ 1 ] == 'text' ? type[ 2 ] : type[ 1 ];
					else
						type = type.replace( /application\//i, '' );
					if ( typeof call.drop[ type ] == 'function' ) {
						r[ i ] = new FileReader();
						r[ i ].onload = function( t, n ) { return function( _ev ) {
							if ( !(call.drop[ type ]( _ev.currentTarget.result, e, n )))
								dialog.setMsg( 'error', 'dataError', e, 5000 );
							if ( !done && !!e && e.type == 'file' ) {
								e.value = n;
								setFocus( e );
								done = true;
							}
						}}( type, f[ i ].name );
						if ( /image|audio|video/.test( type ))
			         	r[ i ].readAsDataURL( f[ i ]);
			         else
							r[ i ].readAsText( f[ i ], "UTF-8" );// start reading file data
		    		}
				}
			}
			else if ( ev.dataTransfer.items.length ) {	// for  password dragging
			var
				f = ev.dataTransfer.items[ 0 ]
				;
				if ( call.drop[ f.type ])
					f.getAsString( call.drop[ f.type ]);
			}
		}
		, image: function( s, e, p ) { // src as text or dataUrl, dropElement, path
			if ( e.src ) {
				e.src = s;
				return true;
			}
			return false;
		}
		, json: function( s, e, p ) { return setJSONValues( JSON.parse( s )); }
		, calendar: function( s, e, p ) {
			metaReaction.calendar.addBlocked = analyseICAL({}, s );
			dialog.setMsg( 'success', filename2Msg( 'fileImport', p ), 0, 3500 );
			return true;
		}
		, css: function( s, e ) {
			e = $( 'uLdraggedDesign' );
			if ( activeDesignLink ) {
				if ( !!!e ) {
				//@ a dragged design needs a style tag to be shown, so create one
				//@ and disable the active linked sheet
					e = $CE([ 'style', { id: 'uLdraggedDesign'
					, type: "text\/css", title: "normal", charset: "utf-8" }]);
					D.head.appendChild( e );
				}
				if ( e ) {
					activeDesignLink.sheet.disabled = true;
					e.innerHTML = s;
					setDesign( e, false );
				}
				return true;
			}
			else if ( e = $( "uLstandSheet" )) {
			var
				oldSheet = e.innerHTML
				;
				e.innerHTML = s.replace( /@import\s+[^;]+?\/useLib\.css[^;]+;/, '' );
				setDesign( e, oldSheet );
				return true;
			}
			return false;
		}
   });

	call.uLinit( 'dynList', (function() {
	//@ method class to manage dynamic wiki-expand lists, a solution between tables
	//@ and normal forms. Expand elements may be added, deleted, sended etc.
	//@ REMARK: it is sourced out from WIKI part because the dynamic may be used
	//@ without it.
	var
		calcStop = false
		, checkDouble = false
		, getName = function( _e ) { return _e.id.slice( 0, -7 ); }
		, removeNode = function( _e ) { DOM.removeNode( $P( _e, 2 )); }
		, getNextNum = function( eL ) {
			for ( var i = 0, max = []; i < eL.length
			; eL[ i++ ].id.replace( /\[(\d+)\]_choice$/, function( _, c ) {
				max = Math.max( max, $I( c ));
			}));
			return max +1;
		}
		, cleanNode = function( e, name ) {
		var
			n = e.cloneNode( true )
      	, nn = $Q( n, 'div.' + cssNS.hasCover )
      	;
   		$A( n, 'id', -1 );
      	for ( var i = 0; i < nn.length; i++ ) {
   			nn[ i ].className = '';
   			$CN.remove( nn[ i ].children[ 1 ], cssNS.covered );
   			DOM.removeNode( nn[ i ].children[ 2 ]);
   		}
      	n.innerHTML = _replace( n );
   		return n;

			function _replace( _n ) {
			var
				s = _n.innerHTML
				, r = new RegExp( '("|>|:|\\.)'
				+ (( s.match( /id="((\.?[\w-]+(\[\d+\])?)+)_choice"/ )
				|| [ 0, '' ])[ 1 ]
				).replace( /(\]|\[)/g, '\\$1' )
				+ '(\\.|\\n*<|"|_)', 'g' )
				, iDs = []
				, i = 0
				;
				s.replace( /\sid="(uL[\w-]+)_\d+?"/g, function( _, id ) {
					if ( iDs.indexOf( id ) < 0 )
						iDs.push( id );
					return _;
				});
				for (; i < iDs.length; s = s.replace( new RegExp(
					'((\\s(for|id|data-ul)=")|,\')' + iDs[ i++ ], 'g' )
					, '$1uL_' + ( counter++ )));
				return s.replace( r, '$1' + name + '$2' );
			}
		}
		, insertNode = function( e, exType, name, add ) {
      //@ creates a new wiki expand with new name
		//@ the trick to avoid the hole WIKI-Script is to clean added covers, 
		//@ recover the type and replace names and ids in the innerhtml of the 
		//@ already existing template struct. Also no WIKI-creation is needed so 
		//@ and the copy is on sub fieldsets also (see \\. in start of RegExp r )!
		DOM.removeNode( $( "uLclearer" ));	// not to double it
      var
      	n = cleanNode( e, name )
      	, pos = add == 1 && $CN.contains( $P( e ), cssNS.top )
      	? 'first' : 'last'
      	, posChild = $P( e )[ pos +'ElementChild' ]
      	;
      	_recoverTypes( e, n );
      	if ( pos == 'first' )
      		posChild = posChild.nextElementSibling;
			else if ( posChild.previousElementSibling.innerHTML.indexOf(
			'.calcIt(' ) > -1 )
				posChild = posChild.previousElementSibling;
			DOM.insertBefore( posChild, n );
			if ( add == 1 ) {
				DOM.scrollIntoView( n );
				DOM.delayRemove( n, cssNS.showActive, 4 );
			}
			return n;

	      function _recoverTypes( _e, _n ) {
	      	_e = $Q( _e, 'input[name]' );
	      	_n = $Q( _n, 'input[name]' );
	   		for ( var i = 0, t; i < _e.length; i++ )
					if ( t = getFormDef( _e[ i ]).type )
						$A( _n[ i ], 'type', t );
	      }
		}
      , setFadeAmount = function( e, d ) {
      	$A( $Q( e, '#'+ e.id +'>div.uLdynfilter', 1 ), 'data-ul-count', ''+ d );
      	$A( $Q( e, '#'+ e.id +'>div.uLdynaction', 1 ), 'data-ul-count', ''+ d );
      }
		;
		return {
			removeNodes: function( nList ) {
			//@ removes all nodes including subNodes except a last one as master
			//@ REMARK: it's recusive, needs ensured id to select only the next node level 
				if ( nList.length ) {
				var
					temp = $P( nList[ 0 ], 2 )
					, s = $A( nList[ 0 ], 'data-ul-dynlist' )
					, nL = $Q( temp, 'fieldset>div>[name]', 1 )	// find first name
					;
					nL = nL.name.slice( 0, nL.name.lastIndexOf( '.' ));
					nL = $Q( temp, 'fieldset>div>[name^="'+ nL +'"]' );
					// if initialisation has overwritten type: replace it from formDef
					for ( var i = 0, t; i < nL.length; i++ )
						if ( t = getFormDef( nL[ i ]).type )
							$A( nL[ i ], 'type', t );
					this.removeNodes( $Q( temp, '#' + ensureId( temp )
					+ '>fieldset.uLdynlist>legend>[data-ul-dynlist*="'
					+ s.slice( s.indexOf( ':' )) + '."]' ));
					temp = DOM.insertBefore( temp, cleanNode( temp, 'uLMaster' ));
		   		for ( var i = 0; i < nList.length; i++ )
		   			removeNode( nList[ i ]);
		   		nList = temp;
				}
				return nList;
			}
			, insertNode: insertNode
			, catchRet: function( ev, exType ) {
				if ( ev.keyCode === 13 ) {
					ev.preventDefault();
					ev.currentTarget.blur();
					this.filterOptions( exType, 'filter' );
				}
			}
			, filterOptions: function( e, exType, act ) {
			// selects all selecting checkboxes of rows if given regExp for value hits
			e = $T( $P( e, 3 ), 'input', 1 );
			var
				dynRoot = $P( e, 3 )
				, fn = act == 'showAll' ? '' : e.value.trim()
      		, fD = formDef[ e.form.name ]
      		, l = $T( $P( e, 2 ), 'select', 1 ).value
				, not = act == 'filterInvers' || act == 'select'
				, sort = act == 'sortup' || act == 'sortdown' ? [] : false
				, faded = act == 'showAll' ? 0
				: $I( $A( $P( e, 2 ), 'data-ul-count' ))
				;
				l = '#' + ensureId( dynRoot )	+'>fieldset.uLdynlist>div>[name$=".'+ l +'"]';
				if ( !( l = $Q( dynRoot, l )))
					return false;
				fn = !!sort ? analyseFilter( fD[ l[ 0 ].name ][ 'value' ][ 0 ], true )
				: fn.length ? analyseFilter( fn )
				: /showAll|select|invert/.test( act );

				for ( var i = 0, v; !!fn && i < l.length; i++ ) {
					e = $P( l[ i ], 2 );
					if ( act == 'showAll' )
						e.style.removeProperty( 'display' );
					else if ( e.style.display != 'none' ) {	//only on visible elements
						if ( fn === true )
							e.firstChild.firstChild.checked = act == 'select'
							|| !e.firstChild.firstChild.checked;
						else {
							v = l[ i ].tagName == 'SELECT'
							? l[ i ].options[ l[ i ].selectedIndex ].text
							: fD[ l[ i ].name ][ 'value' ][ 0 ];
							if ( !!sort )
								sort.push({ e: e, v: v });
							else {
								v = fn( v );
								if (( v && not ) || ( !v && !not )) {
									if ( act == 'filter' || act == 'filterInvers' ) {
										e.style.display = 'none';
										faded++;
									}
									else
										e.firstChild.firstChild.checked = true;
								}
							}
						}
					}	
				}
				if ( !!sort ) {
					sort = sort.sort( function( a, b ) {
						return act == 'sortup' ? fn( a.v, b.v ) : -fn( a.v, b.v );
					});
					for ( var i = 1; i < sort.length; i++ ) {
						DOM.insertAfter( sort[ i -1 ].e, sort[ i ].e );
					}				
				}
				setFadeAmount( dynRoot, faded );
				return false;
			}
	      , scroll: function( ev, e ) {
	      //@ it is nessesary to decide click and dblclick, because click is
	      //@ also fired on dblclick!!
	      	ev.preventDefault();
			   ev.stopPropagation();
	      	if ( ev.type == 'dblclick' ) {
	      		clearTimeout( checkDouble );					// at least not working
	      		checkDouble = false;
	      		if ( $CN.contains( e, cssNS.dynlist ))		// jump to top
	      			e = $Q( $P( e, 2 ), 'input', 1 );
	      		else {
						e = $P( e, 3 ).lastChild.children;
						e = $Q( e[ e.length -1 ], 'select', 1 );
					}
		      	DOM.scrollIntoView( e );
					setTimeout( function() { setFocus( e ); }, 500 );
	      	}
	      	else if ( ev.type == 'click' )
					checkDouble = setTimeout( function() {
						if ( checkDouble )
							e.firstChild.checked =! e.firstChild.checked;
						checkDouble = false;
					}, 400 );
	      }
	      , action: function( ev, exType, act, sourceName, copyName ) {
	      var
		      eT = ev.currentTarget || false
		      , e = eT ? $P( eT, 2 ) : ev
	         , q = '#' + ensureId( e ) + '>fieldset>legend>input[id$="_choice"]'
	         , l
	         ;
			   if ( !act ) {
			   	ev.stopPropagation();
		         l = eT.options ? eT.value : eT.previousSibling
		         ? eT.previousSibling.value : 'xxx';
	         	act = lang.dynActions[ 0 ].indexOf( l );
	         	act = act > -1 ? langConf.en.dynActions[ 0 ][ act ]
	         	: langConf.en.dynActions[ 1 ][ lang.dynActions[ 1 ].indexOf(
	      			l.replace( /\s\(\d+\)/, '' ))		// clear hidden counting!
	      		];
	      		act += act == 'delete' ? 'List' : '';
		      }
		      l = act == 'add' || act == 'delete'
		      ? $P( eT ).firstChild.firstChild
		      : act == 'up' ? $P( eT ).previousSibling
		      : $Q( e, q
		      + ( 'fold in,fold out,show,invert,clear,resize,init'.indexOf(
		      act ) > -1 ? '' : ':checked' ));
		      if ( 'delete,add,up'.indexOf( act ) == -1 && !l.length && eT ) {
					q = eT.options ? eT : eT.previousSibling;
					dialog.setMsg( 'error', 'dynMinSelect', 0, 4500 );
					delayRemove( q, cssNS.keyError, 1 );
		      }
		      else {
	            switch( act ) {	// first step loop all selected
	               case 'delete': l = [ l ];
	               case 'confirm':
	               case 'deleteList':
							// special treat of delete: beginner mode or direct form select?
	               	if ( _delete( act != 'confirm' && ( useLibData.explain === 0
	               	|| eT.options )))
	               		return false;
	           	      break;
	               case 'resize':
	               case 'getjson':
	               case 'export':	q = []; _loopAll( null, 'values' ); break;
	               case 'hide': q = []; _loopAll( _setDisplay, 'none' ); break;
	               case 'show': q = []; _loopAll( _setDisplay, '' ); break;
	               case 'fold in': q = []; _loopAll( _setSubDisplay, 'remove' ); break;
	               case 'fold out': q = []; _loopAll( _setSubDisplay, 'add' ); break;
	               case 'invert': q = []; _loopAll( _setChoice, false ); break;
	               case 'clear': q = []; _loopAll( _setChoice, true ); break;
	            }
 	            switch( act ) {
	            	case 'getjson': return getJSONValues( q );
	            	case 'add':
         			var
         				name = getName( l )
         				, p = name.lastIndexOf( '.' )
         				;
         				name = p > -1 ? [ name.slice( 0, p +1 ), name.slice( p +1 )]
         				: [ '', name ];
	            		if ( $CN.contains( e, 'dynamiclist' ))	{	// is a list?
	            			dialog.prompt( 'dynNameChoise', function( _n ) {
	            				if ( _n === false )
	            					return;
				         		else if ( $Q( e, '[id="' + name[ 0 ] + _n + '_choice"]', 1 ))
										dialog.setMsg( 'error', 'doubleName', 0, 4500 );
									else if ( _n.length ) {
				         			_initNode( insertNode(
				         				$P( l, 2 ), exType, name[ 0 ] + _n, 1
				         			));
				         			call.dynList.action( e, exType, 'resize', name[ 1 ], _n );
				         		}
				         	}, '', '^[a-zA-Z0-9_-]{3,}$' );
				         }
	      				else {
	      					p = name[ 1 ].replace( /\d+\]$/
	      					, zeroInt( getNextNum( $Q( e, q )), 5 ) + ']' );
	      					_initNode( insertNode( $P( l, 2 ), exType, name[ 0 ] + p, 1
	      					));
	               		this.action( e, exType, 'resize', name[ 1 ], p );
	               	}
	         			break;
	               case 'delete':
	               case 'deleteList':
	               case 'confirm':
	               	l[ 0 ].checked = true;
	               	if ( l = e.getAttribute( 'ondelete' )) {
	         				uLeval( l, exType, getJSONValues( q )); }
	               	this.action( e, exType, 'resize' );
	         			break;
	               case 'up':
	               	while( l && l.nodeType != 1 )	// search previous fieldset node
	               		l = l.previousSibling;
	               	DOM.insertBefore( l, $P( eT ));
	               	D.body.scrollBy({ left: 0, behavior: 'smooth'
	               	, top: DOM.getRect( $P( eT )).y - DOM.getRect( l ).y
	               	});
	         			break;
	               case 'init':
	               case 'resize':
	               	$CN[ l.length < conf.filterLimit ? 'add' : 'remove'
	               	]( $Q( e, '.' + cssNS.dynfilter, 1 ), cssNS.hide );
	               	if ( act == 'init' )
	               		break;
	               case 'export':
	               	if ( l = e.getAttribute( act == 'export' ? 'onexport' : 'onresize' )) {
	         				uLeval( l, exType, getJSONValues( q ), sourceName || false
	         				, copyName || false ); }
	         			else if ( act == 'export' ) {
	         				call.saveAs( exType + '.json'
	         				, JSON.stringify( getJSONValues( q )
	         				).replace( /([\}\]]*\})/g, '$1\n' ));
	         			}
	               	break;
	               case 'hide':
	               	q = q.length
	               	+ $I( $A( $Q( e, 'div.uLdynfilter', 1 ), 'data-ul-count' ));
	               case 'show':
	               	setFadeAmount( e, act === 'hide' ? q : 0 );
	               	break;
	            }
            }
	         return false;

	         function _delete( conf ) {
               if ( l.length && $Q( e, q ).length > l.length ) // one must be left!
               	if ( conf ) 
		               dialog.confirm( 'confirmMsg', function( v ) {
								if ( v === true )
									call.dynList.action( e, exType, 'confirm' );
							});
						else {
							q = [];
               		_loopAll( removeNode, 'values' );
               	}
					else {
						q = eT.options ? eT : eT.previousSibling;
						dialog.setMsg( 'error', 'noneLeftError', q, 4500 );
						conf = true;
					}
					return conf;
				}
	         function _loopAll( fn, p ) {
		         for ( var i = 0; i < l.length; i++ )
		         	if ( p === 'block' || fn == _setDisplay
		         	|| $P( l[ i ], 2 ).style.display !== 'none' ) {
		         		if ( p == 'values' )
		         			q = q.concat( getAllValues( [].slice.call( $Q( e.form
		         			, '[name^="'+ l[ i ].id.slice( 0, -7 ) +'"]' ))
		         			, e.form.name ));
		         		else
		         			q.push( l[ i ]);
		         		if ( fn )
	         				fn( l[ i ], p );
		         	}
	         }
	         function _setDisplay( _e, t ) {
	         	if ( t === 'none' ) {
	         		$P( _e, 2 ).style.display = t;
	         		_e.checked = false; 			// hidden can't be selected
	         	}
	         	else
	         		$P( _e, 2 ).style.removeProperty( 'display' );
	         }
	         function _setSubDisplay( _e, t ) {
	         	$CN[ t ]( $T( $P( _e, 2 ), 'fieldset', 1 ), cssNS.open );
	         }
	       	function _setChoice( _e, clear ) {
	       		_e.checked = clear ? false : !_e.checked;
	       	}
	       	function _initNode( _n ) {
	       	//@ there are no intern structures for this new node. ist must be
	       	//@ initialisated and a change event be fired to start calulation etc.
	       	//@ REMARK: only here insertNode is used in different cases...
	       		for ( var i = 0, l = $Q( _n, '[name]' ); i < l.length; i++ ) {
						initViaHTML( l[ i ]);
						initElement( l[ i ]);
						dispatchEvent( l[ i ], 'change' );
					}
	       	}
	      }
	      , getCalcValue: function( e ) {	// changedElement
	      var
	      	mult = getFormDef( e ).type
	      	, n = e.name.replace( /\[\d+\](\.\w+)$/g, '[]$1' )
	      	, fS = $P( e, 3 )
	      	, v = []
	      	;
	      	if ( mult ) {
	      		mult = conf.multiCalc[ mult ] || 1;
		      	e = $Q( fS, '[name^="'+ n +'"]', 1 );
		      	n = e.name;
		      	for ( var i = 1; i < mult; i++ ) {
						v.push( e.value || '' );
						e = $Q( fS, '[name^="'+ n +'_'+ i +'_"]', 1 );
		      	}
		      }
				v.push( mult && e ? e.value : '' );
    			return v;
	      }
	      , getListValues: function( e, n=false, set=false ) {
	      // changedElement, targetName, values
	      // REMARK: it's also used for setListValues := set is given
	      var
      		b = e ? e.name.slice( 0, e.name.lastIndexOf( '[' )) : false
      		, v = []
      		;
      		n = !!n ? '].'+ n : e ? e.name.slice( e.name.lastIndexOf( ']' ))
      		: false;
      		if ( b && n ) {
	      		e = $Q( $P( e, 3 ), 'fieldset>div>[name^="'+ b +'"][name$="'+ n +'"]' );
			      for ( i = 0; i < e.length; i++ ) {
			      	if ( !set )
							v.push( e[ i ].value || '' );
						else
							e[ i ].value = set[ i ] || '';
					}
				}
				return v;
	      }
	      , setListValues: this.getListValues	// changedElement, targetName, values
	      , getValue: function( e, n ) {	// changedElement, targetName
	      var
      		b = e ? e.name.slice( 0, e.name.lastIndexOf( '.' ) +1 ) : false
      		;
      		e =  b ? D.getElementsByName( b + n )[ 0 ] : b;
      		return !e ? false : e.type == 'checkbox' || e.type == 'radio' ?
      		( e.checked ? e.value : false ) : e.value;
	      }
	      , setValue: function( e, n, v ) {	// changedElement, targetName, value
	      var
      		b = e ? e.name.slice( 0, e.name.lastIndexOf( '.' ) +1 ) : false
      		;
      		e =  b ? D.getElementsByName( b + n )[ 0 ] : b;
      		if ( e ) {
      			if ( e.type == 'checkbox' || e.type == 'radio' )
      				e.checked = !!e.value;
      			else
      				e.value = v.toString();
      		}
      		return e; 
	      }
	      , setParentValue: function( e, n, v ) { // baseElement, targetName, value
	      e = $P( e, 4 );
	      var
	      	t = $Q( e, '[name]', 1 )	// 1. step one element of parent
	      	;
				if ( t ) {						// 2. step replace searched name
					n = t.name.slice( 0, t.name.lastIndexOf( '.' ) +1 ) + n;
					if ( t = $Q( e, '[name="'+ n +'"]', 1 ))
						setTimeout( function() { t.value = v; }, 0 );
				}	      	
	      }
	      , setChildValue: function( e, n, v ) { // baseElement, targetName, value
				if ( e = $Q( $P( e, 3 ).lastElementChild
	      	, 'fieldset>div>[name$="'+ n +'"]', 1 ))
					setTimeout( function() { e.value = v; }, 0 );      	
	      }
	      , calcIt: function( _, c, e ) {// empty, changedElement, targetElement
	      var
      		b = c ? c.name.slice( 0, c.name.lastIndexOf( '[' )) : false
      		, n = !!n ? '].'+ n : c ? c.name.slice( c.name.lastIndexOf( ']' ))
      		: false
		      , mult = conf.multiCalc[ getFormDef( c ).type ] || 1
      		, v = []
      		, t
      		;
      		if ( !calcStop && b && n ) {
	      		c = $Q( $P( c, 3 ), 'fieldset>div>[name^="'+ b +'"][name$="'+ n +'"]' );
      			t = e.name.match( /\.(\w+)$/ )[ 1 ];
					if ( !c.length )
						return;
					else if ( t === 'count' )
						e.value = c.length;
					else {
		      		for ( var m = 0, add; m < mult; m++ ) {
		      			add = m > 0 ? '_'+ m +'_' : '';
							for ( var i = 0, v = 0, fD, _v, _d=[]; i < c.length; i++ ) {
								fD = getFormDef( c[ i ]);
								_v = parseFloat( fD[ 'value' + add ][ 0 ]) || 0;
								switch( t ) {
									case 'min': v = min( v, _v ); break;
									case 'max': v = max( v, _v ); break;
									case 'deviation':	_d.push( _v );
									default: v += _v; break;
								}
							}
			      		if ( t === 'average' ) {
								v = ( v / c.length ).toFixed( 2 );
			      		}
			      		else if ( t === 'deviation' ) {
			      			v = v / c.length;
			      			for ( var i = 0, _v = 0; i < _d.length; i++ )
			      				_v += ( _d[ i ] - v ) * ( _d[ i ] - v );
								v = Math.sqrt( _v / ( _d.length -1 )).toFixed( 2 );
			      		}
			      		else
			      			v = fD.type == 'number' ? Math.round( v ) : v.toFixed( 2 );
			      		// put result in a closeure and set it outside interrupt
			      		setTimeout( (function( _e, _v ) {
			      			return function() { _e.value = _v; };
			      		})(
			      			m == 0 ? e : D.getElementsByName( e.name + add )[ 0 ]
								, ''+ ( v || '' )
			      		), 0 );
			      	}
			      }
			   }
		   }
	   }})()
   )
	, lang = $A( $T( D, 'html', 1 ), 'lang' )
	|| navigator.language.slice( 0, 5 ).toLowerCase()
	;
	lang = (function( l ) {	// short'n language access and browserize it ;-)
		conf.country = l.slice( -2 ).toUpperCase();
		l = l.slice( 0, 2 );
		conf.lang = langConf[ l ] ? l : 'en';
		return langConf[ conf.lang ];
	})( lang );
	if ( !( 'reversed' in D.createElement( 'ol' )))
		alert( lang.supportError );

/*****
 *
 *		functions to manage different modal dialogs
 *		@ the dynamically generated dialogs will have a unique id, normaly based
 *		@ on the id of the original element, i. e. numbered. If there are
 *		@ selectable elements, they are created as radio or checkboxes with value
 *		@ and label, so they may also be used as server generated javascript
 *		@ dialog that gets his data via a submit without javascript at the client
 *		@(just in case...). Especially it's possible to use standard html
 *		@ to reset and submit the values.
 *		@ For 'cancel', 'clear' and 'store' (in a other element) special submit
 *		@ are defined by name, there may be defined more for other types of
 *		@ submit like 'before', 'next' etc.
 *		@ ATTENTION: to let HTML do it on reset, it's necessary to set attributes
 *		@ like 'checked' on elements via .setAttribute(checked','checked')
 *****/
/*	for testing !!!!!
	setTimeout( function(){ dialog.alert(
		'viewPassword'
		//'=Dies ist eine Überschrift.\n\n<<Kleiner Satz dazu.>>\n\n<<<<und noch einer.>>>>'
		//[['h1',{},'Überschrift.'],[ 'span',{ CN: "uLinform" },[ 'p',{},'Kleiner Satz dazu.']]]
		//'<h1>Dies ist Überschrift.</h1><span class="uLinform"><p>Kleiner Satz dazu.</p></span>'
		//, function( v ) { alert( "der alte Wert war: 'irgendein Wert',\nder neue lautet: " + v )}
		//, 'irgendein Wert'
		, 8
		);
	}, 1000 );
*/
var
	dialog = ( function() {
	var
		normContent = function( c ) {
			return lang[ c ] ? [ 'span', {}].concat( getStruct.header( lang[ c ], 1, 0, 1 ))
			: typeCheck( 'Array', c ) ? c
			: [ 'span', { IH: c.match( /<\/(code|div|span|p|h\d)>/i ) ? c
			: useLib.WIKI.toHTML( c )}]
			;
		}
		, calcWidth = function ( o ) {
			$S( o ).width = min( 40, max( 15, ( 4 + o.innerHTML.length / 1.41 * 0.45 ))) + 'em';
		}
		, getTextId = function( c ) {
			return 'uL_' + ( lang[ c ] ? c : counter++ );
		}
		, closeBox = function( e, isClicked ) {
		var
			i = -1
			;
			if ( !!isClicked || !$CN.contains( $( 'uLmsghold' ), cssNS.locked )) {
				e = !!e ? e.target || e : false;
				while( e ) { 
					if ( $CN.contains( e, cssNS.modal )
					|| ( e.id && e.id.indexOf( 'listSelect' ) > -1 )) {
						if (( i = e.id.indexOf( 'uL__modal__' )) > -1 ) {
							i = e.id.slice( 0, i );
							if ( useLib.call[ i ])
								useLib.call[ i ]( false );
							openCloseModal( i, 0 );
						}
						else
							DOM.removeNode( e );
						break;
					}
					e = $P( e );
				}
			}
		}
		, initModalForm = function( id, f ) {
			if ( !!f ) {
			var
				attr = { method: 'post'
					, action: 'javascript:void(0);'
					, noValidate: 'noValidate'
					, id: id + cssNS.modal
					, name: id
				}
				;
				for ( var a in attr )
					$A( f, a, attr[ a ]);
				initForm( f );
			}
		}
		, callBcks = []
		, layer = []
		;
		call.uLinit( 'modalDragger', function( ev ) {
		var
			e = $P( ev.currentTarget )
			;
			e = $CN.contains( e, cssNS.dragger ) ? $P( e ) : e;
			DOM.dragStart( ev, e );
		});
		call.uLinit( 'modalSizer', function( ev ) {
			DOM.sizeStart( ev, $Q( $P( ev.currentTarget ), '.' + cssNS.scale, 1 )); 
		});
		call.uLinit( 'modalClose', closeBox );
		call.uLinit( 'callBcks', function( id, i, t ) {
			if (( t == 'confirm' || t == 'refuse' ) && !!callBcks[ i ])
				callBcks[ i ]( t == 'confirm' );
			else if ( t !== false ) {
				if ( t == 'uL__prompt__' ) {
				var
					v = $( t ).value
					;
					v = v.length ? v.trim() : '';
					for ( var j = 0, l = $( t ).form[ t ] || []; v.length && j < l.length
					; j++ ) {
						if ( l[ j ].checked ) {
							v += '#'+ l[ j ].value;
							break;
						}
					}
					callBcks[ i ]( v );
				}
				else {				// reset button of 'uL__prompt__'
					$( 'uL__prompt__' ).value = t;
					return false;
				}
			}
			else
				callBcks[ i ]( t );
			callBcks.splice( i, 1 );
			openCloseModal( id, 0 );
			return false;
		});

		function setRequest( type, content, callBck, countdown, pattern, extend ) {
		//@ PARA type := alert prompt confirm countdown
		//@ 	, content := textId or internStruct or htmlText or wikiText
		//@ 	, callBackFunction
		//@	, countDownFlag or resetValueOfPrompt
		//@ 	, pattern := may be set on prompt to control the input
		//@ analog to JS prompt etc., but callback is needed, that gets user input as
		//@ argument string is trimmed (may be empty!) and callback is only called,
		//@ if not aborted
		var
			textId = getTextId( content )
			, c = "return useLib.call.callBcks('" + textId + "'," + callBcks.length
			, attr = 'draggable'
			, buttons = []
			;
			content = [ 'div', { CN: cssNS.content, S: 'width:100%;' }, normContent( content )];
			if ( type == 'prompt' ) {
				content = content.concat(
					[[ 'fieldset', {}, [ 'div'
					, { S: 'width:100%!important;' }
						, [ 'div', { S: 'width:95%;' }
						, [ 'input', { id: 'uL__prompt__', name: 'uL__prompt__'
						, type: 'text'
						, autocomplete: 'off', value: ( countdown || '' )
						, pattern: pattern || null }]
						]]
					, extend ? _getExtend( extend ) : null ]]
				);
				buttons.push(
					getStruct.button( lang.cancel, c + ",false )" )
					, getStruct.button( lang.reset, c + ",'" + ( countdown || '' ) + "')" )
					, getStruct.button( lang.store, c + ",'uL__prompt__')" )
				);
				callBcks.push( callBck );
				setTimeout( function(){ $( 'uL__prompt__' ).focus(); }, 300 );
			}
			else if ( type == 'confirm' ) {
				buttons.push(
					getStruct.button( lang.refuse, c + ",'refuse' )" )
					, getStruct.button( lang.confirm, c + ",'confirm' )" )
				);
				callBcks.push( callBck );
			}
			else {
				buttons.push( getStruct.button( [ 'OK' ], c + ",'confirm' )" ));
				callBcks.push( callBck || function(){});
			}
			c = cssNS.buttonRow;
			content = [ 'form', { name: 'uL__' + type + '__f' }, content ];
			if ( !!countdown && type != 'prompt' ) {
				if ( countdown < 0 )
					setTimeout(	function(){ openCloseModal( textId, 0 ); }, countdown * -1000 );
				else {
					content.push([ 'div'
						, { id: 'uL__countDown__', CN: cssNS.countdown, IH: $I( countdown ) || 7 }
					]);
					c +=  ' ' + cssNS.disabled;
					setTimeout( _countDown, 1000 );
				}
			}
			else
				attr += ' closable';
			content.push([ 'div', { id: 'uL__button__', CN: c } ].concat( buttons ));
			return setModal( textId, type, attr, content, null, true );

			function _countDown() { 
			var
				e = $( 'uL__countDown__' )
				, c = ( !e ? 0 : $I( e.innerHTML )) - 1
				;
				if ( c >= 0 ) {
					e.innerHTML = String( c );
					setTimeout( _countDown, 1000 );
				}
				else {
					DOM.removeNode( $( 'uL__countDown__' ));
					$CN.remove( $( 'uL__button__' ), cssNS.disabled );
				}
			}

			function _getExtend( ex ) { // checkbox, cN, label, id, name, value, checked
         var
            s = [ 'fieldset', { CN: cssNS.hidden, S: "width:100%;" }]
            ;
            for ( var v in ex.ex )
               s.push( getStruct.radioCheck( 0, null, ex.ex[ v ], '', 'uL__prompt__'
               , v, v == ex.active ));
            return [ 'div', {}, s ];
			}
		}

		function setErrorList( type, content, msgList, fName ) {
		//@ msgList := hints or errors for elements := { elementName: textId or wikiText}
		var
			r = /^(hint|error|technical|success|server)/i
			, id = 'uL__errorList__'
			;
			content = [ 'div', { CN: cssNS.content }, normContent( content )];
			if ( formDef[ fName ])
				for ( var i = 0; i < msgList.length; i++ )
					content.push( _getListStruct( msgList[ i ], formDef[ fName ]));
			DOM.removeNode( $( id + 'uL__modal__' ));
			setModal( id, type, 'draggable closable sizable noShield'
				, content, null, true );

			function _getListStruct( l, fDef ) {
			//@ sets rows of modal dialog to given wiki-text or to predefined text::
			//@ requiredError, formatError, rangeError, checkError, technicalHint, taskHint
				for ( var n in l ) {
					if ( fDef[ n ]) {
					var
						msg = lang[ l[ n ]] ? lang[ l[ n ]] : typeCheck( 'Array', l[ n ])
						? l[ n ] : [ l[ n ]]
						, t = msg[ 0 ].match( r ); 
						;
						t = t ? t[ 0 ] : 'error';
						msg = msg.slice( 0 ); // copy it, to prevent chsnges on original
						msg[ 0 ] = getLabelText( $( fDef[ n ].id ), msg[ 0 ].replace( r, ''
							).replace( /##(min|max)/g, fDef[ n ][ '$1' ] || '' ));
						msg = getStruct.header( msg, 2, t )[ 0 ];
						msg[ 0 ] = 'a';
						msg[ 1 ].href = "javascript:document.getElementById('"
							+ ( fDef[ n ].first || fDef[ n ].id ) + "').focus()";
						return [ 'p', { id: fDef[ n ].id + 'errorList', CN: cssNS[ t ]}, msg ];
					}
				}
				return null;
			}
		}

		function setMsg( type, content, e, timeout ) {
		//@ PARA type := error hint success
		//@ 	, content := textId or internStruct or htmlText or wikiText
		//@ 	, elementMsgBelongsTo [global], timeTillClose [onClick]
		//@ sets modal dialog to given wiki-text content or to predefined content::
		//@ 'success', 'hint', 'error', 'server' or content WIKI-text
		//@ REMARK the callBck may be an array of messages to single elements or a 
		//@ function that get the answer of the user as parameter := accepted, refused
		//@ or inputValue(s) (if more defined as a wiki form, separated by ' :: '!)
			if ( type && content ) {
			var
				id = getTextId( content )
				, msghold = $CN.contains( $( 'uLmsghold' ), cssNS.locked )
				, fn = 'useLib.call.modalClose(this,true)'
				, fD
				;
				type = type.toLowerCase();
				if ( type == 'error' && e ) {
					fD = !$CN.contains( e, 'uL_ignore') ? getFormDef( e ) : false;
					if ( fD )
						cover.errorList.add( fD, content );
					e = fD ? $( fD.first ) : e;
					cover.keyMsg( e, 'Error' );
				}
				content = getLabelText( e, normContent( content ));
				content = $CE( [ 'div'
					, { id: id, CN: cssNS.modal + ' ' + ( cssNS[ type ] || type )
						, role: 'alert', MC: fn
					}, [ 'div',{}
						, [ 'div', { CN: cssNS.content }, content ]
						, msghold ? [ 'a', { href: 'javascript:' + fn
						, CN: cssNS.locked }, 'OK' ] : []
					]
				]);
				calcWidth( content );
				D.body.appendChild( content );
				setOpenClose( content, 1 );
				DOM.setControledPos( content, e || null );
				content.style.zIndex = 300;
				if ( !!timeout && !msghold )
					setTimeout( function(){ closeBox( $( id )) }, timeout );		// clear msg
				return id;
			}
		}

		function setModal( id, type, attr, content, title, width, callBck ) {
		//@ PARAM dialogId, dialogType, attr := draggable sizable closable noShield,
		//@ PARAM titleOnTopOfDialog, contentOfDialog as createElementsArrayStructure
		//@ REMARK: 'noShield' on dialogTypes as error, user must be able to edit 
			if ( !!content ) {
			var
				_call = "useLib.call.modal"
				, isString = typeCheck( 'String', content )
				, dragger = attr.indexOf( 'dra' ) == -1 ? []
				: [ 'div', {
					CN: cssNS.dragger
					, MD: _call + 'Dragger( event )'
					, IH: !!title && title.length ? '<span>' + title + '</span>' : ''
				}]
				, sizer = attr.indexOf( 'siz' ) == -1 ? []
				: [ 'div', {
					CN: cssNS.sizer
					, MD: _call + 'Sizer( event )'
					, T: 'sizer'
				}]
				, closer = attr.indexOf( 'clo' ) == -1 ? []
				: [ 'div', {
					CN: cssNS.closer
					, MD: _call + "Close( this, false )"
					, T: 'closer'
				}]
				, e
				;
				content = [ 'div', { id: id + 'uL__modal__', role: 'dialog'
						, CN: cssNS.modal + ' '
						+ cssNS[ !type || !!metaReaction[ type ] ? 'form' : type ]
					}
					, [ 'div', {}
						, closer
						, sizer 
						, dragger	// last position caused on css hit
						, [ 'div', { CN: cssNS.scale }, normContent( content )]
					]
				];
				e = D.body.appendChild( $CE( content ));
			}
			openCloseModal( id, true, attr.indexOf( 'noShield' ) > -1 );
			if ( !!content ) {	// calculate widths after display:block,
				e = $Q( e, '.' + cssNS.scale, 1 );
				initTooltips( e );
				if ( !!width )
					$S( e ).width = width === true ? calcWidth( e ) : width;
				initModalForm( id, $T( e, 'form', 1 ));
				if ( !!callBck ) {
					call.uLinit( id, callBck );
				}
			}
			return $( id + 'uL__modal__' );
		}

		function openCloseModal( id, open, noModal ) {
		//@ sets and hides protection layer for modal diaglogs
		var
			e = $( id + 'uL__modal__' )
			;
			if ( e ) {
				setOpenClose( e, open );
				if ( !!open ) {
					layer.push( e.id );
					if ( !!noModal )
						$CN.add( e, cssNS.free );
					DOM.setControledPos( e );	// after display block to get offset values!
				}
				else {
					keyControl = false;
					id = D.forms[ id ] ? id : id + cssNS.modal;
					layer.pop();
					delete useLib.call[ id ];
					delete useLib.formDef[ id ];
					setTimeout(		// always clean up, but put e in a closure!
						(function( _e ) {
							return function() {
								ownInput.active = false;	// tell ownInput its ended for sure
								DOM.removeNode( _e );
							}
						})( e )
					, 2000 );
					setFocus( $( id ));
				}
			}
		}

		return {
			setMsg: setMsg
			, closeBox: closeBox
			, setModal: setModal
			, closeModal: openCloseModal
			, setErrorList: setErrorList
			, setError: function( e, msg ) {
				$A( e, 'aria-invalid', 'true' );
				$CN.add( $P( e ), cssNS.error );
				DOM.insertAfter( labels[ e.id ], $CE( 
					[ 'div', { id: e.id + 'errorMsg', CN: cssNS.error, role: 'alert', }].concat(
						getStruct.header( getLabelText( e, lang[ msg ]))
					))
				);
			}
			, removeError: function( e, eC ) {
				DOM.removeNode( $( e.id + 'errorMsg' )); 
				$CN.remove( $P( e ), cssNS.error );
				if ( !!eC )
					delayRemove( eC, cssNS.success, 1 );
				if ( $A( e, 'aria-invalid' ) && ( eC = $( e.id + 'errorList' ))) {
					$CN.remove( eC, cssNS.error );
					$CN.add( eC, cssNS.success );
				}
				$A( e, 'aria-invalid', 'false' );
			}
			, fromFormDef: function( type, content, msgList, fName ) {
				if ( msgList )
					setErrorList( type, content, msgList, fName );
				else
					setMsg( type, content, null, msg.type == 'success' ? 4500 : 0 );
			}
			//@ ( type, content t, callBck fn, countdown c, value v, pattern p, extend e )
			, alert: function( t, fn, c ) { return setRequest( 'alert', t, fn, c ) }
			, prompt: function( t, fn, v, p, e ) {
				return setRequest( 'prompt', t, fn, v, p, e );
			}
			, confirm: function( t, fn, c ) { return setRequest( 'confirm', t, fn, c ) }
			, setEleDiv: function ( e, id, ce, below ) {
			//@ PARA elementNode, idOfSetDiv, arrayStructureForDiv, init := create else remove
			//@ creates a div container from ce, calculates width and postion, so that 
			//@ container appears directly below the given element with same width.
			var
				r = DOM.getRect( e )
				;
				ce = $CE( ce );
				D.body.appendChild( ce );
				ce.id = id;
				$S( ce ).width = r.w + 'px';
				DOM.setPos( ce, r.x, r.y + ( below ? r.h : 0 ));
			}
			, layer: layer
		}
	})()
	, idCover = 'uLcover'	// to mask the id of element belonging covers and forms
	, lastLinkPosition = false
	;
	call.uLinit( 'submitForm', function( f, GET=false ) { 
	var
		type = $A( f, 'data-ul' )
		, sha3 = type ? $I( type.slice( -3 )) : 0
		, id = f.id.slice( 0, -cssNS.modal.length )
		, path = $A( f , 'action' )
		, method = $A( f, 'method' ) || ''
		, cors = ( path +'' ).match( /(https?):\/\/([^\/:]+)/i )
		, v = ''
		, js = {}
		;
		if ( type && type.length && metaReaction[ type ]) {
			f = getFormDef( $( id ));
			if ( !( metaReaction[ type == 'calendar' ? type : f.type ].setValue( $( id ))
			=== false )) {
				setFocus( $( f.first ));
				dialog.closeModal( id );
			}
		}
		else {
			method = GET || method.toUpperCase() == 'GET' ? 'GET' : 'POST';
			if ( cors && cors[ 1 ] == 'http' )
				console.warn( "useLib hint: unsecure protocol of "+ path +"!" );
			cors = location.protocol == 'file:'
			|| ( cors && cors[ 2 ] != location.hostname );
			dialog.closeModal( 'uL__errorList__' );
			if ( method == 'GET' )
				_submit({});
			else if (( $A( f, 'novalidate' ) || _checkForm( f ))
			&& ( v = getAllValues( '', f.name ))) {
				if ( !v.length && !/password/i.test( f.name )) {
					dialog.setMsg( 'hint', 'noDataMsg', 0, 4500 );
					return false;
				}
				else {
					js = v.length ? getJSONValues( v, f.name ) : "";
					if ( conf.taskSupport && new RegExp( conf.tasksteps[ 1 ]
					).test( JSON.stringify( js.tasks )))
						dialog.confirm( 'taskCheck', function( v ) {
							if ( v === true )
								_submit( js );
						});
					else
						return _submit( js );
				}
			}
		}
		return false;

		function _submit( js ) {
		var
			warn = "useLib hint: couldn't submit %%, because ".replace(
			'%%', path || '' )
			;
			if ( useLib.call[ id ]) {	// special call for modal dialog?
				useLib.call[ id ]( js );
				dialog.closeModal( id );
				return false;
			}
			else if ( conf.testSubmitOnly	|| ( conf.dataManagePath && !path )) {
				if ( !conf.testSubmitOnly )
					console.warn( warn +"problem in action path!" ); 
				dialog.alert( '='+ lang.testSubmitOnly[ 0 ] +'\n\n'
				+ JSON.stringify( js, null, 2 ));
				return false;
			}
			else if (( !cors && !type ) || path.indexOf( 'javascript:' ) > -1 )
				return true;				// standard case?
			else if ( !!type && type.slice( -3 ) == 'SHA' ) {
				type = type.slice( 0, -3 );
				if ( useLib.security ) {
					useLib.security[ type == 'CORS' ? 'submitCORS' : 'submitJSONCSS' ](
					path, _afterResponse, method, js, 'POST', true );
					return false;
				}
				else
					console.warn( warn +"useLib.security is missing!" ); 
			}
			else if ( js && ( conf.dataManagePath || type == 'JSONCSS' )) {
				js = '?uL='+ encodeURIComponent( escapeUnicode( JSON.stringify({
					uL_path: path        // extend json a bit before shuffle it
	            , uL_verb: method
	            , uL_timestamp: Math.floor(( new Date()).getTime() / 1000 )
	            , uL_json: js || null
	         }))) +'&lg='+ conf.lang;
			}
			else if ( js ) 
				js = _values2url( js );
			if ( !cors && type == 'AJAX' )
				ajax( conf.dataManagePath ? conf.dataManagePath + js : path + js, _afterResponse
				, 'JSON', 2500, true);
			else if ( type == 'JSONCSS' )
				loadJSONCSS( path + ( !/\.css($|\?)/i.test( path ) ? '.css' : '' )
				+ js, _afterResponse, 4500 );
			else
				console.warn( warn );
			return false;
		}
		function _afterResponse( _js ) {
		var
			r = /^20\d/.test( _js.code ) ? 'success' : 'error'
			;
			if ( _js.msg )
				dialog.setMsg( r, lang[ _js.msg ] || _js.msg, 0, 7500 );
			if ( r == 'success' && _js.data )
				setJSONValues( _js.data );
		}
		function _checkForm( f ) {
		//@ PARAM formElement
		var
			e = filterElements( f )
			, lastLength = 0	// remember errorList.length
			, noteChoise = {} // remember if a name is done
			, pwStored = ''	// remember passwort for repeat check
			;
			cover.errorList.length = 0;
			for ( var i = 0, a, fD, _msg; i < e.length; i++ ) {
				if ( fD = formDef[ f.name ][ e[ i ].name ]) {
					dialog.removeError( e[ i ], null );
					a = getSelectionAttr( e[ i ]);
					if ( $CN.contains( $P( e[ i ]), "uL_ignore" ))
						fD.ignore = true;
					else if ( a == 'checked' && $CN.contains( e[ i ], cssNS.notDone )) {
						if ( !noteChoise[ e[ i ].name ]) {
							fD.id = e[ i ].id;	// must be on namesList, set to first error one
							cover.errorList.add( fD, 'choiseError' );
							dialog.setError( e[ i ], 'choiseError' );
							noteChoise[ e[ i ].name ] = true;
						}
						$CN.add( e[ i ], cssNS.error );
					}
					else if ( a == 'value' )
						__checkFullInput( e[ i ], fD );
					else if ( !!fD.required && !!fD.required.uLall
					&& !( v = getEleValue( e[ i ])).length ) {
						dialog.setError( $( fD.id ), 'requiredError' );
						cover.errorList.add( fD, 'requiredError' );
					}
				}
				lastLength = cover.errorList.length;
			}
			if ( cover.errorList.length )
				dialog.setErrorList( 'error', 'errorMsg', cover.errorList, f.name );
			cover.errorList.length = 0;
			return !lastLength;

			function __checkFullInput( e, fD ) {
			//@ this check is for input fields and their covers
			var
				v = ''
				, notPw = fD.type.indexOf( 'password' ) < 0
				, noValue
				;
				if ( notPw && fD.subs ) {
					v = cover.getValue( fD, fD.type, conf.inputIsLocal ? 'local'
					: 'norm', true );
					if ( cover.errorList.length == lastLength ) {
						activeOnChange = false;	// onchange shall not write the cover again!
						e.value = v;
						activeOnChange = true;
					}
				}
				else
					v = notPw ? String( e.value ) : fD.value[ 0 ];
				if ( cover.errorList.length == lastLength ) {
					noValue = !v.length || ( v.length && fD.hasEmpty );
					if ( noValue = noValue && !!fD.required ? 'requiredError'
					: !noValue && fD.regexp && !fD.regexp.test( v )	? 'formatError'
					: !notPw ? __checkPW() : false ) {
						cover.errorList.add( fD, noValue );
						dialog.setError( $( fD.id ), noValue );
					}
				}

				function __checkPW() {
					pwStored = v;
					return fD.type == 'password' && fD.pwQuality < fD.min ? 'qualityError'
					: fD.type != 'password' && pwStored != v ? 'repeatError' : false;
				}
			}
		}
		function _values2url( v ) {
	   var
	   	tasks = {}
	   	, u = ''
			;
			for ( var i = 0, n, keyList; i < v.length; i++ ) {
				if ( !!v[ i ].tasks )
					tasks[ v[ i ].name ] = v[ i ].tasks;
				u += '&' + v[ i ].name + '='
				+ ( v[ i ].value.join ? v[ i ].value.join( '&' + v[ i ].name + '=' )
				: v[ i ].value );
			}
			tasks = JSON.stringify( tasks );
			return '?' + encodeURIComponent( escapeUnicode( u.slice( 1 )
			+ ( tasks.length == 2 ? '' : '&uLtask=' + tasks )));		}
	});
	call.uLinit( 'resetForm', function( f ) {
		if ( $A( f, 'data-ul' ) == 'calendar' )
			metaReaction.calendar.getValue( $( f.id.slice( 0, -cssNS.modal.length )));
		else	// give a 'event.target' to reset method
			resetElements( f );
		return false;
	});
	call.uLinit( 'closeForm', function( f ) {
		if (useLib.call[ f.name ])
			useLib.call[ f.name ]( false );
		dialog.closeModal( f.id.slice( 0, -cssNS.modal.length ));
		return false;
	});
	call.uLinit( 'clearForm', function( f ) {
	var
		fD = formDef[ f.name ]
		;
		for ( var n in fD ) {
			fD[ n ].value = fD[ n ].resetValue = [];
			for ( var e = $Q(f, '[name="' + n + '"]' ), i = 0, a; i < e.length; i++ ) {
				a = getSelectionAttr( e[ i ]);
				if ( a == 'selected' )
					setEleValue( e[ i ], []);
				else
					e[ i ][ a ] = a == 'value' ? '' :false;
				if ( fD[ n ].defaultValue )
					setEleValue( e[ i ], fD[ n ].defaultValue );
			}
		}
		return false;
	});

	call.uLinit( 'checkWikiLink', function( ev ) {
	var
		e = ev.currentTarget
		, h = ( $A( e, 'href' ) || '' ).split( '#' )
		, stop = true
		;
		if ( !h[ 0 ].length && h[ 1 ] && h[ 1 ].length ) {
			lastLinkPosition = e;
			_scroll();
		}
		else if ( e = $Q( D, 'a[href*="'+ h[ 0 ].replace( /.txt/i, '.html' ) +'"]', 1 )) {
			e.click();
			_scroll();
		}
		else if ( e = h[ 0 ].match( /section([\d\.]+)/i )) {
			if ( e = $( 'uLmainnavi' + e[ 1 ])) {
				e.click();
				_scroll();
			}
			else
				stop = false;
		}
		else if ( /\.txt$/i.test( h[ 0 ])
		&& ( h[ 0 ].indexOf( '/' ) == -1	|| h[ 0 ].indexOf( 'wiki' ) > -1 )) {
		// path tells wiki somehow open it in popup as wiki...
			if ( extern[ h[ 0 ]]) {
				if ( !( extern[ h[ 0 ]] < 0 ))
					_open( extern[ h[ 0 ]]);
			}
			else
				useLib.ajax( h[ 0 ], _open, 'txt', 3000 );
		}
		else
			stop = false;
		if ( stop ) {
			ev.preventDefault();
			ev.stopPropagation();
		}

		function _scroll() {
		if ( h[ 1 ] && h[ 1 ].length )
			setTimeout( function() {
				DOM.scrollIntoView( $Q( D, '[name=' + h[ 1 ] + ']', 1 ), 0, 1 );
			}, 1000 );
		}
		function _open( t ) {
		var
			_e = D.getElementById( cssNS.designer )
			, w = openPopup( _getHead()
				+ getHTMLTag( 'link', 'id="uLstandSheet" title="normal"', ''
				, conf.designDefs[ _e ? _e.options[ _e.selectedIndex ].text
				: conf.designDefault ].path )
				+ getHTMLTag( 'script', 'id="uLuseLibConf"', ''
				, conf.useLibPath +'useLibConf.js' )
				+ getHTMLTag( 'script', 'id="uLuseLib"', ''
				, conf.useLibPath +'useLib.min.js' )
				+ getHTMLTag( 'script', 'id="uLwiki"', ''
				, conf.useLibPath +'uLwiki.js' )
				+ getHTMLTag( 'script', 'id="uLlocal"'
				, 'useLib.conf.userSupport = "sizer explainer designer";' )
			, '<div class="'+ cssNS.layout + ' ' + cssNS.landmark + '">'
			+ useLib.getContainer( 'main', t ).outerHTML
			+ '<div class="' + cssNS.design +'"><span><span><span><span>'
			+ '<span></span></span></span></span></span></div></div>\n' )
			;
			extern[ h[ 0 ]] = t;
			if ( h[ 1 ] && w.document
			&& ( w = w.document.getElementsByName( h[ 1 ])))
				w[ 0 ].focus();

			function _getHead() {
			var
				_h = ''
				;
				t = useLib.WIKI.regs.clean( t ).replace(
				useLib.WIKI.regs.findEngine()
				, function( all, t, def, v ) {
					v = v.trim();
					def = !!def && def == '=';
					if ( !!v.length ) {
						if ( t == "title" )
							_h += "\n<title>"+ v +"</title>";
						else
							_h += '\n'+ getHTMLTag( 'meta', 'name="'+ t 
							+ '" content="'+ v +'"' );
					}
					return '';
				});
				return _h;
			}
		}
	});

	function setListSelect( id, list ) {
	//@ PARAM: idOfElement, listArrayOfSelectableValues, callingParent
	//@ shows a list of selectable values below element of given id. If selected
	//@ it is closed and the value is set to the elementField(s). Simular to
	//@ autocomplete, but not with values stored by the browser.
	//@ REMARK: simple capture, its closed on selection and onblur if not focus
	//@ on dynList
	//@ ATTENTION: on setListSelect stop rotor is a must else rotorSteps will
	//@ change choosen input! listSelect flag is obeyed there (sometimes you have
	//@ to search a long time) :-[ 
	var
		aE = $( id )
		, a = []
		, l = 0		// maximalValueLength
		;
		lastActive = aE;
		_closeListSelect();
		if ( list.length ) {
			for ( var i = 0; i < list.length; i++ ) {
				list[ i ] = list[ i ].split( '::' );
				list[ i ] = list[ i ].join( ' ' );
				a.push( [ 'a', {
					href: 'javascript:useLib.call.listSelectOnKey("' + id + '","'
					+ list[ i ]	+ '")' }, list[ i ]
				]);
				if ( list[ i ].length > list[ l ].length )
					l = i;
			}
			cover.listSelect = 'uLlistSelect';
			dialog.setEleDiv( aE, cover.listSelect
				, [ 'div', { CN: cssNS.listSelect }].concat( a ), 1
			);
			a = $( cover.listSelect );
			$S( a ).width = ((		// optimize width of listSelect
				DOM.textWidth( list[ l ], cssNS.listSelect ) + 16 ) / 10 ) + 'em';
			$E( a, 'KD', function _keydown( ev ) { keyControl = _toListSelect; });
			$E( D, 'MC', _blurListSelect );
		}

		function _closeListSelect() {
			call.modalClose( $( 'uLlistSelect' ));
			$E( D, 'MC', _blurListSelect, -1 );
			cover.listSelect = keyControl = false;
			setFocus( aE );
		}

		function _blurListSelect( ev ) {
		var
			e = $( cover.listSelect )
			;
			if ( !e || !e.contains( ev.currentTarget ))
				_closeListSelect();
		}

		function _toListSelect( c, ev ) {
		var
			fE = D.activeElement
			;
			// ARROWS, TAB (9), ESC (27), SPACE (32), Return (13)
			c = c == 9 || c == 27 ? 9
			: c == 13 || c == 32 ? 13
			: c == 37 || c == 38 ? -1 : c == 39 || c == 40 ? 1 : 0;
			if ( c == 9 )
				_closeListSelect();
			else if ( c && !ev.ctrlKey && !ev.metaKey ) {
				// set the list key control active
				if ( c == 13 )
					fE.click();
				else if ( c == -1 && !fE.previousSibling ) {
					fE = aE;
					_closeListSelect();
				}
				else
					fE = c == -1 ? fE.previousSibling : ( fE.nextSibling || fE );
				setFocus( fE );
			}
			return true;
		};

		call.uLinit( 'listSelectOnKey', function( id, t ) {
		var
			p = id.indexOf( idCover )
			;
			if ( p > -1 ) {
				id = id.slice( 0, p );
				p = getFormDef( $( id ));
				if ( p.subs.COUNTRY && p.subs.COUNTRY.id == p.first )
					p = p.subs.COUNTRY.next;
				else
					p = p.first;
				doEvent.setInput( $( p ), id, t, true );
			}
			else
				$( id ).value = t;
			_closeListSelect();
		});
	}

/*****
 *
 *		metaReaction methods to process varios user interactions and support f
 *		@ unctions on elements Mainly they are for creating and operate metaReaction
 *		@ like a datepicker, but they also may support special interactions like the
 *		@ management of limits,  metadata or influences between different elements
 *		@ i. e. start und end date etc. They shall be capsulated and accessed via
 *		@ the metaReaction struct where they have to be registered. Some standard
 *		@ methods will be called and executed by the cover  management, if they are
 *		@ defined:
 *		@ 1. X.init( e ) : to iniitialize variables, states or structures of a
 *		@ selector etc.
 *		@ 2. X.getContent( e ) : return the body of a formStruct for the selector
 *		@ 		modal dialag
 *		@ 3. X.doButton( ev, e ) : reacts on push a button in a cover (alternatively
 *		@		tox.getContent!)
 *		@ 4. X.getValue( e ) : called on useraction after getContent or doButton,
 *		@		i. e. getcoverValue and put into selector after pushing button
 *		@ 5. X.setValue( e ) : called after finishing the metaReaction to set
 *		@		coverValue,i. e. set selectorValue to coverValue
 *		@ 6. X.doReaction( e, fD ) :		// called from direct interaction methods
 *		@		returns { [all optional]
 *		@ 			init( eC ) : to initialize the reactions i. e. load data
 *		@ 			, onrotor( eC, v, step ) : to react on digit step at direct interact
 *		@ 			, onchange( eC, v ) : to react on new value after direct interaction
 *		@ 			, onblur( eC, readyCombinedValue ) : to react on value after leaving
 *		@			  coverField no ready combined value ? false : value with separators 
 *		@		}
 * 
 *****/
var
	subValue = function( fD, sT ) {
	//@ returns (integer) value of a coverElement by name of subType
		return !!( sT = fD.getId( sT )) ? $I( $( sT ).value ) : 0;
	}
	, setFree = function( id, c, s ) {
		$( id + 'free-' + c ).innerHTML = '<span>' + s + '</span>';
	}
	, getCurrencySign = function( c ) {
	var							// short'n signs for conformity with user expectations
		currSign = { EUR: '€', USD: '$', GBP: '£', JPY: '¥' }
		;
		c = c.toUpperCase();
		return currSign[ c ] || c;
	}
	, patterns = {
		url: /^(https?:\/\/)?(www\.)?[-a-zA-Z0-9@:%_\+.~#?&=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&=]*)?$/i
		, email: /^[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?$/i
	}
	, metaReaction = {	// register of several metaReaction
		days: {
			doButton: function( ev, e ) {
				getFormDef( e ).fixed = $CN.toggle( ev.currentTarget, cssNS.locked );
			}
		}
		, toggle: {
			doButton: function( ev, e ) {
			var
				fD = getFormDef( e )
				;
				fD.toggle = $CN.toggle( ev.currentTarget, cssNS.locked )
				? 'P' : '';
				if ( fD.control )
					fD.control( 'toggle', fD.toggle );
			}
			, setValue: function( fD, calc ) {
			var
				t = fD.toggle
				, v = _getValue( t )
				;
				if ( calc == 0 )
					v = [ 0, 0 ];
				else {
					if ( calc.base != 1. )
						v = _calcValue( v, calc.base, t == 'P' ? '' : 'P' );
					v = _calcValue( v, calc.convers, t );
					v = String(( v ).toFixed( 2 )).split( '.' );
				}
				fD.value_1_ = [ v.join( '.' )];
				t = t == 'P' ? '' : 'P';
				$( fD.subs[ t + 'INT' ].id ).value = v[ 0 ];
				$( fD.subs[ t + 'FIX' ].id ).value = v[ 1 ];

				function _getValue( t ) {
					return (
						subValue( fD, t + 'INT' ) + '.' + subValue( fD, t + 'FIX' )
					);
				}

				function _calcValue( v, f, _t ) {
					return isNaN( f ) ? f( v, _t != 'P' ) : _t == 'P' ? ( v * f )
					: f == 0 ? 0 : ( v / f );
				}
			}
			, setStatus: function( fD ) {
				if ( !$CN.contains( $( fD.id ), cssNS.disabled )) {
					$CN[ fD.toggle != 'P' ? 'remove' : 'add' ](		// find toggle button
						$Q( $P( $( fD.first )), 'button.' + cssNS.toggle, 1 )
						, cssNS.locked
					);
					_setDisabled( '', fD.toggle == 'P' );
					_setDisabled( 'P', fD.toggle != 'P' );
					setFocus( $( fD.subs[ fD.toggle + 'INT' ].id ));
				}

				function _setDisabled( t, on ) {
					setStatus( $( fD.subs[ t + 'INT' ].id ), 'disabled', on );
					setStatus( $( fD.subs[ t + 'FIX' ].id ), 'disabled', on );
				}
			}
		}
		, fon: {
			init: function( e, fD ) {
				metaReaction.world.init( e, fD, conf.defaults.fon );
				fD.world = conf.country;
				fD.getSub( fD.first ).check.doReaction
					= metaReaction.world.doReaction( e, fD );
			}
		}
		, email: {
			doReaction: function( e, fD ) {// df@use-optimierung.de
				return {
					onblur: function( eC, vFull ) {
						if ( vFull && !patterns[ fD.type ].test( vFull ))
							dialog.setMsg( 'error', 'formatError', e, 5500 );
					}
				}
			}
		}
		, title: {
			init: function( e, fD ) { setEleValue( $( fD.first ), fD.value ); }
			, setValue: function( e, v ) { setEleValue( $( getFormDef( e ).first ), v ); }
		}
		, timestamp: {
			init: function( e, fD ) {
				setDisabled( fD, true );
				if ( !e.value || e.value == '' )
					e.value = ~( new Date ).getTime(); // time in seconds
				_set();
				e.onchange = function() { _set(); };

				function _set() {
					$( fD.first ).value = $D.timestamp(	e.value * 1000	);
				}
			}
		}
	}
	;
	metaReaction.fixpin = metaReaction.days;
	metaReaction.url = metaReaction.email;

/*****
 *
 *		functions to create clock control
 *		@ 
 *		@ 
 * 
 *****/
metaReaction.clock = (function() {
var
	doIt = {
		setTimeout: function( fD, t ) {
			setTimeout( function(){
				if ( !fD.clockStop )
					doIt[ t ]( fD, t );
			}, !$I( t ) ? 1000 : t );
		}
		, 0: function( fD, t ) {			// time
			setClock( fD, new Date());	
			doIt.setTimeout( fD, t );
		}
		, 1: function( fD, t ) {			// alarm
			if ( fD.alarmTime )
				clearTimeout( fD.alarmTime );
			t = $D().getTime() + fD[ t ] - fD.startTime;
			fD.alarmTime = setTimeout( function(){ doAlarm( fD );}
				, t < 0 ? t + 24 * 3600000 : t
			);
			$CN.add( $( fD.subs.PSELECT.id ), cssNS.clock );
		}
		, 2: function( fD, t ) {			// stop
			setClock( fD, new Date().getTime() - fD.startTime + fD[ t ]);
			doIt.setTimeout( fD, t );
		}
		, 3: function( fD, t ) {			// countDown
		var
			aT = fD.startTime - new Date().getTime() + fD[ t ]
			;
			setClock( fD, aT );
			if ( aT <= 0 )
				doAlarm( fD );
			else
				doIt.setTimeout( fD, t );
		}
	}
	, getClock = function( fD ) {
		return $I( $( fD.subs.PHOURS.id ).value ) * 3600000
			+ $I( $( fD.subs.PMINUTES.id ).value ) * 60000
			+ $I( $( fD.subs.PSECONDS.id ).value ) * 1000;
	}
	, setClock = function( fD, t ) {
	var
		isD = !!t.getTime
		;
 		_set( 'PHOURS', $I( isD ? t.getHours() : t / 3600000 ));
	 	_set( 'PMINUTES', zeroInt( isD ? t.getMinutes() : t / 60000, 2 ));
	 	_set( 'PSECONDS', zeroInt( isD ? t.getSeconds() : t / 1000, 2 ));

	 	function _set( t, v ) {
	 		t = $( fD.subs[ t ].id );
	 		if ( t )
	 			t.value = v; 
	 	}
	}
	, doAlarm = function( fD ) {
		DOM.scrollIntoView( $( fD.first ), 1.5 );
		_alarmSound( 3 );

		function _alarmSound( t ) {
			sound( conf.sounds[ 'alarm' ]);
			delayRemove( $P( $( fD.first )), cssNS.keyError, 1 );
			if ( t > 0 )
				setTimeout( function(){ _alarmSound( --t )}, 3000 );
		}
	}
	, setDisabled = function( fD, on ) {
		for( var n in fD.subs )
			setStatus( $( fD.subs[ n ].id ), 'disabled', on );
	}
	;
	return {
		init: function( e, fD ) {
			e = $( fD.subs.PSELECT.id );
			$CN.add( $P( e ), "uL_ignore" );
			e.selectedIndex = 0;
			setDisabled( fD, true );
			doIt[ 0 ]( fD, 0 );
			$E( e, 'OC', function _initChange() {
			var
				eC = this
				, i = eC.selectedIndex
				;
				fD.clockStop = true;
				$CN.add( eC.nextSibling, cssNS.locked );
				setClock( fD, fD[ eC.options[ i ].value ]	|| ( i == 2 || i == 3 ? 0
				: new Date()));
				D.activeElement.blur();
			});
		}
		, doButton: function( ev, e ) {
		var
			fD = getFormDef( e )
			, eT = $( fD.subs.PSELECT.id )
			, stop = $CN.toggle( ev.currentTarget, cssNS.locked )
			;
			setDisabled( fD, !stop );
			fD.clockStop = stop;
			eT = eT.selectedIndex;
			if ( !stop ) {
				if ( eT == 0 )
					doIt[ 0 ]( fD, eT );
				else if ( eT == 4 )
					fD.clockStop = true;
				else {
					fD[ eT ] = getClock( fD );
					fD.startTime = new Date().getTime(); 
					doIt[ eT ]( fD, eT );
				}
			}
			else if ( eT == 1 ) {
				$CN.remove( $( fD.subs.PSELECT.id ), cssNS.clock );
				if ( fD.alarmTime )
					clearTimeout( fD.alarmTime );
			}
		}
	};
})();
//@ end of metaReaction.clock

/*****
 *
 *		functions to control password interaction
 *		@ 
 *		@ 
 * 
 *****/
metaReaction.password = ( function(){
var
	pwImg = new Image()
	, eRepeat = false
	, pwStored = ''
	, pwRepeat = ''
	, getMin = function( fD ) {
		return useLib.security ? ( useLib.security.minPW || 80 )	: ( fD.min || 80 );
	}
	, setHash = function( pw, cllbck ) {
		if ( !!W.crypto && !!W.crypto.subtle ) {
   		crypto.subtle.digest( "SHA-512"
   		, new TextEncoder( "utf-8" ).encode( pw )).then( function( hash ) {
            hash = new DataView( hash );
				for ( var j = 0, s = ''; j < hash.byteLength
				; s += String.fromCharCode( hash.getUint8( j++ )));
				cllbck( s );
			});
		}
		else
			return cllbck( pw );
	}
	, log2 = Math.log2 || function(x) { return Math.log(x) / Math.LN2; }	// polyfill
	, calcQuality = function( p ) {
	//@ calculate bit entropie, the given parameter is the minimum entropie value.
	//@ REMARK: It's a rough approach, over five different sign types, elimination
	//@ 	of repetitions and standard pattern from alphabethical and keyboard order.
	var
		t = [ /[a-z]/g, /[A-Z]/g, /[_\.\*+:#!?%\|@\{\}\[\]\(\);=\“&$\\/,-]/g
		, /[0-9]/g, /[\u00C0-\u017F]/g ]
		, syllable = /(ABE|ACH|ALL|AND|ARE|AUF|AUS|BEN|BER|BUT|CHE|CHT|DAS|DEN|DER|DIE|EIN|EIT|END|ENT|ERA|ERE|ERS|ESE|EVE|FOR|GEN|HAD|HAT|HEN|HER|HIN|HIS|ICH|IGE|INE|ING|ION|IST|ITH|LIC|LLE|MEN|MIT|NDE|NEN|NGE|NIC|NOT|NTE|OME|OUL|OUR|REN|SCH|SEI|SEN|SHO|SIC|SIE|STE|TED|TEN|TER|THA|THE|THI|TIO|ULD|UND|UNG|VER|WAS|WIT|YOU|AL|AN|AR|AS|AT|AU|BE|CH|DA|DE|DI|EA|ED|EI|EL|EN|ER|ES|GE|HA|HE|HI|HT|IC|IE|IN|IS|IT|LE|LI|ME|ND|NE|NG|NT|ON|OR|OU|RE|SC|SE|SI|ST|TE|TH|TI|TO|UN|VE|WA)/ig
		, pp = p
		, c = 0         // count of used char amount
		;
		if ( !p.length )
			return 0;
		// first step: check different chartypes to calculate permutation base
		for ( var i = 0; i < t.length; i++ ) {
			if ( pp.match( t[ i ])) {
				c += i < 3 ? 26 : 10; 	// european chars are 201!, but not on keyboards
				p = p.replace( t[ i ], '' );
			}
		}
		if ( p.length )		// still length...
			c += 10;				// ...some very special chars
		p = pp;
		// second step: eliminate more than two repeated patterns.
		//  result => shorter string, that causes a calculation on secured side
		p = p.replace( /(.)\1\1+/g, '$1$1' );
		// third step: get length where standardPattern counted as one sign
		p = replaceStandardPattern( p );
		// fourth step: eliminate frequent syllables to rate secure against dictionary hacks
		p = p.replace( syllable, '\t' );
		// normalize to 10 counting chars with a calced entropie value around 65 
		return Math.min( 99, parseInt( 80 / 65 * log2( c ) * p.length ));

		function replaceStandardPattern( p ) {
		var
			t = '!"§$%&/()=?qwertzuiop+asdfghjkl#<yxcvbnm,.-'
			+ '1234567890abcdefghijklmnopqrstuvwxyz'
			;
			t += t.split('').reverse().join('');    /* also reverse */
			p = _check( p.toLowerCase());
			return p;

			function _check( s ) {
			//@ step through hole password and cut stepwise chars, if result
			//@ is part of test string, replace it with one char instead
				if ( s.length > 5 )
					for ( var i = 0; i < s.length - 4; i++ ) {
						for ( var j = 0; j <= i; j++ )
							if ( t.indexOf( s.slice( i - j, s.length -j )) > -1 ) {
								// replace just with tab
								return _check( s.slice( 0, Math.max( i - j, 0 )))
									+ '\t' + _check( s.slice( s.length - j ));
						}
					}
				return s;
			}
		}
	}
	;
	return {
		init: function( e, fD ) {
		var
			cN = cssNS.indicatorBar
			, free_0 = $( fD.id + 'free-0' )
			, eC
			;
			if ( !!fD.required && !/\(min.\s/.test( labels[ e.id ].innerHTML ))
				labels[ e.id ].innerHTML += ' (min. '+ getMin( fD ) +'%)';
			$A( e, 'size', 325 );
			free_0.previousSibling.onclick = function() {
			var
				p = ''
				;
				for( var n in fD.subs )
					p += $( fD.subs[ n ].id ).value;
				dialog.prompt( 'viewPassword', function( v ) {
					if ( v !== false )
						fD.subs.PASSWORD.check.doReaction.onchange( 0, v );
				}, p, '[\\w\\W]{0,128}' );
				return false;
			};
			for( var n in fD.subs ) {
				eC = $( fD.subs[ n ].id );
				$A( eC, 'onmousedown', -1 );
				$A( eC, 'type', 'password' );
			}
			fD.subs[ n ].check.maxCount = 300;// last password field may be very large...
			if ( fD.type == 'password' ) {
				if ( !$CN.contains( $( fD.id + 'free-2' ), cssNS.password )) {
					setFree( fD.id, 1, '0%' );
					$( fD.id + 'free-2' ).className += ' ' + cssNS.password;
					$P( free_0, 2 ).appendChild( $CE( [ 'span'
					, { CN: cN }]));
					$P( free_0, 2 ).appendChild( $CE( [ 'span'
					, { CN: cN +' '+ cssNS.password, id: cN }]));
				}
			}
			else {
				fD.regexp = /ident/;
				eRepeat = fD;
			}
			//@ check support of for grafical password needed HTML5 methods
			if ( !!conf.puzzlePassword && ( !useLib.password || !( W.URL || W.webkitURL )
			|| !W.CanvasRenderingContext2D )) {
				delete this.getContent;
				this.doButton = function() {
					dialog.setMsg( 'hint', 'noPuzzlePassword', $( fD.id ), 4500 );
				}
			}
			else if ( useLib.password ) {
			var
				iData = $LS( 'pwImgData' );
				if ( iData )
					pwImg.src = iData;				// passwordImage from localStorage
			}
		}
		, getContent: function( e, type ) {
			if ( pw = useLib.password ) {
				return pw.getContent(
				//@ this is also the interface to the internal useLib methods, because
				//@ those are unknown in the extern code for the graphical password
					getStruct						// toCreate standard useLibStructs
					, function( s ) {				// := setContent( useLibStruct )
					var
						_e = $( e.id + cssNS.modal )
						;
						_e.innerHTML = $CE( [ 'span', {}].concat( s )).innerHTML;
						initForm( _e );
					}
					, function( m, _e, t ) {
					// := dialog.setMsg( msgStruct, elementOfMsg, type )
						lang.pwMsg = m;
						dialog.setMsg( t || 'hint', 'pwMsg', _e, 4500 );
					}
					, function( _pwImg, _pwData ) {	// := setValue( pwImg, pwData )
					var
						dR = metaReaction.password.doReaction( e, getFormDef( e ))
						, v = useLib.security.getPasswordHash( _pwData )
						;
						$LS( "pwImgData", _pwImg );
						pwImg.src = _pwImg;
						dR.onchange( false, _pwData );
						e.value = v;
						getFormDef( e ).value = [ v ];
						dialog.closeModal( e.id, v );
					}
					, pwImg.src.length ? pwImg : false
				);
			}
		}
		, doReaction: function( e, fD ) {
			return {
				onchange: function( eC, v ) {
				var
					p = ''
					;
					if ( eC )
						eC.value = v;
					else
						p = v;
					for( var n in fD.subs ) {
						if ( eC )
							p += $( fD.subs[ n ].id ).value;
						else {
							$( fD.subs[ n ].id ).value = v.slice( 0, 5 );
							v = v.slice( 5 );
						}
					}
					internChange = true;
					if ( fD.type == 'password' ) {
						pwStored = p;
						eRepeat.required = !!p.length;
						_qualitityCheck( p );
					}
					else {
						pwRepeat = p;
						e.value = pwRepeat == pwStored ? 'ident' : '';
					}
					fD.value = [ e.value ]; 
					internChange = false;
					return eC.value;

					function _qualitityCheck( p ) {
						p = calcQuality( p ) || 0;
						fD.pwQuality = p;
						setFree( fD.id, 1, p + '%' );
						$( cssNS.indicatorBar ).style.width = p + '%';
						$CN[ p >= getMin( fD ) ? 'add' : 'remove' ]( $( cssNS.indicatorBar )
							, cssNS.success );
					}
				}
				, onblur: function() {
					internChange = true;
					if ( fD.type == 'password' ) {
					var
						salt = conf.hashPassword
						;
						if ( salt && useLib.security )
							_set( useLib.security.getPasswordHash( pwStored + salt ));
						else if ( salt )
							setHash( pwStored + salt, _set );
						else
							_set( pwStored );
						if ( eRepeat && pwRepeat.length )
							_check( $( eRepeat.first ));
					}
					else {
						_check( e );
						_set( pwRepeat == pwStored ? 'ident' : '' );
					}
					internChange = false;

					function _set( v ) {
						e.value = v;
						fD.value = [ v ];
					}

					function _check( _e ) {
						if ( pwStored.length && pwRepeat != pwStored ) {
							setTimeout( function() {
								if ( D.activeElement.type != 'password' ) {
									cover.keyMsg( e, 'Error' );
									dialog.setMsg( 'error', 'repeatError', null, 5000 );
								}
							}, 400 );
						}
					}
				}
			};
		}
	}; 
})();
metaReaction.passwordrepeat = metaReaction.password;
//@ end of metaReaction.password

/*****
 *
 *		functions to create world control
 *		@ 
 *		@ 
 * 
 *****/
metaReaction.world = ( function() {
var							// short'n signs for conformity with user expectations
	tL = false
	, sL = []
	, findIndex = function ( s ) {
	var
		i = false
		;
		s = new RegExp( '^' + s, 'i' );
		sL.some( function( e, _i ) {
			if ( s.test( e.v ))
				i = _i;
			return i !== false;
		});
		return i || 0;
	}
	, initButton = function( e, cc ) {
	var
		b = $Q( e.nextSibling, 'BUTTON[class^="' + cssNS.world + '"]', 1 )
		;
		if ( b && tL )
			b.title = tL[ cc ] || '';
		return !!b ? $P( b ) : b;
	}
	;
	function getWorldList( e, t, w ) {
	var
		fD = getFormDef( e )
		, pos = [ 'region', 'level', 'language', 'domain', 'vehicle', 'currency', 'fon'
		//, 'iban', 'towns'	// not used as value, because only CountryCode is needed
		].indexOf( fD.type )
		, wLevel = fD.data ? $I( fD.data ) : conf.worldIndustrialLevel
		, activeGroup = ''
		, list = []
		, head
		, v
		, l
		;
		w = w || loadJSONCSS( 'uLworld.json.css', function( w ) {
			if ( D.contains( e )) getWorldList( e, t, w );
		});
		if ( !!w ) {
			l = w[ conf.lang ] ? conf.lang : 'en';
			tL = w[ l ].names;
			if ( wLevel ) { //&& fD.type != 'country' ) {
				for ( var level = 1; level <= wLevel; level++ )
					for ( var cc in w[ l ].names )
						if ( w.countryData[ cc ][ 1 ] == level	&& ( v = _getValue( cc )))
							if ( t == 'rotorList' )
								list.push( v );
							else
								_getRow( 'level', cc, v );
			}
			else {
				if ( t == 'rotorList' && fD.type == 'country' )
					for ( var cc in tL )
						list.push( _getValue( cc ));
				else {
					for ( var i = 0; i < w[ l ].regionOrder.length; i++ ) {
						if ( v = _getValue( w[ l ].regionOrder[ i ])) {
							if ( t == 'rotorList' )
								list.push( v );
							else
								_getRow( 'region', w[ l ].regionOrder[ i ], v );
						}
					}
				}
			}
			if ( t == 'rotorList' && fD.type != 'country' )
				list.sort( function( a, b ){ return compare( a.v, b.v ); });
			return t == 'rotorList' ? list
			: metaReaction.singleSelect.getContent( e, 'singleSelect', list );
		}
		return w;

		function _getValue( cc ) {
			return pos < 0 || w.countryData[ cc ][ pos ].length ? {
				v:	fD.type == 'country' ? tL[ cc ] : pos < 0 ? cc
				: w.countryData[ cc ][ pos ]
				, cc: cc
			} : false;		// only countries that have a value
		}

		function _getRow( type, cc, v ) {
			if ( activeGroup != w.countryData[ cc ][ type == 'region' ? 0 : 1 ] ) {
				activeGroup = w.countryData[ cc ][ type == 'region' ? 0 : 1 ];
				head = w[ l ][ type ][ activeGroup ];
				list.push({ type: 'group', textContent: head });
			}
			if ( v.v.length )
				list.push({
					textContent: tL[ cc ]
					, shortCut: pos < 0 ? null : ( fD.type == 'fon' ? '+' : '' ) + v.v
					, value: v.v+ '::' + cc
				});
		}
	}
	return {
		init: function( e, fD, v ) {
		var
			b = initButton( e, fD.world || conf.country )
			;
			if ( fD.type == 'currency' && b )
				b.previousSibling.innerHTML = getCurrencySign( v );
			else
				$( fD.first ).value = v || e.value;
		}
		, getValue: function( e ) {
		var
			fD = getFormDef( e )		// if COUNTRY the value is on the end
			, eA = 'input[value' + ( fD.subs.COUNTRY ? '$' : '^' ) + '="'
			+ $( fD.first ).value + '"]'
			;
			if ( eA = $Q( $( e.id + cssNS.modal ), eA, 1 ))
				eA.checked = true;
			metaReaction.singleSelect.getValue( e );
		}
		, setValue: function( e ) {
		var
			fD = getFormDef( e )
			, eA = $Q( $( e.id + cssNS.modal ), 'input:checked', 1 )
			, oldV = fD.world
			, v
			;
			if ( eA ) {
				v = eA.value.split( '::' );
				fD.world = v[ 1 ];
				if ( v[ 1 ] != oldV ) {
					metaReaction.world.init( e, fD, fD.subs.COUNTRY ? v[ 1 ] : v[ 0 ]);
					if ( fD.control )
						fD.control( 'world', v[ 0 ]);
				}
			}
			setFocus( $( fD.first ));
		}
		, getContent: getWorldList
		, doReaction: function( e, fD ) {
		var
			sC
			;
			return {
				init: function( eC, w ) {
					w = w || loadJSONCSS( 'uLworld.json.css', function( w ) {
						if ( D.contains( eC )) sC.doReaction.init( eC, w );
					});
					sC = fD.getSub( eC.id ).check;
					if ( !!w ) {
						tL = w[ w[ conf.lang ] ? conf.lang : 'en' ].names;
						sL = getWorldList( e, 'rotorList' );
						sC.min = 0;
						sC.max = sL.length - 1;
						sC.value = findIndex( eC.value );
					}
				}
				, onrotor: function( eC, v, step ) {
				//@ ATTENTION: inverts step because its a sorted alphabetic list!!
					if ( tL ) {
						v = $I( sC.value + ( step > 0 ? -1 : 1 ));
						v = v > sC.max ? sC.min : v < sC.min ? sC.max : v;
						sC.value = v;
						fD.world = sL[ v ].cc;
						initButton( e, fD.world );
						return sL[ v ].v;
					}
					return eC.value;
				}
				, onchange: function( eC, v, onrotor ) {
					v = fD.type == 'country' ? v : v.toUpperCase();
					if ( tL && v.length && !onrotor ) {
					var
						r = new RegExp( '^' + v, 'i' )
						, a = []
						;
						eC.value = v;
						sC.value = i;
						for ( var i = findIndex( v ), cV; i < sL.length; i++ ) {
							cV = sL[ i ][ fD.subs.COUNTRY ? 'cc' : 'v' ];
							if ( r.test( cV )) {
								a.push( sL[ i ].v
								+ ( fD.type == 'country' ? '' : '::' + tL[ sL[ i ].cc ]));
								if ( a.length > 15 			// to prevent too long lists...
								|| eC.value == cV ) {		// ...and active value in list
									a = [];
									break;
								}
							}
						}
						if ( a.length )
							setListSelect( eC.id, a );	
					}
					return v;
				}
			};
		}
	}; 
})();
metaReaction.country = metaReaction.world;
//@ end of metaReaction.world

/*****
 *
 *		functions to control currency exchange interaction
 *		@ 
 *		@ 
 * 
 *****/
metaReaction.currency = ( function(){
var
	baseCurrency = 'EUR'
	, checkRate = function( fD, r, left, info ) {
		if ( left || info ) {
			if ( fD.rate.convers == 0. || fD.rate.base * fD.rate.convers == 1. ) {
				dialog.setMsg( 'hint'
					, fD.rate.convers == 0. ? 'noRateGiven' : 'noRateCalc', $( fD.id ), 4500 );
				$( fD.subs.PINT.id ).value = 0;
				$( fD.subs.PFIX.id ).value = 0;
				fD.toggle = '';
				$( fD.subs.PINT.id ).title = lang.noRateGiven[ 1 ];
				info = left = false;
			}
			else if ( r )
				$( fD.subs.PINT.id ).title = lang.noRateCalc[ 1 ] + ' '
					+ ( fD.rate.base / fD.rate.convers ).toFixed( 4 )
					+ ' (' + r.date + ' ECB)';
		}
		metaReaction.toggle.setStatus( fD );
		return info;
	}
	, calcRate = function( eC, fD ) {
		metaReaction.toggle.setValue( fD
			, fD.rate.base * fD.rate.convers == 1. ? 0 : fD.rate );
	}
	;
	return {
		init: function( e, fD ) {
		var
			c = ( fD.data || conf.defaults.currency ).toUpperCase()
			;
			metaReaction.world.init( e, fD, c );
			setFree( fD.id, 0, getCurrencySign( c ));
			fD.subs.FIX.check.max = fD.subs.PFIX.check.max = 99;
			fD.toggle = '';
			fD.rate = { base: 1., convers: 1. };
			$( fD.subs.PINT.id ).value = ""; 
			fD.control = function( t, v, r ) {
				r = r || loadJSONCSS( 'currency.json.css'
				, function( r ) {	if ( D.contains( e )) fD.control( t, v, r ); });
				if ( !!r ) {
					if ( t == 'toggle' )
						checkRate( fD, r, v == 'P' );
					else if ( t == 'world' ) {
						if ( r ) {
							if ( c != baseCurrency )
								fD.rate.base = 1. / ( r.rates[ c ] || 1. );
							fD.rate.convers = v == baseCurrency ? 1.
							: parseFloat( 1. / r.rates[ v ] || 0. );
							if ( checkRate( fD, r, false, true ))
								calcRate( e, fD );
						}
					}
				}
			};
			metaReaction.toggle.setStatus( fD );
		}
		, setValue: metaReaction.world.setValue
		, doReaction: function( e, fD ) {
			return {
				onchange: function( eC, v ) {
					eC.value = v;
					calcRate( e, fD );
					return v;
				}
			};
		}
	}; 
})();
//@ end of metaReaction.currency

/*****
 *
 *		functions to control dimension interaction
 *		@ 
 *		@ 
 *
 *****/
metaReaction.unit = ( function(){
var
	getAllUnitList = function( e, fD, t, uL ) {
	var
		gL = []
		, i = 0
		, h
		, t
		;
		uL = uL || loadJSONCSS( 'uLunit.json.css', function( uL ) {
			if ( D.contains( e )) getAllUnitList( e, fD, t, uL );
		});
		if ( !!uL ) {
			uL = uL.unitData;
			for ( var u in uL ) {
				h = uL[ u ].header[ conf.lang ] || uL[ u ].header[ 'en' ];
				t = uL[ u ][ conf.lang ] || uL[ u ][ 'en' ];
				gL.push({
					textContent: t +' //(' + h + ')//'
					, value: u
					, shortCut: uL[ u ].sign || u
				});
			}
			return metaReaction.singleSelect.getContent( e, 'singleSelect', gL );
		}
		return uL;
	}
	, initUnitSelect = function( fD, v, uL ) {
	//@ ATTENTION: the conversion of the 'calulation array' into the required
	//@ 	function is done inside _getOption() [easiest, if not global onload of JSON]
	var
		eS = $( fD.subs.PSELECT.id )
		;
		$E( eS, 'OC', function _unitChange() {
			fD.unit.convers = this.options[ this.selectedIndex ].value;
			setFocus( $( fD.subs[ fD.toggle + 'INT' ].id ));
			calcUnit( $( fD.first ), fD );
		});
		if ( !!!uL[ v ]) {
			v = -1;
			for ( var u in uL )
				if ( v === -1 ) {
					for ( var c in uL[ u ].convers )
						if ( c == fD.unit.base ) {
							fD.unit.name = u;
							v = c;
							break;
						}
				}
				else								// stop if found
					break;
		}
		if ( v !== -1 ) {
			u = fD.unit.name;
			eS = eS.options;
			eS.length = 0;
			eS[ eS.length ] = _getOption( uL[ u ], u, u == fD.unit.base );
			for ( var c in uL[ u ].convers )
  				eS[ eS.length ] = _getOption( uL[ u ].convers[ c ], c, c == fD.unit.base );
	  	}
		return v == -1 ? '' : uL[ v ] ? ( uL[ v ].sign || v )	: uL[ u ].convers[ v ].sign || v;

		function _getOption( _u, _c, s ) {
			if ( typeCheck( 'Array', _u.calc ))
				_u.calc = new Function( _u.calc[ 0 ], _u.calc[ 1 ]);
			return new Option(( _u.sign || _c ) + ' : '	+ ( _u[ conf.lang ] || _u[ 'en' ])
				, _c, s, s );
		}
	}
	, calcUnit = function( eC, fD, uL ) {
		uL = uL || loadJSONCSS( 'uLunit.json.css', function( uL ) {
			if ( D.contains( eC )) calcUnit( eC, fD, uL );
		});
		if ( !!uL && fD.unit ) {
			uL = uL.unitData;
			metaReaction.toggle.setValue( fD
				, ( fD.unit.base != fD.unit.convers ) ? {
					base: _getCalc( fD.unit.base )
					, convers: _getCalc( fD.unit.convers )
				}
				: 0
			);
		}

		function _getCalc( t ) {
			return uL[ t ] ? uL[ t ].calc : uL[ fD.unit.name ].convers[ t ].calc;
		}
	}
	;
	return {
		init: function( e, fD, v, uL ) {
			uL = uL || loadJSONCSS( 'uLunit.json.css', function( uL ) { 
				if ( D.contains( e )) metaReaction.unit.init( e, fD, v, uL );
			});
			if ( uL ) {
				v = v || fD.data || conf.defaults.unit;
				fD.subs.FIX.check.max = fD.subs.PFIX.check.max = 99;
				fD.toggle = '';
				fD.unit = { name: v, base: v, convers: v };
				$( fD.subs.PINT.id ).value = ""; 
				if ( !!uL && uL.unitData )
					setFree( fD.id, 0, initUnitSelect( fD, v, uL.unitData ));
				fD.control = function( t, _v ) {
					if ( t == 'toggle' ) {
						if ( fD.unit.base == fD.unit.convers ) {
							fD.toggle = '';
							dialog.setMsg( 'hint', 'noUnitCalc', e, 4500 );
						}
						metaReaction.toggle.setStatus( fD );
					}
					else if ( t == 'unit' ) {
						fD.unit.convers = _v;
						calcUnit( e, fD );
					}
				};
				metaReaction.toggle.setStatus( fD );
			}
		}
		, setValue: function( e ) {
		var
			fD = getFormDef( e )
			, eA = $Q( $( e.id + cssNS.modal ), 'input:checked', 1 )
			;
			if ( eA && eA.value != fD.unit.name ) {
				metaReaction.unit.init( e, fD, eA.value );
				if ( fD.control )
					fD.control( 'unit', eA.value );
			}
		}
		, getValue: function( e ) {
		var
			eA = $Q( $( e.id + cssNS.modal ), 'input[value^="'
				+ ( getFormDef( e ).unit.name ) + '"]', 1 )
			;
			if ( eA )
				eA.checked = true;
			metaReaction.singleSelect.getValue( e );
		}
		, getContent: getAllUnitList
		, doReaction: function( e, fD ) {
			return {
				onchange: function( eC, v ) {
					eC.value = v;
					calcUnit( e, fD );
					return v;
				}
			};
		}
	}; 
})();
metaReaction.isounits = metaReaction.unit;
//@ end of metaReaction.unit

/*****
 *
 *		functions to control vat interaction
 *		@ 
 *		@ 
 *
 *****/
metaReaction.vat = ( function(){
var
	calcVat = function( eC, fD ) { metaReaction.toggle.setValue( fD, fD.vat );	}
	;
	return {
		init: function( e, fD ) {
		var
			eS = $( fD.subs.SELECT.id )
			;
			fD.subs.FIX.check.max = fD.subs.PFIX.check.max = 99;
			fD.toggle = '';
			fD.vat = { base: 1., convers: 1. };
			metaReaction.toggle.setStatus( fD );
			fD.control = function() { metaReaction.toggle.setStatus( fD ) };
			_setConvers();
			$E( eS, 'OC', function _vatChange() {
				_setConvers();
				setFocus( $( fD.subs[ fD.toggle + 'INT' ].id ));
			});

			function _setConvers() {
			var
				_v = eS.value
				;
				if ( _v != 'ownInput' ) {	// clear leading NaN for 'correct' parseFloat
					_v = parseFloat( _v.replace( /^[\D]*/, '' ).replace( /,/, '.' ));
					if ( isNaN( _v )) {
						dialog.setMsg( 'error', 'vatError', e, 4500 );
						fD.vat.convers = 1.;
					}
					else 
						fD.vat.convers = fD.type == 'net' ? ( _v + 100 ) / 100.
						: ( 100 + _v ) / _v;
					calcVat( $( fD.first ), fD );
				}
				else
					setTimeout( _setConvers, 500 );
			}
		}
		, setValue: function( e, v ) {
		var
			fD = getFormDef( e )
			;
			calcVat( $( fD.first ), fD );
		}
		, doReaction: function( e, fD ) {
			return {
				onchange: function( eC, v ) {
					eC.value = v;
					calcVat( e, fD );
					return v;
				}
			};
		}
	}; 
})();
metaReaction.net = metaReaction.vat;
//@ end of metaReaction.vat

/*****
 *
 *		functions to control salesident interaction
 *		@ 
 *		@ 
 *
 *****/
metaReaction.salesident = ( function(){ // 546789387646434
var
	regList = {
		'not europe':[/[A-Z0-9]{0,15}/i, 15 ]
		, BE:[/\d{10,10}/i, 10 ]
		, BG:[/\d{9,10}/i, 10 ]
		, DK:[/\d{8,8}/i, 8 ]
		, DE:[/\d{9,9}/i, 9 ]
		, EE:[/\d{9,9}/i, 9 ]
		, FI:[/\d{8,8}/i, 8 ]
		, FR:[/[0-9A-Z]{2,2}\d{9,9}/i, 11 ]
		, EL:[/\d{9,9}/i, 9 ]
		, IE:[/\d[0-9A-Z]\d{5,5}[A-Z][A-I]/i, 9 ]
		, IT:[/\d{11,11}/i, 11 ]
		, HR:[/\d{11,11}/i, 11 ]
		, LV:[/\d{11,11}/i, 11 ]
		, LT:[/\d{9,9}(\d{3,3})?/i, 12 ]
		, LU:[/\d{8,8}/i, 8 ]
		, MT:[/\d{8,8}/i, 8 ]
		, NL:[/[0-9A-Z\*\+]{10,10}\d{2,2}/i, 12 ]
		, ATU:[/\d{8,8}/i, 8 ]
		, PL:[/\d{10,10}/i, 10 ]
		, PT:[/\d{9,9}/i, 9 ]
		, RO:[/[1-9]\d{5,9}/i, 10 ]
		, SE:[/\d{10,10}01/i, 12 ]
		, SK:[/\d{10,10}/i, 10 ]
		, SI:[/\d{8,8}/i, 8 ]
		, ES:[/[0-9A-Z]\d{7,7}[0-9A-Z]/i, 9 ]
		, CZ:[/\d{8,10}/i, 10 ]
		, HU:[/\d{8,8}/i, 8 ]
		, GB:[/(\d{9,9}(\d{3,3})?)|((GD|HA)\d{3,3})/i, 14 ]
		, CY:[/\d{8,8}[A-Z]/i, 9 ]
	}
	, setRegs = function( fD, k ) {
	var
		hasAZ = /A/.test( regList[ k ][ 0 ].source )
		;
		fD.regexp = new RegExp( '^' + k + (regList[ k ][ 0 ]+'').slice( 1, -2 )
		+ '$' );
		fD.subs.PATTERN.check.regexp = regList[ k ][ 0 ];
		fD.subs.PATTERN.check.charRegexp = k == 'NL' ? /[0-9A-Z\*\+]+/gi
		: hasAZ ? /[0-9A-Z]+/gi : /[0-9]+/g;
		fD.subs.PATTERN.check.maxCount = regList[ k ][ 1 ];
		fD.subs.PATTERN.check.upper = 1;
		$( fD.subs.PATTERN.id ).title = ( lang.allowedChars[ '0-9' ]
		+ ( hasAZ ? lang.allowedChars[ 'a-z' ] : '' )
		+ ( k == 'NL' ? '*+  ' : '' )).slice( 0, -2 );
	}
	;
	return {
		init: function( e, fD ) {
		var
			eS = $( fD.subs.SELECT.id )
			, opt = Object.keys( regList )
			;
			$E( eS, 'OC', function _salesChange() { setRegs( fD, this.value ); });
			eS.innerHTML = '<option>' + opt.join( '</option><option>' )
			+ '</option>';
			//cover.types[ 'salesident' ][ 0 ] = '(['+ opt.join( '|' ) +'])';
			eS.value = regList[ conf.country ] ? conf.country : 'DE';
			setRegs( fD, eS.value );
		}
	}; 
})();
//@ end of metaReaction.salesident

/*****
 *
 *		functions to create iban control
 *		@ 
 *		@ 
 * 
 *****/
metaReaction.iban = ( function() {
var
	defaultIban = [ 'c', 15, 'c', 20, /[A-Z]{2}\d\d[A-Z0-9]{10,30}/ ]
	, lastWorld = false
	, ibanRegex = false
	, initCover = function( fD, w ) {
		w = w || defaultIban;
		ibanRegex = new RegExp( w[ 4 ]);
		for ( var s in fD.subs ) {
			if ( s.slice( 0, 4 ) == 'IBAN' ) {
			var
				e = $( fD.subs[ s ].id )
				, t = s == 'IBAN' ? 'n' : w[ s == 'IBAN1' ? 0 : 2 ]
				, m = s == 'IBAN' ? 2 : w[ s == 'IBAN1' ? 1 : 3 ]
				;
				fD.subs[ s ].check.charRegexp = new RegExp(
					'[' + ( t == 'a' ? ' A-Z' : t == 'n' ? ' 0-9' : ' A-Z0-9' ) + ']+', 'gi'
				);
				fD.subs[ s ].check.maxCount = m;
				fD.subs[ s ].check.max = repeat( '9', m );
				e.title = (( t == 'a' || t == 'c' ? lang.allowedChars[ 'a-z' ] : '' )
				+ ( t == 'n' || t == 'c' ? lang.allowedChars[ '0-9' ] : '' )).slice( 0, -2 );
			}
			//else
				//fD.subs[ s ].check.jump = 1;
		}
	}
	, ibanCheck = function( iban ) {
	//@ this follows form rule on https://de.wikipedia.org/wiki/IBAN
	//@ for testing: DE68 2105 0170 0012 3456 78  DE23 2004 1133 0008 3033 0700 + 00
	//@ Greenpeace: DE49 43060967 0000033401, B&B Computer: DE73500694550000021792
		iban = iban.toUpperCase();
		// the longest is St. Lucia with 32 places/digits
		iban = ( iban.slice( 4 ) + iban.slice( 0, 4 )).replace( /[A-Z]/g
			, function( c ) {	// charcode of 'A' == 65 is first position of alphabet + 9 
				return c.charCodeAt( 0 ) - 55;
			}
		);
		// MAX_SAFE_INTEGER / 10 = 900719925474099 :: only 15 digits => special mod function
		return _modulo( iban, 97 ) == 1;

		function _modulo( divident, divisor ) {
		//@ source https://stackoverflow.com/questions/929910/modulo-in-javascript-large-number
		//@ and http://www.devx.com/tips/Tip/39012
	   var
	    	partLength = 10
	    	;
		    while ( divident.length > partLength ) {
		        var part = divident.substring( 0, partLength );
		        divident = ( part % divisor ) +  divident.substring( partLength );
		    }
		    return divident % divisor;
		}
	}
	, msgList = {}
	, getBLZ = function( e, fD, b ) {
	//@ PARAM country
	var
		url = 'uLblz.' + fD.world.toLowerCase() + '.json.css'
		;
		b = b || loadJSONCSS( url, function( b ) {
			if ( D.contains( e )) setExpand( e, fD, b.banks );
		});
		if ( extern[ url ] && extern[ url ] == -2 ) {
			if ( !msgList[ c ] )
				msgList[ c ] = 0;
			if ( msgList[ c ]++ < 3 )	// don't tell too often... ;-)
				dialog.setMsg( 'hint', 'noCountrydata', e, 4500 );
			b = -1;
		}
		return !b || b < 0 ? false : b.banks;
	}
	, setExpand = function( e, fD, b ) {
	var
		_e = _findExpand( '.BIC' )
		, blz
		;
		if ( _e ) {
			b = b || getBLZ( e, fD, false );
			blz = $( fD.subs.IBAN1.id ).value;
			if ( b && ( blz = b.filter( function( v ){ return v[ 0 ] == blz; }))) {
			// structure of bank list: ["10000000", "BBk", "Berlin", "MARKDEF1100"]
			// , but there are doubles of blz and bic with different towns
				if ( blz[ 0 ]) {
					_e.value = blz[ 0 ][ 3 ];
					if ( _e = _findExpand( '.institution' )) {
						b = blz[ 0 ][ 1 ] + '(';
						for ( var i = 0; i < blz.length; i++ )
							b += blz[ i ][ 2 ] + ', ';
						_e.value = b.slice( 0, -2 ) + ')'
					}
				}
			}
		}

		function _findExpand( t ) {
		var
			_e = D.getElementsByName( e.name.split( '.' )[ 0 ] + t )
			;
			return _e && _e[ 0 ] ? _e[ 0 ] : false;
		}
	}
	;
	return {
		init: function( e, fD, w ) {
		var
			w = w || loadJSONCSS( 'uLworld.json.css', function( w ) {
				if ( D.contains( e )) metaReaction.iban.init( e, fD, w );
			});
			fD.world = oldWorld = conf.country;
			metaReaction.world.init( e, fD, fD.world );
			fD.getSub( fD.first ).check.doReaction = metaReaction.world.doReaction( e, fD );
			if ( !!w ) {
				initCover( fD, w.countryData[ fD.world ][ 7 ]);
				if ( e.value.length )
					doEvent.setInput( $( fD.subs.IBAN.id ), e.id );
			}
			else
				initCover( fD, false );
			fD.control = function( t, v ) {
				initCover( fD, w.countryData[ v ][ 7 ]);
				setFocus( $( fD.subs.IBAN.id ), 200 );
			};
		}
		, doReaction: function( e, fD ) {
		var
			sC
			;
			return {
				init: function( eC ) {
				var
					cD = loadJSONCSS( 'uLworld.json.css' )
					;
					sC = fD.getSub( eC.id ).check;
					if ( !!cD && lastWorld != fD.world ) {
						lastWorld = fD.world;
						initCover( fD, cD.countryData[ fD.world ][ 7 ]);
					}
				}
				, onblur: function( eC, vFull ) {
				var
					v = eC.value.trim()
					, l = _clear( v ).length
					;
					eC.value = ibanRegex == defaultIban[ 4 ] || l >= sC.maxCount ? v
					: '^000000000000000000000'.slice( l - sC.maxCount ) + v;
					if ( vFull ) {
						vFull = _clear( vFull );
						if ( !ibanRegex.test( vFull ))
							dialog.setMsg( 'error', 'formatError', e, 5500, eC );
						else if( !ibanCheck( vFull ))
							dialog.setMsg( 'hint', 'ibanCheck', e, 4500, eC );
						else 
							setExpand( e, fD, false );
					}
					else if ( eC.id == fD.subs.IBAN1.id && $( fD.subs.IBAN1.id ).value.length > 5 )
						setExpand( e, fD, false );

					function _clear( _v ) { return _v.replace( /\s/g, '' ); }
				}
			}
		}
	}; 
})();
//@ end of metaReaction.iban

/*****
 *
 *		functions to create list control
 *		@ 
 *		@ 
 * 
 *****/
metaReaction.list = ( function() {
var							// short'n signs for conformity with user expectations
	sL = []
	, sShow = 20
	, multiList = false
	, tempType = false
	, lastUrl = false
	, lastValue = false
	, findValue = function( eC, v, sC ) {
	var
		isSelected = /\u2006$/.test( v ) ? v.slice( 0, -1 ) : false
		, index = []
		, f
		;
		if ( sL.length ) {
			if ( isSelected ) {
				v = eC.value = isSelected;
				if ( sC.isSelected == isSelected )
					return sC.value;
				else
					sC.isSelected = isSelected;
			}
			if ( v.length 
			&& ( f = _getMatch( new RegExp( useLib.WIKI.mask(( v.length < 3 ? '^' : '' ) + v 
			).replace( /\\.|\s|-/g, '(\\\\.| |-)'), 'i' ))).length ) {
				if ( !isSelected && f.length <= sShow ) {
					for ( var j = 0; j < index.length; j++ )
						if ( multiList )
							f[ j ] = extern[ lastUrl ].list[ index[ j ]].join( '::' );
						else
							f[ j ] += '\u2006';	// append unvisible char to ident selection
					setListSelect( eC.id, f );	
				}
				else {
					setListSelect( eC.id, [ '\u2211 '+ f.length +' > '+ sShow ]);	// clears if length == 0
				}		
			}
			else 
				setListSelect( eC.id, []);		// clears if length == 0			
		}
		return index[ 0 ] || 0;

		function _getMatch( reg ) {
			return sL.filter( function( _v, _i ) {
					if ( _v = reg.test( _v ))
						index.push( _i );
					return _v;
				}
			);
		}
	}
	, fillMultiList = function( i ) {
		if ( multiList && extern[ lastUrl ].idList )
			for ( var j = 1, l = extern[ lastUrl ].list; j < l[ 0 ].length; j++ )
				$( extern[ lastUrl ].idList[ j ]).value = l[ i ][ j ];
	}
	; 
	return {
		init: function( e, fD ) {
			if ( fD.data && fD.data.length )		// if no data no completer...
				$CN.add( $P( e ), cssNS[ fD.data.indexOf( '.tmp' ) == -1
				? 'completer' : 'searcher' ]);
		}
		, doReaction: function( e, fD ) {
		var
			sC
			;
			return {
				init: function( eC, list ) {
					if ( fD.data && fD.data.length ) {
						if ( lastUrl != fD.data && fD.data.indexOf( '.tmp' ) > -1 && lastValue
						&& eC.value.indexOf( lastValue ) !== 0 ) {
							// clear jsonp and store of extern data if temporary data and
							// requested value is not part of last requested value
							tempType = true;
							lastValue = false;
							delete extern[ fD.data ];
							DOM.removeNode( $( fD.data ));
							fD.data = fD.data.replace( /&value=[^&]*/, '' )
							+ '&value=' + eC.value;
						}
						else
							tempType = false;
						if ( lastUrl != fD.data )
							lastUrl = fD.data;
						sC = fD.getSub( eC.id ).check;
						list = list || loadJSONCSS( fD.data, function( l ) {
							if ( D.contains( e )) sC.doReaction.init( eC, l );
						});
						if( !!list ) {
							if ( multiList = typeCheck( 'Array', list.list[ 0 ]))
								// get first column
								sL = list.list.map( function( r ) { return r[ 0 ]; });
							else
								sL = list.list;
							sShow = list.show || 20;
							sC.min = 0;
							sC.max = sL.length - 1;
							sC.value = findValue( eC, eC.value, sC );
							// no restriction of chars: a special singular sign may find instantly 
							sC.charRegexp = false;
						}
					}
					return eC.value;
				}
				, onrotor: function( eC, v, step ) {
				//@ ATTENTION: inverts step because its a sorted alphabetic list!!
					if ( sL ) {
						v = $I( sC.value + ( step > 0 ? -1 : 1 ));
						v = v > sC.max ? sC.min : v < sC.min ? sC.max : v;
						sC.value = v;
						fillMultiList( v );
						return sL[ v ];
					}
					return eC.value;
				}
				, onchange: function( eC, v, onrotor ) {
					if ( sL && v.length && !onrotor ) {
						if ( tempType && v.indexOf( lastValue ) !== 0 )
							sC.doReaction.init( eC, false );
						else
							sC.value = findValue( eC, v, sC );
					}
					return eC.value;
				}
			};
		}
	}; 
})();
//@ end of metaReaction.list

/*****
 *
 *		functions to create towns control
 *		@ 
 *		@ 
 * 
 *****/
metaReaction.towns = ( function() {
var
	msgList = {}
	, eA = false
	, tA
	, getCountry = function( c, e, t ) {
	//@ PARAM country
	var
		url = 'uLtowns.' + c.toLowerCase() + '.json.css'
		;
		t = t || loadJSONCSS( url, function() {
			if ( D.contains( e )) getCountry( c, e, t );
		});
		if ( extern[ url ] && extern[ url ] == -2 ) {
			if ( !msgList[ c ] )
				msgList[ c ] = 0;
			if ( msgList[ c ]++ < 3 )	// don't tell too often... ;-)
				dialog.setMsg( 'hint', 'noCountrydata', e, 4500 );
			t = -1;
		}
		else if ( t && !t.postal ) {
		// create postal list with index of postal of country c
			t.postal = Array( t.town.length );
			t.postal.maxCount = 0;
			for( var i = 0; i < t.town.length; i++ ) {
				t.postal[ t.town[ i ][ 3 ]] = i;
				t.postal.maxCount = max( t.postal.maxCount, t.town[ i ][ 1 ].length );
			}
		}
		return t;
	}
	, findValue = function( tL, t, v ) {
	//@ PARAM townListOfCountry, typeOfValue, valueToFind
	//@ if there is already a input it is to search for it in the postal and the
	//@ town list (there may be more than one town per postal or more than one
	//@ postal per town...)
	var
		i = tL[ t ].length - 1
		;
		if ( v.length )
			i = tL[ t ].length && v.length ? _findValue( v, i, i ) : 0;
		while( compare( getValue( tL, t, i ).toLowerCase(), v.toLowerCase()) < 0 )
			i++;
		return i;

		function _findValue( v, i, l ) {
		//@ PARAM valueToFind, index, lengthOfStep
		//@ recursive binary search in the two dimensional array of townsOfCountry
			i = min( tL[ t ].length - 1, max( 0, i ));
			if ( l > 0 ) {
			var
				c = compare( getValue( tL, t, i ).toLowerCase(), v.toLowerCase())
				;
				i = _findValue( v, i - ( c < 0 ? -l : l ), $I( l / 2 ));
			}
			return i;
		}
	}
	, getValue = function ( tL, t, i ) {
	//@ PARAM townListOfCountry, typeOfValue, index
		return t == 'town' ? tL[ t ][ i ][ 0 ] : tL.town[ tL[ t ][ i ]][ 1 ];
	}
	, setValue = function( eSub, tL, t, i ) {
	//@ PARAM coverElementSub, townListOfCountry, typeOfValue, index
	//@ REMARK: if users stay on arrow the focus may jump on set value, therefore
	//@ start element is stored and focus set right on timeout...
		eA = eA || $( eSub.id );
		clearTimeout( tA );
		$( eSub.next ? eSub.next : eSub.before ).value = tL.town[ i ][ !!t ? 0 : 1 ];
		$( eSub.next ? eSub.id : eSub.before ).title = tL.town[ i ][ 2 ];
		tA = setTimeout( function(){ eA.focus(); eA = false; }, 250 );
		return tL.town[ i ][ t ] || '';
	}
	;
	return {
		init: function( e, fD ) {
			fD.world = conf.country;
			fD.doReaction = this.doReaction;
			fD.getSub( fD.first ).check.doReaction = metaReaction.world.doReaction( e, fD );
			metaReaction.world.init( e, fD, fD.world );
			fD.normRegexp = false;
		}
		, doReaction: function( e, fD ) {
		var
			eS
			, tL = false
			, iT
			;
			return {
				init: function( eC ) {
				var
					v = eC.value
					, r
					;
					eS = fD.getSub( eC.id );
					iT = eS.id == fD.subs.PATTERN.id ? 1 : 0;		// index in town array
					tL = getCountry( fD.world, e );
					if ( !tL )
						setTimeout( function(){	eS.check.doReaction.init( eC ) }, 400 );
					else if ( tL == -1 )
						tL = false;
					else {
						eS.check.value = v.length
						? findValue( tL, iT ? 'postal' : 'town', v ) : 0;
						eS.check.min = 0;
						eS.check.max = tL.town.length - 1;
						// depending on country postalcodes have different decimals...
						r = cover.pattern2Char( tL.charRegexp[ iT ]);
						eS.check.charRegexp = r.charRegexp;
						eC.title = r.chars;
						eC = fD.subs.PATTERN.check;
						eC.jump = 1;
						eC.minCount = eC.maxCount = tL.postal.maxCount;
						eC.regexp = eC.charRegexp = r.charRegexp;
					}
					return v;
				}
				, onrotor: function( eC, v, step ) {
				//@ ATTENTION: inverts step because its a sorted alphabetic list!!
				var
					sC = eS.check
					;
					if ( tL ) {
						v = $I( sC.value + ( !!iT ? -step : step > 0 ? -1 : 1 ));
						v = v > sC.max ? sC.min : v < sC.min ? sC.max : v;
						sC.value = v;
						return setValue( eS, tL, iT, !!iT ? tL.postal[ v ] : v );
					}
					return eC.value;
				}
				, onchange: function( eC, v, onrotor ) {
					if ( tL && v.length && !onrotor ) {
					var
						t = !!iT ? 'postal' : 'town'
						, i = findValue( tL, t, v )
						, r = new RegExp( '^' + v, 'i' )
						, a = []
						, ii
						;
						eC.value = v;
						eS.check.value = i;
						while ( i < tL.town.length && r.test( getValue( tL, t, i ))) {
							ii =  !!iT ? tL.postal[ i ] : i;
							a.push( tL.town[ ii ][ 1 ] + ' ' + tL.town[ ii ][ 0 ]);
							if ( a.length > 15 ) {	// to prevent too long lists...
								a = [];
								break;
							}
							i++;
						}
						// avoid set listSelect, if active value is already part of list
						setListSelect( fD.subs[ 'PATTERN' + ( iT ? '' : '1' ) ].id
							, a.indexOf(
								$( fD.subs.PATTERN.id ).value + ' '
								+ $( fD.subs.PATTERN1.id ).value
							) == -1 ? a : []
						);
					}
					else
						setListSelect( fD.subs.PATTERN.id, []); // clears if length == 0
					return v;
				}
			};
		}
	}; 
})();
//@ end of metaReaction.towns

/*****
 *
 *		functions to handle period control
 *		@ 
 *		@ 
 * 
 *****/
metaReaction.period = ( function() {
var
	setValue = function ( fDb, bep, v, t ) {
		if ( bep == 'period' ) {
			if ( t == 'date' )
				_setValue( 'DAY', $I( v / 86400000 ) + 1, true );	// day: 24 * 60 * 60 * 1000
			else
				_setTime( new Date( v ), true );
		}
		else {
			if ( t == 'date' )
				_setDate( $D.toISO( v ).split( '-' ), false);
			else
				_setTime( new Date( v ), false );
			}

		function _setValue( _t, v, p, _change ) {
		//@ overwrite only realy changed values and calculate them separate
		//@ REMARK: not via cover.setValue( fDb[ bep ], _t, v );!!
			if ( _t = $( fDb[ bep ].getId( _t + ( p ? 'S' : '' )))) {
				if ( _t.value != v ) {
					activeOnChange = false;
					//$A( _t, 'disabled', true ); // possible trick to avoid keybord on touch...
					cover.keyMsg( _t, 'Carry' );
					_t.value = v;
					//$A( _t, 'disabled', -1 );	// ... works without
					activeOnChange = true;
					if ( _change )
						dispatchEvent( _t, 'blur' );
				}
				return true;
			}
			return false;
		}

		function _setDate( v, p ) {
			if ( fDb[ bep ].subs.SMALLYEAR )
				v[ 0 ] = String( v[ 0 ]).slice( 2 );
			_setValue( 'YEAR', v[ 0 ], p );
			_setValue( 'MONTH', v[ 1 ], p );
			_setValue( 'DAY', v[ 2 ], p, true );
		}

		function _setTime( v, p ) {
			_setValue( 'SECOND', zeroInt( v % 60, 2 ), p );
			v /= 60;
			_setValue( 'MINUTE', zeroInt( v % 60, 2 ), p );
			v /= 60;
			_setValue( 'HOUR', zeroInt( v % 24, 2 ), p );
		}
	}
	, getValue = function( fDb, bep, t ) {
	//@ a lot of possible combinations have to be differed:
	//@ only date and/or time norm is needed to get value of new Date
	var
		v = 0
		;
		if ( bep == 'period' ) {
			if ( t == 'date' ) {
					v = _getValue( 'DAY', true );
					v = v > -1 ? ( v - 1 ) * 86400000 : false;	// day: 24 * 60 * 60 * 1000
				}
				else
					v = _getTime( true );
		}
		else {
			if ( t == 'date' )
				v = _getDate( false );
			else
				v = _getTime( false );
		}
		return v;

		function _getValue( _t, p, r ) {
		var
			_v = fDb[ bep ].getId( _t + ( p ? 'S' : '' ))	// !_v == seconds
			;
			return !_v ? 0 : _v && ( _v = $( _v )) && ( _v = _v.value )
			? $I( _v ) : r;
		}

		function _getDate( p ) {
		var
			y = _getValue( 'YEAR', p, -1 ) 
			, m = _getValue( 'MONTH', p, -1 )
			, d = _getValue( 'DAY', p, -1 )
			;
			if ( fDb[ bep ].subs.SMALLYEAR ) {
				y += 2000;
				y -= y > $D.y() + conf.smallYearFuture ? 100 : 0;
			}
			return y > -1 && m > -1 && d > -1 ? $D([ y, m, d ]).getTime() : false;
		}

		function _getTime( p ) {
		//@ hour: 3600 = 60 * 60, minute: 60, second
			h = _getValue( 'HOUR', p, -1 ) 
			, m = _getValue( 'MINUTE', p, -1 )
			, s = _getValue( 'SECOND', p, -1 )
			;
			return h > -1 && m > -1 && s > -1 ? h * 3600 + m * 60 + s : false;
		}
	}
	, setAllValues = function( eC, fD, checkIt, isPeriod=false ) {
	//@ checks and sets the different cases of changes in depending cover fields
	//@ date and time treated separate to minimize calculations
	//@ REMARK: value of origin is set on blur and not on each change action!
	var
		fDb = fD.begin
		;
		_calcCheck( 'date' );
		_calcCheck( 'time' );
		return eC.value;

		function _calcCheck( t ) {
		//@ REMARK: periodCheck decides between date and time
		//@ 	period.fixed is steered by the user via the toggle button
			if ( fDb.periodCheck[ t ]) {
				if ( fDb.end === fD )
					__calcCheck( fDb.period.fixed ? 'period' : 'begin', 'end', t );
				else
					__calcCheck( 'begin', 'period', t );
				/*
				//@ alternative with inversion of calc logic on fDb.period.fixed
				//@ may be useful for some work tasks, but failed on usability tests
				if ( fDb.period.fixed && ( fDb.period === fD || fDb.end === fD ))
					__calcCheck( 'period', 'end', t );
				else if (( fDb.period.fixed && fDb.begin === fD ) || fDb.end === fD )
					__calcCheck( 'begin', 'end', t );
				else if ( fDb.begin === fD || fDb.period === fD )
					__calcCheck( 'begin', 'period', t );
				*/
			}

			function __calcCheck( bp, pe, t ) {
			//@ REMARK: test on === false because Date values may be zero 
			var
				b = bp == 'begin' ? getValue( fDb, 'begin', t ) : false
				, p = bp == 'period' || pe == 'period' ? getValue( fDb, 'period', t ) : false
				, e = pe == 'end' ? getValue( fDb, 'end', t ) : false
				, corr = b && p !== false ? 'end' : b && e ? 'period' : 'begin'
				, period = fDb.calendar.period || {}
				;
				b = b || ( e !== false && p !== false ? e - p : false );
				e = e || ( b !== false && p !== false ? b + p : false );
				p = p !== false ? p : e !== false && b !== false ? e - b : false;
				if ( p === false && b && checkIt ) {
				// not yet fully initializied, but start or end value given?
					corr = 'end';
					p = ( e || b ) - b;
					if ( $I( period.begin - 1 ) > 0 && p / 86400000 < period.begin )
						p = ( period.begin - 1 ) * 86400000;
					setValue( fDb, 'period', p, t );
				}
				if ( b !== false && e !== false && p !== false ) {
					if ( e && b && __checkLimit( b, e, p, t, period )) {
						setValue( fDb, corr, corr == 'begin' ? b : corr == 'end' ? e : p, t );
					}
					else {
						corr = fDb.begin === fD ? 'begin' : fDb.end === fD ? 'end' : 'period';
						setValue( fDb, corr
							, corr == 'begin' ? getValue( fDb, 'end', t )
							- getValue( fDb, 'period', t )
							: corr == 'period' ? getValue( fDb, 'end', t )
							- getValue( fDb, 'begin', t )
							: getValue( fDb, 'begin', t ) + getValue( fDb, 'period', t )
							, t
						);
					}
				}
			}

			function __checkLimit( _b, _e, _p, t, _period ) {
			var
				r = true
				, _fD
				, _eC
				;
				if ( t == 'date' ) {
					_fD = fDb.period !== fD ? fD : fDb.period.fixed ? fDb.end : fDb.begin;
					_eC = $( _fD.getId( 'DAY' ) || _fD.first );
					r = metaReaction.date.checkLimits( _fD, _eC, _b, _e, checkIt );
					_eC = fDb.period.getSub( fDb.period.getId( 'DAYS' ));
					_p = $I( _p / 86400000 ) + 1;
					if (( _eC && ( _p < _eC.check.min || _p > _eC.check.max ))
					|| ( _period.end && ( _p < _period.begin || _p > _period.end ))) {
						_eC = $( _eC.id );
						cover.keyMsg( _eC, 'Error' );
						if ( checkIt )
							dialog.setMsg( 'error', 'daysPeriod',  $( _fD.id ), 4500 );
						r = false;
					}
					return r;		// init given values even if disabled
				}
				return _b >= 0 && _p >= 0 && _e >= 0;
			}
		}
	}
	, init = function( e, fD ) {
	var
		fDb = fD.begin || getFormDef( $( fD.data )) || {} // already initialized or given in data?
		;
		fD.begin = fDb;
		if ( !fDb.period ) {
			fDb.period = fD;
			fDb.period.fixed = !!fDb.period.fixed;
			metaReaction.date.setLimits( fD, 'period' );
		}
		if ( fDb.begin && fDb.end && !!fDb.end.subs && !fDb.periodCheck )
		//@ check if all nessesary is known and not already initialized
		//@ REMARK: for some reinitialization resons it must run in a closeure
			setTimeout( _initPeriod( e, fDb ), 250 );

		function _initPeriod( _e, _fDb ) {
			return function() {
				_fDb.periodCheck = {};
				__init( _fDb.begin );
				__init( _fDb.end );
				__init( _fDb.period, true );
				setAllValues( _e, _fDb.begin, true );
			};

			function __init( _fD, isPeriod ) {
			//@ onchange only fires on blur of the element, its need to watch
			//@ values via own interaction methods (checkValue in cover)
			var
				s = _fD.subs
				, t = _fD.type
				;
				for ( t in s )
					s[ t ].check.doReaction = metaReaction.period.doReaction( $( _fD.id )
					, _fD );
				if ( !isPeriod ) {
					fDb.periodCheck.date = s.DAY ? 1 : 0;
					fDb.periodCheck.time = s.HOUR || s.MINUTE || s.SECOND ? 1 : 0;
				}
			}
		}
	}
	;
	return {
		init: init
		, doReaction: function( e, fD ) {
			return {
				init: function( eC ) {
				var
					fDb = fD.begin
					, t = fD == fDb.begin ? 'begin' : fD == fDb.end ? 'end' : false
					;
					if ( t ) {	// to preset 'period' will inhibit to clear the conneted values
						if ( fDb.periodCheck[ 'date' ] && !subValue( fDb[ t ], 'DAY' ))
							setValue( fDb, t, $D().getTime(), 'date' );
						if ( fDb.periodCheck[ 'time' ] && !getValue( fDb, 'begin', 'time' ))
							setValue( fDb, t, _getTime(), 'time' );
					}
					setAllValues( eC, fD, false );

					function _getTime() {
					var
						t = new Date()
						;
						return t.getHours() * 3600 + t.getMinutes() * 60;
					}
				}
				, onchange: function( eC, v, onrotor ) {
					return onrotor ? setAllValues( eC, fD, false ) : v;
				}
				, onblur: function( eC, vFull ) {
				var
					t = fD.getSub( eC.id ).type
					;
					t = t == 'DAYS' || t == 'PDAYS';
					if ( vFull || t )
						setAllValues( eC, fD, true, t );
				}
			};
		}
		, setValue: function( e, v ) {
		var
			fD = getFormDef( e )
			;
			setAllValues( e, fD, true );
		}
	}; 
})();
//@ end of metaReaction.period

/*****
 *
 *		functions for time control
 *		@ 
 *		@ 
 * 
 *****/
metaReaction.time = {
	init: function( e, fD ) {
	var
		fDb = fD
		;
		if ( fD.end === true ) {
			fDb = getFormDef( $( fD.data ));
			fDb.end = fD;
			fD.begin = fDb;
		}
		else
			fDb.begin = fD;
		if ( fD.getId( 'HOURS' ) || fD.getId( 'MINUTES' ))
			metaReaction.period.init( e, fD );
		if( fDb.begin && fDb.end && fDb.period )
			metaReaction.period.init( e, fDb.period );
	}
	, getContent: function ( e, type ) {
	// timepicker wording i. e. german: "Stunden,Nacht,Vormittag,Nachmittag,Abend,Minuten"
	var
		id = e.id + cssNS.modal
		, terms = lang.timepicker.split( ',' )
		, getRadio = function( r, v, t, n ) {
		//@ append an array of radio buttons with given value list to row
			v = v.split( ' ' );
			for ( var i = 0; i < v.length; i++ )
				r.push(
					getStruct.radioCheck( false, null, v[ i ], 'uL' + v[ i ] + t, n, v[ i ])
				);
			return r;
		}
		, dialog = [
			[ 'fieldset', {}].concat(
				getRadio( [[ 'legend', {}, terms[ 0 ]]
					,[ 'span', { CN: cssNS.linebreak }, terms[ 1 ]]]
					,'01 02 03 04 05 06', 0, "uLtimeHOUR" )
				, getRadio( [[ 'span', { CN:cssNS.linebreak }, terms[ 2 ]]]
					, '07 08 09 10 11 12', 1, "uLtimeHOUR" )
				, getRadio( [[ 'span', { CN: cssNS.linebreak }, terms[ 3 ]]]
					, '13 14 15 16 17 18', 2, "uLtimeHOUR" )
				, getRadio( [[ 'span', { CN: cssNS.linebreak }, terms[ 4 ]]]
					, '19 20 21 22 23 24', 3, "uLtimeHOUR" )
			)
			, [ 'div', { CN: cssNS.gap }]	// gap between fieldsets for colon separator
			, [ 'fieldset', {}].concat( 
				getRadio( [[ 'legend', {}, terms[ 5 ]]]
					, '00 05 10 15 20 25 30 35 40 45 50 55', 4, "uLtimeMINUTE" )
			)
			, useLib.getStruct.buttonRow()
		]
		;
		return dialog;
	}
	, getValue: function( e ) {
	var
		fD = getFormDef( e )
		;
		_set( 'HOUR' );
		_set( 'MINUTE' );
		if ( fD.subs.SECOND && ( e = $( fD.subs.SECOND.id )).value.length === 0 )
			e.value = '00';

		function _set( t ) {
		var
			n = $( fD.subs[ t ].id ).value
			, l = $Q( $( e.id + cssNS.modal ), '[name=uLtime' + t + ']' );
			;
			if ( t == 'HOUR' )
				n = ( $I( n ) || ( new Date()).getHours()) - 1;
			else
				n = Math.floor( $I( n ) / 5 );
			l[ n ].checked = true;
		}
	}
	, setValue: function( e ) {
		cover.setValue( getFormDef( e ), 'time', _get( 'HOUR' ) + ':' + _get( 'MINUTE' ));

		function _get( t ) {
			return ( $Q( $( e.id + cssNS.modal ), '[name=uLtime' + t + ']:checked', 1 ) || {} ).value || '';
		}
	}
};
//@ end of metaReaction.time

/*****
 *
 *		functions for date control
 *		@ 
 *		@ 
 * 
 *****/
metaReaction.date = {
	init: function ( e, fD ) {
	var
		fDb = fD
		;
		fDb.begin = fD;
		if ( fD.end === true ) {
			fDb = getFormDef( $( fD.data ));
			fDb.end = fD;
			fD.begin = fDb;
			fD.calendar = fDb.calendar;
			metaReaction.date.setLimits( fD, 'end' );
		}
		else {
			metaReaction.calendar.normCal( fD );
			metaReaction.date.setLimits( fD, 'begin' );
		}
		if ( fD.getId( 'DAYS' ) || fD.getId( 'HOURS' ))
			metaReaction.period.init( e, fD );
		if( fDb.begin && fDb.end && fDb.period )
			metaReaction.period.init( e, fDb.period );
	}
	, setValue: function( e, cData ) {
	var
		fD = getFormDef( e )
		;
		fD = fD.begin || fD;
		activeOnChange = false;
		if ( fD.period && cData.period )
			$( fD.getId( 'DAYS' ) || fD.period.first ).value = String( cData.period );
		if ( this.checkLimits( fD, $( fD.first ), cData.begin, cData.end, true )) {
			cover.setValue( fD.begin, 'date', cData.begin );
			if ( fD.end )
				cover.setValue( fD.end, 'date', cData.end );
			activeOnChange = true;
		}
		else
			return false;

	}
	, getValue: function( e ) {
	var
		fD = getFormDef( e )
		;
		fD = fD.begin || fD;
		e = cover.getValue( fD.begin, 'date', 'norm' );
		return {
			calendar: fD.calendar
			, begin: e
			, end: fD.end ? cover.getValue( fD.end, 'date', 'norm' ) : e
		};
	}
	, setLimits: function ( fD, type ) {
	//@ REMARK: failure tolerance for limits given as date or only as year
	var
		cal = fD.calendar || fD.begin.calendar || { disabled:{}, period: { begin: 1 }}
		, sC
		;
		if ( type == 'period' ) {
			cal.period = {
				begin: cal.period.begin || 1
				, end: cal.period.end || 365
			};
			if ( sC = fD.getSub( fD.getId( 'DAYS' )))
				_setMinMax( cal.period.begin, cal.period.end );
		}
		else {
			fD.min = fD.min && type == 'begin' ? fD.min : fD.end && fD.end.min
			? fD.end.min : false;
			fD.max = fD.max && type == 'end' ? fD.max : fD.begin && fD.begin.max
			? fD.end.max : false;
			if ( fD.min )
				cal.disabled.min = { begin: $D( '100-01-01' ), end: $D( fD.min )};
			if ( fD.max )
				cal.disabled.max = { begin: $D( fD.max ), end: $D( '3100-01-01' )};
			if ( sC = fD.getSub( fD.getId( 'YEAR' ))) {
				_setMinMax( fD.min, fD.max );
				if( fD.subs.SMALLYEAR ) {
					sC.max -= ( sC.max > 2000 ? 2000 : 1900 );
					sC.min -= ( sC.min > 2000 ? 2000 : 1900 );
				}
			}
		}
		( fD.begin || fD ).calendar = cal;

		function _setMinMax( _min, _max ) {
			sC = sC.check;
			sC.min = _min || sC.min;
			sC.max = _max || sC.max;
		}
	}
	, checkLimits: function( fD, eC, vB, vE, info ) {
	var
		cal = fD.calendar || fD.begin.calendar
		;
		if ( cal ) {
			vE = vE || vB;
			if ( fD.subs.WEEK ) {
				vB = $D.getWeekDate( vB, cover.getYear( fD ));
				vE = [ vB, $D.add( vB, 'd', 1 )];
			}
			else {
				vB = $D( vB );
				vE = $D( vE );
			}
			if ( !_checkLimit( 'Disabled' ))
				return false;
			_checkLimit( 'Blocked' );
		}
		return true;

		function _checkLimit( t ) {	// typeOfPeriods
			if ( !_checkType( fD, t.toLowerCase(), vB, vE )) {
				cover.keyMsg( eC, t == 'Disabled' ? 'Error' : 'Hint' );
				if ( info )
					dialog.setMsg( t == 'Disabled' ? 'error' : 'hint', 'date' + t, 0, 4500 );
				return false;
			}
			return true;
		}

		function _checkType( fD, t, _vB, _vE ) {
			for ( var n in cal[ t ]) {
			var
				isD = typeCheck( 'Date', cal[ t ][ n ].begin ) ? false
				: Object.keys( langConf.en.dayNames )
				, b = cal[ t ][ n ].begin
				, e = cal[ t ][ n ].end
				, d
				;
				if ( isD ) {
					b = isD.indexOf( b );
					e = isD.indexOf( e );
				}
				for ( var _b = _vB; _b <= _vE; _b = $D.add( _b, 'd', 1 )) {
					if ( !isD && _b >= b && _vB <= e )
						return false;
					else if ( isD ) {
						d = _b.getDay();
						d = d ? d : 7;
						if ( d >= b && d <= e )
							return false;
					}
				}
			}
			return true;

			function _setSelectDays( b, e, c, t ) {
			var
				bN = Object.keys( langConf.en.dayNames )
				,lN = Object.keys( lang.dayNames )
				;
				for ( var i = bN.indexOf( b ), e = bN.indexOf( e ), l; i > -1 && i <= e; i++ ) {
					// find all cells with day as header
					l = $Q( $( activeID ), 'td[headers=' + lN[ i ] + ']' );
					for ( var j = 0; j < l.length; j++ )
						if ( l[ j ].innerHTML.length )
							setDateClass( l[ j ], cssNS[ c ], false, t )
				}
			}
		}
	}
	, doReaction: function( e, fD ) {
	//@ this is for checking the deadline limits off the hole date value
	//@ REMARK: the checks on direct interaction only secure the limits off each cover part
		return {
			onblur: function( eC, vFull ) {
				if ( vFull )
					metaReaction.date.checkLimits( fD, eC, vFull, vFull, true );
			}
		};
	}
};
//@ end of metaReaction.date

/*****
 *
 *		functions to handle date and to create three month calendar
 *		@ There are two formats for dates: str := yyyy-mm-dd and the javascript
 *		@ internal new Date().
 *		@ 
 * 
 *****/
metaReaction.week = {
	init: function( e, fD ) {
		metaReaction.calendar.normCal( fD, false );
		metaReaction.date.setLimits( fD, 'week' );
		setFree( fD.id, 0, lang.isWeek[ 0 ] );
		if ( !subValue( fD, 'YEAR' ))
			cover.setValue( fD, 'YEAR', $D.y());
	}
	, setValue: function( e, cData ) {
	var
		fD = getFormDef( e )
		;
		activeOnChange = false;
		cover.setValue( fD, 'WEEK', cData.week );
		cover.setValue( fD, 'YEAR', $D.y( cData.begin ));
		activeOnChange = true;
	}
	, getValue: function( e ) {
	var
		fD = getFormDef( e )
		, cData = { calendar: fD.calendar }
		, w = subValue( fD, 'WEEK' ) || $D.getWeekNo( $D())
		, d = subValue( fD, 'YEAR' )
		;
 		d = $D.getWeekDate( w, d );
		cData.week = w || -1;
		cData.begin = d ? $D.toISO( d ) : 0;
		cData.end = d ? $D.toISO( $D.add( cData.begin, 'd', 6 )) : 0;
		return cData;
	}
	, doReaction: metaReaction.date.doReaction
};
//@ end of metaReaction.week

metaReaction.calendar = ( function() {
var
	setDateClass = function( e, c, clear, t ) {
	//@ sets or clears a specific className on calendar field (if exists) and may be a given title
		if ( e ) {
			$CN[ clear ? 'remove' : 'add' ]( e, c );
			if ( !!t && t.length ) {
				if ( clear )
					$A( e, 'title', -1 );
				else {
					clear = $A( e, 'title' );
					$A( e, 'title', ( clear ? clear + '\n' : '' ) + t );
				}
			}
		}
		return e;
	}
	, setSelect = function ( begin, end, cN, clear, t ) {
	//@ sets or clears the date range between begin and end to className and t(itle)
		if ( !!begin ) {
      	begin = begin < startDate ? startDate : begin;
      	end = end > endDate ? endDate : end;
			for ( var e; end >= begin; end = $D.add( end, 'd', -1 )) {
				e = $( 'uL-' + $D.toISO( end ));
				setDateClass( e, cN || cssNS.selected, clear, t );
			}
		}
	}
	, setSelectedDays = function( b, e ) {
		cData.period = b && e ? $I(( e - b ) / 86400000 ) + 1 : 0;
		$( 'uL-days' ).innerHTML = cData.period;
	}
	, week = function( w, y ) {
	//@ comes from week select and calcs and marks begin (to end) off week
		w = $D.getWeekDate( w, y );
		analyze( w, 'start' );
		analyze( w, 'stop' );
	}
	, analyze = function( eC, state ) {
	var
		a = !!!eC ? select.a : !eC.id ? eC		// from week else
		: $D( eC.id.slice( 3 ))						// cut the leading 'uL-'
		, n
		;
		if ( !$CN.contains( eC, cssNS.disabled )) {		// no selection on disabled days
			if ( state == 'start') {
				select = {
					b: cData.begin.length ? $D( cData.begin ) : 0
					, e: cData.end.length ? $D( cData.end ) : 0
					, a: a
					, click: true
				};
			}
			else if ( !!select ) {
				if ( select.click || a.getTime() != select.a.getTime()) {
					if ( cData.week ) {
						cData.week = $D.getWeekNo( a );
						a = $D.getWeekday( a, 1 );
						n = { b: a, e: $D.add( a, 'd', 6 )};
						select.a = a;
					}
					else if ( !select.b || !cData.periodmax || cData.periodmax == 1 )
						n = { b: a, e: a };
					else
						n = _getPeriod();
					setSelect( select.b, select.e, false, true );	// clear old selection...
					setSelect( n.b, n.e );									// ...set new
					setSelectedDays( n.b, n.e );
					select.b = n.b;
					select.e = n.e;
				}
				if ( state == 'stop' ) { 
					cData.begin = select.b > select.e ? '' : $D.toISO( select.b );
					cData.end   = select.b > select.e ? '' : $D.toISO( select.e );
					select = false;
				}
			}
		}

		function _getPeriod() {		
		var
			b = select.b
			, e = select.e
			, s
			;
			select.a = a;
			select.click = false;
			if ( b != e && a >= b && a <= e ) {		// in selected area => clear some days
				if ( a - b > e - a )
					e = $D.add( a, 'd', -1 );
				else
					b = $D.add( a, 'd', 1 );
			}
			else if ( a < b ) {
				s = $D.add( a, 'd', cData.periodmax - 1 );
				if ( e > s ) {
					e = s;
					cover.keyMsg( $( 'uL-days' ), 'Hint' );
				}
				b = a;
			}
			else if ( a > e ) {
				s = $D.add( a, 'd', 1 - cData.periodmax );
				if ( b < s ) {
					b = s;
					cover.keyMsg( $( 'uL-days' ), 'Hint' );
				}
				e = a;
			}
			return { b: b, e: e };
		}
	}
	, countMonth = conf.calendarMonth	// actual displayed months
	, maxMonth = 12							// maximal displayed months
	, activeDate = $D.toISO()
	, activeFD
	, activeID
	, startDate = 0
	, endDate = 0
	, select = false
	, cData = {}
	;
	function getDatePicker( e, type ) {
		activeFD = getFormDef( e );
		activeID = e.id + 'uLcalendarContend';
		return [
			[ 'div', { CN: cssNS.calendar + ' ' + cssNS.noUserSelect 
			,  MU: "useLib.call.calAnalyze( false, 'stop' )"
			}
			, [ 'h1', {}, lang.datepicker[ 0 ]]
			, [ 'span', { CN: cssNS.inform }, lang.datepicker[ 1 ]]
			, [ 'div', { id: activeID }, [ 'table', {}]]
			, [ 'div', { CN: cssNS.legend }
				, [ 'h1', {}, lang.legend[ 0 ]]
				, [ 'dl', {}
					, [ 'dt', { CN: cssNS.today }, 'X' ]
					, [ 'dd', {}, lang.today[ 0 ]]
					, [ 'dt', { CN: cssNS.disabled }, 'X' ]
					, [ 'dd', {}, lang.disableddays[ 0 ]]
					, [ 'dt', { CN: cssNS.blocked }, 'X' ]
					, [ 'dd', {}, lang.blockeddays[ 0 ]]
					, [ 'dt', { CN: cssNS.sunday }, 'X' ]
					, [ 'dd', {}, lang.holidays[ 0 ]]
					, [ 'hr', {}]
					, [ 'dt', { id: 'uL-days', CN: cssNS.selected }, '0' ]
					, [ 'dd', {}, lang.selecteddays[ 0 ]]
				]
				, [ 'div', { CN: cssNS.doublebutton }, getStruct.button( lang.jumptoday
					, 'useLib.call.calToDay()' )]
				, [ 'div', { CN: cssNS.doublebutton }, getStruct.button( lang.jumptoselect
					, 'useLib.call.calToSelect()' )]
				, getStruct.doubleButton( lang.display, 'useLib.call.calAddMonth' )
			]
		]
		, getStruct.buttonRow()
		];
	}

	function setTable( d, data ) {
	//@ PARAM d := timestamp for displayed calendar period [today], dataStruct := for content
	//@ sets closed to dateISOString. The content of calendar start with m-1 y of d.
	//@ The following data struct is defined for steering the behavior of the calendar
	//@ content (example):
	/*
	{
		value: {
			begin: '2016-05-17'	// start of the selection := valueOfElement
			, end: '2016-05-24'	// end of the selection [begin] := valueOfElementEnd
		}
		, period: {
			begin: 1
			, end: 5			// period of maximal selectable days [1]
		}
		, blocked: {			// blocked days incl. reason [none]
			'home office': { 'begin': 'tu' } 
			, 'usability training': { 'begin: '2016-05-14' } 
			, 'business trip': { 'begin: '2016-05-28' } 
			, 'vacation': { 'begin: '2016-06-02', 'end': '2016-07-14' } 
			, 'don\'t work on weekends...': { 'begin: 'sa', 'end': 'su' }  
		}
		, disabled: {
		//@ if the period is marked with the keywords min and max for begin or end
		//@ this border is set as min or max value of a date (only dates in this period).
		//@ REMARK: It is not nessesary to define min or max in the date attributes then.
			"the past is blocked for selection": { "begin": „min“, "end": "2018-01-01" }
			, "no meetings after project end :-)": { "begin": "2018-06-01", "end": „max“ }
			, "project journey with all members": { "begin": "2018-02-12", "end": "2018-02-17" }
		}
		, holidays: 'christian,muslim'	// supported only [christian]
	}
	*/
	var
		e = $( activeID )
		;
		cData = data || cData;
		d = d || $D();          // no date => get actual one
		d.setDate( 1 );
		activeDate = $D.toISO( d );
		// start with one month before if more than two month displayed
		d = $D.add( d, 'm', countMonth < 3 ? 0 : -1 );
		startDate = d;
		endDate = $D.add( $D.add( d, 'm', countMonth ), 'd', -1 );
		e.replaceChild( $CE( _getFullTable( d )), e.firstChild );
		setDateClass( $( 'uL-' + $D.toISO()), cssNS.today );
		// order for more important information: disabled -> holiday -> blocked
		_setDaysState( 'blocked' );
		_setHolidays( cData.calendar.holidays ? cData.calendar.holidays.split( ',' )
			: 'christian' );
		_setDaysState( 'disabled' );
		if ( cData.begin ) {
			setSelect( $D( cData.begin ), $D( cData.end ));
			setSelectedDays( $D( cData.begin ), $D( cData.end ));
		}
		else
			setSelectedDays( 0, 0 );
		$( 'uL-days' ).nextSibling.innerHTML = lang.selecteddays[ 0 ]
		+ ( !cData.week && ( d = cData.calendar.period ) && ( d.begin > 1 || d.end )
		? '<br>(' + d.begin + ( d.end ? ' - ' + d.end : '' ) + ' max.)' : '' );

		function _getFullTable( d ) {
		//@ creates HTML code of calendar table
		var
			yearID = activeID + 'year'
			, th = [	// first column of the one thead row is input of the actual year 
				[ 'th', { id: 'uL-mm', CN: cssNS.year, abbr: lang.month[ 0 ]}
				, getStruct.input( yearID, lang.year, {
					type: 'number'
					, id: yearID
					, value: $D.y( d )
					, size: 4
					, maxlength: 4
					, KD: 'useLib.call.calKeyDown(event,this)'
					, KU: 'useLib.call.calKeyUp(this)'
					, OC: 'useLib.call.calSetYear(this)'
					})
				]
			]
			, tr = []
			;
			for ( var n in lang.dayNames )				// create rest of head row (all days)
				th.push( n == lang.isWeek[ 0 ]
					? [ 'th', { id: n, CN: cssNS.week
						, title: 'Standard of ISO 8601 (correct >1976)' 
					, abbr: lang.dayNames[ n ]	}, n ]
					: [ 'th', { id: n, CN: cssNS.day
					, abbr: lang.dayNames[ n ] + ' ' + lang.isday[ 0 ] }, n ]
				);
			for (var i = 0; i < countMonth; i++ ) {		// one months HTML as step
				tr = tr.concat( __getMonthTable( d
				, !i || i == countMonth -1 ? __getChange( !i ) : null ));       
				d = $D.add( d, 'm', 1 );
			}
			return [ 'table', { summary: lang.calTable[ 0 ]}
			, [ 'thead', {}, [ 'tr', {}].concat( th )]
			, [ 'tbody', {}].concat( tr )
			];

			function __getChange( up ) {
			var
				t = up ? 'up' : 'down'
				;
				return [ 'button', { CN: cssNS[ t ], type: 'button', T: lang[ 'month' + t ]
				, MC: 'useLib.call.calSetMonth(' + ( up ? -1 : 1 ) + ');' }];
			}

			function __getMonthTable( d, monthChange ) {
			//@ creates HTML code of one month
			var
				mLength = $D.daysInMonth( d )		// days in month
				, day =- ( d.getDay() || 7 ) + 1	// day from 1=monday to 7=sunday!!
											// ... start negative, if leading empty days
				, m = $D.m( d )
				, y = $D.y( d )
				, w = $D.getWeekNo( d )						// calendar week
				, rows = Math.ceil(( mLength - day ) / 7 )	// count of rows (weeks) in month
				, t = []
				, tr
				, td
				, id
				;
				for ( var i = 0; i < rows; i++ ) {
					tr = i == 0 ? [[ 'td', { headers: 'uL-mm', CN: cssNS.month, rowspan: rows
					, IH: lang.months[ m - 1 ] + '<br>' + y }, monthChange] ]
					: []
					;
					w = ( w > 52 && m == 12 && !$D.checkWeek53( y )) || w > 53 ? 1 : w;
					for ( var n in lang.dayNames ) {
						td = [ 'td', { headers: n }];
						if ( n == lang.isWeek[ 0 ]) {		// week needs different HTML code
							td[ 1 ].CN = cssNS.week + ( day < 0 ? ( ' ' + cssNS.small ) : '' );
							td[ 1 ].MC = 'useLib.call.calWeek(' + w + ',' + y + ')';
							td.push( String( w++ ) );
						}
						else {
							id = ++day > 0 && day <= mLength// day, if value in range 1 to monthLength
							? 'uL-' + $D.toISO([ y, m, day ]) : false;
							if ( id ) {
								td[ 1 ].id = id;
								td[ 1 ].MD = "useLib.call.calAnalyze( this, 'start')";
								td[ 1 ].MM = "useLib.call.calAnalyze( this, 'move')";
								td.push( String( day ));
							}
						}
						tr.push( td );

					}
					t.push( [ 'tr', {}].concat( tr ));
				}
				return t;
			}
		}

		function _setDaysState( state ) {
		var
			aB = metaReaction.calendar.addBlocked
			;
			for ( var t in cData.calendar[ state ])
				_set( cData.calendar[ state ][ t ], t );
			if ( state == 'blocked'	&& !!aB )
				for ( var t in aB )
					_set( aB[ t ], t );

			function _set( v, _t ) {
				if ( typeCheck( 'Date', v.begin ))	// is Date?
					setSelect( v.begin, v.end, cssNS[ state ], false, _t );
				else
					_setSelectDays( v.begin, v.end, state, _t );
			}
		}

		function _setSelectDays( b, e, c, t ) {
		var
			bN = Object.keys( langConf.en.dayNames )
			,lN = Object.keys( lang.dayNames )
			;
			for ( var i = bN.indexOf( b ), e = bN.indexOf( e ), l; i > -1 && i <= e; i++ ) {
				// find all cells with day as header
				l = $Q( $( activeID ), 'td[headers=' + lN[ i ] + ']');
				for ( var j = 0; j < l.length; j++ )
					if ( l[ j ].innerHTML.length )
						setDateClass( l[ j ], cssNS[ c ], false, t )
			}
		}

		function _setHolidays( h ) {
		var
			d = $D( activeDate )
			, y = $D.y( d )
			;
			__set( y, h );
			if ( $D.y( $D.add( d, 'm', countMonth )) > y )  // ...mind skip of year
				__set( y + 1, h );

			function __set( y, h ) {
			//@ to be extended for international use
				for ( var i = 0, l = {}; i < h.length; i++, l = {}) {
					if ( h[ i ].indexOf( 'christ' ) != -1 )
						l = __getCristHolidays( y );
					// else if ( h[ i ].indexOf( 'musli' ) != -1 )
					//		l = __getMuslimHolidays( y );
					for ( var day in l )
						setDateClass( $( 'uL-' + day ), cssNS.sunday, false, l[ day ]);
				}
			}

			function __getCristHolidays( y ) {
			var
				ES = ___easterSunday( y )
				, christ = {
					goodFriday: -2
					, easterSunday: 0
					, easterMonday: 1
					, theAscension: 39
					, whitSunday: 49
					, whitMonday: 50
					, corpusChristi: 60
					, ashWednesday: -46
				}
				, lC = lang.christHoliday
				, l = []
				;
				for ( var t in christ )
					l[ $D.toISO( $D.add( ES, 'd', christ[ t ]))] = lC[ t ] ;
				for ( var t in lC.fixHoliday )
					l[ y + '-' + t ] = lC.fixHoliday[ t ];
				return l;
				
				function ___easterSunday( y ) {  //http://www.ptb.de/cms/index.php?id=957&L=0
				var
					INT = parseInt
					, MOD = function( a, b ){ return Math.abs( a % b ); }
					, K = INT( y / 100 )
					, M = 15 + INT(( 3 * K + 3 ) / 4 ) - INT(( 8 * K + 13 ) / 25 )
					, S = 2 - INT(( 3 * K + 3 ) / 4 )
					, A = MOD( y, 19 )
					, D = MOD( 19 * A + M, 30 )
					, R = INT( D / 29 ) + (INT( D / 28 ) - INT( D / 29 )) * INT( A / 11 )
					, OG = 21 + D - R
					, SZ = 7 - MOD( y + INT( y / 4 ) + S, 7 )
					, OE = 7 - MOD( OG - SZ, 7 )		// day of easter sunday in march...
					, ES = OG + OE
					, m = 3
					;
					if ( ES > 31 ) {             // ... if more days then march => correction 
						m = 4;
						ES -= 31;
					}
					return $D([ y, m, ES ]);
				}
			}
		}
	}
	function checkYear( e, y ) {
		y = min( max( y, activeFD.subs.YEAR.check.min ), activeFD.subs.YEAR.check.max );
		if( y != $D.y( activeDate )) {
			e.value = y;
			setTable( $D( y + activeDate.slice( 4 )));
			e = $( activeID + 'year' );		// it was rewritten
			e.focus();
		}
	}

	call.uLinit( 'calReset', function( id ) {
		cData.begin = cData.givenBegin;
		cData.end = cData.givenEnd;
		setTable( $D( cData.begin ));
	});
	call.uLinit( 'calToSelect', function() { setTable( $D( cData.begin )); });
	call.uLinit( 'calToDay', function() { setTable(); });
	call.uLinit( 'calAddMonth', function( add ) {
		countMonth = max( 3, min( maxMonth, countMonth + add));  // only a limit is allowed
		setTable( $D( activeDate ));
	});
	call.uLinit( 'calSetMonth', function( add ) {
		setTable( $D.add( activeDate, 'm', add ));
	});
	call.uLinit( 'calSetYear', function( e ) { checkYear( e, e.value ); });
	call.uLinit( 'calWeek', week );
	call.uLinit( 'calAnalyze', analyze );
	call.uLinit( 'calKeyDown', function( ev, e ) {
	//@ mini number rotor (may be inited by cover methods later...)
	var
		step = ev.keyCode == 38 ? 1 : ev.keyCode == 40 ? -1 : 0
		;
		if ( step )
			checkYear( e, e.value.length ? $I( e.value ) + step : $D.y());
	});
	call.uLinit( 'calKeyUp', function( e ) {
	//@ calculates the user edited year in the input box on top 
	var
		y = $I( String( e.value ).replace( /[^0-9]/g, '' ))
		;
		if ( y > activeFD.subs.YEAR.check.min )
			checkYear( e, y );
	});

	function getMetaType( e ) {
	//@ returns selector of type that uses the calendar (could be more in future?)
		return getFormDef( e ).type == 'week' ? 'week' : 'date';
	}
		
	return {
		getContent: getDatePicker
		, getValue:  function( e ) {
		//@ get the cData struct, that contains the calendar parameter
		var
			cData = metaReaction[ getMetaType( e )].getValue( e )
			, p = getFormDef( e ).begin
			;
			if ( p && ( p = p.period ) && p.fixed )
				cData.periodmax = $I( $(( p.subs.DAYS || p.subs.PDAYS ).id ).value );
			if ( !cData.periodmax )
				cData.periodmax = cData.week ? 7 : cData.calendar && cData.calendar.period
				? cData.calendar.period.end : p ? 365 : 1;
			cData.givenBegin = cData.begin;
			cData.givenEnd = cData.end;
			setTable( $D( cData.begin ), cData );
		}
		, setValue: function( e, v ) {
			return metaReaction[ getMetaType( e )].setValue( e, cData );
		}
		, normCal: function( fD, lCal ) {
		//@ adapts a calendar data struct to be shown in calendar 
		var
			cal = fD.calendar || false
			;
			if ( !cal || !cal.loaded ) {
				if ( fD.data
				&& ( cal = lCal || loadJSONCSS( fD.data, function( lCal ) {
					if ( Array.isArray( lCal ))
						lCal = lCal.join( '\n' );
					if ( typeCheck( 'String', lCal )
					&& lCal.indexOf( 'BEGIN:VEVENT' ) > -1 )
						lCal = analyseICAL({}, lCal );
					else
						lCal = false;
					if ( lCal && D.contains( $( fD.id )))
						metaReaction.calendar.normCal( fD, lCal );
				}))) {
					cal.blocked = _toDate( cal.blocked || {} );
					cal.disabled = _toDate( cal.disabled || {} );
					cal.loaded = true;
				}
				else
					cal = { disabled: {}, blocked: {}
					, loaded: !fD.data || extern[ fD.data ] == -2
				};
				cal.holidays = cal.holidays || 'christian';
				cal.period = cal.period || { begin: 1 };
				fD.calendar = cal;
			}
			function _toDate( l ) {
			//@ transforms once to Date and controls value, because of Date() behavior :-[
			var
				b
				, e
				;
				for ( var t in l ) {
					b = l[ t ].begin;
					e = l[ t ].end;
					if ( b == 'min' && !fD.min )
						fD.min = e;
					if ( e == 'max' && !fD.max )
						fD.max = b;
					b = $I( b ) ? $D( b ) : b == 'min' ? $D( '0100-01-01' ) : b;
					e = !e ? b : $I( e ) ? $D( e ) : e == 'max' ? $D( '3100-01-01' ) : e;
					l[ t ].begin = e < b ? e : b;
					l[ t ].end = b > e ? b : e;
				}
				return l;
			}
		}
		, addBlocked: false
	};
})();
//@ end of metaReaction.calendar

/*****
 *
 *		functions to create select meta control
 *		@ 
 *		@ 
 * 
 *****/
metaReaction.singleSelect = ( function() {
	call.uLinit( 'invertSelect', function( e ) { operateValues( e, 1 ); return false; });
	call.uLinit( 'resetSelect', function( e ) { operateValues( e, 0 ) });
	call.uLinit( 'clearSelect', function( e ) { operateValues( e, -1 ); return false; });
	call.uLinit( 'filterSelect', function( id, t ) {
	var
		e = $( id + 'Filter' )
		, l = $Q( $P( e, 3 ), '[name="'+ id +'uL[]"]' )
		, fn = e.value.trim()
		;
		fn = fn.length && t != 'showAll' ? analyseFilter( fn ) : false;
		if ( t == 'showAll' || !!fn ) {
			for ( var i = 0, s, has; i < l.length; i++ ) {
				s = $S( $P( l[ i ]));
				if ( t == 'showAll' )
					s.display = 'block';
				else {
					has = fn( l[ i ].nextSibling.textContent );
					if (( !has && t == 'filter' ) || ( has && t == 'filterInvers' ))
						s.display = 'none';
				}
			}
		}
		return false;
	});
	
	function operateValues( e, type ) {
	//@ PARAM  element, type := -1 = clear, 0 = reset, 1 = invert
		e = $T( $P( e ), 'input' );
		for ( var i = 0; i < e.length; i++ ) {
			if ( !e[ i ].disabled && !$CN.contains( e[ i ], "uL_ignore" )) {
				e[ i ].checked = type === 1 && !e[ i ].checked
				|| type === 0 && $A( e[ i ], 'checked' );
				onchange( e[ i ], null );
			}
		}
	}

	function getContent( e, type, list ) { // singleSelect, multiSelect, ...
	//@ one problem is, to find a rule, that creates a good proportioned
	//@ distribution of columns and rows. Some attempts where made and at least
	//@ viewed in excel... The best is to start with the amount of rows with a
	//@ maximum of 7 and calculate the columns on this base. The result can be
	//@ optimized to avoid single columns
	//@ REMARK: list is an array of groups
	//@ list = [ groupName0, { textContent: valueName0, value: x, shortCut: y }
	//@ , ...], [ ... ] where value and shortCut are optional
	if ( $A( e, 'onfocus' ))// there may be dynamic lists, that have to be updated...
		dispatchEvent( e, 'focus' );
	var
		calcW	= function( c ) {
		var
			wC = width[ c ]
			;
			if ( wC && wC.c )		// calc in pixel per char plus radiobutton and checkbox 
				width[ c ] = Math.ceil(( wC.w * 6.9 / wC.c ) + 35 ) / 10;		
		}
		, sum = function( a ) {
			return a.reduce( function( s, v ) { return s + v; }, 0 );
		}
		, mS = type == 'multiSelect'
		, fD = getFormDef( e )
		, o = $T( e, 'optgroup' )
		, cols = -1			// easier if starts not on 0...
		, rows = 0
		, maxWidth = 0
		, width = []
		, scale = 62.5 / parseFloat( D.body.style.fontSize )
		, t
		;
		if ( type == 'dynamicSelect' )
			list = dynamicSelects[ $A( e, 'data-ul' )];
		else if ( type == 'dynamicExtend' )	// disable active selfgrouper  {
			list = metaReaction.selfgrouper.getDynamicValues( e );
		_doLists( true );	// first step: get base for width calculation
		calcW( cols );		// calc last column, not calced yet
		cols++;				// here add one for correct calculation
		// this special solution allows advanced learned users to control cols per
		// line via window width and dialog position effect works together with css!
		if ( cols < 7 || cols > 14 )
			maxWidth = 'width:auto;';
		else {
			cols = min(
				Math.floor(( W.innerWidth * 0.85 - 32. ) / sum( width ))
				, Math.ceil( cols / ( cols > 3 ? 2 : 1 ))
			);
			for ( var i=0; i < width.length; i += cols )
				maxWidth = max( maxWidth, sum( width.slice( i, i+cols )));
			maxWidth = 'width:' + Math.ceil( maxWidth * scale ) + 'em;'
		}
		return _getFieldset( _doLists( false ), maxWidth +'min-width:28em;' );

		function _doLists( widthCalc ) {
		//@ REMARK: three types of list decided: given list, selectList with
		//@ optGroups and without. For the last one empty headers will be created.
		var
			groupList = []
			, activeHead = ''
			, count = 1
			, l
			;
			if ( !!list ) {
				for( var i = 0, h = 'none'; i < list.length; i++ ) {
					if ( list[ i ].type == 'group' )
						h = list[ i ].textContent;
					else
						__doStep( list[ i ], h );
				}
			}
			else if ( o.length ) {
				for( var j = 0; j < o.length; j++ )
					for( var i = 0; i < o[ j ].children.length; i++ )
						__doStep( o[ j ].children[ i ], o[ j ].label, o[ j ].style.width );
			}
			else {
				for( var i = 0; i < e.children.length; i++ )
					__doStep( e.children[ i ], 'none' );
			}
			return groupList;

			function __doStep( _e, h, w ) {
				if ( !!widthCalc )
					__getWidthBase( _e, h, !!w ? parseFloat( w ) : false );
				else
					__getGroup( _e, h );
			}

			function __getWidthBase( _e, h, w ) {
			var
				_l = _e.textContent.length + ( _e.shortCut ? _e.shortCut.length : 0 )
				;
				if ( type == 'dynamicSelect' && _e.type == 'checkbox' )
					mS = true;
				rows++;
				if ( h != activeHead || ( count++ ) % 5 == 0 ) {
					_l += h == 'none' ? 0 : h.length;
					calcW( cols );
					if ( h != activeHead ) {
						activeHead = h;
						count = 1;
						if ( h.length )
							rows++;
					}
					cols++;
					if ( !width[ cols ])
						width[ cols ] = !!w ? w : { c: h.length > 5 ? 1 : 0, w: 0 };
				}
				if ( !w ) {
					width[ cols ].c++;
					width[ cols ].w += _l;
				}
			}

			function __getGroup( _e, h ) {
				if ( h != activeHead || ( count++ ) % 5 == 0  ) {
					groupList.push( ___getHeader(
						h, h != 'none' && h == activeHead ? cssNS.same : ''
						, width[ groupList.length ]
					));
					if ( h != activeHead ) {
						activeHead = h;
						count = 1;
					}
				}
				l = groupList.length - 1;
				groupList[ l ].push(
					getStruct.radioCheck(
						!!_e.type && _e.type == 'radio' ? false : mS
						, ( _e.textContent.indexOf( conf.beforeOwnInput ) > -1 ? 'ownInput '
						: '' ) + (( _e.className || '' )	+' '+ ( _e.id || '' )).trim()
						, _e.textContent || ''
						, e.id + 'uL-' + l + '-' + ( groupList[ l ].length - 3 )	// minus div & header
						, e.id + 'uL[]'
						, _e.value || _e.textContent
						, fD.value && fD.value.indexOf( _e.value || _e.textContent ) > -1
						, !!_e.disabled
						, _e.shortCut || false
						, mS ? 'this.focus()' : "useLib.call.submitForm(this.form,false)"
					)
				);
				l = groupList[ l ][ groupList[ l ].length -1 ];
				if ( type == 'listSelect' ) {	// dont't show radio buttons
					l[ 1 ].S = 'padding-left:0!important;';
					l[ 2 ][ 1 ].S = 'visibility:hidden;';
				}
				l[ 3 ][ 1 ].title	= _e.title || _e.textContent;	// append title to label

				function ___getHeader( h, cn, w ) {
					h = h == 'none' ? '\u2003' : h;
					return( [ 'div', { CN: cn, S: 'width:' + w + 'em;'}
						, [ 'h2'						// array starts with header...
							, mS
							? {
								MC: 'useLib.call.invertSelect( this )'
								, M2: 'useLib.call.resetSelect( this )'
								, title: h
							}
							: { title: h }
							, h
						]
					]);
				}
			}
		}

		function _getFilter( id ) {
			return [ 'div', { CN: cssNS.filter }
			, [ 'span', {}, [ 'input', { CN: 'uL_ignore', type: 'text'
				, autocomplete: 'off'
				, T: lang.filterButtons[ 1 ], id: id + 'Filter'
				, S: 'width:14em;margin:0 0.3em 0 0;' }]]
			, __getButton( "filter", 2, true ) 
			, __getButton( "filterInvers", 3 ) 
			, __getButton( "showAll", 4 ) 
			]
			;
			function __getButton( t, num, cN ) {
				return [ 'div'
				, { CN: cssNS.button, S: 'display:inline!important;'
				+ 'margin:0!important;padding:0!important;' }
				, [ 'button'
					, { CN: cssNS[ t ] + ( cN ? ' ' + cssNS.invertIcon : '' )
					, MC: "return useLib.call.filterSelect('"+ id + "','"+ t + "')"
					, T: lang.filterButtons[ num ]
					, id: 'uL' + id + num + 'Filter'
				}
				, '' ]];
			}
		}

		function _getFieldset( groupList, w ) {
		//@ multiSelect is a form with buttons and some usability raising
		//@ select features return an complex hierarchical array of html objects
		//@ (head, fieldset with interaction and Buttons)
		var
			h = getLabelText( e )
			, lT = lang[ mS ? 'multiSelect' : 'singleSelect' ]
			;
			return [
				[ 'fieldset', { S: w }
					, !mS && !h.length ? null : [ 'legend', {}
						, mS ? [ 'button', {
							CN: cssNS.invert
							, T: lang.invertSelect[ 0 ]
							, MC: 'return useLib.call.invertSelect( this.parentNode )' 
						}] : null 
						, mS ? [ 'button', {
							CN: cssNS.clear
							, T: lang.clearSelect[ 0 ]
							, MC: 'return useLib.call.clearSelect( this.parentNode )'
						}] : null 
						, [ 'span', {}, h ]
					]
					, type == 'listSelect' ? null
               : [ 'span', { CN: cssNS.inform, IH: useLib.WIKI.toHTML( lT[ 1 ]) }]
					, !lT[ 2 ] ? null
					: [ 'span', { CN: cssNS.explain, IH: useLib.WIKI.toHTML( lT[ 2 ]) }]
					, groupList.length * 5 > conf.filterLimit ? _getFilter( e.id ) : null
				].concat( groupList )
				, mS ? getStruct.buttonRow() : null
			];
		}
	}

	function doKeys( c, ev ) {
	//@ runs through the groups, colums, rows and possible buttons of a 'optionField' via
	//@ arrow keys and set the focus to be (un-)selected via enter.
	//@ REMARK: the groups are numbered and their column and row must be calulated via
	//@	colum count of table.
	var
		e = dialog.layer.length ? $( String( dialog.layer.slice( -1 ))) : null
		, eF = D.activeElement
		, bT
		;
		if ( e ) {
			if ( !eF || !e.contains( eF ))
				focusFirst( e );
			if ( eF ) {
				bT = eF.tagName == 'BUTTON';
				if ( /uL[\w\._-]+Filter/.test( eF.id )) {
					if ( c == 38 )
						setFocus( _doButton( 'top' ));
					else if ( c == 40 )
						focusFirst( e );
					else if ( c = c == 37 || c == 8 ? 'previous' : c == 39 ? 'next'
					: 0 ) {
						if ( $( eF )[ c + 'Sibling' ] && ( eF.tagName != "INPUT"
						|| ( eF.selectionStart == eF.value.length && c == 'next' )))
							setFocus( $( eF )[ c + 'Sibling' ].firstChild );
						else if ( eF.tagName == "INPUT"
						&& eF.selectionEnd == 0 && c == 'previous'
						&& ( bT = _doButton( 'top' )))
							setFocus( bT.nextSibling );
					 	else
					 		return false;
					}
					else if ( c == 'previous' && ( bT = _doButton( 'top' )) != eF )
					 	setFocus( bT.nextSibling );
					else
						return false;
				}
				else if ( bT )
					setFocus( _doButton( c ));
				else {
				var
					p = eF.id.match( /(.+?)-(\d+)-(\d+)$/ )
					;
					if ( p ) {
						if ( c == 32 && eF.type == 'checkbox' )	//SPACE
							eF.checked = !eF.checked;
						else {
							p.shift();
							p[ 1 ] = $I( p[ 1 ]);
							p[ 2 ] = $I( p[ 2 ]);
							if (( eF = _horizontal( c == 37 || c == 8 || ( c == 9 && ev.shiftKey ) ? -1
							: c == 39 || c == 9 ? 1 : 0 ))
							|| ( eF = _vertical( c == 38 ? -1 : c == 40 ? 1 : 0 )))
								eF.focus();
						}
					}
					else
						return false;
				}
				return true;
			}
		}
		return false;

		function _vertical( step ) {
			if ( step ) {
			var
				_p = p[ 2 ] + step
				, f = false
				;
				if ( !( f = _getPos( p[ 1 ], _p ))) {
					_p = _find( p[ 1 ], -1 );
					if ( step < 0 ) {
						if ( !( f = _getPos( _find( _p - 1, -1 ) + p[ 1 ] - _p, -10 )))
							f = _doButton( 'top' );
					}
					else if ( !( f = _getPos( _find( p[ 1 ], 1 ) + 1 + p[ 1 ] - _p, 0 )))
						f = _doButton( 'bottom' );
				}
			}
			return f;
		}

		function _horizontal( step ) {
			if ( step ) {
			var
				_p = _find( p[ 1 ], step )
				, f = false
				;
				if ( _p == p[ 1 ]) {
					if ( step < 0 && p[ 2 ] == 0 )	// first of first in column
						f = _getPos( _p + step, -10 );// ... get end of previous column
					else if ( step > 0 && eF == _getPos( _p, -10 ))	// last of last in column
						f =  _getPos( _find( p[ 1 ], -step ), p[ 2 ] + step )
						|| _getPos( _p + step, 0 );	// ...get next column
					else
						f = _getPos( _find( p[ 1 ], -step ), p[ 2 ] + step )
				}
				else
					f = _getPos( p[ 1 ] + step, p[ 2 ]) || _getPos( p[ 1 ] + step, -10);
			}
			return f;
		}

		function _find( _p, _step ) {
		var
			xPos = _getPos( _p, 0 )
			, x_p
			;
			if ( xPos ) {
				xPos = $P( xPos, 2 ).offsetLeft;
				while (( x_p = _getPos( _p + _step, 0 ))
				&& ( x_p = $P( x_p, 2 ).offsetLeft )
				&& (( _step > 0  && x_p > xPos ) || ( _step < 0  && x_p < xPos ))) {
					xPos = x_p;
					_p += _step;
				}
			}
			return _p;
		}

		function _getPos( _c, _r ) {
		//@ PARAM columnNumber  rowNumber = -10 := getLast of column
		var
			_f = $([ p[ 0 ], _c, !_r || _r == -10 ? 0 : _r  ].join( '-' ))
			;
			return _f && _r == -10	// childNodes are DIVs with inputs minus one header
			? _getPos( _c, $P( _f, 2).childNodes.length - 2 )
			: _f;
		}

		function _doButton( c ) {
		var
			eB = $T( e, 'button' )
			, i
			;
			if ( eB[ 0 ].id.slice( -6 ) == 'Filter' && c == 'top' )
				return $P( eB[ 0 ]).previousSibling.firstChild;
			else if ( eB[ 0 ].className == cssNS.invert ) {
				if ( c == 'top' )
					return eB[ 0 ];
				else if (  c == 'bottom' )
					return eB[ eB.length - 1 ];
				else {
					i = [].indexOf.call( eB, D.activeElement );
					if ( eB[ 2 ].id.slice( -6 ) == 'Filter' 
					&& ( i == 1 && c == 39 ) || ( i == 2 && ( c == 8 || c == 37 )))
						return $P( eB[ 2 ]).previousSibling.firstChild;
					i = i < 2 && c == 40 ? -2 : i > 1 && c == 38 ? -1 : i;
					if ( i < 0 ) {
						eB = $Q( e, "[type=checkbox]" );
						return eB[ i == -2 ? 0 : eB.length - 1 ];	// first or last value
					}
					else {
						i += ( c == 8 || c == 37 ? -1 : c == 39 ? 1 : 0 );
						return eB[ max( min( i, eB.length - 1 ), 0 )];
					}
				}
			}
			return false;
		}
	}

	function focusFirst( e ) {
	// set focus to first checked or at least first input
		setFocus( $Q( e, 'input:checked', 1 )
		|| $Q( e, 'input[type="radio"]:enabled,input[type="checkbox"]:enabled', 1 ));
	}

	function getValue( e ) {
	//@ PARAM  element
		focusFirst( $( e.id + cssNS.modal ));
		keyControl = doKeys;
	}
	
	function setValue( e, v ) {
	// quite a bit of experimental experience: take the selected elements
	// from modal dialog and transfer it to select base element. Fire the
	// onchange by hand and from there the content of the passive element
	// is filled... Even as special as file...
		v = v ? v : _getValue( $( e.id + cssNS.modal ));
		// e.selectedIndex = -1; // clear selectlist & trigger onchange
		// (sometimes it worked :-[
		if ( getFormDef( e ).type == 'dynamicExtend' ) {
			if ( metaReaction.grouper.changeGrouper( e, v[ 0 ]) === false )
				return;
		}
		else
			setEleValue( e, v );
		onchange( e, v );							// => ...do it by hand

		function _getValue( e ) {
		var
			eL = $T( e, 'input' )
			, v = []
			;
			for ( var i = 0; i < eL.length; i++ )
				if ( eL[ i ].checked && !eL[ i ].disabled ) 
					v.push( eL[ i ].value );
			return v;
		}
	}

	function getText( e, v, SET ) {
		for ( var i = 0, o = e.options, t = []; i < o.length; i++ ) {
			if ( !o[ i ].disabled && ( typeof v == 'string' && o[ i ].value == v
			|| v.indexOf( o[ i ].value ) > -1 ))
				t.push( o[ i ].text.replace( /\s/g, '\u00A0' ));
		}
		t = t.length ? t.join( ', ' ) : '';
		if ( SET )
			$( getFormDef( e ).first ).value = t;
		return t;
	}

	return {
		doReaction: function( e, fD ) {
			return {
				onrotor: function( eC, v, step ) {
					v = $I( e.selectedIndex + step );
					v = v > e.options.length - 1 ? 0 : v < 0 ? e.options.length - 1 : v;
					activeOnChange = false;
					e.selectedIndex = v;
					v = e.options[ v ].text;
					onchange( e, v );
					activeOnChange = true;
					return v;
				}
			};
		}
		, getContent: getContent
		, getValue: getValue
		, setValue: setValue
		, getText: getText
	}; 
})();
metaReaction.multiSelect = metaReaction.dynamicSelect
= metaReaction.dynamicExtend = metaReaction.singleSelect;
//@ end of metaReaction.singleSelect

/*****
 *
 *		functions to create toggle meta control
 *		@ 
 * 
 *****/
metaReaction.toggleSelect = ( function() {
	function toggleSelect( e, fD, change ) {
	var
		l = e.options.length - 1
		, index = max( 0, !change ? e.selectedIndex : e.selectedIndex == l ? l - 1
		: l )
		;
		if ( !!change ) {
			activeOnChange = false;
			//e.selectedIndex = index;		// fires no onchange
			e.value = e.options[ index ].value;
			dispatchEvent( e, 'change' );
			activeOnChange = true;
		}
		$( fD.first ).value = e.options[ index ].text;
		$( fD.first ).nextSibling.title = e.options[ e.selectedIndex == l
			? l - 1 : l ].text;
		$CN[ index == l ? 'add' : 'remove' ]( e.nextSibling, cssNS.active );
	}
	return {
		init: toggleSelect
		, doButton: function( ev, e ) {
			if ( !$CN.contains( e, cssNS.disabled ))
				toggleSelect( e, getFormDef( e ), true );
		}
		, setValue: function( e ) { toggleSelect( e, getFormDef( e ), false ); }
	}
})();
//@ end of metaReaction.toggleSelect

/*****
 *
 *		functions to create file meta control
 *		@ 
 * 
 *****/
metaReaction.file = ( function() {
//@ WORKAROUND: fileSelector should be a modal dialog, but browsers don't act
//@ so. (they don't send a onclick event before opens the dialog). All Bowsers
//@ don't tell, if users abort, therefore the modal coverage is stopped on
//@ mousemove So the original input file is pu in front, but with opacity: 0
//@ and the cover is in the background...
var
	stopChange = false
	;
	function setValue( e, v ) {
	var
		fD = getFormDef( e )
		;
		v = fD.value = v == undefined ? getEleValue( e ) : [ v.trim() ];
		$A( e, 'data-ul', v[ 0 ].length ? v[ 0 ] : -1 );
		if ( !v[ 0 ].length && e.value.length ) {
			stopChange = true;
			dispatchEvent( e, 'change' );
			stopChange = false;
		}
		if ( e = $( fD.first ))
			e.value = v[ 0 ].replace( /.*?([^\\\/]+)$/, '$1' );
	}
	return {
		init: function( e, fD ) {
		var
			eC = $( fD.first )
			;
			$A( e.form, "enctype", "multipart/form-data" );
			e.onclick = function() {
				activeOnChange = false;
				_shield( true );
			};
			call.drop.init( e );
			//@ REMARK: there ist also a special management nessesary to delete
			//@ possible content especially if drag and drop is used..., setting
			//@ the focus usable... Don't know what they though (?) designing this!
			//@ At least some events don't fire, also caused on the workaround
			//@ Also the drop methods are used to react onchange...!
			$E( e, 'OC', function _fileChange( ev ) {
				if ( !stopChange ) {
					setValue( e );
					_shield( false );
					setFocus( eC );
					if ( e.files.length )
						useLib.call.drop.ondrop( ev, e.files );
				}
			});
			$E( eC, 'OF', useLib.call.coverOnFocus );
			$E( eC, 'OB', function _fileBlur(){
				setStatus( e, 'focused', 0 );
			});
			$E( eC, 'KD', function _fileKey( ev ) {
				if ( ev.keyCode === 8 || ev.keyCode === 46 ) // clear on BS or DEL
					doEvent.clearInput( getFormDef( e ));
			});
			
			function _shield( open ) {
			var
				e = '_shield_uL__modal__'
				;
				if ( open ) {
					e = D.body.appendChild( $CE([ 'div', { id: e, role: 'dialog'
						, CN: cssNS.modal +' '+ cssNS.file } // just the hole screen...
						, [ 'div', { S: 'width: 100vw; height: 100vh; opacity: 0;' }]]
					));
					e[ attrList[ 'MM' ]] = function() {
					// trick to close modal haze again
						this[ attrList[ 'MM' ]] = null;
						_shield( false );
						setFocus( eC );
					};
				}
				else {
					activeOnChange = true;
					e = $( e );
				}
				setOpenClose( e, open );
			}
		}
		, setValue: setValue
		, doButton: function( ev, e ) {	e.click(); }
		, doReaction: function( e, fD ) {
			return {	onchange: function( eC, v ) {	setValue( e, v );	return v; }}
		}
	}
})();
//@ end of metaReaction.file

/*****
 *
 *		functions to create color meta control
 * 
 *****/
metaReaction.color = ( function() {
var
	hasColor = false
	;
	function setValue( e, v ) {
	e = e.target || e;
	var
		fD = getFormDef( e )
		, v = _clean( v || e.value )
		; 
		if ( v.length ) {
			fD.value = [ '#' + v ];
			$( fD.subs.HEX.id ).value = v;
			e.value = $S( $( fD.subs.PASSIVE.id )).backgroundColor = '#' + v;
			$( fD.subs.PASSIVE.id ).title = "rgb( "+ parseInt( v.slice( 1, 3 ), 16 )
			+ ", "+ parseInt( v.slice( 3, 5 ), 16 ) +", "
			+ parseInt( v.slice( 5, 7 ), 16 ) + " )";
		}
		else
			$( fD.subs.PASSIVE.id ).title = '';
		stopChange = true;
		dispatchEvent( e, 'change' );
		stopChange = false;

		function _clean( _v ) {
			_v = String( _v || '' );
			return ( /^#/.test( _v ) ? _v.slice( 1 ) : _v ).toLowerCase();
		}
	}
	
	return {
		init: function( e, fD ) {
			if ( !!$( fD.subs.HEX.id )) {
				$E( e, 'input', setValue );
				//$E( e, 'OC', setValue );		// is not working on color picker!
				$( fD.subs.HEX.id ).onkeyup = function( ev ) {
				//@ tries to convert a given (coppied) color format into the hex format
				//@ for testing: rgba(30, 255, 50, 0.5) rgba(100%,0%,0%,0.5) rgb(255,127,80) #b25512
				var
					eC = ev.currentTarget
					, v = eC.value.replace(
						/[^\(]+\((\d+%?)\s*,\s*(\d+%?)\s*,\s*(\d+%?).*/
						, function( _, r, g, b ) {	return _dec( r ) + _dec( g )
						+ _dec( b ); }
					).slice( 0, 6 ).toLowerCase()
					, cP = eC.selectionStart
					;
					if ( /[0-9a-f]{6,6}/i.test( v )) {
						eC.value = v;
						eC.setSelectionRange( cP, cP );
					}
					return useLib.call.coverAfterInput( ev, e.id );	// now act standard

					function _dec( v ) {
						v = ( v.slice( -1 ) == '%' ? Math.floor( $I( v ) * 2.55 )
						: $I( v )).toString( 16 );
						return (v.length == 1 ? '0' : '' ) + v;
					}
				};
				setValue( e );
			}
		}
		, setValue: setValue
		, doButton: function( ev, e ) {
			if ( hasColor )
				$A( hasColor, 'type', 'text' );
			hasColor = e;
			$A( hasColor, 'type', 'color' );
			hasColor.click();
		}
		, doReaction: function( e, fD ) {
			return {
				onchange: function( eC, v ) {
					if ( /[0-9a-f]{6,6}/i.test( v ))
						setValue( e, v );
					return v;
				}
			}
		}
	}
})();
//@ end of metaReaction.color

/*****
 *
 *		functions to create (self)grouper meta control
 * 
 *****/
metaReaction.grouper = ( function() {
var
	rSplit = new RegExp( '\\s*'+ wikiSigns.append +'\\s*' )
	, extTimeout = false
	;
	function getSelfGrouper( e, FIRST=null ) {
		e = $P( e, 2 );
		return $Q( FIRST ? e : $P( e, 2 ), '[data-ul=selfgrouper]'
		, FIRST );
	}
	function getGrouper( e, FIRST=1 ) { // if FIRST==null get all
		return $Q( e, '[data-ul-grp="isGrouper"]', FIRST );
	}
	function deleteGrouper( e, ALL=false ) {
	var
		next = $P( e, 1 ).nextElementSibling
		;
		if ( $CN.contains( $P( e, 1 ).firstChild, cssNS.same )) {
			if ( e.form )
				delete formDef[ e.form.name ][ e.name ];
			DOM.removeNode( $P( e, 1 ));
		}
		else if ( !ALL && $CN.contains( next.firstChild, cssNS.same )) {
			next = getGrouper( next );
			if ( e.tagName == 'SELECT' )
				setValue( e, next.value );
			else
				e.value = next.value;
			deleteGrouper( next );
		}
		else if ( ALL || e.tagName == 'SELECT' && $A( e, 'data-ul' )) {
			$P( e ).style.display = 'none';
			e.value = '';
			if ( !ALL )
				doActions( e, 'updateDynamicSelect' );
		}
	}
	function deleteAllGrouper( f ) {
		for ( var i = 0, l = getGrouper( f, null ); i < l.length; i++ )
			deleteGrouper( l[ i ], true );
		// remove the ignore mark to ensure reinit of the groupers
		for ( var i = 0, l = $Q( f, '.uL_ignore.'+ cssNS.grouper ), d
		; i < l.length; i++ ) {
			$CN.remove( l[ i ], 'uL_ignore' );
			d = $A( l[ i ], 'data-ul-def' ).replace( /\d+$/, '0' );
			$A( l[ i ], 'data-ul-def', d );
		}
	}
	function setValue( e, v ) {	// for groupers of selfGrouper
		if ( e.type == 'select-one' )
			e.innerHTML = '<option value="'+ v + '" selected>'+ v +'</option>';
		$A( e, 'value', v );
		onchange( e, v );
	}
	function doActions( e, act ) { // execute all actions on selfgrouper
	var
		sE = getSelfGrouper( e, 1 )					// belonging selfGrouper element
		, lN = sE.value									// name of active group
		, groupL = {}	// groups with their entries
		, indivL = []	// individuals
		, duplL = []	// duplicates 
		, list = []
		;
		if ( lN && lN.trim().length ) {
			for ( var l = getSelfGrouper( e ), i = 0, n; i < l.length; i++ ) {
				n = l[ i ].value.trim();
				if ( n.length ) {
				// find all members...
					for ( var m = getGrouper( $P( l[ i ], 2 ), null )
					, j = 0, a = []; j < m.length; j++ )
						a.push([ m[ j ].value, m[ j ]]);
					// ...only append if members length
					if ( a.length > 1 && a[ 0 ][ 0 ] != 'none' ) {
						groupL[ n ] = a;
						groupL[ n ].e = l[ i ];
					}
					else
						indivL.push( n );
				}
			}
		}
		if ( indivL.length == 0 )	// no individuals found...
			return;
		else if ( act == 'separateActive' ) {
			sE = $Q( $P( sE, 2 ), '[data-ul-def^="defGrp:"]', 1 );
			list = groupL[ e.value ] || [];
			for ( var i = 0, first = true; i < list.length; i++ ) {
				if ( !groupL[ list[ i ][ 0 ]]) {
					if ( first ) {	// first is the clicked grouper
						first = false;
						setValue( e, list[ i ][ 0 ]);
					}
					else
						setGrouperLocal( sE, 'dynamicExtend', list[ i ][ 0 ]);
				}
			}
		}
		else if ( act == 'overviewEntry' ) {
		// find all entries of entryValue in groups and push to list
			for ( var n in groupL )
				if ( groupL[ n ].filter( v => v[ 0 ] == e.value ).length )
					list.push( n );
			dialog.alert( "((pre\n\n"+ list.join( ',\n' ).replace( /(\[|\]|\",)/g, '$1\n' )
			+"\n))" );
		}
		else {
			list = _remove( _separate( groupL[ lN ], groupL, [ lN ]));
			if ( act == 'showGroupers' ) {
				dialog.alert( "((pre\n\n"+ JSON.stringify( list.map( 
				m => m[ 0 ]) ).replace( /(\[|\]|\",)/g, '$1\n' ) +"\n))" );
			}
			else if ( act == 'updateDynamicSelect' )
				_updateDynamicSelect();
			else {	// act == 'removeDuplicates' )
				for ( var i = 0; i < duplL.length; i++ )
					deleteGrouper( duplL[ i ]);
			}
		}

		function _separate( a, fL, n ) {
		// replace all groups with members of group
		var
			_a = []
			;
			if ( !!a )
				a.map( function( v ) {
					if ( !!fL[ v[ 0 ] ] && fL[ v[ 0 ]][ 0 ][ 0 ].length ) {
						_a = [].concat( _a, fL[ v[ 0 ]].filter( _v => 
							n.indexOf( _v[ 0 ]) < 0 ));
						if ( _a.length ) {
							n.push( v[ 0 ]);
							_a = _separate( _a, fL, n );
						}
					}
					else
						_a.push( v );
				});
			return _a;
		}
		function _remove( a ) {
		// remove all duplicates an store in duplL
			if ( !!a )
				return a.sort( function( _a, _b ) {
					return _a[ 0 ] > _b[ 0 ];
				}).filter( function( e, i, _a ) {
					if ( i > 0 && _a[ i -1 ][ 0 ] == e[ 0 ]) {
						duplL.push( _a[ i -1 ][ 1 ]);
						return false;
					}
					return true;
				});
		}
		function _updateDynamicSelect() {
		var
			_n = $A( e, 'data-ul' )
			, dS = __initDynamicSelect( _n );
			;
			dS[ 1 ] = dS[ 1 ].concat( Object.keys( groupL ).map( n => __get( n )));
			dS[ 2 ] = dS[ 2 ].concat( indivL.map( n => __get( n )));
			dynamicSelects[ _n ] = [].concat( dS[ 0 ], dS[ 1 ], dS[ 2 ]);

			for ( var i = 0; i < duplL.length; i++ )	// hint duplicates
				cover.keyMsg( $( getFormDef( duplL[ i ]).first ), 'Hint', 3 );

			function __initDynamicSelect( dN ) {
				for ( var i = 0, dS = [], j = -1, a; i < dynamicSelects[ dN ].length; i++ ) {
					a = dynamicSelects[ dN ][ i ];
					if ( a.type == 'group' ) {
						j++;
						dS[ j ] = [ a ];
					}
					else if ( j == 0 )
						dS[ j ].push( a );
				}
				// initializise with default if not defined
				if ( !dS || dS.length < 3 )
					dS = lang.selfgrouper.slice( 0, 3 ).map( function( a ) {
						return [{ "type": "group", "textContent": a }];
					});
				if ( dS[ 0 ].length < 2 )
					dS[ 0 ].push( { type: "radio", textContent: lang.selfgrouper[ 3 ]
							, value: "removeActive" });
				return dS;
			}
			function __get( n ) {
				return { type: 'radio', textContent: n, value: n };		
			}
		}
	}
	function createGrouper( pre, type=false, v="" ) {
		if ( pre.style.display == 'none' ) {	// first is allways there
			pre.style.display = null;
			pre = getGrouper( pre );
			if ( type )
				$A( pre, 'type', type );
		}
		else {
		var
			e = getGrouper( pre ).cloneNode( true )
			, c = pre.cloneNode( false )
			, name 
			;
			_setNameNumber( pre );	// here name is initialized also
			$CN.remove( c, cssNS.hasCover );
			if ( type )
				$A( e, 'type', type );
			name = getGrouper( pre ).name.replace( /\[\d*\]$/
			, "["+ zeroInt( name, 5 ) +"]" );
			$A( e, 'name', name );
			$A( e, 'id', name );	// just a unique id, well name is unique
			c.innerHTML = '<label class="'+ cssNS.same +'" for="'+ name
			+'">'+ repeat( '&#x2001;', 10 ) +'</label>'+ e.outerHTML;
			c = DOM.insertAfter( pre, c );
			pre = getGrouper( c );
		}
		setValue( pre, v );
		return pre;

		function _setNameNumber( e ) {
			e = $Q( $P( e ), '[data-ul-def^="defGrp:"]', 1 );
			$A( e, 'data-ul-def', $A( e, 'data-ul-def' ).replace( /(\d+)$/
			, function( _, d ){ name = $I( d ) +1; return name; }));
		}
	}
	function setGrouperLocal( e, t, v ) {	// e == button.uLgrouper
		e = createGrouper( $P( e ).previousElementSibling, t, v );
		initViaHTML( e );
		initElement( e );
		return e;
	}

	call.uLinit( 'resizeDynamicSelect', function( n, js, org, copy ) {
		if ( org && copy )
			setEleValue( _getThisSelfgrouper( copy ), '' );
		else {	// some are deleted...
		var
			eS = _getThisSelfgrouper( n )
			, l = getGrouper( $P( eS, 3 ), null )
			, allL = _getLastActive( $A( l[ 0 ], 'data-ul' ))
			, _n = eS.name.slice( eS.name.lastIndexOf( '.' ) +1 )
			;
			js = js[ n ][ n +'[]' ].map( v => v[ _n ]);
			js = allL.filter( v => js.indexOf( v ) < 0 );
			for ( var i = 0; i < l.length; i++ ) {
				if ( js.indexOf( l[ i ].value ) > -1 )
					deleteGrouper( l[ i ]);
			}
			doActions( l[ 0 ], 'updateDynamicSelect' );		
		}

		function _getLastActive( dN ) {
			for ( var i = 0, dS = [], j = -1, a; i < dynamicSelects[ dN ].length; i++ ) {
				a = dynamicSelects[ dN ][ i ];
				if ( a.type == 'group' )
					j++;
				else if ( j > 0 )
					dS.push( a.value );
			}
			return dS;
		}
		function _getThisSelfgrouper( _n ) {
			return $Q( D, '[name^="'+ _n +'"][data-ul="selfgrouper"]', 1 );
		}
	});
	call.uLinit( 'updateDynamicSelect', function( e ) {
		e.value = e.value.trim().split( rSplit ).join( ' '+ wikiSigns.append
		+' ' );
		// 1. change all grouper value which contain selfgrouper value
		for ( var l = getGrouper( $P( e, 3 ), null )
		, i = 0, oV = getFormDef( e ).value; i < l.length; i++ )
			if ( oV && oV[ 0 ].length && l[ i ].value == oV[ 0 ])
				setValue( l[ i ], e.value.trim());
		// 2.	reinit dynamicExtend == dynamicSelect
		doActions( getGrouper( $P( e, 2 ))
		, 'updateDynamicSelect' );
	});
	call.uLinit( 'showGroupers', function( e, id ) {
	var
		_e = getGrouper( $P( e, 3 ).previousElementSibling );
		if ( $P( _e, 1 ).style.display != 'none' )
			doActions( _e, 'showGroupers' );
		else
			dialog.setMsg( 'hint', lang.selfgrouper[ 2 ], e, 3000 );
		return false;
	});

	return {
		init: function( e, fD ) {
		var
			pre = $Q( $P( e ).previousElementSibling
			, 'input,select,textarea', 1 )
			;
			if ( !$CN.contains( e, 'uL_ignore' )) {
				if ( !pre || !/\[\d*\]$/.test( pre.name ))
					console.warn(
						"useLib hint: element (name:" + pre.name + ") is extend to "
						+ "grouper array the name needs to be ended with '[]'!"
					);
				else {
					$A( pre, 'data-ul-grp', "isGrouper" );
					$CN.add( e, 'uL_ignore' );
					if ( !$A( e, 'data-ul-def' )) {
						$CN.add( e, cssNS.grouper );
						$A( $P( e ), 'title', lang.add[ 0 ] );
						$A( e, 'data-ul-def', 'defGrp:'+ getFormDef( pre ).type +':0' );
					}
					if ( fD.type == 'selfgrouper' ) {
						if ( pre.innerHTML.indexOf( '>none<' ) > -1 )
							$P( pre ).style.display = 'none';
						$A( getSelfGrouper( e, 1 ), 'onchange'
						, "useLib.call.updateDynamicSelect(this);" );
						_initInformer( e );
					}
				}
			}
			if ( fD.type == "selfgrouper" ) {
				if ( extTimeout )
					clearTimeout( extTimeout );
				extTimeout = setTimeout( function() {
					extTimeout = false;
					useLib.call.updateDynamicSelect( getSelfGrouper( e, 1 ));
					$A( $P( e, 3 ), 'onresize', 'useLib.call.resizeDynamicSelect' );
					for ( var i = 0, l = $Q( e, 'select[data-ul-grp="isGrouper"]' ), v
					; i < l.length; i++ )
						if ( v = $A( l[ i ], 'value' ))
							$( getFormDef( l[ i ]).first ).value = v;
				}, 333 );
			}

			function _initInformer( e ) {
				e = $Q( $P( e, 1 ), 'button.'+ cssNS.informer, 1 );
				$A( $P( e, 1 ), 'title', lang.show[ 0 ]);
				$A( e, 'onclick', 'return useLib.call.showGroupers(this);' );
			}
		}
		, doButton: function( ev, e ) {
			e = setGrouperLocal( e, $A( e, 'data-ul-def' ).slice( 7
			).replace( /:\d+$/, '' ), '' );
			if ( e.nextElementSibling && e.nextElementSibling.firstChild )
				e.nextElementSibling.firstChild.click();
		}
		, setGrouper: function( e, v, type ) {// for setJSONValues
			for ( var i = 0; i < v.length; i++ )
				e = createGrouper( $P( e ), type, v[ i ]);
		}
		, deleteAllGrouper: deleteAllGrouper	// for setJSONValues
		, changeGrouper: function( e, v ) {	// for metaReaction.dynamicExtend
		var
			r = false
			;
			switch( v ) {
				case 'separateActive':
				case 'removeDuplicates':
				case 'overviewEntry': doActions( e, v ); break;
				case 'removeActive': deleteGrouper( e ); break;
				case 'removeAll':
					dialog.confirm( 'confirmMsg', function( v ) {
						if ( v === true ) {
							deleteAllGrouper( $P( e, 2 ));
							doActions( e, 'updateDynamicSelect' );
						}
					});
					break;
				default:
					setValue( e, v );
					doActions( e, 'updateDynamicSelect' );
					r = true;
					break;
			}
			return r;
		}
		, getDynamicValues: function( e, dL ) {	// for metaReaction.dynamicExtend
			doActions( e, 'updateDynamicSelect' );
			return dynamicSelects[ $A( e, 'data-ul' )];
      }
		, disableActive: function( e, dL ) {	// for metaReaction.dynamicExtend
		//@ disables active extend in dynamicSelect for dialog box
		//@ REMARK: must be done with a copy else the disabled remains...
		var
			n = getSelfGrouper( e, 1 )
			, _n
			;
			if (( n = $A( n, 'value' )) && n && ( n = n.trim()).length ) {		
				dL = JSON.parse( JSON.stringify( dL ) // copy dynamicSelect
				).map( function( _e ) {
					if ( _e.value && _e.value == n )
						_e.disabled = 'disabled';
					return _e;
				});
			}
			return dL;
		}
	}
})();
metaReaction.selfgrouper = metaReaction.grouper;
//@ end of metaReaction.grouper

/*****
 *
 *		functions to create cover div for elements that need to be more useable
 *		@ There are a sub types defined, that have their special usability behavior
 *		@ based on knowlege of cognitive psychologie, user interviews and
 *		@ experimental interaction. The cover div is place above the original
 *		@ element, that is invisible, but still fully in function in the original
 *		@ code, all values set to the original are already checked for errors or
 *		@ it's (almost) impossible for the user to make a mistake.
 *		@
 *		@ * Wrong keys are filtert and shaking reported directly. 
 *		@ * a formatted input sequence is created, where special signs are used to
 *		@ 	 separate the input fields to create a directly understandable view of
 *		@ 	 therequested value
 *		@ * If those (and simular) formatting chars are entert by the user, they
 *		@	are used to jump in the next input of sequence.
 *		@ * A maximum of chars is also used for a auto tab into the next sequence
 *		@	element(this is not the same as maxlength because this must be bigger
 *		@ 	to support paste)
 *		@ * and (no rule with exception...) in some for users very difficult input
 *		@	tasks like long (technical based) identification numbers i. e. IBAN
 *		@	are spaces allowed, that may exceed that maximum of chars. As
 *		@	compromise those additional spaces removed (later) to insure the
 *		@	correct value format...
 *		@ * Conform to user expectations it is possible to use the cursor etc.
 *		@	between the sequence input fields
 *		@ * Input of the full original value (paste, drop) to the first input of
 *		@	 sequence is splited up and set to its position in sequence
 *		@ * For complex or difficult user decissions some metaReaction like 
 *		@	datepicker as tree month calendar are created and reachable by
 *		@	generated click fields
 *		@ * The content of any sequence fields can be deleted by shift+del or
 *		@	click on theclearer symbol
 * 
 *		@ Technical there is used a three step hierachie: MetaTypes, Types and 
 *		@ SubTypes (seeabove). MetaTypes are a combination of Types for example
 *		@ 'datetime' is createt form date and time. Types are like the HTML5 types
 *		@ (they are partial includes/supportet). SubTypes are featured as
 *		@ described. A special SubType is 'pattern', a (([x]{a,b})) regexp where
 *		@ x the allowed chars and a,b the minimal and maximal number of chars. It
 *		@ will be treated inside a formated cover allmost like the predefined
 *		@ subTypes except special test and some special behaviors. Well, the
 *		@ x-part of the pattern type may start with [0-9], ^-?[0-9] or (0|[0-9]),
 *		@ this are almost the subType idents for numbers, integers and zero
 *		@ leading numbers (the + sign is replaced) here are tests active and it
 *		@ is possible to stear the length of the cover input.
 *****/
var cover = ( function() {
var
	dynPattern = 0
	, normYear = $D.y() + conf.fullYearFuture
	, smallYear = $D.y() + conf.smallYearFuture - 2000	// well, there will be a day it fails
	, sChar = {
		empty: { c: '\u200B', r: '/\u200B/g' }
		, sSpace: { c: '\u2006', r: '/\u2006/g' }	// not transfered in e.value (SIX-PER-EM SPACE)
		, lSpace: { c: '\u2003', r: '/\u2003/g' }	// (EM SPACE)
	}
	, getYear = function( fD, check ) {
	//@ gets year as int
	var
		y = subValue( fD, 'SMALLYEAR' )
		;
		if ( y )
			return y + ( y > smallYear ? 1900 : 2000 );
		else {
			y = subValue( fD, 'YEAR' );
			return check && y < fD.subs.YEAR.check.min ? 0 : y;
		}
	}
	, weekCalc = function( fD, eC, w ) {
	var
		y = getYear( fD, 1 )
		, max = 53
		, d = ''
		;
		if ( y ) {
			if ( w > 52 && !$D.checkWeek53( y ))
				max = 52;
			fD.subs.WEEK.check.max = max;
			d = $D.getWeekDate( w > max ? max : w, y );
			d = getLocal( 'date', $D.toISO( d )) + ' > '
			  + getLocal( 'date', $D.toISO( $D.add( d, 'd', 6 )));
		}
		eC.title = d;
		return w;
	}
	, dayCalc = function( fD, eC, d, s ) {
	var
		days = { 2: 28, 4: 30, 6: 30, 9: 30, 11: 30 }
		, m = subValue( fD, 'MONTH' )
		, y = getYear( fD, 1 )
		, max = 31
		;
		if ( m && days[ m ] && d > days[ m ]) {
			if ( m == 2 )			// check for leap year?
				max = !y || $D.checkLeapYear( y ) ? 29 : 28;
			else
				max = days[ m ];
		}
		fD.subs.DAY.check.max = max;
		if ( d > max )
			errorList.add( fD, 'dayError' );
		setFree( fD.id, 0, y && m && d && d <= max ? $D.dayName([ y, m, d ]) + ',' : '' );
		return d;
	}
	, yearCalc = function( fD, eC, y, s ) {
	//@ sets two count years to life span years to help users on less input...
	//@ REMARK: not on steps, only on checkValue as onblur
		if ( s == 0 && ( y < 100 && fD.subs.YEAR.check.min > 999 )) {
			y += ( y > smallYear ? 1900 : 2000 );
			if ( !!eC )
				errorList.add( fD, 'yearError' );
		}
		return y;
	}
	, stepCalc = function( fD, eC, v, step ) {
	var
		sC = fD.getSub( eC.id ).check
		, vStore = v
		;
		if ( !fD.step )												// standard step do nothing
			return v;
		else {
		var
			subI = fD.subs.INT || false
			, subF = fD.subs.FIX || false
			, isFix = subF && eC.id == subF.id
			, _eC
			;
			if ( !!step && Math.abs( step ) < sC.step )				// step not realy big step?
				v += (( step < 0 ? -sC.step : sC.step ) - step );	// calc analyzed step
			if ( isFix ) {
			//@ decimal places have a implemented carry over, but if the defined step is bigger
			//@ than 1., its nessesary to trigger it 'by hand'.
				v = !step || v < sC.min || v > sC.max || fD.step <= 1. ? v
				: v + ( sC.max + 1 ) * ( step < 0 ? -1 : 1 );
			}
			else {
				eC.value = v;
				v = parseFloat(
					( subValue( fD, 'INT' )
					+ ( !isFix || !subI || step == 0 ? 0 : subI.check.step * ( step < 0 ? -1 : 1 )))
					+ '.' + ( subF ? $( subF.id ).value : 0 ) - ( fD.stepZeroshift || 0 )
				);
				v = Math.round( v / fD.step ) * fD.step + ( fD.stepZeroshift || 0 );
				// incorrect cause of rounding errors: v -= ( v % fD.step ) + fD.stepZeroshift;
				v = v.toFixed(	subF ? subF.check.maxCount : 0 ).split( '.' );
				if ( subI && subF ) {
					_eC = $(( isFix ? subI : subF ).id );
					if ( _eC.value != ( isFix ? v[ 0 ] : v[ 1 ]))
						keyMsg( _eC, 'Hint' );
					_eC.value = isFix ? v[ 0 ] : v[ 1 ];
				}
				v = isFix ? v[ 1 ] : v[ 0 ];
			}
			if ( step == 0 && v != vStore )
				keyMsg( eC, 'Hint' );
			return v;
		}
	}
	, subType = {
	//@ ATTENTION: must be defined in upper case
	//@ may be followed by { x, y } to define x = min/maxCount [2]
		PASSIVE: { ident: "\\0" }			// for inactive input as needed for select lists
		, WEEK: { ident: "[1-5][0-3]|0?[1-9]", min: 1, max: 53, zero: 1, jump: 1
			, carry: 'YEAR', extra: weekCalc }
		, DAY: { ident: "[12][0-9]|3[01]|0?[1-9]", min: 1, max: 31, offer: $D.d(), zero: 1
			, jump: 1, carry: 'MONTH', extra: dayCalc }
		, MONTH: { ident: "1[012]|0?[1-9]", min: 1, max: 12, offer: $D.m(), zero: 1, jump: 1
			, carry: 'YEAR' }
		, YEAR: { ident: "(1|2)?\\d{1,3}", min: normYear - 130, max: normYear, offer: $D.y()
			, jump: 1, extra: yearCalc }
		, SMALLYEAR: { ident: "\\d\\d", min: 100 - smallYear, max: smallYear
			, offer: $D.y() - 2000, zero: 1, jump: 1 }
		, HOUR: { ident: "1[0-9]|2[0-3]|0?[0-9]", min: 0, max: 23, offer: 9, zero: 1, jump: 1
			, carry: 'DAY' }
		, MINUTE: { ident: "[0-6]?[0-9]", min: 0, max: 59, zero: 1, jump: 1, carry: 'HOUR' }
		, SECOND: { ident: "[0-6]{0,1}[0-9]", min: 0, max: 59, zero: 1, jump: 1
			, carry: 'MINUTE' }
		, SECONDS: { ident: "0{0,1}[0-6]{0,1}[0-9]", min: 0, max: 59, zero: 1, jump: 1
			, carry: 'MINUTES' }
		, MINUTES: { ident: "0{0,1}[0-6]?[0-9]", min: 0, max: 59, offer: 15, zero: 1, jump: 1
			, carry: 'HOURS' }
		, HOURS: { ident: "0{0,1}[0-9]|1[0-9]|2[0-3]", min: 0, max: 23, offer: 9, zero: 1
			, jump: 1, carry: 'DAYS'	}
		, DAYS: { ident: "[0-9]{0,3}", min: 1, max: 365, offer: 1  }
		, PSECONDS: { ident: "(0{0,1}[0-6]{0,1}[0-9])?", min: 0, max: 59, zero: 1, jump: 1
			, carry: 'PMINUTES' }
		, PMINUTES: { ident: "(0{0,1}[0-6]?[0-9])?", min: 0, max: 59, offer: 15, zero: 1
			, jump: 1, carry: 'PHOURS' }
		, PHOURS: { ident: "(0{0,1}[0-9]|1[0-9]|2[0-3])?", min: 0, max: 23, offer: 9, zero: 1
			, jump: 1, carry: 'PDAYS'	}
		, PDAYS: { ident: "([0-9]{0,3})?", min: 1, max: 365, offer: 1  }
		, PYEAR: { ident: "(2?\\d{1,3})?", min: normYear - 130, max: normYear, offer: $D.y()
			, jump: 1, extra: yearCalc }
		, INT: { ident: "-?[0-9]", min: -999999, max: 999999, extra: stepCalc }
		, ZERO: { ident: "([00-9])", min: 1, max: 999999, zero: 1 }
		, FIX: { ident: "([0-90]+)", min: 0, max: 99, zero: -1, carry: 'INT'
			, extra: stepCalc }
		, PINT: { ident: "(-?[0-9])", min: -999999, max: 999999, extra: stepCalc }
		, PFIX: { ident: "(([0-90]+))", min: 0, max: 99, zero: -1, carry: 'PINT'
			, extra: stepCalc }
		, NUM: { ident: "[0-9]", min: 1, max: 9999999999 }
		, PASSWORD: { ident: ".{0,5}", min: 0, max: 99999, jump: 1 }
		, IBAN: { ident: "[ IBA-Z0-9]", jump: 1, space: 1, upper: 1 }
		, COUNTRY: { ident: "[COA-Z]", jump: 1, upper: 1 }
		, HEX: { ident: "[ABCDEFabcdef0-9]", jump: 1, lower: 1 }
		, UPPER: { ident: "[ UPA-Z]", jump: 1, space: 1, upper: 1 }
		, LOWER: { ident: "[ loa-z]", jump: 1, space: 1, lower: 1 }
		, UPDIGIT: { ident: "[ UPA-Z0-9]", jump: 1, space: 1, upper: 1 }
		, LOWDIGIT: { ident: "[ loa-z0-9]", jump: 1, space: 1, lower: 1 }
	}
	, subTypeAttr = str2obj( 'carry zero jump space upper lower min max extra offer' )
	, types = {
	//@ for calculations via cover automatics, all subTypes have to be unique!
	//@ ATTENTION1: must be defined in lower case AND the double backslashes have
	//@ to be for RegExp use
	//@ ATTENTION2: if you create a new type, don#T forget to declare it in the
	//@ configuration part under types: to insure the recognition of it! 
		singleSelect: [ 'PASSIVE', 'SingleSelect' ]
		, multiSelect: [ 'PASSIVE', 'MultiSelect' ]
		, dynamicSelect: [ 'PASSIVE', 'DynamicSelect' ]
		, dynamicExtend: [ 'PASSIVE', 'DynamicExtend' ]
		, toggleSelect: [ 'PASSIVE', 'ToggleSelect' ]
		, file: [ 'PASSIVE', 'File' ]
		, email: [ '[mm]', '{1,220}', '\\@', '[mm]', '{1,256}' ]
		, fon: [ '\\+|00|\\(\\+\\)', '[0-9]', '{2,5}', 'World', '((0) |(0)| )?'
			, '\\(|\\/|-|\\.|,', '[0-9 ]', '{1,10}', '\\) |\\)|\\/|-|\\.|,'
			, '[0-9 ]', '{2,10}', '-|\\.|,|\\/', 'NUM', '{0,5}' ]
		, number: [ 'INT', '{1,12}', 'Annex' ]
		, float: [ 'INT', '{1,7}', '\\.|,|;|:|_', 'FIX', '{1,2}', 'Annex' ]
		, ifloat: [ 'INT', '{1,7}', '\\.|,|;|:|_', 'FIX', '{1,2}' ]
		, pfloat: [ 'PINT', '{0,7}', '(\\.|,|;|:|_)?', 'PFIX', '{0,2}' ]
		, towns: [ 'COUNTRY', '{2,2}', 'World', ';|-| |,|\\u2009'
			, '[0-9A-Z-]', '{0,10}', ';|,| |\\u2006', '[\\w\\W]', '{3,50}' ]
		//@ example: towns predefined as part of JSON struct: needs
		//@ .replace(/\\"/g, '"') after JSON.stringify... 
		//, towns: [ '{country:"|\\u200B', 'COUNTRY', '{2,2}', 'World', 'Free0'
		//, '",postal:"|-| |,|;|\\u2006', '[0-9A-Z-]'
		//, '{0,10}', '",town:"|;|,| |\\u2006', '[\\s\\S]', '{3,50}'
		//, '("}|\\u200B)?' ]
		, country: [ '[ A-Za-z-e-]', '{5,46}', 'World' ]
		, list: [ '[\\w\\W]', '{2,80}' ]
		, grouper: [ 'Grouper' ]
		, selfgrouper: [ 'Grouper', 'Free1', 'Informer' ]
      , title: [ '([Ladies and Gentlemen#n|Mrs.#f|Mr.#m|Ms. dr.#f|Mr. dr.#m'
         + '|Ms. prof.#f|Mr. prof.#m|ownInput])' ]
		, clock: [ '([time|alarm|stop|count|fixit])?', 'Free0', 'PHOURS', ':|\\.|,|;|-|_'
			, 'PMINUTES', ':|\\.|,|;|-|_', 'PSECONDS', 'Free1', 'Clock' ]
		, currency: [ 'ifloat', 'Free3', 'Toggle', 'pfloat', 'Free5', 'World' ]
		, unit: [ 'ifloat', 'Free4', 'Toggle', 'pfloat', 'Free0', '([mm|mm])?' ]
		, isounits: [ 'ifloat', 'Free4', 'Toggle', 'pfloat', 'Free0', '([mm|mm])?'
			, 'Free1', 'Unit' ]
		, vat: [ 'float', ';|#|_|\\u0024\\u2006', '(['+ lang.ownInputExtend.vat
			+'])', 'Free2', 'Toggle', 'pfloat', 'Annex', '(\\u0024)?' ]
		, net: [ 'float', ';|#|_|\\u0024\\u2006', '(['+ lang.ownInputExtend.vat
			+'])', 'Free2', 'Toggle', 'pfloat', 'Annex', '(\\u0024)?' ]
		, password: [ 'PASSWORD', 'PASSWORD', 'PASSWORD', 'PASSWORD', 'PASSWORD'
		, 'Eye', 'Free0', 'Free5' ].concat( !!conf.puzzlePassword
			? [ 'Password', 'Free0' ] : 'Free0' )
		, passwordrepeat: [ 'PASSWORD', 'PASSWORD', 'PASSWORD', 'PASSWORD'
		, 'PASSWORD', 'Eye', 'Free0', 'Free5' ].concat( !!conf.puzzlePassword
			? [ 'Password', 'Free0' ] : 'Free0' )
		, iban: [ 'COUNTRY', '{2,2}', 'World', 'Free0', 'IBAN', '{2,2}', 'Free0'
			, 'IBAN', '{4,15}', 'Free0', 'IBAN', '{5,20}' ]
		, salesident: [ '([not europe|DE])', 'Free0', '[A-Z0-9]', '{5,15}' ]
		, color: [ '#', 'HEX', '{6,6}', 'PASSIVE', '{0,6}' ]
		, date: [ 'Free5', 'YEAR', '-|\\.|,|;|:|_|\\/', 'MONTH', '-|\\.|,|;|:|_|\\/', 'DAY'
			, 'Calendar' ]
		, enddate: [ 'date', 'End' ]
		, smalldate: [ 'Free5', 'SMALLYEAR', '-|\\.|,|;|:|_|\\/', 'MONTH', '-|\\.|,|;|:|_|\\/'
			, 'DAY', 'Calendar' ]
		, endsmalldate: [ 'smalldate', 'End' ]
		, week: [ 'Free3', 'YEAR', '-W|-|\\.|,|;|:|_|\\/', 'WEEK', 'Calendar' ]
		, time: [ 'HOUR', ':|\\.|,|;|-|_', 'MINUTE', 'Time' ]
		, smalltime: [ 'HOUR', ':|\\.|,|;|-|_', 'MINUTE', ':|\\.|,|;|-|_', 'SECOND', 'Time' ]
		, endtime: [ 'time', 'End' ]
		, endsmalltime: [ 'smalltime', 'End' ]
		, datetime: [ 'date', 'Free0', 'T| |\\.|,|:|;|-|_|\\u2004\\u25F7', 'time' ]
		, smalldatetime: [ 'smalldate', 'T| |\\.|,|:|;|-|_|\\u2004\\u25F7', 'time' ]
		, enddatetime: [ 'datetime', 'End' ]
		, endsmalldatetime: [ 'smalldatetime', 'End' ]
		, daysperiod: [ 'DAYS', 'Free1', 'Days' ]
		, datedaysperiod: [ 'date', '(D|\\.|,|:|;|-|_|\\u2003)?', 'daysperiod' ]
		, timeperiod: [ 'HOURS', ':|\\.|,|;|-|_| ', 'MINUTES', 'Free1', 'Days' ]
		, smalltimeperiod: [ 'HOURS', ':|\\.|,|;|-|_| ', 'MINUTES', ':|\\.|,|;|-|_'
			, 'SECONDS', 'Free1', 'Days' ]
		, timedaysperiod: [ 'DAYS', '#|\\.|,|:| |;|-|_|\\u2004\\u25F7', 'HOURS'
			, ':|\\.|,|;|-|_| ', 'MINUTES', 'Free1', 'Days' ]
		, timeperiodpseudo: [ 'PHOURS', ':|\\.|,|;|-|_| ', 'PMINUTES', 'Free1', 'Days' ]
		, datedaysperiodpseudo: [ 'date', 'Free0', '(D|\\.|,|:|;|-|_|\\u2003)?', 'PDAYS'
			, 'Free1', 'Days' ]
		, weekpseudo: [ 'Free3', 'PYEAR', '-W|-|\\.|,|;|:|_|\\/', 'WEEK', 'Calendar' ]
	}

	//@ a special trick is used: the very private 32 noncharacters of unicode:
	//@ U+FDD0..U+FDEF The verifying regexp will never find one of those chars
	//@ and they are not required because of the following '?'.
	//@ ATTENTION: to decide from  types, the first letter must be defined in upper case
	, subAddon = {  	// [empty]
		SingleSelect: '\\uFDD0?'
		, MultiSelect: '\\uFDD1?'
		, Calendar: '\\uFDD2?'
		, World: '\\uFDD3?'
		, File: '\\uFDD4?'
		, Toggle: '\\uFDD5?'
		, Days: '\\uFDD6?'
		, Fixpin: '\\uFDD7?'
		, Clock: '\\uFDD8?'
		, Switch: '\\uFDD9?'
		, Unit: '\\uFDDA?'
		, Password: '\\uFDDB?'
		, ToggleSelect: '\\uFDDC?'
		, Eye: '\\uFDDD?'
		, Time: '\\uFDDE?'
		, DynamicSelect: '\\uFDDF?'
		, DynamicExtend: '\\uFDE0?'
		// special treated free spaces for addition display
		, Free0: '\\uFDE1?'
		, Free1: '\\uFDE2?'	// to create a span for dynamic use: number = spaces
		, Free2: '\\uFDE3?'
		, Free3: '\\uFDE4?'
		, Free4: '\\uFDE5?'
		, Free5: '\\uFDE6?'
		, Free6: '\\uFDE7?'
		// special treated input elements
		, End: '\\uFDE8?'
		, Grouper : '\\uFDE9?'
		, Informer: '\\uFDEA?'
		, Annex: '\\uFDEB?'
	}
	, identifier = ( function(){	// initialize ones for faster standardization
	var
		ident = {}
		;
		for ( var t in subType )
			ident[ subType[ t ].ident ] = t;
		for ( var t in subAddon )
			ident[ subAddon[ t ]] = t;
		return ident;
	})()
	, getTypeList = function( t, format ) {
	//@ Initializes the RegExp for the test of the whole value to return at end
	//@ and returns the (possible local ordered) struct list of the given type or metaType
		if ( !types[ t ])
			return false;
		else if ( !types[ t ][ format ]) {
			if ( !types[ t ].regexp ) {
			var
				regFormat  = conf.inputIsLocal ? 'local' : 'norm'
				, list = _getList( t, regFormat, true )
				;
				// 1. getList for RegExp depending on conf.
				types[ t ].regexp = new RegExp(( '(('+ list.join( '))((' ) + '))'
					).replace( /\(\((.*?)\)\)/g
						, function( _, t ) {
							return '((' 
								+ ( subType[ t ] ? subType[ t ].ident : subAddon[ t ]
								|| t.replace( /ownInput/g, '(.+)' )) + '))';
						}
					).replace( /\)\)\(\(\{/g, '{'	// no round brackets before size...
				));
				// 2. get struct list for regFormat (it's for at least for setValue)
				types[ t ][ regFormat ] = initTypeList( list );
			}
			if ( !types[ t ][ format ])
				// 3. get possible other struct list for asked format
				types[ t ][ format ] = initTypeList( _getList( t, format ));
		}
		return types[ t ][ format ];

		function _getList( t, format, REPL=false ) {
		var
			tL = format === "local" ? lang.localTypes[ t ] || types[ t ] : types[ t ] || false
			, rL = []
			;
			if ( tL ) {
				for ( var i = 0; i < tL.length; i++ ) {
					if ( types[ tL[ i ]])
						rL = rL.concat( _getList( tL[ i ], format ));
					else
						rL.push( REPL && tL[ i ][ 0 ] == '[' ? replaceShortn( tL[ i ])
						: tL[ i ]);
				}
			}
			return rL;
		}
	}
	, initTypeList = function( l ) {
	//@ returns the struct list of the given type or metaType array definition
	var
		unique = {}
		, tL = []
		, last = 0
		;
		for ( var i = 0, m; i < l.length; i++ )
			_analyze( l[ i ]);
		return tL;

		function _analyze( t ) {
			if ( subAddon[ t ] )		// first letter to lower case
				tL.push( {
					subAddon: t.charAt( 0 ).toLowerCase() + t.slice( 1 )
					, cN: tL[ last ] && !tL[ last ].size ? tL[ last ].type : '' });
			else {
			var
				m = false
				, p = t.match( /^(\(?)(\[)?((([^\|]+\|)+[^\|]+)|(.+))\](\)(\?)?)?$/ )
				;
				if ( !subType[ t ] && !p  && t.charAt( 0 ) != '[' ) {
					if ( !tL[ last ])
						tL[ last ] = { type: false };
					if ( m = t.match( /\{(\d+),(\d+)\}/ ))	// it's a sizer?
						tL[ last ].size = { min: $I( m[ 1 ]), max: $I( m[ 2 ]) };
					else {							// it's a separator, also for pseudos
						m = t.replace( /^\((.*)\)\?/, '$1' ).split( '|' );
						tL[ last ].regexp = new RegExp ( '(' + m.join( '|' ) + ')' );
						tL[ last ].joinStr = ( m[ 0 ]).replace( /\\/g, '' );// unescape
						m = m.pop();				// last == utf ? => display
						tL.push( {
							sepa: m.slice( 0, 2 ) == '\\u' ? unescapeUnicode( m ) : tL[ last ].joinStr
							, cN: tL[ last ] && tL[ last ].id != 'SELECT'
							&& !tL[ last ].size ? tL[ last ].type : ''
						});
					}
				}
 				if ( !m ) {		
					last = tL.length;
					if ( p ) {	// found ([...]) structure => type == SELECT or PATTERN
						t = p[ 1 ] ? p[ 3 ] : p[ 0 ];
						p = ( p[ 8 ] ? 'P' : '' ) + ( p[ 1 ] && p[ 4 ] ? 'SELECT'
						: 'PATTERN' );
					}
					else
						p = t;
					tL.push( { type: t, /*regexp: p,*/ size: false, sepa: false });
					if ( unique[ p ])		// ticket if not unique
						p += unique[ p ]++;
					else
						unique[ p ] = 1;
					tL[ last ].id = p;
				}
			}
		}
	}
	, checkValue = function( fD, eC, v, step, setMsg ) {
	//@ checks and corrects values of subTypes on the definied parameters
	var
		sC = fD.getSub( eC.id ).check
		;
		if ( sC.isdigit ) {
			v = $I( v ) + step;		// standard add step (may be substracted in sC.extra)
			v = sC.extra( fD, eC, v, step );
			if ( v < sC.min || v > sC.max ) {
				if ( sC.carry && _carryCheck( fD.subs[ sC.carry ])) {
					delayRemove( eC, cssNS.keyCarry, 1 );
					if ( sC.step > 1 )
						v += v < sC.min ? sC.max + 1 : -sC.max - 1;
					else
						v = v < sC.min ? sC.max : sC.min;
				}
				else {
					errorList.add( fD, fD.type.indexOf( 'daysperiod' ) > -1
						? 'daysPeriod' : 'rangeError' );
					v = v < sC.min ? sC.min : sC.max;
				}
			}
			sC.value = v;
		}
		else
			v = sC.extra( fD, eC, v, step );
		v = sC.zero ? zeroInt( v, sC.maxCount ) : String( v );
		if ( !setMsg ) {
			if ( !sC.carry && errorList.length )
				// day and year are hints because they are corrected
				keyMsg( eC, sC.type == 'DAY' || sC.type == 'YEAR' ? 'Hint' : 'Error' );
			else if ( !sC.jump && fD.regexp.test( v )) {
				activeOnChange = false;		// onchange shall not write the cover again!
				$( fD.id ).value = v;
				activeOnChange = true;		// onchange shall not write the cover again!
			}
			errorList.length = 0;
		}
		return v;

		function _carryCheck( eSub ) {
		//@ checks if there is a field to carry over and init's it or add a step.
			if ( eSub ) {
			var
				_eC = $( eSub.id )
				, _v = _eC.value
				, offer = subType[ sC.carry ].offer
				;
				keyMsg( _eC, 'Carry' );
				_eC.value = _v == '' && offer ? offer
					: checkValue( fD, _eC, $I( _v ), v < sC.min ? -1 : 1, setMsg );		
				sC.extra( fD, eC, sC.max );	// could be a new sC.max, i.e. on DAY!
			}
			return !!eSub;
		}
	}
	, getLocal = function( t, vA ) {
	var
		tLn = getTypeList( t, 'norm' )	// 1. step: separate v into subType struct
		, tLl = getTypeList( t, 'local' )
		, lastJoin = ''
		, v = ''
		;
		if ( vA.length && tLn.length && JSON.stringify( tLn ) !== JSON.stringify( tLl )) {
			vA = getVsub( false, t, vA );
			for ( var i = 0; i < tLl.length; i++ )
				if ( tLl[ i ].id && vA[ tLl[ i ].id ]) {
					v += lastJoin + vA[ tLl[ i ].id ];
					lastJoin = tLl[ i ].joinStr || '';
				}
		}
		else
			v = vA;
		return v;
	}
	, getValue = function( fD, t, format, setMsg ) {
	//@ PARAM formDef, typeOfValue (also a part of metaType), formatToReturn, msgFlag
	//@ returns the formated value as standard 'norm' or 'local' or empty string
	//@ REMARK: if no value in parts of the formated string, its marked with sChar.empty.c
	if ( fD.type.indexOf( 'password' ) > -1 )
		return fD.value[ 0 ];
	var
		tL = getTypeList( t, format )
		, hasValue = false
		, hasEmpty = false
		, v = ''
		;
		if ( tL.length ) { 
			for ( var i = 0, s = '', eC; i < tL.length; i++ ) {
				if ( tL[ i ].id
				&&( tL[ i ].id.charAt( 0 ) != 'P' || tL[ i ].id.indexOf( 'PATTERN' ) > -1 )
				&& ( eC = $( fD.subs[ tL[ i ].id ].id ))) {
					v += s + _getValue(); // + ( tL[ i ].joinStr || '' );
					s = ( tL[ i ].joinStr || '' );
				}
				else if ( i == 0 && tL[ i ].joinStr )	// may start with fix signs
					s = tL[ i ].joinStr;
			}
			fD.hasEmpty = hasEmpty;
		}
		return hasValue ? v : '';

		function _getValue() {
		var										// could be select, if check is not set
			_v = tL[ i ].id.indexOf( 'SELECT' ) > -1 ? eC.value : __getCoverValue( eC.value )
			;
			if ( _v.length ) {
				if ( format == 'norm' && tL[ i ].type == 'SMALLYEAR' )
					_v += _v > smallYear ? 1900 : 2000; 
				//else if ( tL[ i ].type == 'INT' )
					//_v = _v.replace( sChar.sSpace.r, '' );
				hasValue = true;
			}
			else {
				hasEmpty = true;
				_v = sChar.empty.c;
			}
			return _v;

			function __getCoverValue( __v ) {
			//@ some values depend on others, so they have to be checked all the time
				if ( __v.length ) {
					__v = checkValue( fD, eC, __v, 0, setMsg );
					if ( compare( __v, eC.value ) !== 0 )
						eC.value = __v;
				}
				return fD.getSub( eC.id ).check.space ? __v.replace( /\s/g, '' ) : __v;
			}
		}
	}
	, getVsub = function( fD, t, v, format, _try ) {
	//@ PARAM formDef, typeOfValue (also a part of metaType), valueToSet, formatOfValue ['norm']
	//@ returns the formated value depending to the parameters
	//@ REMARK the parameter try is intern
	var
		vS = v ? String( v )	: ''									// only if a value is given
		, tL = !vS.length ? [] : getTypeList( t, format || 'norm' )
		, vL = {}
		;
		if ( vS.length && fD && t == fD.type && fD.regexp && !fD.regexp.test( vS ))
		// faultily defaults?
			vS = false;
		else
			v = vS;
		if ( fD && fD.getSub( fD.first ).type == 'PASSIVE' )
			vL.PASSIVE = v;
		else if ( vS && vS.length && tL.length ) {
		// 1. step: separate v into subType struct (norm or local depending on conf.)
			for ( var i = 0; i < tL.length; i++ ) {
				vS = tL[ i ].regexp ? v.search( tL[ i ].regexp ) : -1;
				if ( tL[ i ].id ) {
					if ( vS > -1 ) {							// separator found?
						vL[ tL[ i ].id ] = v.slice( 0, vS );
						v = v.slice( vS + 1 );
					}
					else {	// also try if no separator, on special formated strings...
						now = vS = ( subType[ tL[ i ].type ]);
						vS = v.match( '^' + ( vS ? vS.ident : tL[ i ].type
						).replace( /\]$/, ']+' ));
						vL[ tL[ i ].id ] = vS ? vS[ 0 ] : v;
						if ( vS )
							v = v.slice( vS[ 0 ].length );
						else
							break;
					}
				}
				else if ( vS === 0 )						// could start with a separator
					v = v.replace( tL[ i ].regexp, '' );
			}
		}
		else if ( v && v.length ) {
			if ( subType[ t ])
				vL[ t ] = v;
			else if ( !!_try )
				console.warn(
					"useLib hint: element (id:" + fD.id + ") value has wrong format:"
					+ v.slice( 0, -2 ) + '!'
				);
			else	// failure tolerance: often missing zeros (even parseFloat may return a integer)
				return getVsub( fD, t, v + '.0', format, true );
		}
		return vL;
	}
	, setValue = function( fD, t, v, format ) {
	//@ PARAM formDef, typeOfValue (also a part of metaType), valueToTranfer, formatOfValue
	//@ REMARK: it is important to decide the possible different local formats
	var
		vL = getVsub( fD, t, v, format )	// 1. step: separate v into subType struct
		, eC
		;
		for ( t in vL ) {					// 2. step: write value into cover elements
			if ( fD.subs[ t ] && !!( eC = $( fD.subs[ t ].id ))) {
				if ( t == 'SELECT' )		// use setEleValue, could be ownInput
					setEleValue( eC, [ vL[ t ]]);
				else
					eC.value = checkValue( fD, eC, vL[ t ], 0 );
			}
		}
	}
	, extendFormDef = function( fD ) {
		fD.ids = [];
		fD.subs = {};
		fD.check = {};
		fD.first = null;
		fD.getId = function( t ) {	// type
			t = this.subs[ t ] || this.subs[ 'P' + t ] || this.subs[ 'SMALL' + t ];
			return !!t ? t.id : false;
		};
		fD.getSub = function( id ) {
			if ( id && id.length ) {
				if ( id == this.id )
					return this;
				else
					return this.ids[ id ] ? this.subs[ this.ids[ id ]] : false;
			}
			return false;
		};
		fD.initSub = function( id, type ) {
		var
			last = this.ids.shift()		// ...get remember
			;
			if ( last )
				this.getSub( last ).next = id;
			this.ids.push( id );			// just to remember...
			this.ids[ id ] = type;
			this.subs[ type ] = { id: id, type: type, next: false, before: last || false
				, check: { type: type, id: id, regexp: false
					, extra: function( _, __, v ) { return v; }
				}
			};
			if ( !this.first )
				this.first = id;
			this.last = id;				// overwrite last
			return this.subs[ type ];
		};
		return fD;
	}
	, getMetaReaction = function( fD ) {
	//@ date and period selector may appear in many different forms, even via not
	//@ predefined types, therefore it is checked for the subTypes
	var
		s = fD.subs
		;
		return metaReaction[ fD.type ]
			|| metaReaction[ s.DAY || s.MONTH ? 'date'
				: s.HOUR || s.MINUTE || s.SECOND ? 'time'
				: s.DAYS || s.HOURS || s.MINUTES || s.PDAYS || s.PHOURS || s.PMINUTES ? 'period'
				: ''
			] || false;
	}
	, getStructButton = function( bT, _call, title, content ){
		_call = metaReaction[ bT ] ? _call.replace( '%%', 'coverOpen' ) : 'return false';
		return [ 'span', { CN: cssNS.button }, [ 'button', {
			CN: cssNS[ bT ]
			, T: lang[ bT + 'Select' ] ? lang[ bT + 'Select' ][ 0 ] : ( title || null )
			, MC: _call
			, value: bT
			}
			, content || ''
		]];
	}
	, replaceShortn = function ( c ) {
	//@ it's a feature of useLib to short'n the RegExp (see shortnChars above) to do
	//@ an easier internationalisation. Therefore the short'n must be replaced.
		for ( var n in shortnChars )
			c = c.replace( n, shortnChars[ n ]);
		return c;
	}
	, pattern2Char = function( p ) {
	//@ REMARK: in some cases a minus sign creates a RegExp range error, put it to
	//@ end of pattern...
	p = _unmask( typeCheck( 'RegExp', p ) ? p.source : p ); 
	var
		c = _getCharString( p )
		;
		p = replaceShortn( p );
		return { charRegexp: !p.length ? /.+/ : new RegExp( '[' + p + ']+', 'g' )
		, chars: c };

		function _unmask( s ) {
			return s.replace( /^\^|\$$/g, ''					// starts and ends
			).replace( /([^\\])?\\d/g, "$1[0-9]"			// numbers
			).replace( /\[(\\w\\W|\\W\\w)\]/g, ""					// all chars
			).replace( /([^\\])?\\w/g, "$1[a-z0-9_-]"		// alpha numeric
			).replace( /\s|\\\\s(\*|\+)?/g, '\u0020'		// standardized spaces
			).replace( /([^\\]?)\[(.+?[^\\])\](\{\d*,?\d*\}|(\+|\*)\??)?/gi
				, '$1$2'												// counts and repeats
			).replace( /([^\\])?\((.+?)\)\?/g, '$1$2'		// clean possibilities
			).replace( /\\([^\\])/g, '$1' );					// eliminate esc
		}
		function _getCharString( s ) {
		var
			cText = ''
			, done = {}
			;
			if ( !s.length)
				return lang.allowedChars[ 'xx' ];
			for ( var c in lang.allowedChars )
				s = s.replace( new RegExp( c, 'gi' ), function( c ){
					c = c.toLowerCase();
					if ( !done[ c ]) {
						if ( lang.allowedChars[ c ])
							cText += lang.allowedChars[ c ];
						done[ c ] = 1;
					}
					return '';
				});
			if ( done[ 'a-z' ])
				s = s.replace( /[a-z]/ig, '' );
			if ( done[ '0-9' ])
				s = s.replace( /[0-9]/ig, '' );
			return s.length ? cText + unescapeUnicode( s ) : cText.slice( 0, -2 ) || '';
		}
	}
	, keyMsg = function( eC, t, d ) {
	//@ sets className and sound on errors and hints at coverElements while interaction
	//@ PARAM: elementOfCover, typeOfMessage: Error, Carry, Hint
		if ( eC.value )
			delayRemove( eC, cssNS[ 'key' + t ], d || 1 );
		sound( conf.sounds[ 'input' + t ] );
	}
	, errorList = []
	;
	errorList.add = function( fD, mT ) {
	var
		_error = {}
		;
		if ( mT ) {
			_error[ $( fD.id ).name ] = mT;
			this.push( _error );
		}
	};

	function typeList2Cover( e, typeList, fD ) {
	//@ this is the central method to create the cover sequence. Therefore the
	//@ pattern is analyzed and the different parts are treated. 
	//@ returns a pattern where the identifiers replaced
	var
		_call = "return useLib.call.%%(event,'" + e.id + "')"
		, minChars = 5		// to controll the minimal width of input fields
		, pixelSum = 0		// to calculate input width
		, charSum = 0		// 			- " -
		, idNum = 0
		, freeCount = 0
		, coverStruct = []
		;
		for ( var i = 0, t; i < typeList.length; i++ )
			_analyze( typeList[ i ], types[fD.type][ i ]);
		if ( coverStruct.length ) {
			coverStruct = [ 'span', {				// standardize some types... 
					CN: cssNS.isCover + ' ' + cssNS[ 
						/datetime|time|date/.exec( fD.type.replace(/pseudo$/, '' ))
						|| fD.type.replace(/\d+$/, '' )
					]
				}].concat( coverStruct );
		}
		return coverStruct;

		function _analyze( tL, org ) {
			if ( tL.subAddon ) {
				t = tL.subAddon;
				if ( t == 'end' )
					fD[ t ] = true;
				else if ( t == 'annex' ) {
					if ( t = $A( e, 'data-ul' ))
						__getSeparator( t, '' );
				}
				else {
					if ( t.slice( 0, 4 ) == 'free' )
						__getSeparator( t, tL.cN || '', e.id + 'free-' + freeCount++ );
					else		// it's a button...
						coverStruct.push( getStructButton( t, _call ));
				}
			}
			else if ( tL.sepa )
				__getSeparator( tL.sepa, tL.cN || '' );
			else if ( tL.type ) {
			var
				sub = fD.initSub( e.id + idCover + '-' + idNum++, tL.id )
				;
				if ( !!subType[ tL.type ] || tL.size )
					__getInput( __setSubList( sub, tL.type, tL.size ), !!subType[ t ] ? t : null );
				else if ( tL.id.indexOf( 'SELECT' ) > -1 )
					__getSelect( sub, tL.type.split( '|' ));
			}

			function __getInput( sub, cN ) {
			var
				_P = sub.type == 'PASSIVE'
				, rows = $A( e, 'size' ) || null
				;
				coverStruct.push([( rows ? 'textarea' : 'input' ), {
					type: 'text'
					, id: sub.id
					, CN: cN || '' + ( _P ? ' ' + cssNS.noUserSelect : '' )
					, rows: rows
					, T: lang[ fD.type ] ? lang[ fD.type ][ 0 ] : sub.chars
					, disabled: sub.check.disabled ? 'disabled' : null
					, readonly: _P ? 'readonly' : null
					, min: sub.check.min || sub.check.minCount || null	// for shown limits via css
					, max: sub.check.max || sub.check.maxCount || null
					, inputmode: sub.check.isdigit ? 'numeric' : null	// to show numeric keyboard
					, name: conf.noscript ? sub.id : null
					, pattern: conf.noscript ? sub.check.charRegexp
						: DOM.isTouch && sub.check.isdigit ? '[0-9]*' : null
					, autocomplete: "off"
					, KD: _P ? null : _call.replace( '%%', 'coverBeforeInput' )
					, KU: _P ? null : _call.replace( '%%', 'coverAfterInput' )
					, MD: _P && fD.type !== 'singleSelect' ? null
						: _call.replace( '%%', 'coverOnMouseDrag' )
					, MC: _P ? _call.replace( '%%', 'coverOpen' ) : null
					, OF: _P ? null : _call.replace( '%%', 'coverOnFocus' )
					, OB: _P ? "useLib.setStatus(this,'focused',false);"
					: _call.replace( '%%', 'coverCheckOnBlur' )
				}]);
			}

			function __getSelect( sub, oL ) {
			var
				s = []
				;
				org = org ? org.slice( 2, -2 ).split( '|' ) : '';
				org = conf.inputIsLocal || oL.length != org.length ? false : org;
				for ( var i = 0; i < oL.length; i++ ) {
					s.push( [ 'option', { CN: oL[ i ] == 'ownInput' ? 'ownInput' : null
						, value: org && org.length ? org[ i ] : oL[ i ]
						, selected: ( i == 0 ? 'selected' : null ) }
						, oL[ i ].replace( optionExtRegEx, '' )]);
				}
				coverStruct.push([ 'select', {
					id: sub.id
					, size: 1
					, OB: _call.replace( '%%', 'coverCheckOnBlur' )
				}].concat( s ));
			}

			function __getSeparator( sepa, cN, id ) {
			var
				l = 0
				;
				if ( !!id ) {
					sepa = $I( sepa.slice( 4 ));
					cN += ' ' + cssNS.free + sepa; 
					sepa = repeat( '\u2003', sepa );
				}
				cN = cN.trim() + ' ' + cssNS.sepa;
				coverStruct.push([ 'span', { CN: cN, id: id || null }
				, sepa.length ? [ 'span', {}, sepa ] : null ]);
			}

			function __setSubList( sub, t, v ) {
			var
				sC = sub.check
				, mR = getMetaReaction( fD )
				, d
				;
				if ( mR && mR.doReaction )
					sC.doReaction = mR.doReaction( e, fD );
				for ( var n in subTypeAttr )
					___initCheck( n );
				sC.step = 1;
				sC.disabled = sC.disabled || e.disabled;  
				if ( t == 'INT' || t == 'ZERO' || t == 'FIX' ) {
				//@ it is needed a special treatment because of free step and limit definition
					d = ___getDeci() || 2;
					if ( t == 'FIX' ) {
						sC.min = 0;
						sC.max = $I( repeat( '9', d ));
					}
					else {
						sC.min = $I( fD.min ) || 0;
						sC.max = min(
							$I( fD.max ) || $I( repeat( '9', v.max || 5 ))
							, 900719925474099			// MAX_SAFE_INTEGER / 10
						);
					}
					sC.step = ___getStep( fD.step, d );
 				}
				sC.isdigit = t != 'PASSWORD' && sC.max || sC.min;
				sC.sepaRegexp = tL.regexp;
				sC.minCount = !sC.isdigit && v ? v.min : sC.min > 9 ? String( sC.min ).length : 1;
				sC.maxCount = !sC.isdigit && v ? v.max
					: max(( sC.max+'' ).length, ( sC.min+'' ).length );
				charSum += max( minChars, sC.maxCount);
				__initRegExp( sub, t, sC.isdigit ? ( sC.min < 0 ? '-0-9' : '0-9' )
					: tL.size ? ( subType[ t ] ? subType[ t ].ident : t ) : false );
				return sub;

				function ___initCheck( c ) {
				var
					sT = subType[ t ] ? subType[ t ][ c ] : false
					;
					if ( sT !== false && typeof sT != 'undefined' )
						sC[ c ] = typeof sT == 'function' && c != 'extra' ? sT( e )	: sT;
				}

				function ___getDeci() {
				//@ check out maximum decimal positions of possible parameters
				var
					_t = str2obj( 'step min max value' )
					, v = 0
					;
					for ( var n in _t )
						if ( fD[ n ])
							v = max( v, ____deciCalc( fD[ n ]));
					return v;

					function ____deciCalc( v ) {
					//@ get decimal places (it's easier as string cast then mathematical)
						v = String( v ).match( /[\d]*\.([\d]*)/ );
						return v ? v[ 1 ].length : 0;
					}
				}

				function ___getStep( s, d ) {
					s = s ? parseFloat( s ).toFixed( d ) : false;
					if ( s && s != 1 ) {
						fD.step = parseFloat( s );
						fD.stepZeroshift = parseFloat( fD.value ) % fD.step;
						if ( t == 'FIX' ) {
							s *= ( sC.max + 1 );					// convert decimals to integer 
							if ( String( s ).length > d )	{	// cut, if overflow ...
								s = $I( String( s ).slice( -d ));
								sC.disabled = 'disabled';		// ... no usefull input in this field
							}
							return s;
						}
						else
							return Math.ceil( s );	// round up, if step closed over 1 it must be 2
					}
					return 1;
				}
			}

			function __initRegExp( sub, t, r ) {
				if ( r ) {
					r = pattern2Char( r );
					sub.check.charRegexp = r.charRegexp;
					sub.chars = ( lang.inputName[ t ] ? lang.inputName[ t ] + ": " : "" )
					+ r.chars;
				}
				return sub;
			}
		}
	}

	function initFormatedInput( e, type, pattern ) {
	var
		fD = getFormDef( e )
		, typeList = type == 'pattern' ? _analyzePattern( pattern )
		: getTypeList( type, 'local' )
		, coverStruct
		;
		if ( typeList && types[ fD.type ]) {
			fD = extendFormDef( fD );
			fD.regexp = types[ fD.type ].regexp;
			coverStruct = typeList2Cover( e, typeList, fD );
			if ( coverStruct.length )
				return _setCoverDiv();
		}
		return false;

		function _analyzePattern( p ) {
		//@ checks if pattern codes a predefined type via substitutes ident or
		//@ subAddon. if not predefined it returns a list of found cover patterns,
		//@ subTypes or subAddons 
			if ( p.slice( 0, 2 ) == '((' && p.slice( -2 ) == '))' ) {
				for ( t in types ) {
					getTypeList( t, 'local' );
					if ( '/' + p + '/' === String( types[ t ].regexp )) {
						fD.type = t;
						return getTypeList( t, 'local' );
					}
				}
				fD.type = 'pattern' + dynPattern++;
				types[ fD.type ] = {
					regexp: new RegExp( replaceShortn( p ))
					, norm: initTypeList(
						p.replace( /\(\((.+?)\)\)/g, function( _, t ) {
							return '(('+ ( identifier[ t ] || identifier[
							t.replace( /\{[\d\,]+\}$/, '' )] || t ) + '))';
						}).replace( /(\{\d+,\d+\})/g, '))(($1'
						).slice( 2, -2 ).split( '))((' )
					)
				};
				types[ fD.type ].local = types[ fD.type ].norm;
				return types[ fD.type ].norm;
			}
			return false;
		}

		function _setCoverDiv() {
		var
			hasNoInput
			, mR = getMetaReaction( fD )
			, eC
			;
			if ( !$CN.contains( $P( e ), cssNS.hasCover )) {
				$CN.add( $P( e ), cssNS.hasCover );
				$CN.add( e, cssNS.covered );
				DOM.insertAfter( e, $CE( coverStruct ));
			}
			if ( eC = $( fD.first ))
				hasNoInput = fD.getSub( eC.id ).type == 'PASSIVE'
				|| fD.type.indexOf( 'Select') > -1;
			//@ WAI-ARIA: no tabindex for covered element, but if defined set to first
			if ( $A( e, 'tabindex' ) > -1 )
				$A( eC, 'tabindex', $A( e, 'tabindex' ));
			$A( e, 'tabindex', '-1' );	// must be a string, else it removes tabindex!
			if ( hasNoInput )
				eC.value = e.tagName == 'SELECT'
				? metaReaction.singleSelect.getText( e, fD.value, true )
				: fD.value.join( ', ' );
			else {
 				setValue( fD, fD.type, fD.value[ 0 ], conf.inputIsLocal ? 'local'
 				: 'norm' );}
			if ( mR && mR.init )
				mR.init( e, fD );
			if ( eC )
				$E( labels[ e.id ], 'MC', function _mouseClick(){
				// change assignment of label to first new input field
					if ( hasNoInput )
						eC.click();
					else {				// focus and select the first input
						eC.selectionStart = 0;
						eC.selectionEnd = e.value.length;
						eC.focus();
					}
				});
			return coverStruct.length;
		}
	}
	return {
		getContent: initFormatedInput
		, types: types
		, setValue: setValue
		, getValue: getValue
		, getLocal: getLocal
		, getYear: getYear
		, checkValue: checkValue
		, keyMsg: keyMsg
		, errorList: errorList
		, listSelect: false
		, getTypeRegexp: function( t, l ) {
			t = t.toLowerCase();
			return getTypeList( t, !!l ?'local' : 'norm') ? types[ t ].regexp : false;
		}
		, pattern2Char: pattern2Char
		, replaceShortn: replaceShortn
	}; 
})();
//@ end of cover

/*****
*
* section of cover event execution
*
*****/
var doEvent = (function() {
var
	cP			// caretPosition
	, e		// element (original)
	, eC		// elementCover (active element)
	, fD		// formDef
	, eSub	// subStructoFelementCover
	, sC		// checkStructoFelementCover
	, fireChange = function( e ) {
		if ( fD && e && fD.first == fD.last ) {
			// fire onchange if single field
			stopChange = true;
			dispatchEvent( e, 'change' );
			stopChange = false;
		}
	}
	, onblur = function( eC, id ) {
	//@ this function is instead of a onblur event, that causes may difficulties.
	//@ it controls the input of the last edited field and gives a feed back perhaps.
	//@ REMARK: serveral interactions cause a onblur event that may fire arbitrarily
	var
		_e =  $( id )
		, fD = getFormDef( _e )
		, oldV = _e.value
		, v = false
		, c
		;
		eC = eC.target;
		cover.errorList.length = 0;
		if ( eC && eC.value.length ) {			// must be an value to check...
			eC.value = cover.checkValue( fD, eC, eC.value, 0 );
			fD.hasEmpty = false;
			for ( var n in fD.subs ) {
				if (( n.charAt( 0 ) != 'P' || n.indexOf( 'PATTERN' ) > -1 )
				&& (((c = $( fD.subs[ n ].id ).value ).length === 0 )
				|| ( fD.subs[ n ].check.min > 0 && fD.subs[ n ].check.zero
				&& !/[^0]/.test( c )))) {
					fD.hasEmpty = true;
					break;
				}
			}
			c = fD.getSub( eC.id ).check;
			if ( c.doReaction && c.doReaction.onblur )
				c.doReaction.onblur( eC, !fD.hasEmpty ? v : false );
			else if ( !c.doReaction || !c.doReaction.onchange )
				fireChange( _e );
			/*
			if ( sC.isdigit )
			//@ 3.groups for ints for better reading 
			//@ REMARK: it works only on not monospaced fonts in input fields (not yet because
			//@ of caretPosition). ... 
				eC.value = String( eC.value
					).replace( sChar.sSpace.r, ''
					).replace( /(\d)(?=(\d\d\d)+$)/g, '$1' + sChar.sSpace.c	// put
				);
			*/
		}
		else
			$CN.remove( eC, cssNS.error );
		//@ write into origin input field
		v = cover.getValue( fD, fD.type, conf.inputIsLocal ? 'local' : 'norm' );
		if ( !fD.regexp || fD.regexp.test( v ))
			dialog.removeError( _e, oldV != v ? eC : null );
		setClearer( id, false );
		activeOnChange = false;		// onchange shall not write the cover again!
		_e.value = v;
		activeOnChange = true;
		$CN.remove( eC, cssNS.focused );		// ensure no focus...
		setStatus( $( id ), 'focused', false );
	}
	, init = function ( _eC, id ) {
		if ( eC !== _eC ) {
			if ( _eC ) {
				e = $( id );
				fD = getFormDef( e );
				eSub = sC = false;
				if ( fD.getSub && ( eSub = fD.getSub( _eC.id ))) {
					sC = eSub.check;
					/*	see above...
					if ( sC.isdigit )
						eC.value = String( eC.value ).replace( sChar.sSpace.r, '' );
					*/
					doReaction( 'init', _eC );			// init, may be load data etc.
				}
			}
		}
		eC = _eC;
		cP = eC && eC.selectionStart ? eC.selectionStart : 0;
	}
	, cc = 0
	, doReaction = function( type, e, v, step ) {
		if ( sC.doReaction && sC.doReaction[ type ])
			return sC.doReaction[ type ]( e, v, step );
		else if ( type == 'onchange' ) {
			fireChange( e );
			return v;
		}
		return false;
	}
	, setRotor = function( step ) {
	var
		v = String( eC.value )
		, onrotor = sC && sC.doReaction && sC.doReaction.onrotor
		;
		if ( cover.listSelect && step == -1 ) {
			$( cover.listSelect ).firstChild.focus();
		}
		else if ( eC.type.indexOf( 'password' ) < 0 ) {
			cP = min( v.length - 1, cP );
			if ( onrotor )
				eC.value = doReaction( 'onrotor', eC, v, step );
			else if ( sC && sC.isdigit ) { // is digit?
				eC.value = v.length ? String( _rotateDigit( $I( v ), v.length ))
				: cover.checkValue( fD, eC, sC.offer || ( step < 0 ? sC.max : sC.min ), 0 );
				if ( !v.length )
					cP = max( eC.value.length - 1, 0 );
			}
			else
				eC.value = _rotateString( v.split( '' ));
			eC.value = doReaction( 'onchange', eC, eC.value, true );
			if ( eC.setSelectionRange )
				eC.setSelectionRange( cP, cP );
			return false;
		}
		return true;

		function _rotateDigit( v, l ) {
			cP += v < 0 && cP == 0 ? 1 : 0;		// position behind minus sign
			step = sC.type == 'YEAR' || Math.abs( sC.max - sC.min ) < 101
			? step	// till 100 or on YEAR rotate only single steps
			: step * Math.pow( 10, l - cP - 1 );
			if ( Math.abs( step ) > 1 && Math.abs( v + step ) < Math.abs( v / 10 ))
				step /= 10;						// avoid too big jumps
			v = cover.checkValue( fD, eC, v, step );
			cP += v.length-l;					// avoid caret jumping
			return v;
		}

		function _rotateString( v ) {
		var 
			cL = sC && sC.charList ? sC.charList : fD.charList
			, c = v[ cP ] || v[ 0 ] || 'a'
			, i
			;
			if ( !cL )
				cL = _getCharList( sC ? sC : fD );
			i = cL.indexOf( c ) + step;
			v[ cP ] = cL[ i < 0 ? cL.length - 1 : i >= cL.length ? 0 : i ];
			return v.join( '' );

			function _getCharList( _c ) { 		// r == fD.charRegexp
			//@ returns the allowed chars in a array, therefore areas in a regexp
			//@ like a-z expanded to the full list and utf-8 notations decoded
			//@ REMARK: not every case is captured!!
			var
				r = _c.charRegexp == /.+/ ? conf.charDefault
				: String( _c.charRegexp ).slice( 2, -4 )
				;
				r = unescapeUnicode( r ).replace( /([a-z0-9])-([a-z0-9])/gi
				, function( _, b, e ) {
					e = e.charCodeAt( 0 );
					for ( var i = b.charCodeAt( 0 ) + 1; i <= e; i++ )
						b += String.fromCharCode( i );
					return b;
				}).split( '' ).sort().filter( function(v, i, a ) {
					return a.indexOf( v ) == i;
				});
				_c.charList = r;
				return r;
			}
		}
	}
	, setInput = function( _eC, _id, v, doNext ) {
	//@ user expectations are depending on taken actions and state of fields:
	//@ inputs may be pasted or typed in for fill out or correction etc.
	//@ REMARK: Those expectations are inconclusive and conflictive in some cases,
	//@ to solve this, following rules are nessesary:
	//@ a. to paste a value over all fields, they have to be empty (i.e by shift+del)
	//@ b. if not the next field is not empty its content is selected  
	//@ Also on fast inputs not every key causes event, so the hole value has to be
	//@ parsed always
	//@ 1. replace 'better reading' spaces 
	//@ 2. eliminate not allowed chars and show short error
	//@ 3. check jump condition on value logic and put into next field  
	//@ 4. else longer than maxCount, put into next field or show short error
	//@ 5. if separator sign or longer focus next field
	//@ Local values to copy in first field: 271220,17, 29-9-1999 17:23, 368(gjkh)
	//@ 	, DE68 2105 0170 0012 3456 78, DE23 2004 1133 0008 3033 07 + 00
	//@	, df@use-optimierung.de
	init( _eC, _id );
	v = String( v || _eC.value ).replace( /^\s+/, '');
	var
		p = sC.sepaRegexp ? v.search( sC.sepaRegexp ) : -1		// separator found?
		;
		doNext = eSub && eSub.next && ( sC.jump || p > -1 )	// overwrite next possible?
			&& ( doNext || !String( $( eSub.next ).value ).length );
		v = _analyseKey( v, p, { value: '', next: false, digit: 0 });
		if ( !v.value.length )
			sC.extra( fD, _eC, '', 0 );
		else if ( v.digit 		// do not check by checkValue! cause: eC.min, zeros etc.
		&& v.digit > sC.max ) {
			v.value = sC.max;
			cover.keyMsg( _eC, 'Error' );
		}
		if ( _eC.options )
			setEleValue( _eC, [ v.value ]);
		else
			_eC.value = doReaction( 'onchange', _eC, v.value, false );
		if ( v.next !== false ) {
			if ( eSub.next && ( doNext || !v.next.length )) {
				_eC = $( eSub.next );
				_eC.focus();
				setInput( _eC, _id, v.next, doNext );
			}
			else {
				cover.keyMsg( _eC, 'Error' );
				return false;
			}
		}
		return true;

		function _analyseKey( v, p, r ) {
			if ( v.length ) {
			var
				n = v
				, i = 0
				, xx = 0
				, dup
				;
				if ( p > -1 ) { 			// separator was found
					if ( p == 0 )
						v = v.slice( 1 );	// cover starts with separator...
					else {
						r.next = v.slice( p + 1 ) || '';
						v = v.slice( 0, p );
					}
				}
				dup = v.slice( 0 );
				if ( sC.upper )
					v = v.toUpperCase();
				else if ( sC.lower )
					v = v.toLowerCase();
				p = v.slice( 0 ); 
				// delete forbidden chars...
				v = sC.charRegexp ? ( v.match( sC.charRegexp ) || [ '' ]).join( '' ) : v;
				if ( v != p )
					cover.keyMsg( _eC, 'Hint' );
				if ( ( sC.space ? v.replace( /\s/g, '' ).length : v.length ) > sC.maxCount
				|| ( !r.next && p.length > sC.maxCount )) {		// need to cut some signs...
				// find all chars of v in p, the rest is next, but cut from dup ...
					for ( var i = 0, j = 0, l = sC.maxCount; i < l; j++ )
						if ( p.charAt( j ) === v.charAt( i )) {
							i++;
							if ( sC.space && p.charAt( j ) == ' ' )
								l++;
						}
					if ( sC.zero && !doNext // ...cut first zero
					&& ( l - v.length ) == 1 && v.charAt( 0 ) == '0' )
						v = v.slice( 1 );
					else
						v = v.slice( 0, i );
					if ( v.length && !r.next )
						r.next = dup.slice( j );
				}
				r.value = v;
				r.digit = sC.isdigit ? $I( v.replace( /\s/g, '' )) : false;
			}
			return r;
		}
	}
	, clearInput = function( fD, v ) { 
		if ( !!fD.subs ) {
			for ( var n in fD.subs ){			// clear all inputs of one cover
				if ( fD.subs[ n ].type.indexOf( 'SELECT' ) < 0 ) {
					$( fD.subs[ n ].id ).value = '';
					$A( $( fD.subs[ n ].id ), 'value', '' );
				}
			}
			n = $( fD.first );
			if ( !v ) {
				if ( !!( sC = fD.getSub( n.id ).check )) {
					sC.extra( fD, n, '', 0 );
					doReaction( 'onchange', n, '', false );
				}
				if ( fD.subs.COUNTRY )
					$( fD.subs.COUNTRY.id ).value = fD.world || conf.country;
			}
			else
				n.value = v;
			setFocus( n );
		}
	}
	, setClearer = function( id, on ) { 
	var
		e = $( id )
		, fD = getFormDef( e )
		, _e = $( cssNS.clearer )
		, _ex = $( cssNS.grouper )
		;
		if ( on ) {
			if ( !_e )
				_e = $CE([ 'div', { id: cssNS.clearer }]);
			DOM.insertBefore( e, _e );
			if ( !$A( e, 'data-ul-grp' )) {
				DOM.removeNode( _ex );
				_ex = null;
			}
			else if ( !_ex )
				_ex = $CE([ 'div', { id: cssNS.grouper, title: lang.delete }]);
			// overwrite the last function!!
			_e.onclick = function() {
				e.value = '';
				dispatchEvent( e, 'change' );
				setClearer( id, false );
			};
			if ( _ex ) {
				_ex.onclick = function() {
					metaReaction.grouper.changeGrouper( e, 'removeActive' );
				};
				DOM.insertBefore( e, _ex );
			}
		}
		else {
			DOM.removeNode( _e );
			DOM.removeNode( _ex );
		}
	}
	;
	return {
		onKey: {
		//@ this is done for touch interaction. Many Users have problems to do little
		//@ corrections on inputfields. They often type the hole input again and have
		//@ difficulties to posit  the on the wanted position etc. Also the overlaying
		//@ keyboard may hide needed content.
		//@ 37 = ARROWLEFT, 39 = ARROWRIGHT, 38 = ARROWUP, 40 = ARROWDOWN, 45 = INSERT,
		//@ 8 = BACKSPACE
			arrow: function( ev, id, c ) {
				ev.preventDefault();
				init( ev.currentTarget, id );
				return setRotor( c == 38 ? 1 : -1 );
			}
			, input: function( ev, id ) {
			//@
			var
				c = ev.keyCode
				, fD
				;
				if ( !ev.ctrlKey && !ev.metaKey	// ...european, BS, space, delete
				&& ( c == 0 || c == 8 || c == 32 || c == 46
				|| ( c > 47 && c < 112 ) || c > 137 )) {
					id = id || ev.currentTarget.id;
				// don't react on each key (ctrl, pause etc., page up..., arrows,
				// F1 - F12 etc. ), but at possible new chars, the keycode of
				// european chars is 0!
					fD = getFormDef( $( id ));
					if ( ev.shiftKey && c == 46 )
						$( id ).value = '';
					else if ( !!!fD.subs )
						_normInput( ev.currentTarget );
					else if ( fD.type.indexOf( 'password' ) < 0
					&& fD.regexp.test( ev.currentTarget.value )) {
						clearInput( fD, ev.currentTarget.value );
						setInput( $( fD.first ), id );
						c = 13;
					}
					else
						setInput( ev.currentTarget, id );
				}
				return c !== 13;

				function _normInput( e ) {
				var
					v = ( e.value.match( fD.charRegexp ) || [ '' ]).join( '' )
					;
					if ( v != e.value ) {
						cover.keyMsg( e, 'Hint' );
						e.value = v;
					}
					else if ( fD.max && fD.max < v.length ) {
						cover.keyMsg( e, 'Error' );
						e.value = v.slice( 0, fD.max );
					}
				}
			}
		}		// end of onKey
		, onMouse: (function() {
		//@ this is done for touch interaction. Many Users have problems to do little
		//@ corrections on inputfields. They often type the hole input again and have
		//@ difficulties to posit the  on the wanted position etc. Also the overlaying
		//@ keyboard may hide needed content.
		//@ 37 = ARROWLEFT, 39 = ARROWRIGHT, 38 = ARROWUP, 40 = ARROWDOWN, 45 = INSERT,
		//@ 8 = BACKSPACE
		var
			miniCursor = {
			//@ if the standard keyboard is prevented, a useLib-caret is nessesary!
			//@ ATTENTION: it only supports left to right writing on monospaced fonts!
			//@ REMARK: alternatively the text width on selectionStart could be
			//@ measured with a invisible DIV container set to same style attributes
			//@ as the input (didn't test)
				init: function( _eC ) {
					mInit = _eC;
					D.body.appendChild( mC );
					s = $S( mC );
					s.zIndex = $GS( _eC, 'zIndex' ) + 1;
					mCalc = _getCalc();
					$CN.add( mC, cssNS.show );

					function _getCalc() {
					//@ returns a closeure to calc the caret position in a specific cover
					//@ element. It's absolute position has to be fitted to text-align, 
					//@ borders and paddings.
					var
						r = DOM.getRect( _eC )
						, w = DOM.textWidth( 'abcdefghij', cssNS.miniCursor ) / 10.
						, a = $GS( _eC, 'text-align' )
						, _min = r.x + __getBP( 'left' )				// minimum position
						, _max = r.x + r.w - __getBP( 'right' )	// maximum position
						;
						s.top = $I( r.y + __getBP( 'top' )) + 'px';
						return function( p, l ) { 
							return max( _min, min( _max ,
								a == 'left' || a == 'start' ? _min + w * cP
								: a == 'right' || a == 'end' ? _max - w * ( l - cP )
								: _min + ( _max - _min ) / 2 + w * ( cP - l / 2 )
							))
						};

						function __getBP( t ) {
							return parseFloat( $GS( _eC, 'border-' + t + '-width'))
							+ parseFloat( $GS( eC, 'padding-' + t ));
						}
					}
				}
				, set: function( _eC ) {
					if ( _eC ) {
						if ( _eC != mInit )
							this.init( _eC );
						s.left = $I( mCalc( cP, _eC.value.length )) + 'px';
					}
					else {
						$CN.remove( mC, cssNS.show );
						mInit = false;
					}
				}
			}
			, mC = $CE([ 'div', { id: cssNS.miniCursor }])
			, mCalc
			, mInit = false
			, s = false
			, miniKeyboard = {
				init: function() {
				var
					_call = "useLib.call.miniKeyboard(event,"
					, kL = lang.miniKeyBoard
					;
					return $CE(
						[ 'div', { id: cssNS.miniKeyboard, CN: cssNS.noUserSelect, S: 'margin-top:0.3em;' }
						, _getButton( 37, '\u25C4', kL[ 0 ])	// ARROWLEFT
						, _getButton( 39, '\u25BA', kL[ 1 ])	// ARROWRIGHT
						, _getButton( 38, '\u25B2', kL[ 2 ])	// ARROWUP
						, _getButton( 40, '\u25BC', kL[ 3 ])	// ARROWDOWN
						, _getButton(  8, '\u232B', kL[ 4 ])	// backspace
						, _getButton( 45, '\u21AA', kL[ 5 ])	// INSERT
						, [ 'div', { CN: cssNS.closer, MD: _call + "0)" }]	// CLOSE
					]);

					function _getButton( c, sign, t ) {
						return [ 'div', { MD: _call + c + ",0)", MU: _call + "-1)", T: t }
						, [ 'span', {}, sign ]];
					}
				}
				, set: function( ev, on ) {
					if ( on ) {
						DOM.insertAfter( $P( eC ), mK );
						$CN.add( mK, cssNS.show );
						$E( D, 'focusin', _close );
					}
					else {
						D.body.appendChild( mK );
						$CN.remove( mK, cssNS.show );
						$E( D, 'focusin', _close, -1 );
					}
					miniCursor.set( on ? eC : false );

					function _close( ev ) {
					//@ monitoring focus if child of element containing miniKeyboard close
						if ( !$P( mK ).contains( ev.target ))
							miniKeyboard.set( ev, false );
					}
				}
			}
			, mK = miniKeyboard.init()
			, timeout = false
			, elastic = (function() {
			//@ functions to handle elastic stepper a alternative interaction technic especial
			//@ on touch devices
			var
				doElastic = function () {
				//@ calculate leverLength of mouse position to the last position
				var
					r = Math.sqrt( newPos.x * newPos.x + newPos.y * newPos.y )
					, rS = radiusSteps[ r < $I( sizeSteps / 3 ) ? 0
					: min( maxSteps, Math.ceil( r / sizeSteps ))]
					;
					newPos.x /= r;	newPos.y /= r;		// normalize newPos
					$CN[ newPos.x < 0 ? 'add' : 'remove' ]( trackList[ trackSteps - 2 ]
					, cssNS.minus );
					step = newPos.x < 0 ? -rS[ 0 ] : rS[ 0 ];
					timeStep = rS[ 1 ];
					if ( !timeout )		// start it ones, only change steps outside
						doTimeout();
					return r;
				}
				, doTimeout = function () {
					eC.value = doReaction( 'onrotor', eC, $I( sC.value ), step )
					|| cover.checkValue( fD, eC, $I( sC.value ), step );
					eC.value = doReaction( 'onchange', eC, eC.value, true );
					timeout = setTimeout( doTimeout, timeStep );
				}			
				, initTrack = function ( init ) {
					for ( var i = trackSteps; i >= 0; i-- ) {
						if ( init ) {
							trackList[ i ] =
								$CE([ 'div', {
									CN: cssNS[ 'elastic' + ( i == trackSteps - 2 ? 'Head'
									: i == trackSteps ? 'Center' : 'Track' )]
								}]);
							D.body.appendChild( trackList[ i ]);
							if ( i == trackSteps ) {
								$S( trackList[ i ]).width = zeroPos.w + 'px';
								DOM.setPos( trackList[ i ]
									, $I( zeroPos.x - zeroPos.w / 2 )
									, $I( zeroPos.y - trackList[ i ].offsetHeight )
								);
							}
						}
						else if ( trackList[ i ]) {
							DOM.removeNode( trackList[ i ]);
							trackList[ i ] = null;
						}
					}
				}
				, setTrack = function ( r ) {
				//@ position of trackList.length divs between a elasticMin and a maximum distance
					for ( var i = 0, c = trackSteps - 1, d; i < c; i++ ) {
						d = ( 1 - i / c ) * max( min( r, sizeSteps * trackSteps ), trackSteps );
						DOM.setPos( trackList[ i ]
						, $I( zeroPos.x + ( newPos.x * d ) - 1.25	// 2.5 == radius of dots
						- ( W.visualViewport ? W.visualViewport.offsetLeft : 0 ))
						, $I( zeroPos.y + ( newPos.y * d ) - trackList[ i ].offsetHeight / 2
						- ( W.visualViewport ? W.visualViewport.offsetTop : 0 ))
						);
					}
				}
				, sizeSteps = $I( conf.elasticStep * ( DOM.isTouch ? 1.5 : 1 ))
				, trackSteps = 7
				, trackList = Array( trackSteps + 1 )			
				, radiusSteps = [[ 0, 666 ], [ 1, 666 ], [ 1, 333 ], [ 1, 125 ]
					, [ 11, 125 ], [ 111, 125 ], [ 1111, 125 ], [ 11111, 125 ]]
				// rendering and calculation is on older systems not fast enough, also
				// bigger steps are needed. For stepping feedback on every decimal position
				// add/substract 1 on each.   
				, maxSteps
				, timeStep = 100
				, step = 1
				, zeroPos
				, newPos
				;
				return {
					init: function() {
					var
						d = eC.getBoundingClientRect()
						;
						isElastic = eC.id;
						if ( !doReaction( 'init', eC ))
							sC.value = $I( eC.value );
						maxSteps = max( min( max( _calcDeci( sC.min )
						, _calcDeci( sC.max )) + 2, 7 ), 3 );
						zeroPos = DOM.getRect( eC );
						zeroPos.ox = zeroPos.x - Math.round( d.left );
						zeroPos.oy = zeroPos.y - Math.round( d.top );
						zeroPos.x += zeroPos.w / 2;
						zeroPos.y += zeroPos.h;
						if ( W.visualViewport ) {
							zeroPos.x += W.visualViewport.offsetLeft;
							zeroPos.y += W.visualViewport.offsetTop;
						}
						$CN.add( eC, cssNS.elastic );
						$CN.add( eC, cssNS.noUserSelect );
						initTrack( true );		

						function _calcDeci( v ) {			// count decimal positons of value
							// return Math.ceil(Math.log( Math.abs( Number( v )))/Math.LN10)
							return !v ? 0 : String( Math.abs( $I( v ))).length;	// easier! ;-)
						}
					}
					, set: function( ev ){
					//@ is triggert on mousemove, the first request calls init...
						newPos = DOM.getCoord( ev )
						;
						if ( isElastic != eC.id )
							elastic.init( newPos );
						newPos.x -= zeroPos.x - zeroPos.ox;
						newPos.y -= zeroPos.y - zeroPos.oy;
						setTrack( doElastic());
					}
					, stop: function() {
						clearTimeout( timeout );	// stop timeout !!!
						isElastic = timeout = false;
						$CN.remove( eC, cssNS.elastic );
						$CN.remove( eC, cssNS.noUserSelect );
						initTrack( false );
					}
				};
			})()
			, getTime = function() { return (new Date).getTime(); }
			, isElastic
			, wait
			;
			return {
				drag: function( ev, id ) {
				//@ onmousedown or ontouchstart it's to decide if the user wants the
				//@ default behavior or the elastic stepper or rather ontouch the 
				//@ miniKeyboard only. Therefore the contact point has to be moved
				//@ outside the target rect.
					ev.preventDefault();
					ev.stopPropagation();
					init( ev.currentTarget, id );
					if ( sC.type != 'PASSIVE' ) {	// don't select on passive elements
						isElastic = !!( sC.isdigit
						|| ( sC.doReaction && sC.doReaction.onrotor ));
						wait = 'wait';
						$E( D.body, 'MM', doEvent.onMouse.init );
						$E( D.body, 'MU', doEvent.onMouse.stop );
						setTimeout( function() {
							if ( wait == 'start' ) wait = false;
						}, 600 );
						return true;
					}
					return false;
				}
				, init: function( ev ) {
					if ( !wait )
						elastic.set( ev );
					else if ( !DOM.coordInRect( ev, eC ))	// outside cover element?
						wait = isElastic ? 'start' : 'mini';
				}
				, stop: function( ev ) {
					$E( D.body, 'MM', doEvent.onMouse.init, -1 );			
					$E( D.body, 'MU', doEvent.onMouse.stop, -1 );
					if ( !wait )
						elastic.stop();
					else if ( wait != 'wait' ) {
						miniKeyboard.set( ev, 1 );				// on touch only
						D.activeElement.blur();					// close the main keyboard
					}
					else {
						miniCursor.set( false );
						eC.focus();								// focus was stopped, do it now
					}
				}
				, miniKey: function( ev, c, count ) {		// on touch only
					ev.preventDefault();							// prevent magnifier etc.
					if ( c == -1 ) {								// onmouseup / ontouchend
						clearTimeout( timeout );				// stop repeat
						timeout = false;
					}
					else if ( c == 0 ) {
						miniKeyboard.set( ev, 0 );
						eC.focus();
					}
					else {
					var
						miniOnly = eC != D.activeElement 	// (no activeElement = no keyBoard)
						;
						if ( c == 8 && cP == 0 )
							c = 37; 
						if ( c == 8 || c == 45 )				// backspace or insert
							_changeValue( c == 45 );
						else if ( c == 38 || c == 40 )		// ARROWUP, -DOWN
							setRotor( c == 38 ? 1 : -1 );
						else if ( eC.tagName != 'SELECT' )
							_stepCursor( c == 39 ? 1 : -1 );
						if ( miniOnly ) {
							eC.blur();							// to close the appaering keyboard
							miniCursor.set( eC );			// set cursor to position
						}
						else
							miniCursor.set( false );
						// for simulation of keyboard repeat
						timeout = setTimeout( function() {
							doEvent.onMouse.miniKey( ev, c, ++count )
						}, count < 3 ? 400 : 100 );
					}

					function _changeValue( insert ) {
					//@ deletes or inserts sign on actual caret position
					var
						v = String( eC.value ).split( '' )
						;
						v.splice( cP, insert ? 0 : 1, insert ? v[ cP ] : '' );
						eC.value = v.join( '' );
						cP += insert ? 1 : -1;
					}

					function _stepCursor( step ) {
						if (( cP == 0 && step < 0 ) || ( cP == eC.value.length && step > 0 )) {
							// is inside of a cover sequence?...
							if (( step < 0 && eSub.before ) || ( step > 0 && eSub.next )) {
								// ...do step by own logic because of miniKeyboard and -cursor control
								init( $( eSub[ step < 0 ? 'before' : 'next' ]), fD.id );
								if ( step > 0 ) {
									cP = 0;
									eC.setSelectionRange( cP, cP );
								}
							}
							else {
								miniKeyboard.set( 0, 0 );
								setCursorStep( eC, c );
							}
						}
						else {
							cP += step;
							eC.setSelectionRange( cP, cP );
						}
					}
				}
				, miniOff: function(){ miniKeyboard.set( 0, 0 ); }
			}
		})() 	// end of onMouse
		, onblur: onblur
		, setInput: setInput
		, clearInput: clearInput
		, setClearer: setClearer
	}
})()	// end of doEvent
;
/*****
*
* section of cover event declaration
*
*****/
	call.uLinit( 'miniKeyboard', doEvent.onMouse.miniKey );
	call.uLinit( 'coverBeforeInput', function( ev, id ) {
	//@ As standard feature, the caret jumps to end or start on arrow up or down and
	//@ the caretposition gets lost. For rotate the allowed signs or decimal this is to
	//@ avoid on keydown. Also if the cared position is on 0 or end, it shall/may jump
	//@ to the next cover element.
	var
		c = ev.keyCode
		, r = true
		;
		if ( !ev.ctrlKey && !ev.metaKey
		&& ev.currentTarget.tagName != 'TEXTAREA' ) {
			if ( c == 38 || c == 40 )	// up, down
				r = doEvent.onKey.arrow( ev, id || ev.currentTarget.id, c );
			else if ( c == 13 ) {		// leave on return
				ev.stopPropagation();
				setCursorStep( ev.currentTarget, 39, ev );
				//ev.currentTarget.blur();
				r = false;
			}
		}
		return r;
	});
	call.uLinit( 'coverAfterInput', doEvent.onKey.input );
	call.uLinit( 'coverOnMouseDrag', doEvent.onMouse.drag );
	call.uLinit( 'coverOpen', function( ev, id, bodyStruct ) {
	var
		e = $( id )
		, type = ev.currentTarget.tagName == 'BUTTON'
		? ev.currentTarget.value : getFormDef( e ).type
		, mR = metaReaction[ type ]
		;
		if ( mR && !e.disabled ) {			// sometimes target delivers no type...
			doEvent.onMouse.miniOff();
			if ( mR.doButton )
				mR.doButton( ev, $( id ));
			else if ( bodyStruct || mR.getContent )
				_open();
		}
		return false;

		function _open() {
		var
			c = bodyStruct || mR.getContent( e, type );
			;
			if ( c && c != -1 ) {
				if ( typeCheck( 'Array', c ))
					c = [ 'form', { name: id + cssNS.modal
					, 'data-ul': type, CN: cssNS[ type ]}].concat( c );
				dialog.setModal( id, type, 'draggable closable', c, getLabelText( e ));
				if ( mR.getValue )
					mR.getValue( e, type );
				setStatus( e, 'focused', true, true );
			}
			else
				setTimeout( _open, 400 );
		}
	});
	call.uLinit( 'coverCheckOnBlur', doEvent.onblur );
	call.uLinit( 'coverOnFocus', function( ev, id ) {
		id = id || ev.currentTarget.id.replace( /uLcover-\d+$/i, '' );
		doEvent.setClearer( id, true );
		setStatus( $( id ), 'focused', true );
	});
//@ end of event declaration

/*****
 *
 *		Tooltips to inform users in the medium explain level
 *		@ It is possible to steer tooltips by css only by :hover, but tooltips in
 *		@ bottom or right area of the view are hidden then. Therefore it's necessary
 *		@ to check this via javascript and steer it by additional classes: uLright,
 *		@ uLbottom
 * 
 *****/
var
	activeTT = null
	;
	function initTooltips( e ) {
	var
		l = $Q( e || D, '.' + cssNS.inform ) 
		;
		for ( var i = 0; i < l.length; i++ ) {
			// mark node as 'tooltipp' trigger
			$CN.add( _checkPrevious( $P( l[ i ]))
			|| l[ i ].previousElementSibling, cssNS.hasInform );
			$S( $P( l[ i ])).position = 'relative';	// insure position
		}

		function _checkPrevious( _e ) {
		// check for header with div container, like accordeon
			return _e.previousElementSibling && /h\d/i.test(
			_e.previousElementSibling.tagName )	? _e.previousElementSibling : false;
		}
	}

/*****
 *
 *		Start of folding or accordeon methods
 *		@ The accordeons may be nestet, they are animated via css transition.
 *		@ So they are controlled via classNames. Every folding area after a header
 *		@ must be contained in a div container. If classNames as 'pinfold' and 'fold'
 *		@ found in a header tag every following header of the same level will be added
 *		@ to the accordeon. If 'pinfold' also a "locker" element is added.
 * 
 *****/
	call.uLinit( 'foldLock', function( ev ) {
		ev.stopPropagation();			// needed to avoid folding on click
		$CN.toggle( ev.currentTarget, cssNS.locked );
	});

	function initFold( e ) {
	//@ get all header tags beneath element and create a list with parent headers
	//@ REMARK: it also shall work correct, if only one header of a level is marked
	//@ by class. Therefore all header tags of the same level searched and tested for
	//@ initialization. This also avoids double initialisation.
	//@ accessibility: add aria-expanded="true|false", if folded or not
	var
		locker = $CE( [ 'span', { CN: cssNS.locker, MC: "useLib.call.foldLock(event)" }])
		, list = $Q(( e || D.body ), '.' + cssNS.fold + ',.' + cssNS.pinfold )
		;
		for ( var i = 0, cN, l; i < list.length; i++ ) {
			cN = $CN.contains( list[ i ], cssNS.pinfold ) ? cssNS.pinfold : cssNS.fold;
			l = $Q( $P( list[ i ]), list[ i ].tagName );
			for ( var j = 0; j < l.length; j++ ) {
				if ( !$CN.contains( l[ j ], cN ))
					$CN.add( l[ j ], cN );
				if ( cN == cssNS.pinfold 		// check accordeon has locker...	
				&& !$CN.contains( l[ j ].lastChild, cssNS.allpins )
				&& !$CN.contains( l[ j ].lastChild, cssNS.locker )) {
					l[ j ].appendChild( locker.cloneNode( true ));	// ...append locker
					if ( j == 0 ) {
						var
							allpins = locker.cloneNode( true )
							;
							allpins.className = cssNS.allpins;
							allpins.title = lang.allpins[ 0 ];
							allpins.onclick = function() {
								for ( var _j = 0
								, t = $CN.contains( this, cssNS.locked ) ? 'remove' : 'add'
								; _j < l.length; _j++ ) {
									$CN[ t ]( l[ _j ].lastChild, cssNS.locked );
									$CN[ t ]( l[ _j ], cssNS.open );
									$CN[ t ]( this, cssNS.locked );
								}
							};
							l[ j ].appendChild( allpins );
						}
				}
				if ( j == 0 ) {
					$CN.add( l[ j ], cssNS.open );
					$A( l[ j ], 'aria-expanded', "true" );
				}
				_setToggleFold( j )
			}
		}
		
		function _setToggleFold( _j ) {
			$E( l[ _j ], 'MC', (function ( list ) {
				// toggles state of header element if its not locked
				return function _toggleFold( ev ) {
					ev.stopPropagation();
					if ( !$Q( this, '.'+ cssNS.locked ).length ) {
						if ( $CN.contains( this, cssNS.open )) {	// is opened?
							$CN.remove( this, cssNS.open );
							$A( this, 'aria-expanded', "false" );
						}
						else {
							__closeOpened( this );
							$CN.add( this, cssNS.open );
							$A( this, 'aria-expanded', "true" );
							DOM.scrollIntoView( this.nextElementSibling, 1.7 );
						}
					}
			
					function __closeOpened( e ) {
					// searches for a opened header of same level and tree part and closes
						for ( var i = 0; i < list.length; i++ ) {
							if ( list[ i ] != e 
							&& $CN.contains( list[ i ], cssNS.open )
							&& !$Q( list[ i ], '.'+ cssNS.locked ).length ) {
								$CN.remove( list[ i ], cssNS.open );
								$A( list[ i ], 'aria-expanded', "false" );
								return;
							}
						}
					}
				}
			})( l ));
		}
	}

/*****
 *
 *		Tabulator row for interaction
 *		@ Tabs are simple unordered Lists, that are formated via css. Only the
 *		@ cssNs.show content (form or div) after a starting link or normal text is
 *		@ shown on click on a tab the shown tab may change...
 * 
 *****/
	call.uLinit( 'tabs', function( e ) {
	//@ get all unordered lists and set a click event on the list elements to change shown
	var
		l = $P( e, 2 ).children
		;
		for ( var i = 0; i < l.length; i++ ) {
			if ( $CN.contains( l[ i ], cssNS.show )) {
				$CN.remove( l[ i ], cssNS.show );
				break;
			}
		}
		$CN.add( $P( e, 1 ), cssNS.show );
		return false;
	});

/*****
 *
 *		Image for interaction
 *		@ Images may be sized. It shall be possible th see it fully sized
 * 
 *****/
	call.uLinit( 'zoomImage', function( e ) {
		if ( e.firstChild.classList.contains( cssNS.sizer ))
			useLib.DOM.removeNode( e.firstChild );
		else {
		var
			i = D.createElement( 'img' )
			;
			i.src = e.firstChild.src;
			i.className = cssNS.sizer;
			i.onload = function() { useLib.DOM.insertBefore( e.firstChild, i ); };
			e.firstChild.watch( 'src', function( _, o, n ) {
				if ( e.firstChild.classList.contains( cssNS.sizer ))
					setTimeout( function() { e.firstChild.src = n; }, 10 );
				return n;
			});
		}
	});

/*****
 *
 *		functions to init the formDef structure of useLib
 *		@ because of the behavior of the browsers to set element values older
 *		@ user selected values it is necessary for control to manage all values
 *		@ and attributes separately. Some rules for the internal use of formDef:
 *		@
 *		@ * the elements are administered by name, that means elements with
 *		@   selection attributes (selected and checked) are treated special and
 *		@	 can be combinated free
 *		@ * if a status is valid for each element the array has a property: 'uLall'
 *		@ * value, resetValue, status are always arrays in formDef
 *		@ * mnemonics are always an array 
 *		@ * type must be defined and is always identified via formDef
 *		@ * data is variable
 *		@ * onchange (also via watch) values and status attributes are set to
 *		@   formDef and possible reactions are triggered from there.
 *		@
 *		@ Therefore it is possible to define a JSON structure that contains beside
 *		@ all predefined values, predefinitons for the intern formDef structure
 *		@ This JSON may be delivered by AJAX or set to useLib.extern[ formName ].
 *		@ It is used to set or overwrite the HTML-Attributes before initialize  
 *		@ the useLib with it's metaTypes
 *		@
 *		@ The central method to get the right formDef is getFormDef( e ).
 *		@ REMARK: the html5 type and the required attribute are removed and stored
 *		@ in formDef, because the browsers act confusing (even for users) and
 *		@ different in many cases.
 *
 *****/
var
	dynamicSelects = {}
	, formDef = []
	, getFormDef = function( e, tell ) {
		if ( !e || !e.form || e.tagName == 'OPTION' || e.tagName == 'LABEL' )
			return false;
		else {
		var
			f = formDef[ e.form.name ]
			;
			if ( f && f[ e.name ])
				return f[ e.name ];
			else {
				if ( !f )
					f = formDef[ e.form.name ] = {};
				if (( !e.name || !e.name.length )
				&& !$CN.contains( $P( e ), cssNS.isCover )) {
					console.warn( "useLib hint: element (tagName:" + e.tagName
					+ ", id:" + e.id + ")has no name!"+ (tell||'') );
					return {};
				}
				if ( !f[ e.name ])
					f[ e.name ] = { isInit: {}, value: [], resetValue: [], onCalc: false };
				return f[ e.name ];
			}
		}
	}
	, statusAttr = str2obj( conf.statusSupport )	// may be less than possible
	, valueAttr = (function( vA ) {					// separate value attributes
		for ( var a in vA )
			if ( statusAttr[ a ])
				delete statusAttr[ a ];
			else
				delete vA[ a ];
		return vA;
	})( str2obj( 'type value resetValue mnemonic' ))
	, normAttr = str2obj( 'min max step data-ul' )
	, mnemonics = []
	, labels = []
	, fN = ''
	, activeOnChange = true		// to control 'rechange' of coverElements
	, strParse = function( s ) {
		return s == 'string' && /\{\[/.test( s ) ? JSON.parse( s ) : s;
	}
	, pushAttr = function( f, v ) {
	//@ apends the attribute on formDef attribute array
		f = f && f[ 0 ] != '' ? f : [];
		if ( typeCheck( 'String', v ) && f.indexOf( v ) == -1 )
			f.push( v );
		return f;
	}
	, tempStatus = { e: null }
	, setStatus = function( e, s, on, temporary ) {
	//@ PARAM elementNode, status, on/off, onlyTillNext
	//@ a lot HTML structures are not really well structured, so it is necessary to
	//@ set the className on each label and element!! 
		if ( !!e && ( !( s == 'changed' && $A( e, 'disabled' ))
		|| ( s == 'focused' && !internChange ))) {
			internChange = true;
			if ( temporary && temporary != -1 && tempStatus.e && s == tempStatus.s )
				setStatus( tempStatus.e, tempStatus.s, false, -1 );
			if ( temporary === true )
				tempStatus = { e: e, s: s };
			if ( on && s == 'focused' )
				lastActive = e;
			$CN[ !!on ? 'add' : 'remove' ]( e, cssNS[ s ] || s );	// set div of element class
			if ( labels[ e.id ])
				$CN[ !!on ? 'add' : 'remove' ]( labels[ e.id ], cssNS[ s ] || s );
			if ( s == 'required' ) {
			//@ browsers act not comprehensible if set, so only set if no javascript is activ 
				$A( e, s, !!on && conf.noscript ? 'required' : -1 );
				$A( e, 'aria-' + s, !!on ? 'true' : 'false' );
				getFormDef( e ).required = !!on;
			}
			else if ( s == 'disabled' ) {
				if ( $CN.contains( $P( e ), cssNS.hasCover )) {
				var
					fD = getFormDef( e )
					;
					for ( var n in fD.subs )			// clear all inputs of one cover
						$A( $( fD.subs[ n ].id ), s, !!on ? 'disabled' : -1 );
				}
				$A( e, s, !!on ? 'disabled' : -1 );
			}
			else if ( s == 'changed' && $CN.contains( e.form, cssNS.cloud )) {
				$CN[ $Q( e.form, 'form .'+ cssNS.changed, 1 ) ? 'add' : 'remove'
				]( e.form, cssNS.changed ); 
			}
			internChange = false;
		}
	}
	, internChange = false
  	, reInitValues = false
	, onchange = function( e, v ) {
	e = e.target || e;
	var
		fD = e.form ? formDef[ e.form.name ] : false
		;
		if ( !!fD && !internChange && !!( fD = fD[ e.name ])) {
		var
			t = 'checked'
			, n = '[name="' + e.name + '"]'
			, rc = e.type == 'radio' ? 'r' : e.type == 'checkbox' ? 'c' : false
			, changed
			, _e
			;
			// start a special treatment if there are mixed structures of radio buttons and
			// select lists. ATTENTION: only one list is supported (else makes no sense)!
			if (( e.type == 'select-one'
			&& ( _e = $Q( e.form, '[type=radio]'+ n + ':checked' )).length )
			|| ( rc == 'r' && ( _e = $Q(e.form, 'select' + n )).length
			&& _e[ 0 ].type == 'select-one' )) {
				internChange = true;
				if ( rc == 'r' ) {
					if (( n = _e[ 0 ].selectedIndex ) > -1 )
						$A( _e[ 0 ].options[ n ], 'selected', -1 );
					if ( $CN.contains( _e[ 0 ], cssNS.covered ))
						$( fD.first ).value = '';
				}
				else {
					$A( _e[ 0 ], t, -1 );
					_e[ 0 ].checked = false;
				}
				internChange = false;
			}
			v = v === undefined ? getEleValue( e ) : typeCheck( 'Array', v ) ? v
			: [ v ];
			if ( reInitValues ) {
				changed = false;
				if ( valueAttr.resetValue )
					fD.resetValue = v;
			}
			else {
				if ( rc == 'c' ) {
					changed = fD.resetValue.indexOf( e.value );//|| e.text );
					t = typeof e[ t ] != 'undefined' ? e[ t ] : $A( e, t );
					changed = t ? ( changed < 0 ) : ( changed > -1 );
				}
				else if ( fD.resetValue )
					changed = compare( v.toString(), fD.resetValue.toString()) !== 0;
				if ( changed ) {
					if ( /useLib/i.test( v[ 0 ]) && !$( 'uL_useLib' )) {
						// just a easteregg with copyright
						lang.useLib[ 1 ] = lang.useLib[ 1 ].replace( /%%/
						, copyright + ', Version ' + useLibVersion );
						dialog.setMsg( 'hint', 'useLib', 0, 12000 );
					}
					fD.storeValue = v.slice( 0 );
					if ( !rc )
						$A( e, 'value', v[ 0 ] && v[ 0 ].length ? v[ 0 ] : -1 );
				}
			}
			setStatus( e, 'changed', changed );
			fD.value = v;
			// special treatment for changed radiobuttons (state before could be wrong...)
			if ( rc == 'r' ) {
				setStatus( fD.activeRadio, 'changed', 0 );
				fD.activeRadio = e;
			}
			if ( e.name && e.name.length && !$A( e, 'oncalc' ))
				_checkCalc();
			// special treatment for covered elements
			if ( activeOnChange === true && $CN.contains( e, cssNS.covered )
			&& fD.getSub ) {
				internChange = true;
				if ( !v.join( ', ' ).length )
					doEvent.clearInput( fD );
				else if ( fD.type != "PASSIVE" ) {
					cover.setValue( fD, fD.type, v.join( ', ' )
						, conf.inputIsLocal ? 'local' : 'norm'
					);
				}
				// there might be some more actions...
				if (( t = metaReaction[ fD.type ]) && t.setValue ) {
					if ( fD.type == 'singleSelect' || fD.type == 'multiSelect'
					|| fD.type == 'dynamicSelect' || fD.type == 'dynamicExtend' )
						t.getText( e, v, true );
					else
						t.setValue( e, v.length > 1 ? v : v[ 0 ]);
				}
				internChange = false;
			}
			// special treatment for tristate of required radio buttons and checkboxes 
			if ( $CN.contains( e, cssNS.notDone )) {	// clear all radio?
				_e = e.type == 'radio' ? $Q( e.form, '[name="' + e.name
				+ '"][type="radio"]' ) : [ e ];
				for ( var i = 0; i < _e.length; i++ ) {
					$CN.remove( _e[ i ], cssNS.notDone );
					$CN.remove( _e[ i ], cssNS.error );
				}
			}
			if ( $A( e, 'onchange' ) && $CN.contains( $P( e ), cssNS.hasCover ))
			// finish first... =>
				setTimeout( _onChange( e ), 750 );
		}

	   function _checkCalc() {
	   var
	      fS = $P( e, 2 )
	      , nL = ''
	      ;
	      if ( $CN.contains( fS, cssNS.dynlist ))
	      	fS = $P( fS, 1 );
	      for ( var i = 0, l = $Q( fS, '[oncalc]' ), oC, t = ''
	      ; i < l.length; i++ ) {
	      	oC = $A( l[ i ], 'oncalc' );
	      	if ( oC.indexOf( "useLib.call.dynList.calcIt" ) > -1 ) {
	      		if (( t = !/\.count$|_\d_$/.test( l[ i ].name ))
	      		&& ( t = e.name.match( /\.\w+$/ ))) // searchName
	      			t = l[ i ].name.indexOf( t[ 0 ] +'.' ) > -1;
	      	}
	      	else if ( nL = oC.match( /('|")([\w,]+)\1/ )) {
	      		nL = nL[ 2 ];
	      		t = e.name.match( new RegExp( '\.('+ nL.replace( /,/g, '|' )
	      		+')$' ));
	      	}
	      	if ( t ) {
	      	// timeout because values should be updated correct...
	      	// ... in a interrupt a closeure is needed!
	      		setTimeout( __eval( oC.replace( /\([^\)]+\);?/, '' ), nL
	      		, e, l[ i ] ), 0 );
	      	}
	      }

	      function __eval( _oC, _nL, _e, _l ) {
	      	return function() { uLeval( _oC, _nL, _e, _l ); };
	    	}
	   }
		function _onChange( _e ) {
			return function() { try { _e.onchange(); } catch(_){}};
		}
	}
	, initStatusControl = function( e ) {
	//@ REMARK: watch, because a code (also forign) could change instead of user interaction
	var
		t = getSelectionAttr( e )
		;
		if ( t == 'selected' ) {
			e.watch( 'selectedIndex', function( _, o, n ) { // wait a bit for setting
				if ( e.options[ n ])
					onchange( this, e.options[ n ].value );
				return n;
			});
			t = 'value';
		}  
		e.watch( t, function( _, o, n ) { // wait a bit for setting
			n = e.type == 'file' && n.length ? o : n;
			onchange( e, n );
			return n;
		});
		e.watch( 'disabled', function( _, o, n ){
			setStatus( e, 'disabled', !!n );
			return n;					// don't forget! ;-[
		});
		e.watch( 'required', function( _, o, n ){
			setStatus( e, 'required', !!n );
			return n;
		});
		// if change on value toggle class, on comparing of old to new value
		// NOTICE: onchange on radio is missleading, it is recommended to use
		// therefore onclick and it is needed a special treatment for required 
		$E( e,  e.type == 'radio' ? 'MC' : 'OC', onchange );
	}
	, initMnemonic = function( e, f ) {
	var
		l = labels[ e.id ]
		, m = $T( l, conf.mnemonicsMarker )
		, hasMnemonic = f[ f.count ] && f[ f.count ].length ? f[ f.count ][ 0 ] : false
		, span = '<span style="display:inline-block;width:0;">'
		, s = false
		;
		if ( !hasMnemonic ) {
			f[ f.count ] = [];
			for (var i = 0; i < m.length; i++ ) {
				s = mnStandard( m[ i ].textContent.trim());
				f[ f.count ].push( s );
				mnemonics[ s ] = e.id;
			}
			hasMnemonic = f[ f.count ][ 0 ];
		}
		if ( hasMnemonic && hasMnemonic.length > 1
		&& m[ 0 ].innerHTML.indexOf( span ) < 0 ) {
			// separate by space chars for screen readers
			hasMnemonic = hasMnemonic.split( '' ).join( span +'&#8288;</span>' );
			if ( m )
				m[ 0 ].innerHTML = hasMnemonic;
			else
				DOM.insertBefore( l.firstChild
				, $CE( [ conf.mnemonicsMarker, {}, hasMnemonic ]));
			while ( m.length > 1 )					// remove more than one mnemonic node
				DOM.removeNode( m[ 1 ] );
		}

		function mnStandard( _s ) {
			return _s.replace( /(ctrl|alt|tab)\s*\+/gi
				, function( _, t ){ return '\\' + t.charAt( 0 ); }
			);
		}
	}
	, checkInit = function( fD, a ) {
	// logs already initializized elements
		if ( !fD.isInit )
			fD.isInit = {};
		if ( !fD.isInit[ a ]) {
			fD.isInit[ a ] = true;
			return true;
		}
		return false;
	}
	;
	if ( conf.dataManagePath && conf.dataManagePath.length ) {
		for ( var i = 0, n, a; i < D.forms.length; i++ ) {
			n = D.forms[ i ].name;
			a = D.forms[ i ].action;
			if ( n && n.length && a && a.length && a != '#' ) {
				ajax( 
					conf.dataManagePath + '?method=get&formname=' + n +'&action=' + a
					, function( _v ) { setJSONValues( JSON.parse( _v )); }
					, 'JSON', 2500, true
				);
			}
		}
	}

	function initViaHTML( e ) {
	//@ initialize HTML elements via defined global rules in htmlSupport
	//@ REMARK: it's necessary to overwrite type definition because of the different
	//@ behavior of the browsers, i. e. they throw errors on european date formates
		if ( !formDef[ e.form.name ] || !formDef[ e.form.name ][ e.name ]) {
		var
			fD = getFormDef( e )
			, a
			;
			if ( valueAttr.type ) {
				a = e.tagName == 'SELECT' ? e.type : e.tagName == 'TEXTAREA'
				? 'textarea' : e.tagName == 'BUTTON' ? 'button'
				: $A( e, 'type' ) || e.type;
				if ( !a || a == 'text' ) {
					a = 'text';
					for ( var _a in htmlSupport.type )
						if ( _checkAttr( 'type', _a ))
							a = _a;
				}
				_getType( a );
			}
			for ( var a in htmlSupport.status )
				if ( statusAttr[ a ] && _checkAttr( 'status', a ) )
					_getStatus( a );
		}
		function _checkAttr( s, a ) {
			return !!( typeof htmlSupport[ s ][ a ] == 'function'
			? htmlSupport[ s ][ a ]( e )
			: $Q( $P( e ), htmlSupport[ s ][ a ], 1 ));
		}
		function _getType( a ) {
			if ( checkInit( fD, 'type' ))
				fD.type = a;
			if ( htmlSupport.type[ a ] && a != 'file' && a != 'password' )
				// overwrite browser behavior on type (may be inherited)
				e.type = 'text';
			else if ( a == 'file' )
				$A( e.form, 'enctype', "multipart/form-data" );
		}
		function _getStatus( a ) {
		//@ REMARK: it is nesessary to set all known attribute also explicite to a
		//@ value, because its a browser 'feature' that not the html defined states are
		//@ shown, but those the browser thinks they are actual... i. e. after a reload!
			if ( checkInit( fD, a )) {
				fD[ a ] = true;
			}
		}
	}

	function fullFormDef( e, fD ) {
	//@ initialize at least elements not yet assigned in formDef
	//@ REMARK: complication because of browser features and different (older)
	//@ possibilities to define attributes i. e. selected> or selected="selected".
	//@ It is necessary to ask for the attribute on each element and also use the option
	//@ tag and not options, because disabled options not listed etc.
	//@ (if not, getAttribute === null)
	var
		sA = getSelectionAttr( e )
		, a
		, v
		;
		fD.id = ensureId( e );
		for ( a in normAttr ) {			// type min max step data
			if ( !fD[ a ]) { 
				v = $A( e, a );
				if ( !!v && a == 'data-ul' ) {	// special handling for data-ul...
					fD.data = strParse( v );
					if ( v == 'isGrouper' ) {		// change to intern grouper identifer...
						$A( e, a, -1 );				// ... from ensemble (wiki) standard
						$A( e, a +'-grp', v );
					}
				}
				else
					fD[ a ] = !v ? v : a == 'type' ? v || e.type	: v;
			}
		}
		a = 'mnemonic';
		if ( valueAttr[ a ] && checkInit( fD, a )) {
			if ( !fD[ a ])
				fD[ a ] = { count: 0 };
			else
				fD[ a ].count++;
			initMnemonic( e, fD[ a ] );
		}
		_getAttribute( 'value' );
		_getAttribute( 'defaultValue' );
		a =  'resetValue';
		if ( valueAttr[ a ] && checkInit( fD, a ))
			fD[ a ] = fD.value.slice( 0 );	// copy value for reset
		_setStatus();
		delete fD.isInit;
		return fD;

		function _getAttribute( a ) {
			if ( valueAttr[ a ] && checkInit( fD, a )) {
			internChange = true;
			// care about and not already initializised?
			var
				def = a == 'value' ? false : 'default'
				+ ( sA.slice( 0, 1 ).toUpperCase() + sA.slice( 1 ))
				;
				if ( sA == 'value' )
					fD[ a ] = pushAttr( fD[ a ], $A( e, a ) || '' );
				else if ( sA == 'selected' ) { // disabled is not listed in options etc.
					for ( var i = 0, l = $T( e, 'option' ); i < l.length; i++ ) {
						v = $A( l[ i ], def || sA );
						fD[ a ] = pushAttr( fD[ a ], v === false ? v : l[ i ].value );
					}
				}
				else { 
					v = $A( e, def || sA );
					if ( v !== false && $CN.contains( e, cssNS.notDone ))
					//@ REMARK: well, but it misses willfully not checked checkBoxes!!
						onchange( e, null );
					fD[ a ] = pushAttr( fD[ a ], v === false ? v : e.value );
				}
				if ( !fD[ a ])
					fD[ a ] = [];
				setEleValue( e, fD[ a ]);
			}
			internChange = false;
		}

		function _setStatus() {
			for ( var a in htmlSupport.status )
				if ( !!fD[ a ]) {
					setStatus( e, a, true );
					if ( a == 'required' && sA == 'checked' )
						// special treatment for tristate
						$CN.add( e, cssNS.notDone );
				}
		}
	}

/*****
 *
 *		functions to init elements of forms with useLib's meta controls
 *		@ this includes the call of some formDef initialisations (see above)
 *
*****/
	function initForm( f ) {
	//@ sets all useLib controls of a form. Especial creates the class for the
	//@ optimized display of required or optional elements
		if ( !!conf.doUseLib ) {
			internChange = true;
			for ( var i = 0, l = $T( f, 'label' ); i < l.length; i++ )
				_initLabel( l[ i ]);
			for ( var i = 0, l = $T( f, 'legend' ); i < l.length; i++ )
				_initLegend( l[ i ]);
			for ( var i = 0, l = filterElements( f ); i < l.length; i++ ) {
				initViaHTML( l[ i ]);
				initElement( l[ i ]);
			}
			internChange = false;
			initOnCalc( f );
			ownInput.init( f );
			setRequiredLegend( f );
			f.noValidate = true;
			$E( f, 'reset', _callFormButtons );
			$E( f, 'submit', _callFormButtons );
		}

		function _callFormButtons( ev ) {
		//@ prevents defaults and 'forign' on submits and resets
		//@ REMARK: also prevent double submits i. e. on double click!
		var
			e = ev.currentTarget
			;
			if ( $A( e, 'data-submit' )
			|| !call[ ev.type + 'Form' ]( e )) { // call onreset or onsubmit
				ev.preventDefault();
				ev.stopPropagation();
			}
			$A( e, 'data-submit', 'true' );
			setTimeout( function() { $A( e, 'data-submit', -1 ); }, 1000 );
		}

		function _initLabel( l ) {
		var
			e = $( l.htmlFor )
			;
			if ( !e									// try the best to assign the label...
			&& ((( e = l.nextSibling) && e.form )
			|| (( e = l.previousSibling) && e.form )))
				l.htmlFor = ensureId( e );
			if ( !!e ) {
				labels[ l.htmlFor ] = l;
				if ( e.type == 'radio' || e.type == 'checkbox' )
					// some Browsers not fireing...
					$E( l, 'MC', function _lableFocus(){ e.focus(); });
				else {
					$E( l, 'M2', resetElements );
					if ( conf.taskSupport && !$CN.contains( e, 'noTask' )
					&& !$CN.contains( l.lastChild, cssNS.task ))
						l.appendChild( $CE([ 'span', { CN: cssNS.task
							, MD: "useLib.call.taskstep(this.parentNode)" }
						]));
				}
			}
		}
		function _initLegend( l ) {
		// set double click reset on 
			$E( l, 'M2', resetElements );
		}
	}

	function initElement( e ) {
	var
		iCheck = { 'radio': 1, 'checkbox': 1, 'button': 1 }
		, fD = getFormDef( e )
		;
		if ( checkInit( fD, 'uLelement' )) {
			fullFormDef( e, fD );
			if ( e.tagName == 'SELECT' )
				__initSelect( e, fD );
			else if ( e.tagName == 'TEXTAREA' )
				__initTextArea( e, fD );
			else if ( !iCheck[ e.type ])
				__initInput( e, fD );
			initStatusControl( e, fD );
			if ( $P( e ).tagName != 'DIV' )
				console.warn( "useLib hint: parentNode of element: " + e.id
				+ " has no DIV-container!" );
		}

		function __initSelect( e, fD ) {
		var
			coverType = fD.data ? ( $I( $A( e, 'max' )) ? 'dynamicExtend' : 'dynamicSelect' )
			: e.type == 'select-multiple' || $A( e, 'min' ) == '1' ? 'multiSelect'
			: ( $I( $A( e, 'max' )) || e.options.length ) > conf.selectLimit  
			? 'singleSelect' : false
			, o
			;
			if ( fD.required ) {
				o = e.options[ 0 ];
				if ( !!o && ( o.value === "" || ( !o.value && !o.text ))) {
					if ( coverType ) {
						e.remove( 0 );
						o = null;
					}
					else
						___setOption();
				}
				else if ( !coverType && e.options.length != 2 ) {
					o = D.createElement( "option" );
					___setOption();
					e.add( o, 0 );
				}
			}
			if ( !coverType && (( fD.required && e.options.length == 3 )
			|| ( !fD.required && e.options.length == 2 ))) {
				coverType = 'toggleSelect';
				if ( e.selectedIndex == -1 )
					e.selectedIndex = 0;
				fD.value = fD.resetValue = getEleValue( e );
			}
			if ( coverType) {
				fD.type = coverType;
				cover.getContent( e, coverType );
				if ( coverType == 'toggleSelect' && ( e = $( e.id + 'uLcover-0' )))
					e.style = "width:100%";
				else if ( coverType.slice( 0, 7 ) == 'dynamic' ) {
					if ( coverType == 'dynamicExtend' ) {
						if ( fD.first && fD.value[ 0 ])
							$( fD.first ).value = fD.value[ 0 ];
					}
					else if( !dynamicSelects[ fD.data ])
						dynamicSelects[ fD.data ] = [];
				}
			}
			else
				fD.type = 'select';

			//@ It was tried to configure the normal select list with elastic also,
			//@ but the combo box is not realy to control and acts not equal on the
			//@ different browsers. Its possible to stop the combo box via ev.
			//@ preventDefault(), but its impossible to restart it. It works in some
			//@ cases to set a className with select.hide * { display:none; }, but
			//@ it's nessesary then to create a temporary cover... On webkit even if
			//@ the select itself gets display:none; or the options
			//@ deleted after mousedown there are still combobox relicts. Well, at
			//@ least no crossBrowser solution found...

			function ___setOption() {
				o.text = lang.requiredSelect[ 0 ];
				o.value = "";
				if ( e.selectedIndex == -1 )
					e.selectedIndex = 0;
			}
		}

		function __initInput( e, fD ) {
		var
			pattern
			;
			switch( fD.type ) {
				case 'hidden':
				case 'checkbox':
				case 'radio': break;
				case 'icon':
					$CN.add( $P( e ), cssNS[ fD.type ]);
					$CN.add( $P( e ), e.value );
					$A( $P( e ), 'title', getLabelText( e ));
					$E( e, 'OC', function _iconChange() {
						$CN.remove( $P( e ), fD.resetValue );
						$CN.add( $P( e ), e.value );
					});
					break;
				case 'picture':
					$CN.add( $P( e ), cssNS[ fD.type ]);
					$A( $P( e ), 'title', getLabelText( e ));
					_set( e );
					$E( e, 'OC', _set );
					function _set( ev ) {
						ev = ev.target || ev;
						$A( $P( ev ), 'style', $A( $P( ev ), 'style' ).replace(
						/--ulpicture:url\([^\)]+\);/, '' ) +'--ulpicture:url("'
						+ ev.value +'");' );
						ev.value = '';
					}
					break;
				case 'pattern':
					pattern = $A( e, 'pattern' ) || '';
					if ( pattern.length && !cover.getContent( e, 'pattern', pattern ))
						___initPattern( e, fD, pattern );
					break;
				default:
					if ( !cover.getContent( e, fD.type ) && e.type == 'text' )
						___initPattern( e, fD, '' );
					break;
			}
		}
		
		function __initTextArea( e, fD ) {
			___initPattern( e, fD, $A( e, 'pattern' ) || '' );
			fD.type = 'textarea';
		}

		function ___initPattern( e, fD, p ) {
		var
			r = cover.pattern2Char( p )
			;
			if ( e.title.indexOf( r.chars ) == -1 )
				e.title += ' ' + r.chars;
			if ( p.length ) {
				p = ( p[ 0 ] != '^' ? '^(' : '' ) + cover.replaceShortn( p )
				+ ( p.slice( -1 ) != '$' ? ')$' : '' );
				fD.regexp = new RegExp( p );				
				fD.type = 'pattern';
				$E( e, 'OB', function _patternBlur() {
					if ( e.value.length && !fD.regexp.test( e.value )) {
						cover.keyMsg( e, 'Error' );
						dialog.setMsg( 'error', 'formatError', e, 5500 );
					}
					else
						dialog.removeError( e, e );
				});
			}
			fD.max = fD.max || ( !p.length ? conf.maxLengthInput
			: _calcMax(( r.charRegexp +'' )[ 2 ]));
			fD.charRegexp = r.charRegexp;
			if ( r.charRegexp && e.tagName != 'TEXTAREA' ) {
				$E( e, 'KD', useLib.call.coverBeforeInput );
				$E( e, 'KU', useLib.call.coverAfterInput );
			}
			$E( e, 'OF', useLib.call.coverOnFocus );

			function _calcMax( c ) {
			//@ if no max defined, try to figger out how long a good string could be.
			//@ Begin at longest value, because the shortest could be zero at least.
				for ( c = repeat( c, conf.maxLengthInput ); c.length; c = c.slice( 1 ))
					if ( fD.regexp.test( c ))
						break;
				return c.length || conf.maxLengthInput;
			}
		}
	}
				
	call.uLinit( 'taskstep', function( e, set ) {
	var
		t = conf.tasksteps[ 0 ]
		, i = -1
		;
		t.map( function( n, _i ){ if ( $CN.contains( e, cssNS[ n ])) i = _i; });
		if ( i > -1 ) {
			$CN.remove( e, cssNS[ t[ i ]]);
			getFormDef( e ).task = false;
		}
		if ( set || i < t.length -1 ) {
			$CN.add( e, cssNS[ set || t[ i + 1 ]]);
			getFormDef( $( $A(e,'for'))).task = set || t[ i + 1 ];
		}
	});

	function initOnCalc( f ) {
      for ( var i = 0, l = $Q( f, '[oncalc]' ), fS, nL = '', oC, t = ''
      ; i < l.length; i++ ) {
			fS = $P( l[ i ], 2 );
      	if ( $CN.contains( fS, cssNS.dynlist ))
      		fS = $P( fS, 1 );
      	oC = $A( l[ i ], 'oncalc' );
      	if ( oC.indexOf( "useLib.call.dynList.calcIt" ) > -1
      	&& !/_\d_$/.test( l[ i ].name ))
      		t = l[ i ].name.match( /(\.\w+)\.([\w-]+)$/ )[ 1 ]; // searchName
      	else if ( nL = oC.match( /('|")(([\w]+,)*([\w]+))\1/ )) {
      		t = nL[ 4 ];
      		nL = nL[ 2 ];
      	}
      	if ( t = $Q( fS, '[name$="'+ t +'"]', 1 )) {
      		uLeval( oC, nL, t, l[ i ] );
      		if ( $A( t, 'onchange' ))
      			setTimeout(( function( _t ) {
      				return function() { try { _t.onchange(); } catch(_){} };
      			})( t ), 100 );
			}
      }
   }

	function initExtend() {
	var
		ext
		;
		for ( var n in useLib.extend ) {
			ext = eval( n );
			for ( var v in useLib.extend[ n ]) {
				ext[ v ] = typeof useLib.extend[ n ][ v ] == 'function'
				? useLib.extend[ n ][ v ]
				: JSON.parse( JSON.stringify( useLib.extend[ n ][ v ]));
			}
		}
	}
	
	function initLandmarks() {
	var
		hL = htmlSupport.landmarks
		, e
		;
		for ( var l in hL )
			if ( e = hL[ l ] ? $Q( D, hL[ l ].selector, 1 ) : false ) {
				$A( e, 'role', l );
				if ( !$CN.contains( e.lastChild, cssNS.design ))
					e.innerHTML += '<div class="' + cssNS.design
					+ '"><span><span><span><span><span></span></span></span></span></span></div>';
			}
	}
	
	function initLinks( e ) {
	var
		t
		;
		e = $T( e, 'a' );
		for ( var i = 0; i < e.length; i++ )
			$E( e[ i ], 'click', function(){ delayRemove( this, cssNS.jumper, 5 ); });
	}

	function initTitle( e ) {
	var
		t
		;
		e = $Q( e, '[title]' );
		for ( var i = 0; i < e.length; i++ ) {
			t = e[ i ].title;
			if ( lang[ t ] ) {
				if ( lang[ t ][ 1 ] )
					DOM.insertBefore( e[ i ].firstChild
					, $CE([ 'span', { CN: cssNS.explain }, lang[ t ][ 1 ]])
					);
				if ( lang[ t ][ 0 ] )
					DOM.insertBefore( e[ i ].firstChild
					, $CE([ 'span', { CN: cssNS.inform }, lang[ t ][ 0 ]])
					);
				$A( e[ i ], 'title', -1 );	// remove title
			}
		}
	}

	function setRequiredLegend( f ) {
	var
		fD = formDef[ f.name ]
		, has = 0
		, all = 0
		;
		for ( var n in fD ) {
			all++;
			if ( !!fD[ n ].required ) 
				has++;						// ...count how many required elements
		}
		$CN.add( f
			, has == 0 ? ''								// ... to define legend of form
			: has == all ? cssNS.onlyRequired
			: has > 0.7 * all ? cssNS.manyRequired
			: cssNS.someRequired
		);
	}

var ownInput = ( function() {
   call.uLinit( 'ownInput', function( e, t ) {
   	if ( !ownInput.active ) {
   		if ( t && e.tagName == 'SELECT' ) {
   		// browsers fires no events on options, some browsers (chrome, edge) fire the
   		// select click on mouse down (wrong!). So a workaround is needed, because it's
   		// impossible to select another option then ownInput if its selected... Arggghhh!
   			setTimeout( function() {
   				if ( $CN.contains( e.options[ e.selectedIndex ], 'ownInput' ))
   					set( e, false );
   			}, 2000 );
   		}
   		else
   			set( e, false );
   	}
   });
	
   function set( e, SET=false ) {
   var
      p = '^['+ conf.charDefault +']+$'
   	, r = /^\d+(\.\d+)?(cm|mm|in|px|pt|pc|em|ex|ch|vw|vh|%)?$/
      , ex = false
      , o = false
      , d = false
      , v
      ;
		if ( e.tagName == 'OPTION' ) {
      	o = e;
      	e = $P( e ).tagName == 'SELECT' ? $P( e, 1 ) : $P( e, 2 );
      }
      if ( e.tagName == 'SELECT' ) {
      	if ( o || ( e.selectedIndex != -1 && ( o = e.options[ e.selectedIndex ])
      	&& $CN.contains( o, 'ownInput' )))
      		v = $A( e.options[ 0 ], 'value' );
         else
            return true;
      }
      else
      	v = $A( $Q( e.form, '[name="'+ e.name +'"]', 1 ), 'value' );
      if ( p = v.match( r )) {
      	d = p[ 2 ] || false;
         p = r.source.replace( /\([^\+]+\)/, d ? '('+ d +')' : '' );
      }
      else if ( !SET && ( v = v.match( optionExtRegEx ))
      && ( r = e.name || $P( e, 1 ).previousSibling.name )) {
         for( var n in lang.ownInputExtend ) {
            if (( ex = r.indexOf( n )) > -1 ) {
               ex = lang.ownInputExtend[ n ];
               break;
            }
         }
         ex = ex != -1 ? { ex: ex, active: v[ 1 ]} : false;
      }
      r = v = $A( o ? o : e, 'value' );
      if ( v == 'ownInput' )
         v = '';
      else {
         if ( ex )
            ex.active = v.match( optionExtRegEx )[ 1 ];
         v = v.replace( optionExtRegEx, '' );
      }
      if ( SET )
			_set( SET );
		else {
			ownInput.active = true;
			dialog.prompt( 'ownInput', _set, v, p, ex );
		}

      function _set( v ) {
      var
         t = conf.beforeOwnInput + lang.ownInput[ 0 ]
         ;
         if ( v === false )
         	v = r;
         if ( v.length && v != 'ownInput' ) {
            if ( d && v.indexOf( d ) < 0 )
            	v += d;
            t = conf.beforeOwnInput + v.replace( optionExtRegEx, '' );
         }
         else
            v = 'ownInput';
         if ( !!o && o.tagName == 'OPTION' ) {
            o.value = v;
            o.text = t;
            dispatchEvent( e, 'change' );
         }
         else {
            labels[ e.id ].lastChild.data = t;
         	$A( e, 'value', v );
            $A( e, 'checked', v == 'ownInput' ? -1 : 'checked' );
            e.checked = v != 'ownInput';
         }
         ownInput.active = false;
      }
   }
	
	return {
		init: function( f ) {
		//@ if option value is key word 'ownInput' a new option with input of user is added.
		//@ A element with ownInput needs this as className. The value may be ownInput or
		//@ the value tipped by the user. The belonging label or option text will contain
		//@ the value of conf.beforeOwnInput
		var
			l = $Q( f, '[value=ownInput],.ownInput' )
			, before = conf.beforeOwnInput
			;
			for ( var i = 0; i < l.length; i++ ) {
				if ( l[ i ].tagName == 'OPTION' )
					_initSelect( l[ i ]);
				else
					_initRadioCheckbox( l[ i ]);
			}

			function _initSelect( e ) {
				if ( e.text.indexOf( before ) < 0 )
					e.text = before + ( e.value == 'ownInput' ? lang.ownInput[ 0 ]	: e.value );
				$A( $P( e ), 'OC', "useLib.call.ownInput(this,0)" );
				$A( $P( e ), 'MC', "useLib.call.ownInput(this,1)" );
				$CN.add( e, 'ownInput' );
			}

			function _initRadioCheckbox( e ) {
				e = e.tagName == 'INPUT' ? e : e.firstChild;
				if ( labels[ e.id ]) {
				var
					v = labels[ e.id ].lastChild.data
					;
					if ( v.indexOf( before ) < 0 )
						labels[ e.id ].lastChild.data = before + ( e.value == 'ownInput'
						? lang.ownInput[ 0 ] : e.value );
					else if ( v.indexOf( lang.ownInput[ 0 ]) < 0 ) {
						e.checked = true;
						$A( e, 'checked', 'checked' );
						$A( e, 'value', labels[ e.id ].lastChild.data.replace( before, '' ));
					}
					$A( e, 'MC', "useLib.call.ownInput(this)" );
				}
			}
		}
		, set: set
		, active: false
	};
})();

/*****
 *
 *		functions to react standardized on arrowKeys, mnemonics and fastFeeder
 *		@ it was nessasary to do this 'global'
 *
*****/
var
	keyStore = ''			// fleeting store for mnemonic of next activeElement
	, lastActive = false	// approach to user expectations on next activeElement
	, keyControl = false	// flag to disable the arrow behavior
	, setCursorStep = function( e, c, ev ) {
	//@ this is done to give the same behavior to the cursor keys as tab and
	//@ shift+tab on allowed input elements it is to check, if the text cursor
	//@ stays left or right
	//@ see: https://html.spec.whatwg.org/multipage/input.html#do-not-apply on
	//@ selectionStart, especially safari dies in case...
	//@ REMARK: it was startet with a keyDownEvent on each element, but the event
	//@ did not fire in some cases and browser!
	var
		selectionNotAllowed =
		!/text|password|passwordrepeat|file|search|email|number|date|color|datetime|datetime-local|month|range|search|tel|time|url|week/.test( e.type )
		, step = ( c == 8 && $CN.contains( $P( e ), cssNS.isCover )
		&& !( e == $P( e ).firstChild ))
		|| c == 37 ? -1 : c == 39 ? 1 : false 			// BS, left, right
		, coverOpen = !e.onclick ? false
		: String( e.onclick ).indexOf( 'coverOpen' ) > -1 ? 1 : -1
		;
		if ( step && ( selectionNotAllowed || coverOpen
		|| step > 0 && e.selectionStart === e.value.length
		|| step < 0 && e.selectionEnd === 0 )) {
		var
			l = filterElements( e.form, true )
			;
			for ( var i = 0; i < l.length && l[ i ] !== e; i++ );	// find indexof id
			if ( l[ i ] !== e )	// not found?
				return false;
			// stop default behavior else selectedIndex changes i. e. firefox
			// WORKAROUND: ev.preventDefault() is not working
			if ( e.tagName == 'SELECT' ) {
				e.disabled = 'disabled';
				setTimeout( function(){
					e.disabled = false; setStatus( e, 'focused', 0 )}
				, 0 );
			}
			do {
				i += ( c == 39 ? 1 : -1 );	
			} while( l[ i ] && ( l[ i ].disabled	// ...selectable...
			|| !$P( l[ i ]).offsetWidth ));	// and displayed?
			if ( l[ i ]) {
				setFocus( l[ i ]);
				lastActive = l[ i ];
				$A( $( 'doFastfeeder' ), 'aria-label', getFormDef( l[ i ]).type
				+':'+ getLabelText( l[ i ]) +'='+ l[ i ].value );
			}
			else
				$A( $( 'doFastfeeder' ), 'aria-label', lang.fastLabel[ 1 ]);
			return true;
		}
		else if ((( selectionNotAllowed && e.type != 'range'
		&& e.tagName != 'SELECT' ) || !!e.onclick )
		&& ( c == 38 || c == 40 || c == 13 )) {	// ARROWUP or -DOWN, ENTER
			e.click();
			lastActive = e;
			return true;
		}
		else if ( !selectionNotAllowed && ev.shiftKey && c == 46 )
			e.value = '';
		else if ( c === 27 || c == 160 || c == 192 )	{ // ESC or keycodes with no keyup event
			lastActive = e;
			e.blur();
			return true;
		}
		return false;
	}
	;
	function bodyKeyControl( ev ) {
	var
		c = ev.keyCode
		, p = false
		, id
		;
		// don't react on input in fastfeeder
		if ( !ev.ctrlKey && !ev.metaKey
		&& ev.currentTarget.id != 'doFastfeeder' ) {
			if (( p = c === 113 ) && $( cssNS.fastfeeder )) {		// F2
				if ( !$CN.toggle( $( cssNS.fastfeeder ), cssNS.minimized ))
					$( 'doFastfeeder' ).focus();
			}
			else if (( c == 13 || c == 27 )	// ENTER or ESC
			&& ( id = String( dialog.layer.slice( -1 ))).length ) {
				id = id.slice( 0, -11 );	// remove uL__modal__, that was appended
				if ( c == 13 )	{
					if (( D.activeElement.type == 'radio' )
					|| /uL[\w\._-]+Filter/.test( D.activeElement.id ))
						D.activeElement.click();
					else {
						id = $Q( $( id + cssNS.modal ), 'button' );
						if ( id.length )
							id[ id.length - 1 ].click();
					}
				}
				else
					dialog.closeModal( id );
				p = true;
			}
			else if ( keyControl )
				p = keyControl( c, ev );
			else if ( D.activeElement.tagName == 'BODY' ) {
			//@ collect ''free'' input sign as possible mnemonic to jump there 
			// in case... SPACE (32), 0-9a-z, , 0-9 (@numpad), A-Z
				if ( !( c === 32 ) && ev.key.length == 1) {
					keyStore += ev.key;
					if ( !!mnemonics[ keyStore ] && !!( c = $( mnemonics[ keyStore ]))) {
						setFocus( c );
						keyStore = '';
						p = true;
					}
				}
				else if ( !!( lastActive = lastActive || D.forms[ 0 ].elements[ 0 ])
				&& ( p = ( c == 8 || c == 37 || c == 39 ))) // TAB, BS, LEFT, RIGHT
					setFocus( lastActive );
			}
			else if ( D.activeElement ) {
				keyStore = '';
				if ( D.activeElement.tagName == 'A') { 
					if ( !( c === 8 ) && ( p = c === 32 ))	// BS, SPACE
						D.activeElement.click();
				}
				else if ( D.activeElement && D.activeElement.form )
					p = setCursorStep( D.activeElement, c, ev );
			}
			if ( !!p ) {
				ev.preventDefault();
				ev.stopPropagation();
			}
		}
		else if ( ev.metaKey && c === 67 && D.activeElement ) {
			p = $( $A( D.activeElement, 'id' ).replace( /uLcover-\d+$/, '' ));
			if ( $CN.contains( p, cssNS.covered ) && ( p = getFormDef( p )))
				p = cover.getValue( p, p.type, 'local' );
			else
				p = p.value;
			if ( p && p.length ) {
				ev.preventDefault();
				navigator.clipboard.writeText( p );
			}
		}
	}

	function manageFormData( STORE, type='intern' ) {
	var
		stock = useLib[ type ]
		;
		for ( var i = 0, n; i < D.forms.length; i++ ) {
			n = D.forms[ i ].name;
			if ( n && n.length ) {
				if ( STORE )
			 		stock[ n ] = getJSONValues( null, n );
			 	else if ( stock[ n ])
			 		setJSONValues( stock[ n ], D.forms[ n ]);
			 	else if ( type != 'extern' )
			 		initForm( D.forms[ i ]);
			}
			else
				console.warn( "useLib hint: form has no name!" );
		}
	}

	function initBody() {
		DOM.scrollStop( true );
		if ( useLib.beforeUseLib )
			useLib.beforeUseLib();
		controls.init();
		initExtend();
		initLandmarks();
		initLinks( D.body );
		initTitle( D.body );
		initFold( D.body );
		initTooltips();
		if ( !conf.dataManagePath ) {
			manageFormData( false, 'extern' );
			manageFormData( false );
		}
		DOM.scrollStop( false );
	}
	function initUseLib() {
	//@ initialize the useLib controls
		$CN.add( D.body, cssNS.hasScript );
		$E( W, 'focus', firstAct );
		for ( var s in conf.sounds ) {
			if ( typeof conf.sounds[ s ] == 'string' )
				conf.sounds[ s ] = new Audio( conf.sounds[ s ]);
		}
		if ( !!conf.doUseLib ) {
			initId();
			$E( W, 'KD', bodyKeyControl );
	   	$E( W, 'paste', function _windowPaste( ev ) {
	   	var
	   		e = ev.target
	   		;
	   		if ( $P( e )
	   		&& $CN.contains( $P( e, 2 ), cssNS.hasCover )) {
	   			setTimeout( function() {
						e.dispatchEvent( new KeyboardEvent( 'keyup', { key: '' }));
	   			}, 0 );
	   		}
	   	});

			if ( !D.body.id )
				D.body.id = 'useLibKnown';
			initBody();
		}
	}
	$E( D, "DOMContentLoaded", function() {
	var
		nomix = 'DOCTYPE HTML'
		;
		useLib.ownHTML = '<!' + nomix + '>\n' + $T( D, 'html', 1 ).outerHTML.replace(
		/<style\s+id="uLsecureStore"[\s\S]+?<\/style>\n/, '' ).replace(
		/<link[^>]+uLaccess.json.css"[^>]+>/, '' );
		initUseLib();
	});

	return {
		conf: conf
		, htmlSupport: htmlSupport
		, cssNS: cssNS
		, wikiSigns: wikiSigns
		, langConf: langConf
		, patterns: patterns
		, call: call
		, calc: {}
		, intern: {}
		, extern: extern
		, landmarks: htmlSupport.landmarks
		, DOM: DOM
		, WIKI: this.WIKI
		, ajax: ajax
		, loadJSONCSS: loadJSONCSS
		, getHTMLTag: getHTMLTag
		, openPopup: openPopup
		, setMsg: dialog.setMsg
		, offMsg: function( id ) { dialog.closeBox( $( id )); return null; }
		, alert: dialog.alert		// ( t, fn, c )
		, prompt: dialog.prompt		// ( t, fn, v, p )
		, confirm: dialog.confirm	// ( t, fn, c)
		, setModal: dialog.setModal // ( id, type, attr, content, title, wF, fn )
		, listSelect: function( e, type, list, title, cllbck ) {
		//@ element, type =: 'listSelect' without radio buttons, 'singleSelect' or
		//@ 'multiSelect', listOfviewable/selectableElements, titleOfDialog,
		//@ callbackFunction() which gets selected value
		//@ REMARK: list is an array of groups
		//@ list = [ groupName0, { textContent: valueName0, value: x, shortCut: y }
		//@ , ...], [ ... ] where value and shortCut are optional
		//@ if no groupName is given empty headers will be created.
		//@ if list is a pure array of strings it is transformed to a list struct
		var
			id = ensureId( e )
			;
			if ( !e.value )
				e.value = "";
			if ( !!cllbck )
				call.uLinit( id, function( js ) {
					js = js[ id+'uL[]' ];
					cllbck( type == "multiSelect" ? js : js[ 0 ]);
				});
			if ( Array.isArray( list ))
				for ( var i = 0, gL=[]; i < list.length; i++ )
					list[ i ] = { textContent: list[ i ]};
			list = metaReaction.singleSelect.getContent( e, type, list );
			list = [ 'form', { name: id + cssNS.modal, 'data-ul': type
			, CN: cssNS[ type ]}].concat( list );
			dialog.setModal( id, false, 'draggable closable', list, title || '' );		
			return false;
		}
		, setStatus: setStatus
		, init: initUseLib
		, initForm: initForm
		, manageFormData: manageFormData
		, formDef: formDef
		, getFormDef: getFormDef
		, getStruct: getStruct
		, getHeadElement: getHeadElement
		, getContainer: getStruct.container // landmark, wikiContent
		, setContainer: function( lm, c ) { // landmark, wikiContent
		//@ replaces a DIV container in a content area
		var
			e = getLandmark( lm )
			, f
			;
			if ( e ) {
				f = $T( e, 'form' );
				for ( var i = 0; f && i < f.length; i++ )
					delete formDef[ f[ i ].name ];
				f = getStruct.container( lm, c );
				e = $P( e ).replaceChild( f, e );
				initBody();
			}
			return W;
		}
		, getBanner: function( tC, banner ) {
		var
			b = ""
			;
			if ( !!banner && banner.text )
				b = banner.text.replace( WIKI.regs.findEngine()
				, function( _, t ){ return _get( tC, t ); });
			else {
				b = '((logo))((copyright '+ _get( tC, 'copyright' )
				+ '))\n\n='+ _get( tC, 'title' ) +'\n\n=='+ _get( tC, 'description' );}
			return getStruct.container( 'banner', b );

			function _get( tC, t ) {
				return !!tC[ t ] ? tC[ t ] : t == 'copyright'
				? ( tC.author ? '© '+ tC.author : '' )
				: '';
			}
		}
		, toggleNavigation: function( ev, tId ) {
		//@ PARAMETER: event, textId
		//@ toggles the active link of a navigation area
		//@ REMARK: no LinkButtonId it's called not directly of naviButton =>
		//@ toggle naviButtons only and no call of new content
		var
			e = ev ? ev.currentTarget
			: $( "uLmainnavi" + tId.replace( /[^a-z0-9-_:\.]/gi, '' ))
			, old = getLandmark( 'navigation' )
			, wS = useLib.WIKI.sections
			, opener = false
			, stop = !ev
			; 
			if ( old = !!!old ? old : $Q( old, '.' + cssNS.active, 1 ))
				$CN.remove( old, cssNS.active );
			$CN.add( e, cssNS.active );
			if ( e == old )
				stop = true;
			else {
				if ( old = e.href.match( /[\/\?]?([\d\.]+)$/ ))
					tId = old[ 1 ];
				try {	// browsers crash on opener check if opened via _blank-Link!!!! >:[
					if( W.opener && W.opener.preview )
						opener =true;
				} catch( e ) {}
	         if( opener )
	         	stop = W.opener.preview.changeText( tId.replace( /uLmainnavi/, '' ), W );
	         else if ( wS && ( wS = wS[ 0 ] || wS[ 1 ]) && wS[ tId ]) {
	         	useLib.WIKI.getHeadElements( wS, tId );
	         	dispatchEvent( W, 'beforeunload' );	// just in case or other scripts...
	         	manageFormData( true );
	         	setTimeout( function() {
		         	useLib.setContainer( 'main', wS[ tId ].htmlWiki );
	         		DOM.replaceNode( getLandmark( 'banner' )
	         		, useLib.getBanner( wS[ tId ], wS[ 'banner' ]));
		         	useLib.WIKI.setHeadElements( D, wS[ tId ], null );
		         	D.body.id = tId;
		         	initBody();
		         	dispatchEvent( W, 'load' );
		         }, 500 );
		         stop = true;
	         }
         }
			if ( ev && stop ) {
				ev.preventDefault();
				ev.stopPropagation();
			}
		}
		, getTypePattern: function( t ) {
			return String( cover.getTypeRegexp( t )).slice( 1, -1 );
		}
		, extend: {}
		, checkDesign: checkDesign
		, getCheckSum: getCheckSum
		, extendCoverTypes: function( type, typeDef, mReaction=false, jsExpand=false ) {
			if ( !!useLib.WIKI.find && useLib.WIKI.find.type.indexOf( type ) < 0 )
				useLib.WIKI.find.type.push( type );		// tell WIKI the new type
			htmlSupport.type[ type ] = '[type="' + type + '"]';
			cssNS[ type ] = type;
			cover.types[ type ] = typeDef;
			if ( mReaction )
				metaReaction[ type ] = mReaction;
			if ( jsExpand )
				JSONexpands[ type ] = jsExpand;
		}
		, extendDynamicSelect: function( dName, js ) {
		var
			dS = {}
			, group
			;
			if ( !!dName && js ) {
				for( var i = 0; i < js.length; i++ )
					js[ i ].textContent = _get( js[ i ]);
				dynamicSelects[ dName ] = js;
			}

			function _get( _jsT ) {
				_jsT = _jsT.textContent;
				return ( _jsT[ conf.lang ] || _jsT.en || _jsT ).trim();
			}
		}
		, extendWikiEnsemble: function( type, langDef, typeDef ) {
			if ( this.WIKI.find ) {
				this.WIKI.find.type.push( type );
				this.WIKI.find.typeTrans.push(
					!!langDef ? langDef[ conf.lang ] || langDef.en : type );
			}
			this.wikiEnsemble[ type ] = typeDef;
		}
		, wikiEnsemble: wikiEnsemble
		, toTop: function(){ W.focus(); }
		, sortListAlphanumeric: function( url, i ) {
		//@ PARAM urlRelativeToUseLibPath or absolute, indexOf2DarrayToSort
		//@ sorts the list of the given JSON url in the right i. e. german way...
		//@ better before use ;-)
		var
			l = loadJSONCSS( url, function() { useLib.sortListAlphanumeric( url, i ); })
			;
			if ( !!l ) {
				i = typeof i == 'undefined' || i === false || isNaN( i ) ? false : i;
				l = l.list;
				if ( i === false ) 
					l.sort( function( a, b ){ return compare( a, b ); });
				else
					l.sort( function( a, b ){ return compare( a[ i ], b[ i ] ); });
				dialog.alert( "=sorted JSON of " + url + "\n\n((pre\n\n"
					+ JSON.stringify( l ).replace( i === false ? /(\[|\]|\",)/g
					: /(\",)/g, '$1\n')
					+ '\n))'
				);
			} 	// just  "because of the editor
		}
		, dynListAct: function( type, act ) {
			return call.dynList.action( $P( $Q( D, '[data-ul-dynlist^="'+ type +':"]', 1
			), 3 ), type, act );
		}
		, setJSONValues: setJSONValues
		, getJSONValues: getJSONValues
		, escapeUnicode: escapeUnicode
		, unescapeUnicode: unescapeUnicode
		// shorteners for extern use
		, $:$,$T:$T,$Q:$Q,$I:$I,$D:$D,$A:$A,$E:$E,$GS:$GS,$LS:$LS,$CN:$CN,$CE:$CE
		, repeat:repeat, compare:compare, str2obj: str2obj, optionExtRegEx:optionExtRegEx
		, delayRemove: delayRemove
	}
	;
})( window, document );
