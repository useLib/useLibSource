/*****
 *
 *		Start of the full wiki to html parser section
 *
 *		@ this is the full wiki version used in wikiEditor.html
 *		@ Here are allowed for sections:
 *		@ 		headers, nested listing or numeration with indent, all attributes,
 *		@		links, images, engine definitions
 *		@		blocks, especial forms with all interaktion types.
 *		@ 		The meta methods like section analize, table of content etc.
 * 
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *****/
/* CHANGES
	1.03: _____getAttr := mismatch if attribut without width
	1.04: __getChecked := workflowMarker from header/legend to element
	1.04: __getSelect := set class cssNS.select to enclosing container to find in css
	1.05: doForm := set info blocks to next and decide also, for easier testing; add
			buttontext to raise opportunities
	1.05: __getSelect := set attributs more flexible and a bit more transparent
	1.05: _getElements := handle optgroups and select correct, including id and rows
	1.05: __getOption / __getChecked := short define of value via $value on Text start
	1.05: ___getEnsemble := unique standard ids starting with uL and end with number
	1.06: ___getEnsemble := correct recursive ensemble name
	1.06: consistent use of data-ul
	1.07: __getChecked := empty string if not inter.attr.match( /title="[^"]+"/i ) 
*/
if ( !!window.useLib )
useLib.WIKI = (function( W, D ) {
	var u=useLib,$=u.$,$T=u.$T,$Q=u.$Q,$I=u.$I,$A=u.$A,$LS=u.$LS,repeat=u.repeat
	,$CN=u.$CN,DOM=u.DOM;
	var
		uLwikiVersion = '1.05'
	 	, wS = u.wikiSigns
	 	, conf = u.conf
	 	, langConf = u.langConf
	 	, lang = langConf[ conf.lang ]
	 	, cssNS = u.cssNS
	 	, extern = u.extern
	 	, wikiEnsemble = u.wikiEnsemble
		, sec = [ wS.header, wS.bullet, wS.number, wS.indent, wS.define, wS.table ]
		, shortestImg = "data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
		, interface = []
		, inter = {}			// last found interface object
		, sections = []
		, blocks = []
		, counter = 0
		, regs = (function() {
		var
			mask = function( t ) {
				return t.replace( /([\*\+\-\|\/\\\(\)\{\}\[\]])/g, "\\$1" );
			}
		 	, allSigns = (function() {
		 	var
		 		s = ''
		 		;
				for ( var c in wS )
					s += wS[ c ].slice( -1 );
				return mask( s );
		 	})()
			, attrReg = function( sign ) {
			//@ don't use ([\s\S]*?) instead of (.*?) to delimit it on single paragraph
				sign = mask( sign );
				return new RegExp( sign + '(.*?[^\\\\])' + sign, 'g' );
			}
			, getAttrSpan = function( _, t ) {
				return getHtmlTag( 'span', analyzeParameter( t ));
			}
			, putBlocks = function( g, l, t, i, r ) {
				g = !l || ( !!l && !!r ) ? '' : l;
				r = !r || ( !!l && !!r ) ? '' : r;
				r = g + blocks[ i ] + r;
				if ( t == 'I' )		// there may be standard blocks in I-blocks...
					r = regs.repl( r, 'putBlocks' );
				blocks[ i ] = '';		// clean storage a bit
				return t ? r : regs.repl( r, 'putBlocks' );
			}
			;
			return {
				mask: mask
				, buffer: ''
				, clean: function( s ) {
					// clean carriage return and comments
					return s.replace( /\r/gm, '' ).replace( /\/\*[\s\S]*?\*\//gm, '' );
				}
				, repl: function( s, t ) {
					if ( s && t == 'attributes' )
						for ( var i = 0; i < this[ t ].length; i++ )
							s = s.replace( this[ t ][ i ][ 0 ], this[ t ][ i ][ 1 ]);
					else
						s = s ? s.replace( this[ t ][ 0 ], this[ t ][ 1 ]) : s;
					return s || '';
				}
				, saveBlock: function( s, fn ) {
					blocks.push( fn ? fn( s ) : s );
					return '_uL_$ave' + ( blocks.length - 1 );
				}
				, getBlocks: function( b, e, s, fn, myBlocks ) {
				//@ REMARK: includes a special treatment to paragraph inform and exlain
				//@	blocks, else it would need a normInfo. Here is also the bock type
				//@	differentiated and marked. So they can be reinserted separate.
				//@	A analog behavior on links: following informs and exlains
				//@	belonging to them and have to block$aved with them (already
				//@	block$aved before) i. e. on links in navigation or behind images
				var
					t = b == wS.link ? -1 : b.slice( 0, 2 ) == wS.inform ? 1 : 0
					, n = ''
					, _blocks = myBlocks || blocks
					;
					if ( b.indexOf( 'pre' ) > -1 )
						b = mask( wS.block ) +'(code|pre)';
					else
						b = '('+ mask( b ) +')';
					e = mask( e );
					if ( t < 0 )
						e += "(\\n*_uL_\\$aveI\\d+){0,2}";
					else if ( t > 0 ) {
						n = '\n\n';
						b = '\\n*' + b;	// could already be paragraphed
						e += '\\n*';
					}
					return !!s ? s.replace( new RegExp( b + '([\\s\\S]*?)' + e, 'gm' ), 
						function( ss, c, _s ) {
							_blocks.push( _s.length ? fn( _s, t > 0 ? b.slice( 3 ) : b, c )
							+ ( t < 0 && ss.length > ( e = ss.indexOf( wS.linkEnd ) +2 )
							? ss.slice( e +2 ) : '' ) : '' );
							return n + '_uL_$ave' + ( t > 0 ? 'I' : '' )
							+ ( _blocks.length - 1 ) + n;
						}
					) : '';
				}
				, findEngine: function( e, t, lb ) {	// lb is for markup \u2003
					return new RegExp( mask( wS.engine ) + '\\s*('
					+ ( e || '[^\\s=\\}\\n]+') + ')\\s*(=)?([\\s\\S]*?)'
					+ ( e && /\|(extend|script|wikiEnsemble|dynamicSelect)\|/.test(
					'|'+ e +'|' ) ? lb || '\\n' : '' )
					+ mask( wS.engineEnd ), t || 'img' );
				}
				// put stored and operated inform and exlain blocks back into string
				, putIBlocks: [ /(<p>\s*)?_uL_\$ave(I)(\d+)(\s*<\/p>)?/g, putBlocks ]
				// put stored and operated blocks back into string
				, putBlocks: [ /(<p>\s*)?_uL_\$ave(X?)(\d+)(\s*<\/p>)?/g, putBlocks ]
				// mask backSlashes
				, maskSlashes: [ new RegExp( '\\\\([' + allSigns + '])', 'g' ), '\\$1' ]
				// mask backSlashes
				, unmaskSlashes: [ new RegExp( '\\\\([' + allSigns + '])', 'g' ), '$1' ]
				, markStringEngine: [ new RegExp( '([\\\\"|\'])' + mask( wS.engine )
				+ '\\s*(.*?)' + mask( wS.engineEnd ) + '\\s*\\1', 'g' ), '$1' + wS.engine
				+ 'uLjs-$2'	+ wS.engineEnd + '$1' ]
				// separate all section rows with \n
		 		, normSec: [ new RegExp( '\\n([' + mask( sec.join( '' )) + '])(?!'
		 			+ mask( wS.table ) + ')', 'g' ), '\n\n$1' ]
		 		// first join single linebreaks
		 		, normLines: [ /([^\n])\n([^\n])/gm, '$1 $2' ]
				// join rows with defined linebreak
				, normBreak: [ new RegExp( mask( wS.split ) + '\\n+', 'mg' ), '</br>\n' ]
				// join table rows defined to be continued
				, normTable: [ new RegExp( '\\n+' + mask( wS.table + wS.append ), 'g' )
					, wS.append ]
				// separate all <<hint>> and <<<<explanation>>>> marks to own rows with 
				// owns signs, so that they are unique against html tags
		 		, normInterface: [ new RegExp( '\\s*' + wS.interface + '([\\s\\S]*?)'
		 			+ wS.interface, 'gm' ), function( _, n ) {
		 			var
		 				a = { has: true }
		 				;
		 				// separate possibly automatically created attributes
						a.attr = !n ? '' : ( ' ' + n.trim()).replace(
							/\s+(value|name|id|type|data-ul)\s*=\s*"[^"]*"/g
							, function( s, t ) { a[ t ] = s;	return ''; }
						);
		 				interface.push( a );
		 				inter = a;
		 				return '@@' + ( interface.length - 1 );
		 			}
		 		]
		 		, restricted: [ /[^\s]/g, 'x' ]
		 		, clearShortCut: [ attrReg( wS.kbd ), '' ]
		 		, remainShortCut: [ new RegExp( '('+ mask( wS.kbd ) + '(.*?[^\\\\])'
		 		+ mask( wS.kbd ) + ').*$|[^'+ wS.kbd +']+', 'g' ), '$2' ]
		 		, getShortCut: [ attrReg( wS.kbd ), function( _, n ) {
		 		var
		 			b = '<' + conf.mnemonicsMarker + '>'
		 			, e = '</' + b.slice( 1 ) 
		 			;
	 				return b + n.trim().split( wS.split ).join( e + b ) + e;
	 			}]
				, attributes: [
					[ attrReg( wS.bold ), '<strong>$1</strong>' ]
					, [ attrReg( wS.italic ), '<em>$1</em>' ]
					, [ attrReg( wS.under ), '<u>$1</u>' ]
					, [ attrReg( wS.strike ), '<span class="' + cssNS.strike
					+ '">$1</span>' ]
					, [ attrReg( wS.mark ), '<span class="' + cssNS.mark
					+ '">$1</span>' ]
					, [ attrReg( wS.quote ), '<q>$1</q>' ]
					, [ attrReg( wS.sup ), '<sup>$1</sup>' ]
					, [ attrReg( wS.sub ), '<sub>$1</sub>' ]
					, [ attrReg( wS.kbd ), '<kbd>$1</kbd>' ]
					, [ attrReg( wS.typ ), getAttrSpan ]
					//, [ attrReg( wS.size ), getAttrSpan ]
					//, [ attrReg( wS.color ), getAttrSpan ]
				]
				, formText: [ new RegExp( mask( wS.typ ) + '([\\s\\S]+?)'
					+ mask( wS.typ ), 'gm' ), getAttrSpan ]
		 		, sec: new RegExp( '^[' + mask( sec.join( '' )) + ']+' )
				, header: new RegExp( '^' + mask( wS.header ) + '+' )
		 		, sectToSplit: new RegExp( '\\n+[' + mask( sec.join( '' ))
		 		+ ']+|\\n+', 'g' )
		 		, selectable: new RegExp( '\\s*([\\*#:-]+)?\\s*(.+?)\\s*'
		 		+ wS.append, 'g' )
		 		, findInterface: new RegExp( wS.interface + '(\\d+)' )
		 		, parameter: new RegExp(
				'^((-?\\d+((\\.|,)\\d+|%)?)|(#[0-9a-f]{6,6})|([a-z-_]+))('
				+ wS.split + ')?', 'i'
				)
				, numberation: /^\d+(\.\d+)*/   // id of section may be an numberation
				, fold: new RegExp( '(' + cssNS.fold + ')|(foldin)' )
				, appendSplit:  new RegExp( '\\s*'+ wS.append +'\\s*', 'g' )
				, ownInput: new RegExp(( 'own\\s?Input|'+ langConf.en.ownInput[ 0 ] +'|'
				+ lang.ownInput[ 0 ]).replace( /\./g, '\\.?' ).replace( / /g, ' ?' ), 'gmi' )
			};
		})()
	 	, find = {
	 		type: ( langConf.en.wikiTypes + ',' + conf.wiki.inputtypes.en
	 		).split( ',' )
	 		, className: ( langConf.en.wikiClasses + ',' + conf.wiki.classnames.en
			// add internals
	 		+ ',singleselect,multiselect,toggleselect,dynamiclist,dynamicarray,top'
	 		).split( ',' )
	 		, typeTrans: ( lang.wikiTypes + ',' + conf.wiki.inputtypes[ conf.lang ]
	 		).split( ',' )
	 		, classNameTrans: ( lang.wikiClasses + ','
	 		+ conf.wiki.classnames[ conf.lang ]).split( ',' )			
			, get: function( s, t ) {
			//@ for failure tolerance a keyword is searched in different languages  
			//@ (don't forget commas in wiki definition lists)
			var
				i = find[ t ].indexOf( s )
				;
				if ( i < 0 )
					i = find[ t ].indexOf( s.toLowerCase());
				if ( i > -1 )
					return find[ t ][ i ];
				else if (( i = find[ t + 'Trans' ].indexOf( s.toLowerCase())) > -1 )
					return find[ t ][ i ];
				return false;
			}
		}
		, getInterface = function( s ) {
				inter = {};
				return !s ? '' : s.replace( regs.findInterface
				, function( __s, _n ){ inter = interface[ _n ]; return ''; }
		)}
		, analyzeParameter = function( s, type ) {
		//@ analyze string after wiki signs for given parameters by separating them
		//@ into an array, standarize language and put it in a parameter struct
		//@ find them with /(#|==)?[\w]+%?/
		/* {
			color: HexValue				#[a-f0-9]{3,6}
			size: percentValue			[\d*%]
			width: givenPixel				[\d*] => em units	
			name:	givenName				==[\w]*
			position: nameOfPosition	[left|center|right]
			className: givenString		[\w]*
			text: restOfGivenText
		} */
		var
			p = { className: '', style: '', attr: '', width: false, inter: {} }
			, v = s.indexOf( '\n' )
			, l = -1
			;
			s = s.split( '\n' );
			s[ 0 ] = getInterface( s[ 0 ]);
			p.inter = inter;
			while ( l != s[ 0 ].length ) {
				l = s[ 0 ].length;
				s[ 0 ] = s[ 0 ].replace( regs.parameter
					, function( _s, _, d, __, ___, c, t ) {
					//@ directly replace identified parameter against '' and write them into a
					//@ a structure for different variations of use while creating the HMTL
					//@ REMARK: base for fast finding is a special regexp for parameter
						if ( d ) {							// decimal value
							if ( type == 'input' || type == 'textarea' ) { 
								d = d.replace( /,/, '.' );
								v = p.min ? ( p.max ? 'step' : 'max' ) : 'min';
								p[ v ] = d;
								p.attr += ' ' + v + '=' + d; 
							}
							else {
								p.value = $I( d );
								if ( d.slice( -1 ) != '%' )
									d = ( $I( d ) / 10 ) + 'em';	
								if ( !!type )
									p.width = d; 
								d = ( !type ? 'font-size:' : 'width:' ) + d + ';';
								p.style += d;
							}
						}
						else if ( c ) {
							if ( type == 'block' )
								p.style += 'background-';
							p.style += 'color:' + c + ';';
						}
						else if ( t ) {
							if ( v = find.get( t, 'type' ))
								p.type = v;
							else if ( v = find.get( t, 'className' )) {
								if ([ 'linebreak', 'gap', 'separator' ].indexOf( v ) !== -1 )
									p.break = v;
								p.className += ( p.className.length ? ' ' : '' )
								+ ( cssNS[ v ] || v || t );
							}
							else if (( v = langConf.en.colors.indexOf( t.toLowerCase())) > -1
							|| ( v = lang.colors.indexOf( t.toLowerCase())) > -1 ) {
								if ( type == 'block' )
									p.style += 'background-';
								p.style += 'color:' + langConf.en.colors[ v ] + ';';
							}
							else
								return _s;
						}
						return '';
					}
				);
			}
			p.text = s.join( '\n' ).trim();
			return p;
		}
		, createAttr = function( type, _s ) {
		//@ creates a AttributString based on the given string
		var
			attrList = {
			T:  'title'
			, S:  'style'
			, MC: 'onclick'
			, M2: 'ondblclick'
			, KD: 'onkeydown'
			, KU: 'onkeyup'
			, OF: 'onfocus'
			, OB: 'onblur'
			, OC: 'onchange'
			}
			;
			type = attrList[ type ] || type;
			_s = _s === true ? type : !_s ? '' : ( _s +'' ).trim(); 
			return _s.length ? ' ' + type + '="' + _s + '"' : '';
		}
		, getHtmlTag = function( tN, p, tC ) {
		//@ PARAM tagName, parameter: className, style, attr(ibuteString), textContent
		//@ REMARK characteristic of inline elements is, that they respect whitespaces
		//@ 	in the markup, so the entire tag is close on the next line to create
		//@ 	a readable html code.
		var
			tCdo = tC !== false
			;
			if ( tCdo )
				tC = !!tC ? tC : !!p.text && p.text.length ? p.text : '';
			return tCdo && ( !tN.length || !tC.length ) ? '' : '<' + tN
				+ createAttr( 'class', p.className || p.CN )
				+ createAttr( 'style', p.style || p.S )
				+ ( p.attr || '' ) + '>' + ( tCdo ? tC.trim() + '</' + tN + '>'
				: '' );
		}
		, createId = function( _s, id, asArray ) {
		//@ PARAM string, id := true ? id : name, flag: array to deliver as array
		//@ REMARK created a //field name// to secure delivery of all values
			_s = _s.length ? regs.repl( _s, 'clearShortCut' ) : '';
			_s = _s.match( /[a-z0-9_-]/gi );
			_s = _s ? _s.join( '' ) : '_';
			return !!id ? 'uL' + _s + ( counter++ ) : _s + ( !!asArray ? '[]' : '' );
		}
		, initAttr = function( p, _inter, a1, a2 ) {
			p.attr = ( p.attr || '' ) + ( _inter.attr || '' ) + ( a1 || '' ) + ( a2 || '' );
			return p;
		}
		, getButton = function( type, o, s ) {
			s = repeat( '<span>', 6 ) + s + repeat( '</span>', 6 );
			o.className = (( o.className || '' ) + ' ' + cssNS.button ).trim();
			if ( !!type )
				return getHtmlTag( type, o, s );
			else		// only repeat and className added... 
				return s;
		}
		, async = function() {
		var
			async = this					// declare what is 'this'...
			, store = {}
			, loadMain
			, loadBack
			, r
			;
			async.loading = 0;
			async.execute = function( _loadMain, _loadBack ) {
				loadMain = loadMain || _loadMain;
				loadBack = loadBack || _loadBack;
				r = loadMain();
				if ( async.loading === 0 ) {
					store = {};
					if ( typeof loadBack === "function" )
						loadBack( r );
					r = loadMain = loadBack = null;
				}
			};
			async.load = function( p ) {
				if ( !!!store[ p ] ) {
					async.loading++;
					store[ p ] = p;
					if ( /\.(png|gif|jpg)$/i.test( p ))
						(function( _p ) { _getImageData( _p ); })( p );
					else
						useLib.ajax( p, function( s ) { async.set( p, s ); }, 'txt', 2500
						, false
						, function( _ ) { async.set( p, p + ' missing / fehlt!' ); });
					return false;
				}
				else
					return store[ p ];

				function _getImageData( _p ) {
			   var
			      img = new Image()
			      ;
			      img.crossOrigin = 'Anonymous';
			      img.onload = function() {
			      var
			         can = D.createElement( 'CANVAS' )
			         , ctx = can.getContext( '2d' );
			         ;
			         can.height = this.naturalHeight;
			         can.width = this.naturalWidth;
			         ctx.drawImage( this, 0, 0 );
			         img = null;
			         async.set( _p, can.toDataURL( 'image/png' ));
			      };
			      img.onerror = function() {
			      var
			         msg = lang.fileMsg.slice( 0 )
			         ;
			         msg[ 0 ] = '=' + msg[ 0 ].replace( /%%/, _p );
			         useLib.setMsg( 'error', msg.join( '\n\n' ), null, 6500 );
			         img = null;
			         async.set( _p, _p );
			      };
			      img.src = _p;
			   }
			};
			async.set = function( p, s ) {
				store[ p ] = s;
				if ( --async.loading === 0 ) // run again, could be more loading
					async.execute( loadMain, loadBack );
			};
		}
	 	;

		function doStandard( s ) {
			s = regs.getBlocks( wS.htmlcomment, wS.htmlcommentEnd, s, _doComment );
			s = regs.repl( regs.repl( regs.repl( regs.repl( regs.repl( regs.clean( s )
			, 'maskSlashes' ), 'normSec' ), 'normBreak' ), 'normLines' ), 'normTable' );
			s = regs.getBlocks( wS.inform, wS.informEnd // block beginner information...
			, regs.getBlocks( wS.explain, wS.explainEnd, s, _doInfo ), _doInfo
			).replace( /</g, '&lt;' ).replace( />/g, '&gt;' );
			return s;

			function _doComment( _s ) {
				return wS.htmlcomment + _s + wS.htmlcommentEnd;
			}
			function _doInfo( _s, type ) {
				return getHtmlTag( 'span'
					, { className: cssNS[ type.slice( 1, -1 ) == wS.inform ? 'inform'
					: 'explain' ]}, doText( _s )
				);
			}
		}

	 	function doLinks( _s ) {
		//@ creates link or image depending on the parameters
		//@ REMARK: is called by replace, therefore dummy is needed
		var
			p = analyzeParameter( _s, 'block' )
			, attr = ( p.inter.name || '' )	+ ( p.inter.id || '' ) + ( p.inter.attr || '' )
			, i = p.text.indexOf( ' ' )
			, h = p.text.slice( 0, i > -1 ? i : 1000 ).trim()
			;
			_s = i > -1 ? regs.repl( p.text.slice( i + 1 ), 'attributes' ) : '\u0020';
			if ( p.type == 'button' )
				_s = getButton( false, p, _s );
			else if ( h == '#' )
				h = 'javascript:void(0)';
			else if ( p.className.indexOf( cssNS.extern ) > -1 )
				p.attr += ' target="_blank"';
			else if ( /data:image|\.(gif|jpeg|jpg|jpe|png|svg)\??/.test( h )) {
				if ( p.className.indexOf( cssNS.inline ) > -1 ) {
					p.attr += createAttr( 'alt', _s.replace( /<\/?[^>]+?>/g, '' ))
					+ createAttr( 'src', /dummy\./i.test( h ) ? shortestImg : h );
					_s = getHtmlTag( 'img', p, false );
				}
				else {
					// here: special attribute direct behind tagName
					i = { attr: createAttr( 'src', /dummy\./i.test( h ) ? shortestImg : h )
					+ ' ' + attr };
					p.className += ' ' + cssNS.image;
					if ( p.width ) {
						//p.style += 'position:relative;';
						p.attr += createAttr( 'MC', 'useLib.call.zoomImage(this)' );
						i.attr += createAttr( 'S', 'width:100%')
						+ createAttr( 'alt', _s.replace( /<\/?[^>]+?>/g, '' ));
					}
					_s = getHtmlTag( 'span', p, getHtmlTag( 'img', i, false )
					+ doText( _s, true ));
				}
				return _s;		// it's a image...
			}
			p.attr += createAttr( 'href', h ) + ' ' + attr
				+ ( !/(Section[\d\.]+|\.txt)($|#|\?)|^#.+/.test( h ) ? ''
				: createAttr( 'MC', 'useLib.call.checkWikiLink(event)' ));
			return getHtmlTag( 'a', p, _s );
		}

	 	function doForm( p, s, dialog ) {
		var
			a = regs.getBlocks( wS.typ, wS.typ, s, analyseBlock )
			, checkType = function( t, c ) {	// type, char
			//@ checks if char indicates type
				return t == 'select' ? c == wS.indent || c == wS.table
				: t == 'checked' ? c == wS.bullet || c == wS.number
				: false;
			}
			, buttonText = a.match( /^[^=\n]+/ )
			, headerStore = [ 0 ]
			, listType = []
			, inter = {}
			, dynType
			;
			if ( buttonText ) {
				buttonText = buttonText[ 0 ];
				a = a.slice( buttonText.length );
			}
			// buffer interface attributes and separate element rows 
			a = a.replace( regs.ownInput, 'ownInput' ).split( /[\n]+\n/ );
			p.attr += ( p.inter.name || ' name="formName' + a.length + '"' )
			+ ( p.inter.id || '' ) + ( p.inter.attr || '' )
			+ ( p.inter[ 'data-ul' ] || '' );
			s = getHtmlTag( 'form', p, false );
			for ( var i = 0, last = 0, lastHeader = 0, lastStart, decide = []
			; i < a.length; i++ )
				a[ i ] = _analyseRow( a[ i ]);
			_setDecide();	// set if lastHeaders are part of list wiki-sign lines
			for ( var i = a.length -2; i >= 0; i-- ) {
				if ( !a[ i ].type ) {
					a[ i ].next = a[ i +1 ].next;
					a[ i ].decide = a[ i +1 ].decide;
				}
			}
			/* for testing!!
			for ( var i = 0, ss=''; i < a.length; i++ ) {
				ss += i + ' ' + (a[ i ].type || 0) + ' ' + (a[ i ].p.type||'/')
					+ ' ' + a[ i ].l + ' ' + a[ i ].next
					+ ' ' + a[ i ].decide
					+ ' ' + (a[ i ].p ? a[ i ].p.text.slice(0,10).replace(/@@\d+/,'') : '' )
					+ ' ' + (a[ i ].p ? a[ i ].p.text.match(/@@\d+/) : '' ) + '\n';
			}
			alert(ss);*/
			if ( dialog == 'wikiEnsemble' )
				return a;
			for ( var i = 0, interName, name, id, wfMark = '', sCut, select = 0
			, optgroup = 0; i < a.length; i++ )
				a[ i ] = _getElements( a[ i ], a[ i ].p.text );
	 		return regs.repl( s + a.join( '' ) + _getElements( 1, '' )
	 		+ _getButtonRow( buttonText ), 'formText' ) + '</form>';

			function _analyseRow( _a ) {
			//@ it's quite tricky to decide what type of html tag: FIELDSET, DIV with#
			//@ H1 or H2..., LABEL or the trickiest OPTGROUP is to assign to a header
			//@ wiki. Therefore the sections are analysed in a pre loop and prepared a bit.
			//@ REMARK: there are also sections with other type like block or info, they
			//@ 	are not part of the hierarchy!
			var
				t = ( _a.match( regs.sec ) || [ '' ])[ 0 ]
				, t0 = false
				, l = 0
				, p
				;
				if ( t.length ) {
					t0 = t.charAt( 0 );
					l = t0 == wS.header ? t.lastIndexOf( wS.header ) + 1 : 0;
					p = analyzeParameter( a[ i ].slice( t.length )
						, l ? 'block' : t0 == wS.define ? 'input'
						: checkType( 'select', t0 ) ? 'select' : checkType( 'checked', t0 )
						? 'checked' : ''
					);
					a[ last ].next = t0;
					// set info blocks to t0 also, for easier testng 
					//@ it's tricky, to decide if its a optgroup: two following headers
					//@ have to be followed by a list wiki-sign.
					if ( p.break ) //&& p.break == 'separator' )
						decide = [];
					else if ( checkType( 'select', t0 )) {
						if ( decide.slice( -1 ) != lastStart )	// push only once
							decide.push( lastStart );
					}
					else if ( decide.length && ( !l || l != a[ decide.slice( -1 )].l ))
						_setDecide();
					if ( p.type && wikiEnsemble[ p.type ])// check expand => mark in lastHeader
						a[ lastHeader ].expand = true;
					if ( p.className.indexOf( cssNS.same ) > -1 && lastHeader && !p.style )
						p.style = a[ lastHeader ].p.style;	// take style of header before
					if ( t0 == wS.header ) {
						lastHeader = i;
						if ( a[ last ].l < l )
							lastStart = i;
						else if ( a[ lastHeader ].l > l )
							lastHeader = lastStart = 0;
					}
					last = i;
				}
				return { type: t0, l: l, p: p || { text: _a }, next: 0, decide: 0
					, wfMark: t.slice( l || 1 )
				};
			}

			function _setDecide() {
				decide.push( i );
				for ( var j = 0; decide.length > 2 && j < decide.length -1; j++ )
					for ( var k = decide[ j ]; k < decide[ j +1 ]; k++ )
						a[ k ].decide = '-';
				decide = [];
			}
			function _getElements( _a, _s ) {
			//@ The analysed prepared rows are transfered to HTML tags. Those tags have
			//@ diverent attributes that may //inherited// to the next rows depending on
			//@ the detected //header//. This attributes are stored in: id and name,
			//@ they may be set to false on changes of hierarchie and overwritten by
			//@ wiki interface tags.
			//@ Further attributes for wiki-extends: wfMark and sCut used for inputs.
			//@ For lists the flags: select and optgroup. 
			var
				s = ''
				;
				if ( _a === 1 )
					return __closeTags( _a, _s );	// just to end all started tags
				else if ( !_a.type )
					return _s;							// info or text etc...
				else {
					if ( _a.l ) {
						if ( _a.p.break )
							s = __getBreak( _s );
						else {
						// different cases have to be decided...
							s += __closeTags( _a.l, s );
							if ( _a.decide == '-' ) {
								s += ( !select ? __getSelect( id ) : '' )
								+ __getOptgroup( 'optgroup', _s );
								name = false;
							}
							else {
								if ( _a.l <= headerStore.length ) {
									if ( _a.next == wS.header )
										name = false;
									wfMark = '';
								}
								inter = _a.p.inter;
								wfMark = __analyseWorkflowMarker( _a.wfMark );
								if ( _a.p.type == 'hidden' )
									_a.p.className = cssNS.hidden;

								if ( _a.l == 1 || !!_a.expand || checkType( 'checked', _a.next )
								|| ( _a.next == wS.header && a[ i +1 ].decide != '-' ))
									s += __getFieldset( 'fieldset', _s, _a.l );
								else
									s += __getLabel( 'label', _s );
							}
						}
					}
					else {
						if ( checkType( 'select', _a.type ))
							s = ( !select ? __getSelect( id ) : '' )
							+ __getOption( __checkInter( _s ));
						else {
							s = __closeTags( headerStore.length	- optgroup, s );
							s += _a.type == wS.define ? __getInput( _s )
							: checkType( 'checked', _a.type )
							? __getChecked( __checkInter( _s ))
							: _s;
						}
					}
					return s;
				}

				function _checkType( t, _i ) {
					return checkType( t, a[ _i ].next ) || checkType( t, a[ _i ].next )
					|| checkType( t, a[ _i ].next )
				}

				function __closeTags( l, s ) {
					if ( l < headerStore.length ) {
					var
						_s = [ 'select' ]// insert select, but consider three cases
						, subList = 0
						;
						sCut = null;
						s = headerStore.slice( l ).reverse();
						if ( select && !checkType( 'select', _a.next )) {
							if ( s[ 0 ] == 'optgroup' ) {	// optgroup, select, ...?
								_s.unshift( s[ 0 ]);
								s = s.slice( 1 );
							}
							_s.push( 'div' );
							s = _s.concat( s );
							select = optgroup = 0;
						}
						s = '</' + s.join( '></' ) + '>';
						s = s.replace(
							/<\/(dynamiclist|dynamicarray):([^>]+)>/g
							, function( _, lType, xType ) {
								return '</fieldset>' + __getListEnd( lType, xType, subList++ );
							}
						);
						headerStore.length = l;
					}
					else
						s = '';
					return s;
				}

				function __checkInter( __s ) {
					if ( !_a.p.inter.has )		// first interface is found in parameters
						__s = getInterface( __s );
					else {
						inter = _a.p.inter;
						inter.has = false;
					}
					return __s;
				}

				function __analyseWorkflowMarker( marker ) {
				//@ adds flags for workflowmarker to parameter structure
				var 		//possible more... , xx: '#', yy: '+'
					mList = { required: '*', disabled: '-', restricted: ':', checked: '#' }
					, wfM = ''
					;
					for ( m in mList )
						wfM += createAttr( m, wfMark.indexOf( m ) > -1
							|| marker.indexOf( mList[ m ]) > -1
						);
					return wfM;
				}

				function __getSelect( _id ) {
				//@ creates a selectTag for different cases: there are //planned//
				//@ selects and selectLists that appears abrupt i. e. as part of a
				//@ starting radioField. The 2 type needs a including DIV-Container,
				//@ that has to be unrolled extra.
				//@ REMARK: if an id (and name) not initiated, they needed to create
				var
					attr = wfMark + ( inter.attr || '' )
					+ ( inter[ 'data-ul' ] || '' )
					;
					select = true;
					name = name || interName || inter.name || 'noName[]';
					attr += _id || inter.id || ( ' id="selectOf' + name.slice( 7 ));
					if ( _a.type == '+' || _a.next == '+' )
						attr += createAttr( 'multiple', true )
						+ ( attr.indexOf( ' size' ) < 0 ? ' size="3"' : '' ); 
					if ( _a.p.value ) {	// decide two values for width on lists...
						_a.p.width = _a.p.style.slice( _a.p.style.indexOf( ';' ) + 1 );
						_a.p.style = _a.p.style.slice( _a.p.width.length );
					}
					return '<div class="'+ cssNS.select +'">' + getHtmlTag( 'select', {
						S: _a.p.width, attr: name + attr
					}, false	);	
				}

				function __getHidden( tN ) {
					headerStore[ _a.l ] = tN;
					return getHtmlTag( tN, {}, false );
				}

				function __getBreak( s ) {
					id = false;
					if ( _a.p.break == 'linebreak' )
						s = _s.length ? getHtmlTag( 'span', _a.p, s ) : '</br>';
					else if ( _a.p.break == 'separator' ) {
						s = _a.p.type == 'hidden' ? '' : '<hr>';
						interName = name = false;
						inter = {};
						wfMark = '';
					}
					else
						s = getHtmlTag( 'div', _a.p, s || ' ' );
					return __closeTags( _a.l ) + s;
				}

				function __getFieldset( tN, s, l ) {
				var
					hasFold = _a.p.className.match( regs.fold ) || ''
					, j
					;
					dynType = _a.p.className && useLib.call.dynList
					? _a.p.className.match( /dynamiclist|dynamic(array)/i ) || false
					: false;
					headerStore[ l ] = tN;
					interName = inter.name || ( l == 1 ? false : interName );
					if ( dynType ) {
						listType.push( dynType[ 0 ].toLowerCase());
						// find type distance (and store between) caused on informs...
						dynType = false;
						for ( var j = i +1; j < a.length; j++ )
							if ( a[ j ].p.type ) {
								if ( !!wikiEnsemble[ _a.p.type ])
									dynType = __getFilterRow( a[ j ].p.type
									, interName.slice( 7, -1 ), _a.p.className );
								break;
							}
						if ( !hasFold ) {	// always foldable dynamic lists
							hasFold = [ 1 ];
							_a.p.className += ' ' + cssNS.fold;
						}
					}
					if ( !!hasFold ) {
						if ( hasFold[ 0 ] == 'foldin' )
							_a.p.className += ' ' + cssNS.formfold;
						else
							_a.p.className = _a.p.className.replace( cssNS.fold
							, cssNS.formfold + ' ' + cssNS.open );
						hasFold = createAttr( 'MC'
						, "if(!this.parentNode.classList.contains('"+ cssNS.disabled
						+ "'))this.parentNode.classList.toggle('"+ cssNS.open + "')" );
					}
					id = inter.id || createAttr( 'id', createId( s, 1, 0 ));
					name = interName
					|| createAttr( 'name', createId( s, 0, _a.next != ':' ));
					if ( l > 1 || ( _a.next != '=' && _a.next != ':' )) {
					// in case fieldset is 'groupLabel'
						sCut = s.match(regs.getShortCut[ 0 ]);
						sCut = sCut ? sCut[ 0 ].slice( 0, -2 ) : sCut;
						sCut = sCut ? regs.repl( sCut + conf.mnemonicsSeparator + '%%'
						+ sCut.slice( 0, 2 ), 'getShortCut' ) : '';
						if ( l > 1 && checkType( 'checked', _a.next ))
							_a.p.className += ' ' + cssNS.inline;
					}
					_a.p.style = _a.p.width ? '--ul'+ _a.p.style : "--ulwidth:100%;";
					return getHtmlTag( tN, initAttr( _a.p, inter, inter.id ), false )
						+ ( !s.length ? '' : getHtmlTag( 'legend', initAttr({
						// set className required if radio or checkbox group...
							className: !checkType( 'checked', _a.next ) ? null
							: _a.wfMark.indexOf( '*' ) < 0 ? cssNS.status : cssNS.required
						}, inter, hasFold || '' )
						, ( regs.repl( s, 'clearShortCut' ) || '&nbsp;' ))
					);
				}

				function __getLabel( tN, s ) {
				var
					_style = ''
					, _attr = ( inter.attr || '' ).replace( /style=("|')(.*?[^\\])\1/, function( _, __, s ) {
						_style = s.replace( /\\"/g, "'" );
						return '';
					})
					;
					headerStore[ _a.l ] = 'div';
					id = inter.id || createAttr( 'id', createId( s, 1, 0 ));
					inter.id = id;
					name = inter.name || interName
					|| createAttr( 'name', createId( s, 0, _a.next != ':' ));
					s = regs.repl( s, 'getShortCut' );
					if ( _attr.indexOf( 'restricted' ) > -1 )
						s = regs.repl( s, 'restricted' );
					_a.p.style = ( _a.p.width ? '--ul'+ _a.p.style : "width:auto;" ) + _style;
					return getHtmlTag( 'div', _a.p, false )
					+ getHtmlTag( tN, { attr: ' for' + id.slice( 3 ) + _attr }, s );
				}

				function __getOptgroup( tN, s ) {
					optgroup = 1;
					headerStore[ _a.l ] = tN;
					return getHtmlTag( tN, {
						S: _a.p.style 	// it's for the select modal dialog
						, attr: createAttr( 'label',  s.length ? s : '--------' )
						}, false
					);
				}

				function __getOption( s ) {
					return ( _a.wfMark + s + wS.append ).replace( regs.selectable
						, function( _, _attr, __s ) {
						var
							v = __s.match( /\$([^\s]+)\s+/ )
							;
							if ( v ) {
								__s = __s.slice( v[ 0 ].length );
								v = createAttr( 'value', v[ 1 ]);
							}
							else
								v = inter.value || createAttr( 'value', __s );
							_attr = _attr ? __analyseWorkflowMarker( _attr
							).replace( /checked/g, 'selected' ) : '';
							return getHtmlTag( 'option', { attr: _attr + v }
							, regs.repl( _attr.indexOf( 'restricted' ) > -1
							? regs.repl( __s, 'restricted' )	: __s, 'clearShortCut' ));
						}
					);
				}

				function __getChecked( s ) {
					return ( _a.wfMark + s + wS.append ).replace( regs.selectable
						, function( _, _attr, __s ) {
						var
							_id = ( inter.id || createAttr( 'id', createId( __s, 1 )))
							, v = regs.repl( __s, 'clearShortCut' ).match( /\$([^\s]+)\s+/ )
							;
							if ( v ) {
								__s = __s.replace( v[ 0 ], '' );
								v = createAttr( 'value', v[ 1 ]);
							}
							else
								v = inter.value || createAttr( 'value', regs.repl( __s, 'clearShortCut' ));
							_attr = _attr ? __analyseWorkflowMarker( _attr ) : '';
							return getHtmlTag( 'div', _a.p, false )
							+ getHtmlTag( 'input', {
								attr: _id + ( inter.name  || interName || name ) + v
								+ createAttr( 'type', _a.type == wS.bullet ? 'radio'
								: 'checkbox' )	+ _attr + ( inter.attr || '' ) + wfMark
								}, false
							)
							+ getHtmlTag( 'label', { attr: ' for' + _id.slice( 3 )
								+ ( inter.attr ? inter.attr.match( /title="[^"]+"/i ) || '' : '' )}
								, regs.repl( _attr.indexOf( 'restricted' ) > -1
								? regs.repl( __s, 'restricted' )
								: __s, 'getShortCut' )
							)
							+ '</div>';
						}
					);
				}

				function __getInput( s ) {
				var
					attr
					;
					inter = _a.p.inter;
					if ( _a.p.type == 'button' ) {
						attr = initAttr( _a.p, inter, inter.id, inter.name );
						attr.attr += wfMark;
						s = getButton( 'button', attr, s );
					}
					else {
						name = inter.name || name || interName;
						attr = initAttr( _a.p, inter, ( id || '' ) + name );
						if ( _a.p.type == 'textarea' ) {
							attr.attr = attr.attr.replace( 'min=', 'rows='
							).replace( 'max=', 'maxlength=' ) + wfMark;
							s = getHtmlTag( 'textarea', attr, s + ' ' );
						}
						else if ( !!wikiEnsemble[ _a.p.type ]) {
							if ( s.trim().length && !inter[ 'data-ul' ] )
								inter[ 'data-ul' ] = createAttr( 'data-ul', s );
							s = ( dynType || '' ) + ___getEnsemble( _a.p.type );
						}
						else {
							attr.attr += wfMark
							+ ( inter.type || createAttr( 'type', _a.p.type || 'text' ))
							+ ( inter.value || '' )
							+ ( inter[ 'data-ul' ] || '' )
							+ createAttr( s[ 0 ] == '[' ? 'pattern' : 'data-ul', s );
							s = getHtmlTag( 'input', attr, false );
						}
					}
					return s;

					function ___getEnsemble( type ) {
					var
						_s = ''
						;
						if ( listType.length && listType[ listType.length -1 ].slice( -4 )
						!== 'list' ) {	// ensure unique names in array (for all in path!)
							name = name.replace( /\[\d*\]/g, '[00000]' );
							if ( name.slice( -2 ) != ']"')
								name = name.slice( -2 ) + '[00000]"';
							id = createAttr( 'id', createId( type, 1, 0 ));
						}
						_s += ____getEnsemble( _s, type, 1. );
						if ( listType.length ) {
							_s = __getListChoicer( listType[ listType.length -1 ]
							, name.slice( 7, -1 ), type, _s );
						}
						return _s;

                  function ____getEnsemble( __s, type, percent ) {
                     for ( var k = 0, ex, _id, _attr, _label, isType, isEnsemble
                     ; k < wikiEnsemble[ type ].length; k++ ) {
                        ex = wikiEnsemble[ type ][ k ];
                        _label = ex.name[ conf.lang ] || ex.name.en;
                        _id = _attr = '';
                        if ( !( isType = ex.type == 'separator' )) {
                           // ensure unique ids on same named expands
                           _id = id.slice( 0, -1 ) + '_' + ( counter++ )+ '"';
                           _attr += name.slice( 0, -1 )
                           + '.' + ( ex.name.code || ex.name.en ) + '"' + wfMark;
                        }
                        for ( var t in ex ) {
                           if ( t == 'data' || t == 'data-ul' ) {
                              if ( ex[ t ] == 'inter' )
                                 inter[ 'data-ul' ] = ' data-ul' + _id.slice( 3 );
                              else if ( ex[ t ] == '%id%' )
                                 _attr += ( inter[ 'data-ul' ] || '' );
                              else
                                 _attr += createAttr( t, ex[ t ]);
                           }
                           else if ( t == 'type' ) {
                     			isEnsemble = false;
                              isType = ex[ t ].en || ex[ t ];
                              if ( !!wikiEnsemble[ isType ])
                                 isEnsemble = true;
                              else if ( isType[ 0 ] == '[' ||  isType[ 1 ] == '[' ) {
                                 if ( !!!ex[ t ].en )
                                    ex[ t ] = { en: ex[ t ], code: ex[ t ] };
                                 for ( var n in ex[ t ])
                                    ex[ t ][ n ] = ex[ t ][ n ].slice(
                                    isType[ 0 ] == '[' ? 1 : 2, -1 ).split( /\s*\|\s*/ );
                                 if ( isType[ 0 ] == wS.bullet
                                 || isType[ 0 ] == wS.number ) {
                                    _attr += createAttr( t, isType[ 0 ] == wS.bullet
                                    ? 'radio' : 'checkbox');
                                    isType = 'checked';
                                 }
                                 else {
                                    if ( isType[ 0 ] == wS.table )
                                       _attr += createAttr( multiple );
                                    isType = 'select'; // ...set default '-' also
                                 }
                              }
                              else {
                                 isType = ex[ t ];
                                 if ( isType != 'textarea' )
                                    _attr += createAttr( t, ex[ t ]);               
                              } 
                           }
                           else if ( t == 'title'
                           && ex[ t ].toLowerCase() == 'formname' ) {
                              _attr += createAttr( t, _label );
                              _label = "&#x200A;";
                           }
                           else
                              _attr += t == 'width' || t == 'style' || t == 'name'
                              || t == 'classname' ? '' : createAttr( t, ex[ t ]);
                        }
                        __s += isType == 'hidden'
                           ? getHtmlTag( 'input', { attr: _id + _attr }, false )
                           : isEnsemble ? ____getEnsemble( '', isType
                           	, _____getWidth( ex.width, 1 ))
                           : isType == 'checked'
                           ? _____getChecked( _id.slice( 0, -1 )) 
                           :  getHtmlTag( 'div'
                              , { style: '--ulwidth:' + _____getWidth( ex.width, 0 )
                              + ';' + ( ex.style || '' )
                              , className: isType ? ( cssNS[ isType ] || '' ) + ' '
                              + ( ex.classname || '' ) : null
                              , attr: isType == 'separator' ? _attr : null
                           }
                           , isType == 'button' ? getHtmlTag( 'button', { attr: _id + _attr
                              , className: cssNS.button + ' ' + ( ex.classname || '' )
                              }, false )
                           : isType == 'separator' ? getHtmlTag( 'span', {}, _label )
                           : ( isType == 'checkbox' || isType == 'radio'
                              ? getHtmlTag( 'input', { attr: _id + _attr }, false ) : ''
                           )
                           + getHtmlTag( 'label', { attr: ' for' + _id.slice( 3 )}
                              , (sCut ? sCut.replace( /%%/, k + 1 ) : '' ) + _label )
                           + ( isType == 'select' ? _____getSelect()
                              : isType == 'textarea'
                              ? getHtmlTag( 'textarea', { attr: _id + _attr }, ' ' )
                              : isType != 'checkbox' && isType != 'radio'
                              ? getHtmlTag( 'input', { attr: _id + _attr }, false ) : ''
                           )
                        );
                     }
                     return __s;

                     function _____getWidth( w, QUOT ) {
                        w = ( w || '' ).split( ';' )[ 0 ];  // clean it...
                        if ( w.indexOf( 'calc(' ) == -1 ) {
                        var
                           p = w.match( /(cm|mm|in|px|pt|pc|em|ex|ch|vw|vh|%)$/i )
                           ;
                           p = p ? p[ 1 ] : '%';
                           w = !w.length ? 100. : parseFloat( w );
                           w = !!QUOT ? w / 100 : Math.floor( w * percent ) + p;
                        }
                        return w;
                     }

                     function _____getAttr( c, r, SINGLE=false ) {
                     var
                        w = r.match( /^([#:-]+)?(\d+(em|%))?/) 
                        , attr = { r: r.replace( useLib.optionExtRegEx, '' ), w: ''
                        , a: createAttr( 'value', w ? c.slice( w[ 0 ].length )
                        : c )}
                        ;
                        if ( w[ 0 ].length ) {
                        	attr.r = r.slice( w[ 0 ].length );
                           if ( w[ 1 ]) {
                              attr.a += __analyseWorkflowMarker( w[ 1 ]);
                              if ( attr.a.indexOf( 'restricted' ) > -1 )
                                 attr.r = regs.repl( attr.r, 'restricted' );
                           }
                           if ( w[ 2 ])
                              attr.w = SINGLE ? '--ulwidth:'+ w[ 2 ]
                           	: 'width:' + w[ 2 ] +'!important';
                        }
                        return attr;
                     }

                     function _____getChecked( __id ) {
                     var
                        r = ex.type[ conf.lang ] || ex.type.en
                        , l = _label.length && _label != '&nbsp;'
                        , width = '--ulwidth:' + _____getWidth( ex.width, 0 )
                        ;
                        for ( var i = 0, ___s = '', attr; i < r.length; i++ ) {
                           attr = _____getAttr(( ex.type.code || ex.type.en )[ i ]
                           , r[ i ], r.length == 1 );
                           ___s += getHtmlTag( 'div', { S: attr.w }
                           , getHtmlTag( 'input', { attr: __id + i +'"'
                              + _attr + attr.a }, false )
                           + getHtmlTag( 'label', { attr: ' for' + __id.slice( 3 )
                              + i +'"' }
                           , (sCut ? sCut.replace( /%%/, k + 1 ) : '' ) + attr.r )
                           );
                        }
                        if ( r.length > 1 )
                           ___s = getHtmlTag( 'fieldset', { CN: cssNS.inline + ( l ? '' : ' '+ cssNS.hidden ), S: width },
                              ( l ? getHtmlTag( 'legend', {}, _label ) : '' ) + ___s );
                        return ___s;
                     }
                     function _____getSelect() {
                     //@ two types of select 1. "[a|b|c]" := value and text same
                     //@ 2. { code: "[a|b|c]", en: "[a|b|c]", de: "[a|b|c]"... }
                     //@ also there could be optGroups "[<x>a|b|c|<y>d|e|f]" with
                     //@ header x y and groups abc, def
                     var
                        r = ex.type[ conf.lang ] || ex.type.en
                        , v = ex.type.code || ex.type.en
                        , opt = r.indexOf( '<' ) > -1
                        ;
                        for ( var i = 0, __s = '', label, attr; i < r.length; i++ ) {
                           if ( !!opt ) {
                              label = false;
                              r[ i ] = r[ i ].replace( /<([^>]+)>/, function( _, l ){
                                 label = l;
                                 if ( v && v[ i ])
                                    v[ i ].replace( /<([^>]+)>/, '' );
                                 return '';
                              });
                              if ( label ) {
                                 if ( opt == -1 )
                                    __s += '</optgroup>';
                                 __s += '<optgroup label="' + label + '">';
                                 _opt = -1;
                              }
                           }
                           attr = _____getAttr( v[ i ], r[ i ]);
                           __s += '<option'+ createAttr( 'S', attr.w ) + attr.a +'>'
                           + attr.r //.replace( useLib.optionExtRegEx, '$1' )
                           + '</option>';
                        }
                        if ( opt == -1 )
                           __s += '</optgroup>';
                        r = getHtmlTag( 'select', { attr: _id + _attr }, __s );
                        return r;
                     }
                  }
               }
				}

				function __getListChoicer( lType, name, exType, s ) {
				//@ REMARK: don't use name tag for checkbox, so it is not processed on submit
				//@ and its a bit tricky to change checked on click caused on double events
				var
					call = "useLib.call.dynList.scroll(event,this)"
					;
					headerStore[ headerStore.length ] = lType + ':' + exType;
					return '\n' + getHtmlTag( 'fieldset', { className: cssNS.dynlist
							, style: 'width:100%;' }
						, getHtmlTag( 'legend', { attr: createAttr( 'MC', call )
							+ createAttr( 'M2', call )}
							, getHtmlTag( 'input', { attr: createAttr( 'id', name + '_choice' )
							+ createAttr( 'data-ul-dynlist', exType + ':' + name )
							+ createAttr( 'type', 'checkbox' )
							//+ createAttr( 'onclick', 'this.checked=!this.checked' )
							, className: 'uL_ignore'
							}, false
							) + getHtmlTag( 'span', {}, inter[ 'data-ul' ]
							? inter[ 'data-ul' ].slice( 10, -1 )
							:	name.replace( /\[0*(\d+)\]$/, '[$1]' ))
						)
						+ ___getButton( 'delete', '\u2796' )
						+ ___getButton( 'up', '\u25b2' )
						+ ___getButton( 'add', '\u2795' )
						+ s
					).slice( 0, -11 );

					function ___getButton( act, sign ) {
						return getHtmlTag( 'button', { attr: createAttr( 'MC'
							, "return useLib.call.dynList.action(event,'" + exType + "','"
							+ act + "')" ) + createAttr( 'T', lang[ act ][ 0 ])
							, className: cssNS[ act ]}, sign
						);
					}
				}

				function __getListEnd( lType, exType, subList ) {
				//@ REMARK: don't use name tag for select, so it is not processed on submit
		      var
		      	actions = _getOptions( lang.dynActions[ 0 ])
		      	+ _getOptions( lang.dynActions[ 1 ].slice( 0, subList ? 10 : -2 ))
		      	, call = "return useLib.call.dynList.action(event,'" + exType + "')"
		      	;
					listType.pop();
					return __getCalcRow( exType, interName ) + '\n'
						+ getHtmlTag( 'div', { className: cssNS.dynaction
							, style: 'width:100%;'
							, attr: ' data-count="0"' + createAttr( 'M2'
								, "useLib.call.dynList.scroll(event,this)" )
							}
						, getHtmlTag( 'label', {}, lang.dynActionInfo[ 0 ])
						+ getHtmlTag( 'span', { className: cssNS.inform }
							, regs.repl( lang.dynActionInfo[ 1 ], 'attributes' ))
						+ getHtmlTag( 'select', { attr: createAttr( 'OC', call )
						, className: 'uL_ignore' }, actions )
						+ getButton( 'button', { attr: createAttr( 'MC', call )}, 'go' )
					);

					function _getOptions( a, sel ) {
						return '<optgroup label="' + a[ 0 ] + '"><option' 
						+ ( sel ? ' selected="selected">' : '>' )
						+ a.slice( 1 ).join( '</option><option>' )+ '</option></optgroup>';
					}
		    	}

				function __getFilterRow( type, name, cN ) {
				var
					ex = wikiEnsemble[ type ]
					, hasAction = cN.indexOf( cssNS.noaction ) < 0
					, lEn = ex.code || ex.en
					, lLo = ex[ conf.lang ] || ex.en	
					, call = "return useLib.call.dynList."
					;
					ex = ex.types;
					for ( var k = 0, o = ''; k < ex.length; k++ )
						if ( !/separator|checkbox|radio|hidden|button/.test( ex[ k ].type ))
							o += '<option value="'+ lEn[ k ].replace( ' (optional)', '' )
							+ ( !k ? '" seleted="selected"' : '"' ) +'>'+ lLo[ k ] +'</option>';
					return '\n' + getHtmlTag( 'div', { style: "width:100%;"
					, CN: cssNS.dynfilter, attr: ' data-count="0"' }
					, getHtmlTag( 'label', {}, lang.dynFilterInfo[ 0 ])
					+ getHtmlTag( 'span', { CN: cssNS.inform }
						, regs.repl( lang.dynFilterInfo[ 1 ], 'attributes' ))
					+ getHtmlTag( 'div', {}, getHtmlTag( 'select', { CN: 'uL_ignore'
						, attr: createAttr( 'T', lang.filterButtons[ 0 ])
						}, o )
					)
					+ getHtmlTag( 'div', {}, getHtmlTag( 'input'
						, { CN: 'uL_ignore', attr: createAttr( 'type', 'text' )
						//+ createAttr( 'name', name + '.filterContent' )
						+ createAttr( 'KD', call + "catchRet(event,'"+ type + "')" )
						+ createAttr( 'T', lang.filterButtons[ 1 ])
						+ createAttr( 'autocomplete', 'off' )
						}, false )
					)
					+ getHtmlTag( 'div', { CN: cssNS.button },
						___getButton( "filter", 2, true ) 
						+ ___getButton( "filterInvers", 3 ) 
						+ ___getButton( "showAll", 4 ) 
						+ ___getButton( "sortup", 5 ) 
						+ ___getButton( "sortdown", 6 )
						+ ( hasAction ? ___getButton( "select", 7 ) : '' )
						+ ( hasAction ? ___getButton( "invert", 8, true ) : '' )
					));

					function ___getButton( t, num, invert ) {
						return getHtmlTag( 'div', {}, getHtmlTag( 'button'
							, { CN: cssNS[ t ] + ( invert ? ' ' + cssNS.invertIcon : '' )
								, attr: createAttr( 'T', lang.filterButtons[ num ])
								+ createAttr( 'MC', call +"filterOptions(this,'"+ type +"','"
								+ t + "')" )
							}, ' ' )
						);
					}
				}

				function __getCalcRow( type, _name ) {
				//@ REMARK: at least similar to ___getEnsemble(), but less complex
				var
					ex = wikiEnsemble[ type ]
					, cL = ___getCalcList( ex )
					, __s = ''
					;
					_name = cL.length && !!_name ? _name.slice( 7, -1
					).replace( /\[\](?=.*\[\])/g, '[00000]' ) : '';
					for ( var j = 0, k, calc, width; j < cL.length; j++ ) {
						k = cL[ j ];
						calc = ex[ k ].name.calc || '';
						width = ex[ k ].width || '100%';
						if (( m = conf.multiCalc[ ex[ k ].type ]) > 1 ) {
							width = $I( $I( width ) / m ) +'%';
							for ( _i = 0; _i < m; _i++ )
								__s += ___getCalc( 'float', width, calc
								+ ( _i > 0 ? '_'+ _i +'_' : '' ) );
						}
						else
							__s += ___getCalc( ex[ k ].type, width, calc );
					}
					return __s.length ? '\n' + getHtmlTag( 'div'
					, { S: "width:100%;", CN: cssNS.dynlist }, __s ) : '';

					function ___getCalc( type, width, calc ) {
					var
						lEn = ( ex[ k ].name.code || ex[ k ].name.en )
						;
						return getHtmlTag( 'div', {
						style: 'width:' + width
						, className: type == 'separator' ? cssNS.separator : null
						}
						, '<span'+ ( calc.length ? ' class="' + calc +'"' : '' )
						+ '></span>'+ ( calc.length ? getHtmlTag( 'input'
							, { CN: 'uL_ignore', attr:
							createAttr(	'type', type == 'float'	? 'float' : 'text' )
							+ createAttr( 'name', _name +'.'+ lEn +'.'+ calc )
							+ ( id ? id.slice( 0, -1 ) + '_' + ( counter++ )+ '"' : '' )
							+ createAttr( 'oncalc'
							, "useLib.call.dynList.calcIt(this)" )
							+ createAttr( 'disabled', true )
						}, false	)
						: '' ));
					}

					function ___getCalcList( ex ) {
					//@ if a wikiEnsemble has several rows (sum of width <= 100%) and
					//@ a row has no calculations => stepped over that row. This must
					//@ be pre-screened...
						for ( var k = 0, width = 0, w, cL = [], sub = [], has = false
						; k < ex.length; k++ ) {
							if ( ex[ k ].type != 'hidden' ) {
								sub.push( k );		// store indexes of row
								if ( !!ex[ k ].name.calc )
									has = true;		// row has calc, because of one calc found
								w = $I( ex[ k ].width );
								if (( !w || ( width + w >= 100 ))	// check if row is full...
								&& k < ex.length -1 ) {					// ... and next line
									if ( has )
										cl = cL.concat( cL, sub );
									has = false;
									sub = [];
									width = 0;
								}
								else
									width += w;		// width of active row
							}
						}
						if ( has )
							cL = cL.concat( cL, sub );
						return cL;
					}
				}
			}
	 		function _getButtonRow( buttonText ) {
	 		var
	 			s = ''
	 			, o = {}
				;
	 			if ( !!dialog ) {
	 				s = __getButton( dialog == 2 ? lang.cancel : lang.clear, 'button'
	 					, dialog == 2 ? 'closeForm' : 'clearForm' )
	 				+ __getButton( lang.reset, 'reset', 0 )
	 				+ ( dialog != 2 && useLib.call.storeself
	 				? __getButton( lang.storeself, 'button', 'storeself' ) : '' )
	 				+ __getButton( buttonText
	 				|| ( dialog == 2 ? lang.store : lang.submit ), 'submit', 0 );
					s = getHtmlTag( 'div', { className: cssNS.buttonRow }, s );
	 			}
	 			return s;

	 			function __getButton( s, t, c ) {
					return getButton( 'button', { attr: createAttr( 'type', t )+ ( c 
					? ' onclick="return useLib.call.' + c + '(this.form)"' : '' )}, s );
	 			}
	 		}
	 	}

		function doDynamicSelect( s ) {
		var
			a = doStandard( s ).split( /[\n]+\n\s*/ )
			, langorder = conf.wiki.langorder || [ 'de', 'en', 'code' ]
			, t = ''
			;
			for ( var i = 0, ex = false, name, act = ''; i < a.length; i++ ) {
				c = a[ i ].slice( 0, 1 );
				if ( c == '=' ) {
					if ( ex && a[ i ].slice( 1, 2 ) == '=' )
						_getRow( a[ i ].slice( 2 ), 'group' );
					else {
						_getExtend( name, ex );
						name = a[ i ].slice( 1 );
						ex = [];
					}
				}
				else if ( ex )
					_getRow( a[ i ].slice( 1 ), c == '*' ? 'radio' : 'checkbox' );
			}
			_getExtend( name, ex );
			return t;

			function _getRow( p, type ) {
			var
				js = { type: type }
				;
				js.shortcut = regs.repl( p, 'remainShortCut' );
				if ( !js.shortcut.length )
					delete js.shortcut;
				p = regs.repl( p, 'clearShortCut' ).split( regs.appendSplit );
				if ( p.length == 1 )
					js.textContent = p[ 0 ];
				else {
					js.textContent = {};
					for ( var j = 0, m; j < langorder.length -1; j++ ) {
						m = p[ j ] || m;
						js.textContent[ langorder[ j ]] = m;
					}
				}
				if ( type != 'group' )
					js.value = p[ p.length -1 ].trim();
				ex.push( js );
			}
			function _getExtend( _name, _ex ) {
				if ( _ex )
					t += '\nuseLib.extendDynamicSelect("'+ _name +'",'
					+ JSON.stringify( _ex ) +');\n';
			}
		}

		function doWikiEnsemble( s ) {
		var
			a = doForm({ attr: '', inter: {}}, regs.repl( doStandard( s )
			, 'normInterface' ), 'wikiEnsemble' )
			, calc = 'count,min,max,sum,average,deviation'.split( ',' )
			, langorder = conf.wiki.langorder || [ 'de', 'en', 'code' ]
			, t = ''
			;
			for ( var i = 0, k, w, ex = false, name, attr=''; i < a.length; i++ ) {
				if ( a[ i ].type == '=' ) {
					k = _getName( a[ i ].p.text.split( regs.appendSplit ));
					if ( a[ i ].l == 1 ) {
						_getExtend();
						name = k;
						ex = [];
					}
					else {
						attr = ( a[ i ].p.inter.has ? a[ i ].p.inter.attr : '' )
						+ ( a[ i ].p.inter[ 'data-ul' ] || '' )
						+ createAttr( 'multiple', a[ i ].next == '+' )
						+ createAttr( 'disabled', a[ i ].wfMark == '-' );
						ex.push( { name: k, width: a[ i ].p.width || '100%' });
						k = ex.length -1;
					}
				}
				else if ( ex ) {
					if ( a[ i ].type == wS.define )
						ex[ k ].type = a[ i ].p.type || 'text';
					else {
	               w = a[ i ].p;
	               w = ( w.width.length ? w.width : w.value || '' ) +' ';
						_getSelect( a[ i ].type, a[ i ].wfMark + w );
					}
					if ( attr.length || !!a[ i ].p.inter.has )
						( attr + a[ i ].p.inter.attr + ( a[ i ].p.inter.value || '' )
							).replace( /([\w-]+)="((.*?)[^\\])"/g
							, function( _, l, v ) { ex[ k ][ l ] = v; }
						);
				}
			}
			_getExtend();
			return t;

			function _getName( p=false ) { 
			var
				n = {}
				;
				if ( calc.indexOf( p[ p.length -1 ]) > -1 )
					n.calc = p.pop();
				for ( var j = 0, m = '&nbsp;'; j < langorder.length; j++ ) {
					m = p[ j ] || m;
					n[ langorder[ j ]] = m;
				}
				return n;
			}
			function _set( type, nL, wf, SET ) {
				nL = _getName( nL );
				for ( var _n in nL ) {
					if ( SET )
						ex[ k ].type[ _n ] = type +'['+ wf + nL[ _n ] +']';
					else
						ex[ k ].type[ _n ] = ex[ k ].type[ _n ].slice( 0, -1 ) +'|'
						+ wf + nL[ _n ] +']';
				}
			}
			function _getSelect( type, wf ) {
			var
				_t = a[ i ].p.text.split( regs.appendSplit )
				;
				if ( ex[ k ].type ) {
					if ( typeof ex[ k ].type == 'string' )
						ex[ k ].type = ex[ k ].type.slice( 0, -1 )+ '|'+ wf
						+ _t[ 0 ] +']';
					else
						_set( type, _t, wf, false );
				}
				else {
					if ( _t.length == 1 )
						ex[ k ].type = type +'['+ wf + _t[ 0 ] +']';
					else {
						ex[ k ].type = {};
						_set( type, _t, wf, true );
					}
				}
			}
			function _getExtend() {
				if ( ex ) {
				var
					_n = name.code
					;
					delete name.code;
					t += '\nuseLib.extendWikiEnsemble("'+ _n +'",'
					+ JSON.stringify( name ) +','
					+ JSON.stringify( ex ).replace( /,{/g, '\n,{' )// better readability
					+ ');\n';
				}
			}
		}
	 	function doPre( s, TOHTML=true ) {
	 		return regs.getBlocks( wS.block +'pre', wS.blockEnd, s
	 		, function( _s, _, t ) {
	 			if ( TOHTML ) {
				_s = _s.replace( /</g, '&lt;' ).replace( />/g, '&gt;'
					).replace( /\)\\\)/g, '))' );
					if ( t == 'code' )
						_s = getHtmlTag( 'code', {}, _s );
					return getHtmlTag( 'pre', {}, _s );
				}
				else
					return wS.block + t + _s + wS.blockEnd;
	 		});
	 	}
	 	function doText( s, noPara ) {
	 	//@ nPara := HTML 4.01 specification: The P element represents a paragraph. It
	 	//@ cannot contain block-level elements (including P itself). =>
		//@ span class=cssNS.Para split wiki text into sections and analyze begin for
		//@ wiki of header and lists and add belonging html tags before and after
		s = '\n'+ regs.repl( s, 'attributes' );
		var
			sec = [''].concat(( s.match( regs.sectToSplit ) || [])) // cut to fields of secType...
			, text = s.split( regs.sectToSplit )				// ... and content
			, i = -1
			;
			sec = sec.join( '|' ).split( /\s*\|\s*/g ); 		// trim all sec
	 		return regs.repl( _getLevel( 0 ), 'unmaskSlashes' );
		
			function _getLevel( headLevel ) {
			var
				_s = ""
				, type
				;
				while( ++i < text.length ) {
					if ( sec[ i ].length ) {	// has wiki of header or list...
						type = sec[ i ].charAt( 0 );
						if ( type == wS.header ) {
							if ( headLevel == 0 || sec[ i ].length == headLevel ) {
								headLevel = sec[ i ].length;
								_s += getHtmlTag( 'h' + headLevel, analyzeParameter( text[ i ]))
								+ _getLevel( headLevel + 1 );
							}
							else {
								i--;
								break;
							}
						}
						else {						// ... its a form, table, list or definition
							i--;
							if ( type == wS.table )
								_s += _getTable( type, 0 );
							else
								_s += _getList( type, 0 );
						}
					}
					else 	// ... its a normal paragraph
						_s += _getPara( text[ i ]);
				}
				return _s;
			}
		
			function _getList( type, level ) {
			//@ a quite complicate recursion, because li-Tags have to surround listType-tags
			//@ and the nesting of the wiki list signs needs also a look forward
			var
				cDL = false		// className for width of definition list or tab row
				, tNext = ""	// result of recursion for next level from look forward
				, aType			// actual type
				, t = ""			// result html text
				, p				// parameter
				;
				while( ++i < text.length ) {
					if (( aType = sec[ i ].charAt( level )) == type || aType == wS.indent ) {
						p = analyzeParameter( text[ i ], true );
						if ( sec[ i ].length > level +1 ) {		// change on same level...
							tNext = sec[ i ].charAt( level +1 );// ... no content on this level 
							p.text = "";
							i--;
						}
						else // next line list wiki with higher level => include tNext in LI-tag
							tNext = sec[ i +1 ] ? sec[ i +1 ].charAt( level +1 ) : ""; 
						tNext = tNext.length && tNext != wS.header ? _getList( tNext, level +1 ) : "";
						if ( type == wS.define ) {		// its a definition list
							p.text = ( p.className + p.text ).split( wS.define + wS.define );
							cDL = !cDL && p.width ? 'uLdl-' + parseInt( p.value /10 ) : cDL;
							t += getHtmlTag( 'dt', {}, p.text[ 0 ] )
								+ getHtmlTag( 'dd', {}, p.text[ 1 ] + ( tNext || '' ));
						}
						else {									// its a normal list, may be a indent
							cDL = cDL || p.className;
							if ( p.className.indexOf( cssNS.tabs ) > -1 ) {
								p.text = p.text.replace( /^(.+)( _uL_)/
									, function( _, t, e ) {
										return getButton( 'a', { attr: createAttr( 'href'
											, 'javascript:void(0)' ) + createAttr( 'MC'
											, 'return useLib.call.tabs(this);' )}, t 
										) + e;
								});
							}
							t += getHtmlTag( 'li'
								, { className: aType == wS.indent ? cssNS.indent
								: p.className.indexOf( cssNS.show ) > -1 ? cssNS.show
								: null }
								, p.text + ( tNext || '' )
							);
						}
						tNext = "";							// clear for next level change
					}
					else {
						i--;									// restart one level higher
						break;
					}
				}
				return getHtmlTag( type == wS.define ? 'dl' : type == wS.number ? 'ol' : 'ul'
					, { className: cDL }
					, t 
				);
			}

			function _getTable( type, level ) {
			//@ collects all rows of a table, analyzes parameter and instructions and
			//@ creates a (simple) html table struct (not accessibility like!)
			var
				reg = /::[\*-]*/g	// to split rows if table is structured in lines
				, col = [ '' ]		// to store column format information
				, row = ''			// to store if rows longer then one line
				, span				//	to analyze instruction of merging columns and rows
				, t = ""				// result htmltext
				, p = analyzeParameter( text[ i + 1 ], true )
				;
				text[ i + 1 ] = p.text;
				while ( ++i < text.length ) {
					row += text[ i ];
					if (sec[ i ].charAt( 0 ) == wS.table ) {
						if (sec[ i ].charAt( 1 ) == wS.number ) {	// table format info
							col = row.split( reg );
							col[ 'group' ] = '';
							for (var j=1; j < col.length; j++) {
								col[ j ] = analyzeParameter( col[ j ].trim(), true );
								col['group'] += '<col width="'
								+ ( col[ j ].size || col[ j ].width || 'auto' ) + '">';
							}
							if (col['group'].length)
								t += getHtmlTag( 'colgroup', {}, col[ 'group' ] );
						}
						else {										// its a full table row
							span = row.match( reg ) || [];
							span.unshift( sec[ i ].slice( 1 ));
							row = row.split( reg );
							for ( var j = 0; j < row.length; j++ )// create all cells of a row
								row[ j ] = getHtmlTag( 'td', {
										className: col[ j + 1 ] ? col[ j + 1 ].position : ''
										, attr: _getSpan( span[ j ] )
									}
									, row[ j ].trim()
								);							
							t += getHtmlTag( 'tr', {}, row.join( '' ));
						}
						row = "";										// clear for next row
					}
					else {
						i--;		// to restart after end of table
						break;
					}
				}
				return getHtmlTag( 'table', p, col[ 0 ] + t );
			
				function _getSpan( t ) {
				//@ analyze amount of merging signs on each cell to create col- and rowspan
				var
					span
					; 
					return ((
						span = t.match( /-/g ))
						? ' colspan="' + ( span.length +1 ) + '"' : '' )
						+ ((
						span = t.match( /\*/g ))
						? ' rowspan="' + ( span.length +1 ) + '"' : '' );
				}
			}
			function _getPara( _s ) {
				if ( !_s.trim().length )
					_s = ''; 
				else if ( _s.slice( 0, 13 ) == '_uL_$ave' )
					_s = ' ' + _s;
				else if ( noPara )
					_s = getHtmlTag( 'span', { className: cssNS.para }, _s );
				else
					_s = getHtmlTag( 'p', {}, _s );
				return _s;
			}
		}

		function analyseBlock( _s ) {
		//@ blocks have to be treated special, i. e. pre is not changed...
		//@ they also may load extern elements and text
		var
			p = analyzeParameter( _s, 'block' )
			, d = [ cssNS.console, cssNS.form, cssNS.dialog ].indexOf( p.className )
			;
			if ( d > -1 )
				_s = doForm( p, doStandard( p.text ), d );
			else if ( p.className == cssNS.inline )
				_s = doText( doStandard( p.text ));
			else if ( p.className == 'iframe' ) {
				p.attr += createAttr( 'src', p.text );
				_s = getHtmlTag( 'iframe', p, '&nbsp;' );
			}
			else {
				p.attr += ( p.inter.name || '' )	+ ( p.inter.id || '' ) + ( p.inter.attr || '' );
				if ( p.type == 'button' )
					p.className = cssNS.buttonRow;
				_s = getHtmlTag( 'div', p, p.text.length ? doText( doStandard( p.text ) 
					, p.type == 'button' ) : '\u200B' );
			}
			return _s;
		}

		function toHTML( s ) {
			blocks = [];
			interface = [];
			s = regs.getBlocks( wS.block, wS.blockEnd
				, regs.getBlocks( wS.link, wS.linkEnd
					, regs.repl( doPre( s ).replace( regs.findEngine(), '' )
					, 'normInterface' )
				, doLinks )
			, analyseBlock );
			s = regs.repl( doText( doStandard( s )), 'putBlocks'
			).replace( /<p>\s*<\/p>/g, ' ' );		// clear empty paragraphs
			for ( var i = 1; i < 7; i++ )
			 	_headerContainer( i );
			return  regs.repl( s, 'putIBlocks'		// iBlocks after headerContainer
			//).replace( regs.unmask, "$1"			// unmask masked chars   ?????
			).replace( /<p>\s*<div|<(\/)div>\s*<\/p/gm, '<$1div'// clean block </?p>
			).replace( /([^&])#/g, "$1&#x23;"		// well, html is special...
			).replace( /&lt;\/br&gt;/g, '</br>' );

			function _headerContainer( c ) {
			//@ insure div containering of content belonging to each header of
			//@ given level this allows easy accordeons for example
			//@ REMARK: all hierarchic acting ends on form tags
			var
				p = 0
				, b
				, e
				;
				while ( p < s.length && ( p = s.indexOf( '<h' + c, p )) > -1 ) {
					e = s.indexOf( '<form ', p );
					if ( s.slice( p -6, p ).indexOf( '<li' ) < 0 ) {
						b = ( s.slice( p ).match( // find header end incl. following iBlocks
							new RegExp( "[\\s\\S]*?<\\/h" + c + ">(\\n*_uL_\\$aveI\\d+){0,2}"
						)) || [''])[ 0 ].length + p;
						if ( e == -1 ) {
							e = s.slice( b ).search( // find next >=-header or end
								new RegExp(
									"<h(" + '123456'.slice( 0, c ).split( '' ).join( '|' ) + ")"
								)
							);
							e = e == -1 ? s.length : e + b;
							p = e + 12;			
						}
						else
							p = s.indexOf( '</form>', e ) +7;
						s = s.slice( 0, b ) + '<div>' +  s.slice( b, e ) + '\n</div>'
						+  s.slice( e );
					}
					else
						p = ( e == -1 ? p : s.indexOf( '</form>', e )) +7;
				}
			}
		}
		
		function extendClasses( v, sec ) {
			v = v.trim().split( /,\s*|\s+/g );
			v.filter( function( a ) {
				if ( find.className.indexOf( a ) < 0 )
					find.className.push( a );	// add only if not found
				return false;
			});
			v = v.join( ',' );
			if ( !sec.classes || sec.classes.indexOf( v ) < 0 )
				sec.classes = ( sec.classes ? sec.classes +',' : '' )	+ v;
		}

		function setHeadElements( doc, sec, extend ) {
		var
			tL = ( 'title description author keywords style script stylePath scriptPath'
			).split( ' ' )
			, k = 0
			;
			while( e = doc.getElementById( 'uLdynId' + k++ ))
				e.parentNode.removeChild( e );
			DOM.removeNode( $( 'uLdynIdstyle' ));
			DOM.removeNode( $( 'uLdynIdscript' ));
			k = 0;
			for ( var i = 0, t; i < tL.length; i++ ) {
				t = tL[ i ];
				if ( sec[ t ]) {
					if ( t == 'style' || t == 'script' ) {
						useLib.getHeadElement( doc, t, 'uLdynId' + t, null, sec[ t ]);
						// if ( t == 'script' ) {
						// 	doc.body.appendChild( doc.getElementById( 'uLdynId' + j ));
						// 	execute( sec[ t ][ j ]);
						// }
					}
					else if ( t == 'stylePath' || t == 'scriptPath' ) {
						for ( var j = 0; j < sec[ t ].length; j++ ) {
							useLib.getHeadElement( doc, t == 'stylePath' ? 'link' : 'script'
							, 'uLdynId'+ k, sec[ t ][ j ], null );
						}
					}
					else
						useLib.getHeadElement( doc, 'meta', 'uLlocal' + t, t, sec[ t ]);
				}
			}
			if ( !!extend )
				execute( extend );

			function execute( s ) {
			// it's a bit tricky to execute a dynamic body inline script...
			// The window or defaultView of the given head is nessesary
			// It may be a child window... Several attempts where made...
			// (create scriptTag, setTimeout, new Function etc. )  
				// useLib.getHeadElement( $T( doc, 'body', 1 ), 'script'
				// , 'uLscriptLocal', null, sec[ t ]);
				//setTimeout( new Function( sec[ t ]), 0);
				try {
					( doc.defaultView || W ).eval( s );
				} catch( e ) {
					useLib.setMsg( 'error', '=Error in script:\n\n'+ e.message
					, null, 7000 );
				}
			}
		}

		function getScriptStyle( sec, type, v ) {
		//@ REMARK: always append non path definitions to last entry, because they
		//@ may overwrite declarations done in global files...
		v = v.trim();
		var
			path = /^[^ ]+\.(css|js)(\?[^ ]+)?$/.test( v ) ? 'Path' : ''
			;
			if ( !sec[ type + path ])
				sec[ type + path ] = path.length ? [] : '';
			if ( path.length ) {
				if ( sec[ type + path ].indexOf( v ) < 0 )
					sec[ type + path ].push( v );
			}
			else if ( sec[ type ].indexOf( v ) < 0 )
				// ensure styles against ugly error causing signs...
				sec[ type ] += '\n'+ ( type == 'style' ? v.replace( /\s/g, ' ' ) : v );
		}

		function getHeadElements( section, tId ) {
		var
			sec = section[ tId ]
			, uLextend = ''
			, locals = { 'description': 1, 'date': 1, 'keywords': 1, 'copyright': 1
			, 'filename': 1 }
			;
			delete sec.style;
			delete sec.script;
			delete sec.stylePath;
			delete sec.scriptPath;
			blocks = [];
			sec.htmlWiki = regs.repl(regs.clean( doPre( sec.text, false )
			).replace( regs.findEngine(), function( all, t, def, v ) {
				v = v.trim();
				def = def || '';
				if ( !!v.length ) {
					if ( locals[ t ] && def.length )
						sec[ t ] = v;
					else if ( locals[ t ] && sec[ t ])
						return sec[ t ];
					else if ( t == 'extend' ) {
						if ( uLextend.indexOf( v ) < 0 )
							uLextend += '\n' + def + v;
					}
					else if ( t == 'dynamicSelect' )
						uLextend += '\n' + doDynamicSelect( def + v );
					else if ( t == 'wikiEnsemble' )
						uLextend += '\n' + doWikiEnsemble( def + v );
					else if ( t == 'classes' )
						extendClasses( v, sec );
					else if ( t == 'style' || t == 'script' )
						getScriptStyle( sec, t, def + v );
					else if ( !!t.length ) {
						section.placeHolder[ t ] = v.replace( /\n/g, '\\n'
						).replace( /"/g, '\\x22' ).replace( /'/g, '\\x27' );
					}
					else
						return all;
				}
				else if ( !!t.length && section.placeHolder[ t ])
					return section.placeHolder[ t ];
				else
					return all;
				return '';
			}), 'putBlocks' );
			if ( !!uLextend.length )
				( new Function( 'try {'+ uLextend + '} catch( e ) {'
				+ 'console.warn( "useLib.extendWikiEnsemble: define failure" ); }'
				))();
			section.uLextend = ( section.uLextend || '' ) + uLextend;
			return sec;
		}

		function analyseMetaStruct( s, loadBack, LOADALL ) {
		//@ PARAM StringToAnalyse, loadFileIfReadyCallBack
		var
			isLocal = String( location.protocol ) == 'file:' // testing: || true
			, metaDefs = 'title|version|author|coauthors|description'
			, _async = new async()
			, append = 0
			;
			blocks = [];
			s = s.split( regs.findEngine( 'section' ));
			if ( s.length > 1 ) {
				append = sections.length || 1;
				sections[ append ] = isLocal ? { uLloading: [], uLfound: []} : {};
				setTimeout( function(){ _async.execute( _analyseMetaStruct
				, loadBack )}, 100);
			}
			else
				sections[ append ] = {};
			sections[ append ].placeHolder = {};
			_initSec( 'main', s[ 0 ].trim() );
			return sections[ append ].main.text;

			function _initSec( id, _s ) {
			var
				sec = sections[ append ][ id ] || { include: false }
				, engineDefs = ( "classes keywords date language copyright script style "
				+ "extend wikiEnsemble dynamicSelect" ).split( ' ' )
				, ignoreEngines = 'script style extend'.split( ' ' )
				, __s
				;
				// 1. not in orgText
				_s = _s.replace( regs.findEngine( metaDefs +'|include|filename' )
				, function( g, t, __, v ) {
					if ( t == 'include' ) {
						if ( !sec[ t ]) {
							sec.filename = sec.filename || v.replace( /.txt$/i, '.html' );
							sec[ t ] = [];
						}
						if ( sec[ t ].indexOf( v ) < 0 )
							sec[ t ].push( v );
						return '\u280F\u280F' + v + '\u280F\u280F';
					}
					else if ( t == "coauthors" )
						sec[ t +'[]' ] = v.split( /[,;]/ );
					else
						sec[ t ] = v;
					return !v.length && metaDefs.indexOf( t ) > -1 ? g : '';
				});
				regs.clean( _s ).replace( regs.findEngine( 'script|style' )
				, function( _, t, __, v ) {
					if ( /^http[s]?:\/\//.test( v ))
						getScriptStyle( sec, t, v );
					else if ( LOADALL
					&& /^[^ ]+\.(css|js)(\?[^ ]+)?$/.test( v ))	{
						if( __s = _async.load( v ))
							getScriptStyle( sec, t, v );
					}
					else 
						getScriptStyle( sec, t, v );
				});
				_s = _s.replace( regs.findEngine(), function( all, t, def, v ) {
					if ( t == 'classes' )
						extendClasses( v, sec );
					else if ( engineDefs.indexOf( t ) > -1 && v.length ) {
						if ( ignoreEngines.indexOf( t ) == -1 )
							sec[ t ] = v;
					}
					else if ( !!t.length && v.length
					&& !sections[ append ].placeHolder[ t ]) {
						sections[ append ].placeHolder[ t ] = v.replace( /\n/g, '\\n'
						).replace( /"/g, '\\x22' ).replace( /'/g, '\\x27' );
					}
					return all;
				});
				sec.text = _s.trim();
				sections[ append ][ id ] = sec;
				return sections[ append ][ id ];
			}

			function _analyseMetaStruct( _s, nested ) {
			// if call of function is !nestet, take main S and return last section!!
				_s = doPre( _s, false );
				_s = !!nested ? _s.split( regs.findEngine( 'section' )) : s;
				for ( var i = 1; i < _s.length; i += 4 )
					__includeSec( _s[ i +2 ], _s[ i +3 ].trim(), nested );
				return !!nested ? _s[ 0 ] : sections[ append ];

				function __includeSec( id, __s, nested ) {
				var
					sec = _initSec( id, __s )
					;
					if ( sec.include ) {
						for ( var j = 0; j < sec.include.length; j++ ) {
							if ( isLocal ) {
								__s = $LS( sec.include[ j ]) || $LS( sec.include[ j ], null, true );
								if ( !__s )
									sections[ append ].uLloading.push( sec.include[ j ]);
								else
									sections[ append ].uLfound.push( sec.include[ j ]);
							}
							else
								__s = _async.load( sec.include[ j ]);
							if ( __s) {
								__s = __s.replace( regs.findEngine(	metaDefs )
								, function( _, t, __, v ) { // clean doubles...
									if ( !sec[ t ])
										sec[ t ] = v;
									return '';
								});
								if ( LOADALL )
									__s = __s.replace( /(\[\[([^\s]+\s)?)(.+?\.(png|gif|jpg))/gmi
									, function( _, b, __, p ) {
										return b + _async.load( p );
									});
								sec = _initSec( id, sections[ append ][ id ].text.replace(
								'\u280F\u280F' + sec.include[ j ] + '\u280F\u280F', __s ));
								sec.text = _analyseMetaStruct( sec.text, true );

								sec.text = regs.repl( sec.text, 'putBlocks' );
								sections[ append ][ id ] = sec;
							}
						}
					}
					else
						sections[ append ][ id ] = sec;
				}
			}
		}

		function getTableOfContents( level, secs, check ) {
		// PARAMETER: maxLevelToBeShown, sectionStruct, checkIfContentIsAvailable 
		var
			ToC = []
			, num
			, s
			, l
			;
			level = min( max( level || 2, 1 ), 5 );
			for ( var n in secs ) {
				if ( n != 'uLloading' && n != 'uLfound' ) {
					s = secs[ n ];
					if (( num = n.match( regs.numberation ))
					&& ( l = num[ 0 ].split( '.' ).length ) <= level ) {
						num[ 1 ] = ( '.' + num[ 0 ]).replace( /\.(\d+)/g, function( _, d ) {
							return ( '000' + d ).slice( -3 );
						});
						num[ 1 ] = $I(( num[ 1 ] + '00000000000000' ).slice( 0, level * 3 ));
						// tell missing, if check and no text
						if ( check && s.include
						&& ( s.loading || !s.text || !s.text.length ))
							secs[ n ].text = lang.fileMsg[ 0 ].replace( /%%/, s.include[ 0 ]);
						ToC.push({ id: n, level: l, number: num[ 0 ], order: num[ 1 ]
							, title: s.title || 'title missing!'
							, filename: s.filename || s.title.replace( /\W/g, '' )
							+ '.html' || 'file'+ n +'.html'
							, description: s.description || ''
							, date: new Date( s.date || Date.now()).toISOString()
							, language: s.language || useLib.conf.lang
							, hasContent: s.text ? s.text.replace( regs.findEngine(), '' ).trim().length
							: s.include ? s.include[ 0 ].length : 0, include: s.include
						});
					}
				}
			}
			ToC.sort( function( a, b ) {
				return ( a.order > b.order ) ? 1 : ( a.order < b.order ) ? -1 : 0;
			});
  			return ToC;
		}

      function getNavigation( ToC, active=false ) {
      var
         _ToC = []
         ;
         for ( var i = 0, s, d, fN, extern; i < ToC.length; i++ ) {
            s = ' '+ wS.bold + ToC[ i ].number +'.'+ wS.bold +' '
            + ToC[ i ].title;
            fN = ToC[ i ].filename || false;
            d = !ToC[ i ].description ? false
            : '\n'+ wS.inform + ToC[ i ].description + wS.informEnd +'\n';
            if ( ToC[ i ].hasContent ) {
            	extern = fN && /^https?:\/\//.test( fN );
            	fN = extern ? fN : fN ? fN.slice( fN.lastIndexOf( '/' ) +1 ) : false;
               s = ( d ? wS.block + '\n\n' : '' ) + wS.link
            	+ ( active == ToC[ i ].number ? 'active;;' : '' )
               + 'button '
               + ( !fN ? '#' : extern ? fN : fN +'?'+ ToC[ i ].number)
               + s +' @@ id="uLmainnavi'
               + ToC[ i ].id.replace( /[^a-z0-9-_:\.]/gi, '' )
               + ( extern ? '' : '" onclick="useLib.toggleNavigation(event,this.id);' )
               + '" @@ '+ wS.linkEnd + ( d ? d + wS.blockEnd : '' );
				}
            else
               s = wS.block + 'inline' + useLib.repeat( wS.header, ToC[ i ].level )
               + s + ( d || '' ) + wS.blockEnd;
            _ToC[ i ] = useLib.repeat( wS.bullet, ToC[ i ].level ) + s;
         }
         return _ToC.join( '\n\n' );
      }
	 	return {
			toHTML: toHTML
			, analyseMetaStruct: analyseMetaStruct
			, getHeadElements: getHeadElements
			, setHeadElements: setHeadElements
			, getTableOfContents: getTableOfContents
			, getNavigation: getNavigation
			, sections: sections
			, sectionSigns: sec
			, mask: regs.mask
			, regs: regs
			, find: find
			, async: async
		};
	})( window, document );
	//******* end of WIKI part ********************************************//