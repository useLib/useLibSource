/*****
 *
 *		KONFIGURATION:: you may change some defaults, to customize...
 *
 *****/
window.useLibConf = (function() {
var useLibPath = location.host == 'localhost' ? "./useLib/"
: 'https://[ ownServerAdress ]/useLib/'
, designPath = useLibPath + "designs/"
, conf = {
	conf: {
		doUseLib: true
		, useLibPath: useLibPath
		, designPath: designPath
		, cssPath: 'https://[ ownServerAdress ]/useLib/'
		, dataManagePath: false	 // ajax path to formDef management i.e. 'getDataApi.php'
		, wikiPath: useLibPath + 'wikis/'	// path for wiki files (if filename only)
		, testSubmitOnly: false	// for testing reasons a submit values shows as alert box
		, inputIsLocal: false // true := input.values checked as and return localTypes
		, userSupport: 
		//@ this is to customize the behavior of the user configuration supplement
			'sizer explainer designer security jumper fastfeeder hamburger showhelp'
		, statusSupport:
		//@ to customize the tracking of the status of each form element. Also,
		//@ which parameter keys are initialized via formDef (delete for no support)
			'type value resetValue defaultValue mnemonic disabled focused changed '
			+ 'required seacher completer '
			+ 'invalid technical banned restricted completed archived '
			+ 'requested estimated adjusted verified'
		, designDefs: {
		//@ there must be a link like:
		//@ <link rel="Stylesheet" href="designs/useLib.css" title="normal"
		//@ type="text/css">
		//@ It is used to change the href by useLib, to prevent collisions with
		//@ alternate stylesheets, that may not be used in your framework...
		//@ you may add more Designs with language specific names
			normal: { path: designPath + "standard.css" }
			, 'into the blue': {
				path: designPath + 'intoTheBlue.css'
			}
			, 'old style': {
				path: designPath + 'oldstyle.css'
			}
			/*
			, 'green legend': {
				path: designPath + 'greenLegend.css'
				, copyright: 'Irmhild Hockemeyer Schmallenberg'
				, email: 'irmhild.hockemeyer@t-online.de'
			}
			, modern: { path: designPath + 'modern.css' }
			, 'green fantasy': {
				path: designPath + 'greenFantasy.css'
				, copyright: 'DFI, Cologne'
				, email: 'df@use-optimierung.de'
			}
			, thebrownone: {
				path: designPath + 'theBrownOne.css'
				, en: 'the brown one'
				, de: 'das Braune'
			}
			// example is useful in dark environment and for special disabilities
			, indarkness: {
				path: designPath + 'inDarkness.css'
				, en: 'in darkness'
				, de: 'im Dunkeln'
			}
			// example without any css
			, designless: {
				path: designPath + 'designless.css'
				, en: 'designless'
				, de: 'designfrei'
			}
			*/
		}
		, designDefault: 'normal'
		, designVersion: 1
		, worldIndustrialLevel: 0 // 0 := all countries sorted on regions or up to 4 levels
		, sounds: {
			inputError: useLibPath + 'sounds/error.wav'
			, inputCarry: useLibPath + 'sounds/ping.wav'
			, inputHint: useLibPath + 'sounds/inputHint.wav'
			, alarm: useLibPath + 'sounds/alarm.mp3'
		}
		, defaults: {
			currency: 'EUR'
			, unit: 'm'
			, fon: '49'
		}
		, wiki: { // defined special wiki identifiers (only a-z and -_)
			classnames: { en: "", de:"" }
			, inputtypes: { en: "", de:"" }
			//, langorder: [ 'de', 'en', 'code' ] // [DEFAULT]
		}
		// some 
		, beforeOwnInput: '\u270F\uFE0F\u2009'	// signs to mark and identify owninputs 
		, mnemonicsMarker: 'tt'			// html tag for mnemonic identification
		, mnemonicsSeparator: '.'
		, fullYearFuture: 50				// max years behind today for full year input
		, fullYearPast: 150				// max years before today for full year input
		, smallYearFuture: 10			// max years behind today for small year input
		, calendarMonth: 3 				// standard displayed months in date selector
		, selectLimit: 12					// over value option length it's a modal dialog
		, filterLimit: 6					// over value length multiArrays get filterRows
		, maxLengthInput: 255			// default, if no length is defined
		, charDefault: ' 0-9a-zA-Zww.\\-'// chars available via rotor if no ...
												// ... pattern is defined, false := no charRotor
		, multiCalc: { vat: 2 , net: 2 , unit: 2, isounits: 2, currency: 2 }
		, elasticStep: 35					// pixel per speed-up steps
		, fastFeederRows: 3
		, hashPassword: "5$?ß@"			// password is transformed to hash directly.
												// The given value is used as salt!
		, puzzlePassword: true			// puzzle password on password widget
		, taskSupport: true				// task status can be set by user and submited
		// Attention: If list is extended, file: useLib.css must be adapted!
		, tasksteps: [ 'requested|estimated|adjusted|verified'.split( '|' )
			, 'requested|estimated|adjusted' ]// for taskCheck if not verified
		, errorCheck: true
		, noscript: false					// run if javascript is not allowed on a client 
	}
	, wikiSigns: {
	//@ may change the Wiki signs to your special belongings. But please be careful,
	//@ it may cause formating problems. DON'T USE < and >, they are not changeable or
	//@ allowed. A new sign has to be relatively in the same position.
		block: '((', blockEnd: '))'
		, link: '[[', linkEnd: ']]'
		, engine: '{{', engineEnd: '}}'
		, explain:'<<<<', explainEnd:'>>>>'	// explain before inform to insure matches
		, inform: '<<', informEnd: '>>'
		, comment: '/*', commentEnd: '*/'	// not changeable it's realy standard 
		, htmlcomment: '<!--', htmlcommentEnd: '-->'	//  - " -
		
		, interface: '@@'
		
		, header: '='
		, bullet: '*'
		, number: '#'
		, indent: '-'
		, define: ':'
		, table:  '+'

		, bold:	'++'
		, italic:'//'
		, under: '__'
		, strike:'--'
		, quote: '""'
		, sup: 	"''"
		, sub: 	',,'
		, kbd: 	'§§'
		, typ: 	'&&'
		, mark: 	'~~'

		, split:  ';;'
		, append: '::'
	}
	, wikiEnsemble: {
		// REMARK: pattern correct without slashes, but chrome throws errors
		person: [
			{ name: { de: "Anrede", en: "title" }, type: "title", width: "25%", required: 'required' }
			, { name: { de: "Vorname", en: "prename" }
			, type: "text", width: "30%", pattern: "[ A-Za-zww.\\-]{2,30}" }
			, { name: { de: "Nachname", en: "name" }
			, type: "text", width: "45%", pattern: '[ A-Za-zww\\-]{2,55}'}
		]
		, address: [
			{ name: { de: "Organisation", en: "organization" } 
			, type: "text", pattern: "[ A-Za-zww+*.&#'~;?!@;,_\\-]{2,55}"
			}
			, { name: { en: "", code: "person" }, type: "person", required: 'required' }
			, { name: { de: "Zusatz", en: "additional" }, type: "text" }
			, { name: { de: "Straße", en: "street" }
			, type: "text", pattern: '[ 0-9A-Za-zww.\\-]{5,55}', required: 'required'
			}
			, { name: { de: "Ort", en: "location" }
			, type: "towns", required: 'required'
			}
		]
		, shipping: [
			{ name: { de: "Organisation", en: "organization" } 
			, type: "text", pattern: "[ A-Za-zww+*.&#'~;?!@;,_\\-]{2,55}"
			}
			, { name: { en: "", code: "person" }, type: "person" }
			, { name: { de: "Zusatz (z. B. Postnr. oder Abteilung)"
				, en: "additional (i. e. post number or department)"
				, code: "additional" }, type: "text", maxlength: 60 }
			, { name: { de: "Straße (auch Packstation etc.)"
				, en: "street (also packing station etc.)", code: "street" }
			, type: "text", pattern: '[ 0-9A-Za-zww.\\-]{5,55}', required: 'required'
			}
			, { name: { de: "Ort", en: "location" }
			, type: "towns", required: 'required'
			}
		]
		, period: [
			{ name: { de: "von", en: "begin" }
			, type: "datedaysperiodpseudo", width: "60%", data: 'inter'
			}
			, { name: { de: "bis", en: "end" }
			, type: "enddate", data: "%id%", width: "40%"
			}
		]
		, timespan: [ 
			{ name: { de: "Anfangszeit", en: "starttime" }
				, type: "time", data: "inter", width: "35%"
			}
			, { name: { de: "Dauer", en: "duration" }
				, type: "timeperiodpseudo", data: "%id%", value: "00:30", CN: "noTask"
				, width: "30%;padding:0 1%"
			}
			, { name: { de: "Ende", en: "endtime" }
				, type: "endtime", data: "%id%", width: "35%"
			}
		]
		, appointment: [
			{ name: { de: "Betreff", en: "subject" }, type: "text" }
			, { name: { de: "Ort", en: "location" }, type: "text", width: "80%"
			}
			, { name: { de: "Raum", en: "room" }, type: "text", width: "20%"
			}
			, { name: { de: "Anfangsdatum", en: "begin" }
				, type: "datedaysperiodpseudo", width: "60%", data: 'inter'
			}
			, { name: { de: "Enddatum", en: "end" }
				, type: "enddate", data: "%id%", width: "40%"
			}
			, { name: { de: "Anfangszeit", en: "starttime" }
				, type: "time", width: "35%", data: "inter"
			}
			, { name: { de: "Dauer", en: "duration" }
				, type: "timeperiodpseudo", data: "%id%", value: "00:30", CN: "noTask"
					, width: "30%;padding:0 1%"	// width => style tag
			}
			, { name: { de: "Ende", en: "endtime" }
				, type: "endtime", data: "%id%", width: "35%"
			}
			, { name: { de: "Notiz", en: "notice" }
				, type: "textarea", rows: "3", maxlength: "2048"
			}
		]
		, bankaccount: [
			{ name: { de: "Inhaber(in)", en: "holder" }
				, type: "text", required: 'required'
			}
			, { name: { de: "IBAN", en: "IBAN" }
				, type: "iban", required: 'required'
			}
			, { name: { de: "BIC", en: "BIC" }, type: "text"
			, pattern: "^[0-9a-zA-Z]{4}[a-zA-Z]{2}[0-9a-zA-Z]{2}([0-9a-zA-Z]{3})?$"
			}
			, { name: { de: "Kreditinstitut", en: "institution" }
				, type: "text", disabled: "disabled"
			}
		]
	}
	, JSONexpands: {
	//@ this is to customize the behavior of some input types defined in htmlSupport
	//@ if they are transfered via JSON. It's possible to separate formated inputs
	//@ into different json key names. The following struct may be defined:
	//@ typeName: { names : [ separated json key names ] (key names may be repeated)
	//@ , reg: /(xx)yy(zz)/ (bracket values are assigned to keynames, nested
	//@ brackets are forbidden!)
		towns: { names: [ 'country', 'postcode', 'town' ]
			, reg: "([^;]+);([^;]+);([^;]+)", active: true
		}
		, title: { names:[ "salutation", "gender" ], reg: "([^#]+)#(.)", active: true
		}
		, person: { names: [ 'title', 'prename', 'name' ]
			, reg: "([^;]+);([^;]+);([^;]+)", active: true
		}
		, net: { names: [ 'amount', 'percent' ], reg: "([^_]+);([^_]+)", active: true
		}
		, vat: { names: [ 'amount', 'percent' ], reg: "([^_]+);([^_]+)", active: true
		}
	}
	, cssNS: {		// css-name space
	//@ this is to customize the used css classnames, that control the appearance of
	//@ useLib elements and elements treated by the useLib
		hasScript: 'uLhasScript'
		, noScript: 'uLnoScript'
		// design control namespace for sliding doors (plus defined ids in landmarks)
		, layout: "uLlayout"
		, landmark: "uLlandmark"
		, design: "uLdesign"
		, outer: "uLouter"
		, logo: "uLlogo"
		, active: "uLactive"
		, para: "uLpara"
		// to toggle navigation view
		, hiddenNavi: "uLhiddenNavi"
		, hamburger: "uLhamburger"
		// useLib controls
		, selector: "uLselector"
		, sizer: "uLsizer"
		, explainer: "uLexplainer"
		, designer: "uLdesigner"
		, sound: "uLsound"
		, secDragg: "uLsecDragg"
		, msghold: "uLmsghold"
		, help: "uLhelp"
		, seminar: "uLseminar"
		, security: "uLsecurity"
		, keyStroke: 'uLkeyStroke'
		, headLight: 'uLheadLight'
		, copyright: "uLcopyright"
		, minimizer: "uLminimizer"
		, jumper: "uLjumper"
		, helpTxt: "uLhelpTxt"
		, fastfeeder: "uLfastfeeder"
		, fastLabel: "uLfastLabel"
		, minimized: "uLminimized"
		, noUserSelect: "uLnoUserSelect"
		// explain level view control
		, explainNone: "uLexplainNone"
		, explainLess: "uLexplainLess"
		, explainAll: "uLexplainAll"
		, hasInform: "uLhasInform"
		, inform: "uLinform"
		, explain: "uLexplain"
		// wiki control
		, indent: "uLindent"
		, same: "uLsame"
		, left: "uLleft"
		, center: "uLcenter"
		, right: "uLright"
		, hidden: "uLhidden"
		, inline: "uLinline"
		, top: "uLtop"
		, middle: 'uLmiddle'
		, bottom: "uLbottom"
		, strike: 'uLstrike'
		, mark: 'uLmark'
		, tt: 'uLtt'
		, quote: 'uLquote'
		, example: "uLexample"
		, tip: "uLtip"
		, extern: 'uLextern'
		// header folding or accordeon
		, fold: "uLFold"
		, formfold: "uLformFold"
		, pinfold: "uLpinFold"
		, allpins: "uLallpins"
		, locker: "uLlocker"
		, locked: "uLlocked"
		// modal dialog elements
		, modal: "uLmodal"
		, scale: "uLscale"
		, dragger: "uLdragger"
		, sizer: "uLsizer"
		, closer: "uLcloser"
		, content: "uLcontent"
		, picture: "uLpicture"
		, icon: "uLicon"
		, image: "uLimage"
		// modal types
		, form: "uLform"
		, dialog: 'uLdialog'
		, console: 'uLconsole'
		, tabs: "uLtabs"
		, status: "uLstatus"
		, countdown: "uLcountdown"
		, prompt: "uLprompt"
		, confirm: "uLconfirm"
		, alert: "uLalert"
		// animations etc.
		, show: "uLshow"
		, open: "uLopen"
		, close: "uLclose"
		// form element status management
		// ...interaction
		, disabled: "uLdisabled"
		, focused: "uLfocused"
		, changed: "uLchanged"
		, technical: "uLtechnical"
		, keyError: "uLkeyError"
		, keyHint: "uLkeyHint"
		, keyCarry: "uLkeyCarry"
		, keySuccess: "uLsuccess"
		, errorMsg: 'uLerrorMsg'
		, error: "uLerror"
		, success: "uLsuccess"
		, hint: "uLhint"
		// ...task flow
		, required: "uLrequired"
		, task: "uLtask"
		, requested: "uLrequested"
		, estimated: "uLestimated"
		, adjusted: "uLadjusted"
		, verified: "uLverified"
		, cloud: "uLcloud"
		// process organisation status:
		, banned: "uLbanned"
		, restricted: "uLrestricted"
		, completed: "uLcompleted"
		, archived: "uLarchived"
		, onlyRequired: "uLonlyRequired"
		, someRequired: "uLsomeRequired"
		, manyRequired: "uLmanyRequired"
		, notDone: 'uLnotDone'
		, seacher: 'uLseacher'
		, completer: 'uLcompleter'
		// button types
		, sendButton: "uLsendButton"
		, controlButton: "uLcontrolButton"
		, stepperButton: "uLstepperButton"
		// marker for formated element types
		, hasCover: 'uLhasCover'
		, isCover: 'uLisCover'
		, covered: 'uLcovered'
		, sepa: 'uLseparator'
		, separator: 'uLseparator'
		, linebreak: 'uLlinebreak'
		, gap: 'uLgap'
		, invisible: 'uLinvisible'
		, hide: 'uLhide'
		, filter: 'uLfilter'
		, filterInvers: 'uLfilterInvers'
		, select: 'uLselect'
		, invert: "uLinvert"
		, showAll: 'uLshowAll'
		, sortdown: 'uLsortdown'
		, sortup: 'uLsortup'
		, add: 'uLadd'
		, up: 'uLup'
		, down: 'uLdown'
		, delete: 'uLdelete'
		, flow: 'uLflow'
		, noaction: 'uLnoaction'
		, uLnofilter: 'uLnofilter'
		, indicatorBar: "uLindicatorBar"
		, singleSelect: "uLsingleSelect"
		, multiSelect: "uLmultiSelect"
		, dynamicExtend: "uLsingleSelect"
		, dynamicSelect: "uLmultiSelect"
		, toggleSelect: "uLtoggleSelect"
		, invertIcon: "uLinvertIcon"
		, clear: "uLclear"
		, dynlist: "uLdynlist"
		, dynfilter: "uLdynfilter"
		, dynaction: "uLdynaction"
		, listSelect: "uLlistSelect"
		, showActive: "uLshowActive"
		, color: "uLcolor"
		, date: "uLdate"
		, datetime: "uLdatetime"
		, free: "uLfree"
		, file: "uLfile"
		, email: "uLemail"
		, fon: "uLfon"
		, iban: "uLiban"
		, salesident: "uLsalesident"
		, currency: "uLcurrency"
		, time: "uLtime"
		, number: "uLnumber"
		, float: "uLfloat"
		, unit: "uLunit"
		, isounits: "uLisounits"
		, pattern: "uLpattern"
		, towns: "uLtowns"
		, country: "uLcountry"
		, vat: "uLvat"
		, net: "uLnet"
		, list: "uLlist"
		, grouper: "uLgrouper"
		, selfgrouper: "uLgrouper"
		, informer: "uLinformer"
		, title: "uLtitle"
		, person: "uLperson"
		// calendar
		, calendar: 'uLcalendar'
		, legend: 'uLlegend'
		, today: 'uLtoday'
		, sunday: 'uLsunday'
		, holidays: 'uLholidays'
		, blocked: 'uLblocked'
		, selected: 'uLselected'
		, day: 'uLday'
		, week: 'uLweek'
		, month: 'uLmonth'
		, year: 'uLyear'
		, small: 'uLsmall'
		// icon buttons
		, password: 'uLpassword'
		, passwordrepeat: 'uLpassword'
		, world: 'uLworld'
		, days: 'uLdays'
		, fixpin: 'uLfixpin'
		, toggle: 'uLtoggle'
		, clock: 'uLclock'
		, elastic: 'uLelastic'
		, eye: 'uLeye'
		, elasticHead: 'uLelasticHead'
		, elasticTrack: 'uLelasticTrack'
		, elasticCenter: 'uLelasticCenter'
		, elasticLayer: 'uLelasticLayer'
		, plus: 'uLplus'
		, minus: 'uLminus'
		, clearer: 'uLclearer'
		, miniKeyboard: 'uLminiKeyboard'
		, miniCursor: 'uLminiCursor'
		// buttons
		, buttonRow: 'uLbuttonRow'
		, button: 'uLbutton'
		, doublebutton: 'uLdoublebutton'
	}
	, langConf: {
	//@ here may be customized different languages and the wording inside a language
	//@ all structures must have identical named elements
		en: {
			useLib: [ "useLib is a add on to optimize the usability of the interface"
				, "According to the EU Directive for user interfaces, those must be "
				+ "designed ergonomically on workstations. Here is improved the user "
				+ "interaction corresponding to user adjustments.\n\n%%" ]
			// message wording
			, loadMsg: [ 'Some data are loaded now (JSONCSS).\nThis may take a moment!' ]
			, encryptedMsg: [ 'Highly encrypted data are currently being exchanged. These '
				+ 'high security (JSONONEPAD) needs unfortunately a moment!' ]
			, serverMsg: [ 'Loading of %% unfortunately unsuccessful: try again immediately?'
				, 'It could be a network, e.g. WLAN or server problem. The access attempt '
				+ 'can be repeated immediately with click on //confirm//.\n\n'
				+ 'Otherwise it can also be tried later!' ]
			, fileMsg: [ 'Unfortunately the file: "%%" could not be loaded.'
				, 'An attempt was made to load this file, but this was not successful. '
				+ 'Because of the diversity of possible reasons please contact the operator.' ]
			, fileFormat: [ 'The file: "%%" was not loaded.'
				, 'The file type is not allowed in this field.' ]
			, fileImport: [ 'The file: "%%" was successful imported.' ]
			, designVersion: [ 'Design outdatet', 'There\'s a problem with the '
				+ 'selected design version. It is obsolete, the screen elements would '
				+ 'be faulty or is displayed shifted. Therefore, the standard version '
				+ 'is set.' ]
			, designAuth: [ 'Design conflict', 'The selected design is unfortunately '
				+ 'not authorized, i.e. using it causes a copyright violation or '
				+ 'its manipulated for other reasons!' ]
			, sendMsg: [ 'Edited data will be transferred now.\nThis may take a moment!' ]
			, noDataMsg: [ 'No data has been entered yet.\nPlease fill in first the form!' ]
			, confirmMsg: [ 'Attention, a delete cannot be undone'
				, '<<You can export the data before and restore it if necessary.'
				+ 'Therefore, please confirm your intention.>>','<<<<++Export:++ '
				+ 'Action selection (below): //invert//, then //export// and save '
				+ 'to a json file (depending on browser settings).>>>>' ]
			, successMsg: [ 'The data have been transferred successfully!' ]
			, hintMsg: [ 'Please note the following hints!'
				+ 'Please check the fields if necessary. \n\n'
				+ 'With ++click++ on the message the form element is focused.' ]
			, errorMsg: [ 'There are unfortunately discrepancies in the form inputs. '
				+ 'Please check the fields specified.\n'
				+ 'With ++click++ on the message the form element is focused.' ]
			// %% is a variable placeholder
			, requiredError: [ 'In %% it is required to fill in the hole input.'
				, 'It\'s a mandatory field (see legend on top of form).' ]
			, choiseError: [ 'The elements related to %% must be decided.'
				, 'They are mandatory field (question marks on elements).' ]
			, qualityError: [ 'The security of your password isn\'t high enough.'
				, 'From Differences between the characters in your input (large/small, '
				+ 'special characters, frequency, etc.) the security is appreciated.' ]
			, repeatError: [ 'The two password entries are not the same.'
				,'Please repeat the input to avoid typing errors in the password.' ]
			, formatError: [ '%% is not filled in fully or like specified.'
				, 'There is a predefined formatting that does not fit to the input. '
				+ 'Mostly characters are missing or it\'s just a sign muddling.\n\n'
				+ 'Clearing belonging inputs avoid this message also.  [^del]' ]
			, rangeError: [ 'The input of %% is not in the required range between ##min '
				+ 'and ##max.'
			, 'The value range is displayed at the input.' ]
			, dayError: [ "The day of date in %% has been adjusted, please check!" ]
			, yearError: [ "The year of date in %% has been adjusted, please check!" ]
			, checkError: [ '%% has been externally checked. Please check here for '
				+ 'correctness.'
				, 'Problem details can be found directly at the input.' ]
			, noneLeftError: [ 'One line must remain at least!\n\n(uncheck one)']
			, dataError: [ 'Unfortunately, the data just transferred could not be '
				+ 'taken  because they do not fit to the active file!' ]
			, accessError: "Transmission error\n\nUnfortunately, the requested "
				+ "data could not be accessed."
			, supportError: 'Browser was not tested!\nA more modern Bowser (Firefox, '
				+ 'Chrome, Chromium, Safari...) should be used. It may not work.'
			, technicalHint: [ '%% has been adapted by automatic checks in the'
				+ 'background.']
			, taskHint: [ 'The status of %% has been changed in another step.']
			, taskCheck: [ 'There are still input fields marked with open '
				+ 'processing status. Should the data be transferred anyway?' ]
			// useLib control wording
			, help: [ 'Introduction to use the corner helpers', 'There are '
				+ 'additional functionalities for experienced users. This text can '
				+ 'be retrieved by clicking //top right// on the ? sign in the '
				+ 'view options.\n\nThere is also a 2-page [[extern '
				+ 'http://www.use-optimierung.de/useLib/useLibHelperExplaination.'
				+ 'en.pdf short manual]] available.' ]
			, interaction: [ 'fold Inputs to useLib fields'
				, 'Advanced to standard the ++arrow keys++ and any defined short '
				+ 'cut can be used to change between the input fields. Particularly '
				+ 'supported are by marked the //red copy sign// in the lower left '
				+ 'corner.\n\n'
				+ '* using arrow keys: ++right/left++ can move back and forth '
				+ 'between fields\n\n'
				+ '* you can also use the arrow keys: ++up/down++ to scroll all '
				+ 'possible values, on numeric inputs ++carries++ are also taken '
				+ 'into account.\n\n'
				+ '* especially for touch surfaces is a //arrow key// ++miniature '
				+ 'keyboard++ faded in which the finger is pulled out of the field'
				+ 'and this is lifted (this takes little practice).\n\n'
				+ '* If the finger is left on the surface while dragging, a'
				+ '""rubber band"" input appears. The possible values can be controlled by '
				+ 'dragging quickly through; becoming smaller to the left and becoming '
				+ 'larger to the right.' ]
			, options: [ 'view options', 'Here you can ++adjust++ the view of the '
				+ 'interface to your ++instantaneous demand++.' ]
			, localStorage: [ "++CAUTION:++ no saving of %%!"
				, "Your browser unfortunately not supports this or the allowed "
				+ "data amount is exceeded. Settings will only stay "
				+ "as long as the window or its tab is not closed." ]
			, sizer: [ 'Size ', 'Change font size and breaks.'
				, 'Click to change the font size according to needs and task.' ]
			, msghold: [ 'fade messages out' ]
			, sound: [ 'input sounds on/off' ]
			, seminar: [ 'seminar mode on/off' ]
			, security: [ 'Drag&drop login', 'The login data can be easily take over'
				+ 'from one useLib application to another. To do this, just dragg '
				+ 'button: //Drag&Drop login// into the view options of the target '
				+ 'window pulled. The login is successful if e-mail and '
				+ 'password are vaild.' ]
			, explainer: [ 'manual', 'The integrated handbook allows different '
				+ 'detailings of information.'
				, 'The ++self-description++ of the surface can be adjusted to '
				+ 'the momentary needs. Additional information is shown or hidden.' ]
			, explainNone: [ "none" ]
			, explainLess: [ "less" ]
			, explainAll: [ "full" ]
			, designer: [ 'design', 'Change to suitable designs.'
				, 'The ++design++ that ++fits the situation++ can be selected. '
				+ 'At any time, all data is retained.' ]
			, minimizer: [ 'Toggle %%', 'Click on the minimizer '
				+ 'icon reduces or enlarged the associated helper display.' ]
			, hamburger: [ 'Toggle navigation bar' ]
			, jumper: [ 'Jump to top', 'On click to the jump sign the '
				+ 'window content ++automatically scrolls up++, simplifying '
				+ 'working with longer pages.' ]
			, fastfeeder: [ 'fastfeeder'
				, '* Write values: shortcut + possibly values + §§enter§§.\n\n'
				+ '* Display values: shortcut + §§esc§§.\n\n'
				+ 'Values for selection lists = number(s) of the element(s) (also negative'
				+ 'and ranges)\n\n'
				+ 'Instead of shortcut also §§>§§, §§<§§, §§+§§ if necessary with number or '
				+ '§§tab§§, possibly multiple.'
				, 'shortcut, values are separated by blanks. Element numbers with a '
				+ 'negative sign are deselected.\n\n'
				+ 'A range of item numbers will appear at a §§>§§ sign in between.\n\n'
				+ 'Should the given data be incorrect, the fastfeeder will shake'
				+ 'short and indicate an error. Correctly processed values are removed from '
				+ 'the fastfeeder.\n\n'
				+ 'Transfers by cut & paste or drag & drop will be processed also.\n\n'
			]
			, fastLabel: [ 'show with label'
				, 'here on arrow key navigation: active element' ]
			, popUp: [ "++ ATTENTION:++ Another window will open!"
				, "This window remains, but it may be hidden.\n\n"
				+ "Your browser settings may prevent this. Please enable opening at the "
				+ "top of the header." ]
			// special select element wording
			, requiredSelect: [ 'Please choose! *' ]
			, fileSelect: [ 'opens fileselector' ]
			, calendarSelect: [ 'opens datepicker' ]
			, timeSelect: [ 'opens timepicker' ]
			, dialogSelect: [ 'opens selection dialog' ]
			, SingleSelect: [ 'opens single select', 'Click on the value vou need, please!' ]
			, multiSelect: [ 'opens multi select', 'Select all boxes you need, please!\n\n'
			+ 'Click on a header will invert the hooks.;;'
			+ '\u202F\u202F\u202FDoubleclick will clear that section.;;'
			, 'Arrow key navigation is supported, §§space§§ key selects,'
			+ 'changes are marked as support.' ]
			, daysSelect: [ 'Duration: selection fixes end date as calculation base' ]
			, ownSelect: [ 'Fill in your choice for ##:' ]
			, invertSelect: [ 'invert hooks' ]
			, clearSelect: [ 'clear hooks' ]
			, allpins: [ 'set all pins opened or closed' ]
			, up: [ 'shift element up ones' ]
			, add: [ 'double this element' ]
			, delete: [ 'delete this element' ]
			, show: [ 'display separated' ]
			, filterButtons: [ "element choice", "filter input", "filter", "invert filter", "show all", "sort up", "sort down", "select", "invert select" ]
			// button wording
			, submit: [ 'submit' ]
			, store: [ 'resume' ]
			, storeself: [ 'save local' ]
			, reset: [ 'reset' ]
			, request: [ 'request' ]
			, cancel: [ 'cancel' ]
			, clear: [ 'clear' ]
			, refuse: [ 'refuse' ]
			, confirm: [ 'confirm' ]
			, miniKeyBoard: 'left,right,up,down,backspace,insert'.split( ',' )
			, testSubmitOnly: [ 'The following values would be send to the server now:' ]
			// currency wording
			, noRateCalc: [ 'rate mechanism not useful, because same exchange rate'
				, 'Exchange rate:' ]
			, noRateGiven: [ 'unfortunately no daily exchange rates of European Central Bank'
				, 'no rate' ]
			, noUnitCalc: [ 'Calculation with the same unit is not useful!' ]
			, noCountrydata: [ 'No detailed data available for this country' ]
			, noPuzzlePassword: [ 'Sorry, no puzzle password support on this Browser!' ]
			, viewPassword: [ 'Password in plain text', 'Changes made here, will be taken '
				+ 'over to the password.', 'Please insure to your own safety, that no '
				+ 'unauthorized third parties can spy out the password.' ]
			, ibanCheck: [ 'Unfortunately the IBAN check of %% shows an error!',
				'The IBAN is formalized using the 2-digit check digit (cc) checked for '
				+ 'correctness.\n\nIt is not checked, if the account realy exists.' ]
			, vatError: [ 'Please give a percentage like the given ending with §§%§§ for %%.' ]
			, dateDisabled: [ 'At least one day of selection in %% is in a disabled '
				+ 'calendar area!'
				, "The date may be not changeable. Details can be viewed in the calendar. "
				+ "Free periods are displayed."
				, "Calendar is opened via a click on the icon."
			]
			, dateBlocked: [ 'At least one day of selection in %% is in a blocked '
				+ 'calendar area!'
				, "It has to be checked, if the term shall be setted anyhow. "
				+ "Details may be seen in the calendar."
				, "Calendar is opened via a click on the icon."
			]
			, daysPeriod: [ "Period of %% wouldn't fit into the given limits."
				, 'Details may be seen in the calendar.'
			]
			, doubleName: [ 'Name already assigned, please select a different one.'
				+ 'To ensure that assignments are unique, no identical names may be used.'
			]
			, ownInput: [ 'manual input...', "Please type your own choise:" ]
			// calendar wording
			, datepicker: [ 'datepicker', 'All dates can be selected, but the selection of '
				+ 'blocked or restricted days may cause additional clarifications.' ]
			, calTable: [ 'this table is a calendar' ]
			, legend: [ 'legend' ]
			, today: [ 'today' ]
			, holidays: [ 'Holiday(s)' ]
			, disableddays: [ 'restricted' ]
			, blockeddays: [ 'blocked' ]
			, selecteddays: [ 'selected' ]
			, jumptoday: [ 'to today' ]
			, jumptoselect: [ 'to selection' ]
			, display: [ 'display' ]
			, year: [ 'year' ]
			, month: [ 'month' ]
			, monthup: [ 'one month earlier' ]
			, monthdown: [ 'one month later' ]
			, isWeek: [ 'ww' ]        // first of days
			, isday: [ ', the ' ]
			, isSunday: [ 'su' ]      // last of dayNames
			, selfgrouper: [ 'section action', 'groups', 'individuals'
				, 'remove entry', 'separate entry', 'remove duplicates'
				, 'remove all' ]
			, uLlang: 'en'
			, timestamp: 'YYYY-MM-DD HH:mm'
			, timepicker: "hours,night,morning,afternoon,evening,minutes"	// time picker wording
			, wikiClasses: 'left,center,right,inline,ignore,show,active,'
				+ 'kbd,tt,quote,' + 'fold,foldin,pinfold,tabs,'
				+ 'extern,' + 'noaction,uLnofilter,flow,delete,'
				+ 'pre,code,iframe,example,tip,dialog,form,console,logo,'
				+ 'group,same,invisible,' + 'linebreak,gap,separator,' + 'copyright'
			, wikiTypes: 'hidden,search,checkbox,' + 'icon,picture,'
				+ 'password,passwordrepeat,email,towns,title,person,country,address,'
				+ 'shipping,fon,iban,bankaccount,salesident,' + 'file,color,url,'
				+ 'number,float,range,unit,isounits,currency,vat,net,'
				+ 'clock,text,textarea,list,button,grouper,selfgrouper,'
				+ 'appointment,date,enddate,smalldate,endsmalldate,week,'
				+ 'timestamp,time,endtime,smalltime,endsmalltime,datetime,'
				+ 'smalldatetime,enddatetime,'
				+ 'endsmalldatetime,period,daysperiod,datedaysperiod,'
				+ 'timespan,timeperiod,smalltimeperiod,timedaysperiod,'
				+ 'timeperiodpseudo,datedaysperiodpseudo,weekpseudo'
			, dynNameChoise: [ "Please enter the name or identifier of the new row."
				, "This information is used to administrate the data in the program sequence, "
				+ "i.e. create and find them again", "Therefore the selected name must be "
				+ "unique, it can not be chosen more than once."
			]
			, dynActions: [ 'for selected:,hide,export,delete,invert'
				, 'do with all:,clear,show,fold in,fold out' ]
			, dynFilterInfo: [ 'Selection search', 'Lines with text contents are selected. '
				+ 'For sorting: label specification or by 1st value. (For experts: '
				+ 'enclosed in §§"§§ exactly value; also >, <, >= oder <= as prefix; '
				+ 'RegExp search is possible'
			]
			, dynActionInfo: [ 'Action selection', 'Please select the desired action. '
				+ 'If it is not executed immediately with the selected lines, you may '
				+ 'trigger it by clicking on //go//.<br>'
			]
			, dynMinSelect: [ 'Please select a list element', 'At least you need one '
				+ 'element or more to act with them. Just select element via the '
				+ 'leading checkboxes (top left).' ]
			, ownInputExtend: {  'vat': '19%|7%|0%|ownInput'	// these two must be defined
				, '.title': { f: 'female', m: 'male', n: 'neutral', b: 'non-binary' }
			}
			, allowedChars: {
			// short for allowed special char in input fields
				'a-z': 'all letters \u00B7 '
				, '-0-9': 'numbers incl. leading minus \u00B7 '
				, '0-9': 'numbers \u00B7 '
				, 'gg' : "german chars \u00B7 "	// german: ÄÖÜäöüß
				, 'ff' : "french chars \u00B7 "	// french: éàçèûîô
				, 'ss' : "spanish chars \u00B7 "	// spanish: çñáéíúü¿¡
				, 'pp' : "portugese chars \u00B7 "	// portuguese: çºªãõáéúóíàò
				, 'ww' : "selection of european chars \u00B7 "	// -g + -f + -s + -p
				, 'ee' : "european chars \u00B7 "	// euChars
				, 'uu' : "all letters, number or +._!~:?#@$&'*,;=/)(   "
				, 'mm' : "all letters, Numbers or ._- and divers rar used chars   "
				, 'xx' : "any signs   "
				, ' ' : "space "
			}
			, inputName: {
				DAY: 'day'
				, MONTH: 'month'
				, YEAR: 'year'
				, HOUR: 'hours'
				, MINUTE: 'minutes'
				, SECOND: 'seconds'
				, HOURS: 'hour term'
				, MINUTES: 'minute term'
				, SECONDS: 'second term'
				, DAYS: 'day term'
				, PHOURS: 'hour term'
				, PMINUTES: 'minute term'
				, PSECONDS: 'second term'
				, PDAYS: 'day term'
			}
			, colors: ('black gray maroon red green lime olive yellow navy blue '
			+ 'purple fuchsia teal aqua silver white' ).split( ' ' )
			, months: ('January February March April May June July'
			+ 'August September October November December').split(' ')
			, dayNames: {
				ww: "It is week ## of the year "
				, mo: "monday"
				, tu: "tuesday"
				, we: "wendsday"
				, th: "thursday"
				, fr: "friday"
				, sa: "saturday"
				, su: "sunday"
			}
			, christHoliday: {
				goodFriday: "Good Friday"
				, easterSunday: "Easter Sunday"
				, easterMonday: "Easter Monday"
				, theAscension: "The Ascension"
				, whitSunday: "Whit Sunday"
				, whitMonday: "Whit Monday"
				, corpusChristi: "Corpus Christi"
				, ashWednesday: "Ash Wednesday"
				, fixHoliday: {
					'01-01': "New Year's Day",
					'01-06': "Epiphany",
					'05-01': "May Day",
					'08-15': "The Assumption",
					'10-31': "Reformations Day",
					'11-01': "All Saints' Day",
					'12-25': "Christmas Day",
					'12-26': "Boxing Day"
				}
			}
			, localTypes: {
			}
		}
		, de: {
			useLib: [ "useLib ist ein Programmzusatz zur Optimierung der Gebrauchstauglichkeit"
				, "Gemäß der deutschen Arbeitsstättenverordnung müssen Benutzungsoberflächen "
				+ "an Bildschirmarbeitsplätzen software-ergonomisch gestaltet werden "
				+ "(Usability). Hier wird die Interaktion mit entsprechenden "
				+ "Einstellmöglichkeiten durch Benutzer verbessert.\n\n%%" ]
			// message wording
			, loadMsg: [ 'Es werden gerade Daten geladen (JSONCSS). Das kann einen '
				+ 'Augenblick dauern!' ]
			, encryptedMsg: [ 'Gerade werden hochverschlüsselte Daten ausgetauscht. '
				+ 'Diese hohe Sicherheit (JSONONEPAD) benötigt einen Augenblick!' ]
			, serverMsg: [ 'Laden von %% leider erfolglos: sofort noch mal versuchen?'
				, 'Es könnte eine Netzwerk, z. B. WLAN oder auch Server-Störung vorliegen. '
				+ 'Der Zugriffsversuch kann mit Klick: //bestätigen// sofort wiederholt '
				+ 'werden.\n\nAnsonsten kann es auch später noch mal versucht werden!' ]
			, fileMsg: [ 'Die Datei: "%%" konnte leider nicht geladen werden.'
				, 'Es wurde versucht diese Datei zu laden, aber dies war nicht '
				+ 'erfolgreich. Aufgrund der Vielfältigkeit der möglichen Gründe '
				+ 'kontaktieren Sie bitte den zuständigen Ansprechpartner.' ]
			, fileFormat: [ 'Die Datei: "%%" wurde nicht geladen werden.'
				, 'Der Datei-Typ ist in diesem Feld nicht zugelassen.' ]
			, fileImport: [ 'Die Datei: "%%" wurde erfolgreich importiert.' ]
			, designVersion: [ 'Design veraltet', 'Es gibt leider ein Problem mit der '
				+ 'gewählten Version. Sie ist veraltet, die Bildschirmelemente würden '
				+ 'fehlerhaft bzw. verschoben dargestellt. Daher wird die Standard-'
				+ 'Version eingestellt.' ]
			, designAuth: [ 'Designkonflikt', 'Das gewählte Design ist nicht '
				+ 'zulässig, d. h. der Einsatz erfolgt unter Verletzung des '
				+ 'Copyrights oder es wurde anderweitig manipuliert!' ]
			, sendMsg: [ 'Die eingegebenen Daten werden jetzt übertragen. '
				+ 'Das kann einen Augenblick dauern!' ]
			, noDataMsg: [ 'Es sind noch keine Daten eingegeben.\nBitte füllen Sie '
				+ 'zunächst das Formular aus!' ]
			, successMsg: [ 'Die Daten wurden erfolgreich übertragen!' ]
			, confirmMsg: [ 'Achtung, das Löschen kann nicht rückgängig gemacht '
				+ 'werden.','<<Sie können die Daten zuvor exportieren und bei '
				+ 'Bedarf wieder herstellen. Bitte bestätigen Sie daher Ihre '
				+ 'Absicht.>>', '<<<<++Exportieren:++ Aktionsauswahl (unten): '
				+ '//invertieren//, dann //exportieren// und in Datei.json '
				+ ' speichern (letzteres abhängig von Browser-Einstellungen).>>>>' ]
			, hintMsg: [ 'Bitte beachten Sie die folgenden Hinweise'
				+ 'Bitte überprüfen Sie gegebenenfalls die angegebenen Felder.\n\n'
				+ 'Mit ++Klick++ auf die Meldung wird das Formularelement angesprungen.' ]
			, errorMsg: [ 'Es gibt leider Unstimmigkeiten bei den Formulareingaben.'
				, 'Bitte überprüfen Sie die angegebenen Felder.\nMit ++Klick++ '
				+ 'auf die Meldung wird das Formularelement angesprungen.' ]
			// %% is variable placeholder
			, requiredError: [ '%% muss vollständig eingetragen werden.'
				, 'Es handelt sich um ein Pflichtfeld (siehe Formularlegende oben).' ]
			, choiseError: [ 'Die mit %% zusammenhängenden Elemente müssen entschieden werden.'
				, 'Es handelt sich um Pflichtentscheidungen (Fragezeichen über Elementen).' ]
			, qualityError: [ 'Die Sicherheit Ihres Passworts reicht noch nicht aus.'
				, 'Aus Ihrer Eingabe wurde aus der Unterschiedlichkeit der Zeichen '
				+ '(groß/klein, Sonderzeichen, Häufigkeit etc.) die Sicherheit geschätzt.' ]
			, repeatError: [ 'Die beiden Passwörter sind nicht gleich.'
				,'Wiederholen Sie die Eingabe bitte, um Tippfehler im Passwort '
				+ 'auszuschließen.' ]
			, formatError: [ '%% ist nicht vollständig bzw. korrekt ausgefüllt.'
				, 'Es gibt eine vorgegebene Formatierung, der die Eingabe nicht '
				+ 'entspricht. Meist fehlen Zeichen oder es ist einfach nur ein Dreher.\n\n'
				+ 'Auch das Leeren aller Teilfelder beseitigt diese Meldung. [^Entf.]' ]
			, rangeError: [ 'Die Eingabe von %% liegt nicht im erwarteten Bereich zwischen '
				+ '##min und ##max.'
				, 'Der Wertebereich wird an der Eingabe eingeblendet.' ]
			, dayError: [ "Das Tagesdatum von %% wurde angepasst, bitte überpüfen!" ]
			, yearError: [ "Das Kalenderjahr von %% wurde angepasst, bitte überpüfen!" ]
			, checkError: [ '%% wurde extern überprüft. Bitte prüfen Sie hier die Richtigkeit.'
				, 'Problemdetails finden Sie ggf. direkt an der Eingabe.' ]
			, noneLeftError: [ 'Eine Zeile muss mindestens übrig bleiben!\n\n(1 abwählen)']
			, dataError: [ 'Die gerade übergebenen Daten konnten leider nicht übernommen '
				+ 'werden, da diese nicht zur aktiven Datei passen!' ]
			, accessError: "= Übertragungsfehler\n\nLeider konnten die "
			+ "angeforderten Daten nicht ermittelt werden."
			, supportError: "Browser wurde nicht getestet!\nEs sollte ein modernerer "
				+ "Bowser (Firefox, Chrome, Chromium, Safari...) genutzt werden. "
				+ "Möglicherweise funktioniert dieser nicht."
			, technicalHint: [ '%% wurde aufgrund von automatischen Überprüfungen '
				+ 'im Hintergrund angepasst.']
			, taskHint: [ 'Der Status vom %% wurde in einem anderen Arbeitsschritt verändert.']
			, taskCheck: [ 'Es sind noch Eingabefelder mit offenen '
				+ 'Bearbeitungsstand gekennzeichnet. Soll die Daten dennoch '
				+ 'übernommen werden?' ]
			// useLib control wording
			, help: [ 'Erläuterung der kleinen Helferlein', 'Es gibt '
				+ '++Zusatzfunktionalitäten++ für geübte Benutzer. Diesen Text '
				+ 'erhält man wieder durch Klick //oben rechts// auf das ?-Zeichen '
				+ 'bei den Ansichtsoptionen.\n\nEs liegt auch eine 2-seitige [[extern '
				+ 'http://www.use-optimierung.de/useLib/'
				+ 'useLibHelperExplaination.de.pdf Kurzanleitung]] bereit.' ]
			, interaction: [ 'Die Eingaben in den useLib-Feldern'
				, 'Erweitert zum Standard kann mit den ++Pfeiltasten++ sowie ggf. '
				+ 'definierten Kürzeln zwischen den Eingabefeldern gewechselt '
				+ 'werden. Besonders unterstützte sind durch den //roten Punkt// in '
				+ 'der linken unteren Ecke gekennzeichnet.\n\n'
				+ '* mittels Pfeiltasten: ++rechts/links++ kann zwischen den Feldern '
				+ 'hin und her gesprungen werden\n\n'
				+ '* ebenfalls können mittels der Peiltasten: ++rauf/runter++ die '
				+ 'möglichen Werte gescrollt werden, wobei bei zahlenorientierten '
				+ 'Eingaben auch ++Überträge++ berücksichtigt werden.\n\n'
				+ '* insbesondere für ++Touch-Oberflächen++ kann dazu eine '
				+ 'Pfeiltasten-++Minitastatur++ eingeblendet werden, in dem der '
				+ 'Finger aus dem Feld gezogen und dieser abgehoben wird (ein wenig '
				+ 'Übung nötig).\n\n'
				+ '* Wird der Finger beim Ziehen auf der Fläche belassen, erscheint '
				+ 'die ++""Gummiband-Eingabe""++. Durch Ziehen laufen die möglichen '
				+ 'Werte steuerbar schnell durch; nach links kleiner, nach rechts '
				+ 'größer werdend.' ]
			, options: [ 'Ansichtsoptionen', 'Hier kann die Ansicht der Oberfläche '
				+ 'an den augenblicklichen Bedarf angepasst werden.' ]
			, localStorage: [ "++ACHTUNG:++ kein Sichern von %%!"
				, "Ihr Internet-Programm (Browser) unterstützt dies leider nicht "
				+ "oder die zulässige Datenmenge wurde überschritten."
				+ "Die Einstellungen bleiben nur solange erhalten, wie das Fenster "
				+ "bzw. der Reiter nicht geschlossen wird." ]
			, sizer: [ 'Größe', 'Ändern von ++Schriftgröße++ und ++Umbruch++.'
				,'Durch Klick die Schriftgröße je nach Bedarf und Aufgabe wechseln.' ]
			, msghold: [ 'Nachrichten automatisch schließen' ]
			, sound: [ 'Eingabetöne ein/aus' ]
			, seminar: [ 'Seminarmodus ein/aus' ]
			, security: [ 'Drag&Drop-Login', 'Die Login-Daten lassen sich einfach '
				+ 'von einer useLib-Anwendung auf eine andere schieben. Hierzu wird '
				+ 'einfach der Knopf: Drag&Drop-Login auf die Ansichtsoptionen des '
				+ 'Zielfensters gezogen. Die Anmeldung erfolgt, wenn E-Mail und '
				+ 'Passwort für diesen Zugang die gleichen sind.' ]
			, explainer: [ 'Handbuch', 'Das integrierte Handbuch ermöglicht unterschiedliche Detaillierungen von Informationen.'
				, 'Die Selbstbeschreibung der Oberfläche lässt sich an '
				+ 'den momentanen Bedarf anpassen. Zusatzinformationen werden '
				+ 'entsprechend ein- bzw. ausgeblendet.' ]
			, explainNone: [ "keins" ]
			, explainLess: [ "grob" ]
			, explainAll: [ "gesamt" ]
			, designer: [ 'Design', 'Wechsel zum passenden Desgins.'
				, 'Das zur Situation passende Design kann ausgewählt werden.'
				+ 'Jederzeit, alle Daten bleiben erhalten.' ]
			, minimizer: [ 'ein- und ausblenden', 'Mit Klick auf '
				+ 'das Minimierer-Zeichen wird die zugehörige Anzeige verkleinert '
				+ 'bzw. vergrößert.' ]
			, hamburger: [ 'Navigation ein- und ausblenden' ]
			, jumper: [ 'Sprung nach oben', 'Mit Klick auf das Sprung-'
				+ 'Ikon scrollt der Fensterinhalt ++automatisch nach oben++ und '
				+ 'vereinfacht so das Arbeiten mit längeren Seiten.' ]
			, fastfeeder: [ 'Blitzeingabe'
				, '* Werte schreiben: Kürzel + ggf. Werte + §§enter§§.\n\n'
				+ '* Werte anzeigen: Kürzel + §§esc§§.\n\n'
				+ 'Werte für Auswahllisten = Nummer(n) des(r) Elements(e) (auch negativ '
				+ 'und Bereiche)\n\n'
				+ 'Satt Kürzeln auch §§>§§, §§<§§, §§+§§ ggf. mit Zahl oder §§tab§§, '
				+ 'ggf. mehrfach.'
				, 'Kürzel, Werte und ggf. Werte untereinander werden durch Leerzeichen '
				+ 'getrennt. Elementnummern mit negativem Vorzeichen werden abgewählt.'
				+ 'Bereiche von Elementnummern werden durch §§>§§ Zeichen getrennt.\n\n'
				+ 'Sollten die übergebenen Daten fehlerhaft sein, schüttelt sich '
				+ 'das Blitzeingabefeld kurz und zeigt so einen Fehler an. Korrekt '
				+ 'verarbeitete Werte werden dabei aus der Blitzeingabe entfernt.\n\n'
				+ 'Es können auch Übergaben durch Kopieren oder Rüberziehen (Cut&Paste bzw. '
				+ 'Drag&Drop) vorgenommen werden.\n\n'
			]
			, fastLabel: [ 'mit Wertanzeige'
				, 'hier bei Pfeiltastennavigation: aktives Feld' ]
			, popUp: [ "++ACHTUNG:++ Ein weiteres Fenster wird geöffnet!"
				, "Dieses Fenster bleibt erhalten, es ist aber möglicherweise verdeckt.\n\n"
				+ "Möglicherweise verhindern dies Ihre Browser-Einstellungen. Bitte geben "
				+ "Sie dann das Öffnen oben in der Kopfzeile frei." ]
			// special select element wording
			, requiredSelect: [ 'Bitte wählen! *' ]
			, fileSelect: [ 'öffnet Dateiauswahl' ]
			, calendarSelect: [ 'öffnet Datumsauswahl' ]
			, timeSelect: [ 'öffnet Stundenauswahl' ]
			, dialogSelect: [ 'öffnet Auswahldialog' ]
			, singleSelect: [ 'öffnet Auswahldialog', 'Bitte wählen Sie einen Wert aus!' ]
			, multiSelect: [ 'öffnet Mehrfachauswahl'
				, 'Bitte wählen Sie gegebenenfalls auch mehrere Werte!;;\n'
				+ 'Ein Klick auf Überschrift bzw. Abschnitt kehrt zugehörige Häkchen '
				+ 'um.;;\nDoppelklick entfernt diese Häkchen.'
				, 'Pfeiltastennavigation ist unterstützt, §§Leer-Taste§§ wählt aus,'
				+ 'Veränderungen werden zur Unterstützung markiert.' ]
			, daysSelect: [ 'Dauer: Anwahl fixiert Enddatum für Berechnung' ]
			, ownSelect: [ 'Tragen Sie bitte Ihre Wahl für ## ein:' ]
			, invertSelect: [ 'Häkchen umkehren / invertieren' ]
			, clearSelect: [ 'Alle Häkchen entfernen' ]
			, allpins: [ 'alle Pins geöffnet bzw. geschlossen setzen' ]
			, up: [ 'Element eine Position hoch schieben' ]
			, add: [ 'Element duplizieren' ]
			, delete: [ 'Element löschen' ]
			, show: [ 'vereinzelt anzeigen' ]
			// 'Suchfeld(er)'
			, filterButtons: [ "Elementauswahl", "Filtereingabe", "filtern"
				, "invertiert filtern", "alle einblenden", "aufsteigend sortieren"
				, "absteigend sortieren", "anwählen", "invertiert anwählen" ]
			// button wording
			, submit: [ 'Senden' ]
			, store: [ 'Übernehmen' ]
			, storeself: [ 'Sichern (lokal)' ]
			, reset: [ 'Rücksetzen' ]
			, request: [ 'Abfragen' ]
			, cancel: [ 'Abbrechen' ]
			, clear: [ 'Leeren' ]
			, refuse: [ 'Ablehnen' ]
			, confirm: [ 'Bestätigen' ]
			, miniKeyBoard: 'links,rechts,hoch,runter,löschen,einfügen'.split( ',' )
			, testSubmitOnly: [ 'Folgende Werte würden jetzt zum Server-Rechner gesendet:' ]
			// currency wording
			, noRateCalc: [ 'Wechselkursmechanismus ohne Umrechnung nicht sinnvoll!'
				, 'Wechselkurs:' ]
			, noRateGiven: [ 'Leider stell die Europäische Central Bank für '
				+ 'dieses Land keinen Wechselkurs zur Verfügung.', 'kein Kurs' ]
			, noUnitCalc: [ 'Umrechnung bei gleicher Einheit nicht sinnvoll!' ]
			, noCountrydata: [ 'Keine Detaildaten hinterlegt für dieses Land!' ]
			, viewPassword: [ 'Passwort in Klarschrift', 'Hier vorgenommene '
				+ 'Änderungen können in das Passwort übernommen werden.', 'Achten Sie '
				+ 'zu ihrer eigenen Sicherheit bitte darauf, dass keine unbefugten '
				+ 'Dritten das Passwort ausspähen können.' ]
			, noPuzzlePassword: [ 'Das Puzzle-Passwort ist mit Ihrem Internet-'
				+ 'Programm leider nicht möglich!' ]
			, ibanCheck: [ 'Der IBAN-Check in %% zeigt leider einen Fehler an!',
				'Die IBAN wird mittels der 2-stelligen Prüfziffer (cc) formal '
				+ 'auf Richtigkeit geprüft.\n\nOb das Konto tatsächlich existiert'
				+ 'wird nicht geprüft.'	]
			, vatError: [ 'Bitte geben Sie einen Prozentwert wie die vorgegebenen mit '
				+ '§§%§§ am Ende für %% ein.' ]
			, dateDisabled: [ 'Mindestens ein Tag der Auswahl in %% liegt in einem '
				+ 'gesperrten Kalenderbereich!'
				, "Gegebenenfalls kann das Datum nicht verändert werden. Details "
				+ "können im Kalender eingesehen werden. Dort werden freigegebene "
				+ "Perioden angezeigt."
				, "Den Kalender öffnet man mittels Klick auf das Kalendersymbol."
			]
			, dateBlocked: [ 'Mindestens ein Tag der Auswahl in %% liegt in einem '
				+ 'geblockten Kalenderbereich!'
				, "Es muss geprüft werden, ob der Termin trotzdem gesetzt werden kann. "
				+ "Details dazu können auch im Kalender eingesehen werden."
				, "Den Kalender öffnet man mittels Klick auf das Kalendersymbol."
			]
			, daysPeriod: [ 'Die Dauer in %% läge nicht innerhalb der vorgegebenen '
				+ 'Grenzen.', 'Details dazu können im Kalender eingesehen werden.'
			]
			, doubleName: [ 'Diese Benennung ist bereits vergeben, bitte wählen Sie '
				+ 'eine andere.', 'Damit die Zuordnung eindeutig ist, dürfen keine '
				+ 'gleichen Benennungen vergeben werden.'
			]
			, ownInput: [ 'eigene Eingabe...', "Tragen Sie bitte Ihre Angabe ein:" ]
			// calendar wording
			, datepicker: [ 'Kalenderauswahl', 'Alle Termine können ausgewählt '
				+ 'werden, aber die Auswahl blockiert oder beschränkt markierter '
				+ 'Tage kann zusätzliche Klärungen bedingen.' ]
			, calTable: [ 'Diese Tabelle dient als Kalender' ]
			, legend: [ 'Legende' ]
			, today: [ 'heutiger Tag' ]
			, holidays: [ 'Feiertag(e)' ]
			, disableddays: [ 'gesperrt' ]
			, blockeddays: [ 'blockiert' ]
			, selecteddays: [ 'ausgewählt' ]
			, jumptoday: [ 'zu heute' ]
			, jumptoselect: [ 'zur Auswahl' ]
			, display: [ 'Anzeige' ]
			, year: [ 'Jahr' ]
			, month: [ 'Monat' ]
			, monthup: [ 'Einen Monat zurück.' ]
			, monthdown: [ 'Einen Monat vor.' ]
			, isWeek: [ 'KW' ]        // first of days
			, isday: [ ', der ' ]
			, isSunday: [ 'So' ]      // last of dayNames
			, selfgrouper: [ 'Abschnittsaktion', 'Gruppen', 'Einzelpersonen'
				, 'Eintrag entfernen', 'Eintrag vereinzeln', 'Dopplungen entfernen'
				, 'alle entfernen' ]
			, uLlang: 'de'
			, timestamp: 'DD.MM.YYYY HH:mm'
			, timepicker: "Stunden,Nacht,Vormittag,Nachmittag,Abend,Minuten" // time picker wording
			, wikiClasses: 'links,mittig,rechts,innen,ignorieren,zeigen,aktiv,'
				+ 'taste,telex,zitat,' + 'falten,einfalten,pinfalten,reiter,'
				+ 'extern,' + 'noaction,uLnofilter,flow,delete,'
				+ 'unformatiert,code,iframe,beispiel,hinweis,dialog,'
				+ 'formular,konsole,logo,' + 'gruppe,gleich,unsichtbar,'
				+ 'umbruch,abstand,trenner,' + 'copyright'
			, wikiTypes: 'versteckt,suche,checkbox,' + 'ikon,bild,'
				+ 'passwort,passwordwiederholung,email,orte,anrede,person,land,anschrift,'
				+ 'lieferadresse,telefon,iban,bankverbindung,umstridentnr,'
				+ 'datei,farbe,web,'
				+ 'zahl,kommazahl,schieber,masseinheit,isoeinheiten,waehrung,mwst,netto,'
				+ 'uhr,eingabe,eingabefeld,liste,knopf,gruppierer,selbstgruppierer,'
				+ 'termin,datum,enddatum,kurzdatum,endkurzdatum,woche,'
				+ 'zeitstempel,zeit,endzeit,kurzzeit,endkurzzeit,datumzeit,'
				+ 'kurzdatumzeit,enddatumzeit,endkurzdatumzeit,zeitraum,'
				+ 'tagedauer,datumtagedauer,'
				+ 'zeitspanne,zeitdauer,kurzzeitdauer,zeittagedauer,'
				+ 'zeitdauerpseudo,datumstagedauerpseudo,wochepseudo,'
			, dynNameChoise: [ "Bitte geben Sie den Namen bzw. Identifikator der "
				+ "neuen Zeile an.", "Mittels dieser Angabe werden die Daten im "
				+ "Programmablauf verwaltet, also angelegt und wieder gefunden."
				, "Die gewählte Bezeichnung muss deshalb eindeutig sein, sie darf "
				+ "daher nicht mehrfach gewählt werden."
			]
			, dynActions: [ 'nur Auswahl:,ausblenden,exportieren,löschen,invertieren'
				, 'auf alle anwenden:,abwählen,einblenden,einfalten,ausfalten' ]
			, dynFilterInfo: [ 'Auswahlsuche', 'Zeilen mit Textinhalten werden angewählt. '
				+ 'Bei Sortierung: Label-Angabe oder nach 1. Wert. (Für Experten: '
				+ 'eingeschlossen in §§"§§ genau Wert; >, <, >= oder <= voranstellbar; '
				+ 'RegExp-Suche möglich)'
			]
			, dynActionInfo: [ 'Aktionsauswahl', 'Wählen Sie bitte die gewünschte Aktion. '
				+ 'Wird diese nicht sofort mit den ausgewählten Zeilen ausgeführt, so kann '
				+ 'sie mit Klick auf //go// ausgelöst werden.<br>'
			]
			, dynMinSelect: [ 'Bitte wählen Sie ein Listenelement aus'
				, 'Sie benötigen mindestens ein Listenelement oder mehr, um damit zu '
				+ 'arbeiten. Wählen Sie Elemente einfach mittels der führenden '
				+ 'Kontrollkästchen (oben links) aus.' ]
			, ownInputExtend: { 'vat': '19%|7%|0%|ownInput'	// these two must be defined
				, '.title': { f: 'weiblich', m: 'männlich', n: 'neutral', b: 'divers' }
			}
			, allowedChars: {
			// short for allowed special char in input fields
				'a-z': 'alle Buchstaben \u00B7 '
				, '-0-9': 'Ziffern mit führendem Minus \u00B7 '
				, '0-9': 'Ziffern \u00B7 '
				, 'gg' : "Umlaute \u00B7 "	// german: ÄÖÜäöüß
				, 'ff' : "französische Schriftzeichen \u00B7 "	// french: éàçèûîô
				, 'ss' : "spanische Schriftzeichen \u00B7 "	// spanish: çñáéíúü¿¡
				, 'pp' : "portugisische Schriftzeichen \u00B7 "	// portuguese: çºªãõáéúóíàò
				, 'ww' : "Auswahl der EU-Schriftzeichen \u00B7 " // -g + -f + -s + -p
				, 'ee' : "alle EU-Schriftzeichen \u00B7 "	// eu Chars
				, 'uu' : "alle Buchstaben, Ziffern sowie +._!~:?#@$&'*,;=/)(   "
				, 'mm' : "alle Buchstaben, Ziffern sowie ._- und diverse selten "
					+ "verwendete Zeichen   "
				, 'xx' : "beliebige Zeichen   "
				, ' ' : "Leerzeichen "
			}
			, inputName: {
				DAY: 'Tag'
				, MONTH: 'Monat'
				, YEAR: 'Jahr'
				, HOUR: 'Stunden'
				, MINUTE: 'Minuten'
				, SECOND: 'Sekunden'
				, HOURS: 'Stundendauer'
				, MINUTES: 'Minutendauer'
				, SECONDS: 'Sekundendauer'
				, DAYS: 'Tagedauer'
				, PHOURS: 'Stundendauer'
				, PMINUTES: 'Minutendauer'
				, SECONDS: 'Sekundendauer'
				, PDAYS: 'Tagedauer'
			}
			, colors: ('schwarz grau altrosa rot grün hellgrün oliv gelb marineblau blau '
			+ 'lila rosa petrol aquamarin silber weiß' ).split( ' ' )
			, months: ("Januar Februar M&auml;rz April Mai Juni "
				+ "Juli August September Oktober November Dezember").split(' ')
			, dayNames: {
				KW: "Es ist die Kalenderwoche ##"
				, Mo: "Montag"
				, Di: "Dienstag"
				, Mi: "Mittwoch"
				, Do: "Donnerstag"
				, Fr: "Freitag"
				, Sa: "Samstag"
				, So: "Sonntag"
			}
			, christHoliday: {
				goodFriday: "Karfreitag"
				, easterSunday: "Ostersonntag"
				, easterMonday: "Ostermontag"
				, theAscension: "Christi Himmelfahrt"
				, whitSunday: "Pfingstsonntag"
				, whitMonday: "Pfingstmontag"
				, corpusChristi: "Fronleichnam"
				, ashWednesday: "Aschermittwoch"
				, fixHoliday: {
					'01-01': "Neujahr",
					'01-06': "Heilige Drei Könige",
					'05-01': "1. Mai",
					'08-15': "Maria Himmelfahrt",
					'10-03': "Tag der Deutschen Einheit",
					'10-31': "Reformationstag",
					'11-01': "Allerheiligen",
					'12-25': "1. Weihnachtstag",
					'12-26': "2. Weihnachtstag"
				}
			}
			, localTypes: {
				date: [ 'Free5', 'DAY', '\\.|-|,|;|:|_|\\/', 'MONTH', '\\.|-|,|;|:|_|\\/'
					, 'YEAR', 'Calendar' ]
				, smalldate: [ 'Free5', 'SMALLYEAR', '\\.|-|,|;|:|_|\\/', 'MONTH'
					, '\\.|-|,|;|:|_|\\/', 'DAY', 'Calendar' ]
				, float: [ 'INT', '{1,7}', ',|\\.|;|:|-|_', 'FIX', '{1,2}', 'Annex' ]
				, ifloat: [ 'INT', '{1,7}', ',|\\.|;|:|-|_', 'FIX', '{1,2}' ]
				, pfloat: [ 'PINT', '{1,7}', ',|\\.|;|:|_', 'PFIX', '{1,2}' ]
				, week: [ 'Free3', 'WEEK', 'KW|-|\\.|,|;|:|_|\\/| |\\u2004', 'YEAR'
				, 'Calendar' ]
				, title: [ '([Damen und Herren|Frau|Herr|Frau Dr.|Herr Dr.|Frau Prof.|'
					+ 'Herr Prof.|ownInput])' ]
				, vat: [ 'float', ';|_|#|\\u20ac\\u2006', '([19%|7%|0%|ownInput])'
					, 'Free2', 'Toggle', 'pfloat', 'Annex', '(\\u20ac\\u2105)?' ]
				, net: [ 'float', ';|_|#|\\u20ac\\u2006', '([19%|7%|0%|ownInput])'
					, 'Free2', 'Toggle', 'pfloat', 'Annex', '(\\u20ac\\u2105)?' ]
				, clock: [ '([Uhr|Alarm|Stop|Count|Merker])?', 'Free0', 'PHOURS'
					, ':|\\.|,|;|-|_', 'PMINUTES', ':|\\.|,|;|-|_', 'PSECONDS'
					, 'Free1', 'Clock' ]
			}
		}
	}
};
	conf.htmlSupport = {
	//@ this is to customize the behavior of the HTML5 support and more: input fields
	//@ treated by useLib will be identified by element.querySelector( '[yyy]' ),
	//@ so it's possible to replace the default by an other selector like: class=XXX,
	//@ id=XXX, pattern=XXX, etc. or false, if the native behavior is favored
	//@ It's also possible to define a own function i. e.
	//@ 	status: {
	//@		required: function( elementNode ){
	//@		var
	//@			l = elementNode.parentNode.getElementsByTagName( 'label' )
	//@			, t = '*</span>';
	//@			if ( l.innerHTML.slice( -t.length ) == t )
	//@				return true;
	//@			return false;
	//@		}
	//@	} 
	//@ ATTENTION: you only need to care about, if you don't use the html standard 
	//@ and not the initialisation via JSON.attrDef!
		type: {
			// standard html5
			file: "[type=file]"
			, email: "[type=email]"
			, fon: "[type=fon]"
			, number: "[type=number]"
			, float: "[type=float]"
			, towns: "[type=towns]"
			, country: "[type=country]"
			, title: "[type=title]"
			, person: "[type=person]"
			, clock: "[type=clock]"
			, currency: "[type=currency]"
			, unit: "[type=unit]"
			, isounits: "[type=isounits]"
			, password: "[type=password]"
			, passwordrepeat: "[type=passwordrepeat]"
			, iban: "[type=iban]"
			, salesident: "[type=salesident]"
			, color: "[type=color]"
			, icon: "[type=icon]"
			, picture: "[type=picture]"
			, url: "[type=url]"
			, pattern: "[pattern]"
			, vat: "[type=vat]"
			, net: "[type=net]"
			, list: "[type=list]"
			, grouper: "[type=grouper]"
			//, range: "[type=range]"		// no support via useLib
			//, search: "[type=search]"	// no support via useLib
			, date: "[type=date]"
			//, 'datetime-local': "[type=datetime-local]"// its defined via localTypes in languages
			, enddate: "[type=enddate]"
			, smalldate: "[type=smalldate]"
			, endsmalldate: "[type=smallenddate]"
			, week: "[type=week]"
			, timestamp: "[type=timestamp]"
			, time: "[type=time]"
			, smalltime: "[type=smalltime]"
			, endtime: "[type=endtime]"
			, endsmalltime: "[type=endsmalltime]"
			, datetime: "[type=datetime]"
			, smalldatetime: "[type=smalldatetime]"
			, enddatetime: "[type=enddatetime]"
			, endsmalldatetime: "[type=endsmalldatetime]"
			, daysperiod: "[type=daysperiod]"
			, datedaysperiod: "[type=datedaysperiod]"
			, timeperiod: "[type=timeperiod]"
			, smalltimeperiod: "[type=smalltimeperiod]"
			, timedaysperiod: "[type=timedaysperiod]"
			, dateauto: "[type=timeauto]"
			, enddateauto: "[type=timeauto]"
			, timeauto: "[type=timeauto]"
			, endtimeauto: "[type=timeauto]"
			, timeperiodpseudo: "[type=timeperiodpseudo]"
			, datedaysperiodpseudo: "[type=datedaysperiodpseudo]"
			, weekpseudo: "[type=weekpseudo]"
		}
		, status: {
			// process organisation status (for whole set of data):
			banned: '.' + conf.cssNS.banned
			, restricted: '.' + conf.cssNS.restricted
			, completed: '.' + conf.cssNS.completed
			, archived: '.' + conf.cssNS.archived
			// workflow flow status:
			, required: "[required]"
			, requested: '.' + conf.cssNS.requested
			, estimated: '.' + conf.cssNS.estimated
			, adjusted: '.' + conf.cssNS.adjusted
			, verified: '.' + conf.cssNS.verified
			// interaction status: focused, changed and error are managed directly
			, disabled: "[disabled]"
			, focused: '.' + conf.cssNS.focused
			, changed: '.' + conf.cssNS.changed
			, invalid: "[invalid]"
			, technical: '.' + conf.cssNS.technical
			// interaction helpers: seacher, completer
			, seacher: "[seacher]"
			, completer: "[completer]"
		}
		, landmarks: {
		//@ it's to define the container selector and a belonging designFrame
		//@ The ARIA role type is used as key.
		//@ REMARK: if you use WIKI container creation selector has to be className!
			navigation: { selector: ".uLmainnavigation", column: 1 }
			// Site-orientated content, such as the title of the page and the logo.
			// Content that is directly related to or expands on the main content.
			, main: { selector: ".uLmainContent", column: 2 }
			// Content that contains the links to navigate (related documents and forms).
			, banner: { selector: ".uLheader"}
			// Content that makes sense in its own right, such as a complete blog post...
			, article: { selector: ".uLarticle", column: 3 }
			// Child content, such as footnotes, copyrights, links to privacy statement...
			, contentinfo: { selector: ".uLcontentinfo" }
			// Supporting content for the main content, but meaningful when separated.
			, complementary: false
			// This section contains a search form to search the site.
			, search: false
		}
	}
	;
	return conf;
})();
