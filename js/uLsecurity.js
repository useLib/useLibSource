/*****
 *
 *    functions for password interactions:
 *    @ encryption and especially the graphical password. The functions
 *    @ are loaded via ajax and included via new Function( thisContent ) 
 *    @ © Dr. Dirk Fischer, use-Optimierung, Köln.
 *    @ Possibly other copyrights reported at the place (as Streamable SHA-3). 
 * 
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *****/
/* CHANGES
   1.06: _getMist() modulo for correct calculation of mist length (clean up fault:-[ 
*/

(function( W, D ) {
   /*****
    *
    *    Start of JSONCSS section
    *
    *    @ To realize cross origin or local file service on data exchange a 
    *    @special technic for the data layer is implemented. 
    *    @ Data can be tranfered via text/css mime type or coded as crypted url
    *    @ path All JSON structures are encoded as base64url and may be crypted
    *    @ via a very good random bytes (CSRNG) form SHAKE128 or SHAKE256.
    *    @ Also the JSON struct may be shuffled to create different cryt strings
    *    @ even on identic data
    * 
    *****/
var
   version = '1.06'
   , conf = { // must be identic to useLibSecureApi.php
      apiPath: useLib.conf.useLibPath + "api/useLibSecureAPI.php"
      , identPath: useLib.conf.useLibPath + "api/useLibIdentAPI.php"
      , corsAPI: 'api/useLibCorsAPI.html'
      , SHA3: 'SHA3_512'      // or 'SHA3_256' := must be same in uLsecurity.php
      , transferLowSize: 512  // minimum of transfered content in chars (blocksize)
      , externalAuth: false   // externalAuth path is defined to ask for auth
      , hashAuth: false       // true := send hash of email and password
      , encryptAuth: false    // true := send data encrypted to externalAuth
      , savedateAuth: true    // true := saving dates & email are saved in file
      , email: false          // predefined email
      // predefined transferBase := may be changed on local store of transferSecret
      , transferBase: "b_2cZt1BvLelSYYOWLah4eeERafYgCLbT83Gx6cfsbDR92Nud4-aqw"
      , encryptSalt: false    // store transferSalt encrypted
      , originalPW: false     // transfer original password string
      , base64crypt: false    // true ? crypted content is stored as base64 : unicode
      , onePadLength: 50000   // length of crypt onePad (more length => more time)
      , passwordQuality: 80
      , filePasswordQuality: 0
      , pseudoCrypt: "just4web"     // password that causes an auto (de)cryption 
      , wikiEnsemble: false // to extend password input with additional features
      // carrying safety conditions via a Wiki form part => principle example
      // '= germanFieldsetName :: englishFieldsetName\n'
      // + '<< germanInformation :: englishInformation>>\n'
      // + '<<<< germanDetail Explaination :: englishDetail Explaination>>>>\n'
      // + '==germanInputName :: englishInputName :: codeName\n'
      // + '<< germanInformation :: englishInformation>>\n'
      // + '<<<< germanDetail Explaination :: englishDetail Explaination>>>>\n'
      // + ':type @@ attributs="content" @@'      // password or text...
      // + ... // and so on information and detail explanation may be left out
      // EXAMPLE:
      // '= E-Mail-Konto :: email account\n'
      // + '<<Integriert die Abfrage der an eine bestimmte E-Mail-Adresse '
      // + 'gesendeten Auftragsdaten :: Integrates the query of the order data '
      // + 'sent to a specific e-mail address>>\n'
      // + '<<<<Diese Abfrage wurde für alle Mitarbeitenden der Abteilung mit '
      // + 'den Ihnen von der IT mitgeteilten Zugangsdaten angelegt. :: This '
      // + 'query has been created for all employees in the department with the '
      // + 'credentials provided to you by IT.>>>>\n'
      // + '== Auftragsablage :: Order deposit :: redirectAUTH\n'
      // + '<<Bitte geben Sie die mitgeteilte E-Mail-Adresse an'
      // + ':: Please enter the provided email to access>>\n'
      // + ':email\n'
      // + '== Zugangspasswort :: Access password :: redirectPW\n'
      // + '<<Bitte geben Sie das mitgeteilte Zugangspasswort an'
      // + ':: Please enter the provided access password>>\n'
      // + ':text @@ pattern="[0-9A-Za-z-w.-]{16,16}" @@\n'
      // Attention: for password protected extern ANAYLSE access predefined are 
      // redirectAUTH and redirectPW (please use them as codeName in case
   }
   , langConf = {
   //@ here may be customized different languages and the wording inside a
   //@ language all structures must have identical named elements
      en: {
         PWdialogAPI: 'Login'
         , PWdialogNEW: 'Confirm'
         , PWdialogENC: 'Encryt'
         , PWdialogDEC: 'Decrypt'
         , hPasswordAPI: [ 'Input of your access data required (Login)'
         , 'To use the service ++your access authorization++ must be checked. For '
         + 'this purpose your e-mail address and a ++highly encrypted++ version '
         + 'of your password are required. A new password is securely confirmed in '
         + 'two steps.'
         , 'Your email address ++belongs++ to your account and is ++fixed in the '
         + 'system++. The password has to be changed during ++registration++ or in '
         + 'case of ++forgetting++ to be confirmed. For this purpose a check code '
         + 'is send to the email address to confirm the password and the '
         + 'address itself over this second contact way to confirm.' ]
         , hPasswordNEW: [ 'Confirm the new password?'
         , 'The control link was clicked, which confirms the password and email '
         + 'address through a second contact channel.\n;;'
         + 'If the new password was not requested by yourself, the email can be '
         + 'ignored.'
         ]
         , hPasswordSTO: [ 'Encrypted local storage content!'
         , 'Previously security-related access data ++personalized++ were encrypted '
         + 'with the password. Also to simplify the input, only the password must '
         + 'be entered. The encryption applies to the following e-mail address:'
         ]
         , hPasswordENC: [ 'Save form file encrypted'
         , 'The HTML file can be protected with all its contents against access '
         + 'by third parties by means of password encryption. There is ++no '
         + 'possibility to change the file password later++ or to request a '
         + 'replacement password. Without this password the text can never be '
         + 'made readable again. Therefore, a ++mnemonic++ should be entered. '
         + 'This appears with every password request.\n\n'
         + 'It is also possible to enter ++no password++. If the pseudo password: ++'
         + conf.pseudoCrypt +'++;;\nis entered, the content will be stored '
         + 'encrypted and compressed, but automatically decrypted when loaded.' ]
         , hPasswordDEC: [ 'Verification of access required!'
         , 'The file has been protected against access by third parties by means '
         + 'of encryption. Only with the correct password it is possible to '
         + 'display the encrypted file content and data!' ]
         , l0Password: [ 'Your email address', 'Your e-mail address is used for '
         + '++identification++ and as a ++contact++ for password confirmation. '
         + 'The link to the confirmation form will be sent to this address.' ]
         , l1Password: [ 'Your password', 'Your password is your secret. Therefore '
         + 'it cannot be known to ++third parties++. Therefore, it is also '
         + 'protected by many security measures and ++highly encrypted++ already '
         + 'during the input.'
         , 'Just enter your password continuously (any characters), the cursor '
         + 'will jump automatically. If you fill in the fields separately, they '
         + 'are automatically joined. By clicking on the ++eye symbol++: the '
         + 'password can be edited as plain text.' + ( useLib.conf.puzzlePassword
         ? 'Especially secure: the innovative puzzle password with a click on '
         + 'the back symbol.' : '' )]
         , lnewPW: [ 'Use as __new__ password.'
         , 'In case of ++registration++ a new password must __always__ be assigned. '
         + 'Also if the password was ++forgotten++, the checkmark must be set!;;\n\n'
         + '++Attention++: An ++email++ will be sent directly! Please check the '
         + 'corresponding e-mail box. The old password will remain until confirm '
         + 'the new one here.' ]    
         , l0Mnemonic: [ 'Password repeat', 'When entering the password for the '
         + 'first time, it must be repeated, because an incorrect entry will make '
         + 'the content unreadable.' ]    
         , l1Mnemonic: [ 'Password mnemonic', 'Any text can be entered as a note. '
         + 'This can be used variably, especially in project work.' ]
         , confirmPersonal: [ "Encrypted ++local storage++ (optional)"
         , "If this option is used, only the administration password needs to be "
         + "entered from the next login onwards. Other fields, including the e-mail "
         + "address %%, are encrypted stored locally in the HTML file of the APP.\n\n"
         + "In addition, this HTML file can be made the only allowed login option. "
         + "For this purpose, the file receives a kind of branding (personalized "
         + "transfer salt), which is also stored. However, the application must "
         + "then __always__ be used by opening this HTML file.\n\n"
         + "Should the application be stored personalized locally?"
         , "as well as entered values for %% etc."
         , "create own secret (transfer salt)." ]
         , newStore: '++Attention:++ The values previously ++personalized++ '
         + 'encrypted with the password must be entered again belonging to the '
         + 'e-mail address: %%. This file must be saved encrypted with the new '
         + 'password, and the new password must then be confirmed again.'
         , confirmNewident: '= Request new identification?\n\n'
         + 'For further processing, the HTML file may be created with a different '
         + 'identification and encryption. For this purpose it should be saved '
         + 'under a different name: %%'
         , confirmEmail: '=The e-mail for password or account confirmation was '
         + 'sent successfully!'
         , errorEmail: '=The e-mail for password or account confirmation couldn\'t '
         + 'unfortunately be send!'
         , confirmWiki: '= The read permission for this file is given!\n\n'
         + 'Decryption is being performed, which may take a moment in certain cases.'
         , errorWiki: '= Unfortunately, there is no read permission for this file '
         + 'with the transmitted access data!'
         , passwordError: '= No security codes in this file that fit to email '
         + 'address and password!'
         , errorDecrypt: '= Faulty decryption\n\nData were received, but they '
         + 'could not be decrypted.'
         , errorDrag: '= No password transfer, because not yet entered!'
         , noAuthorization: '= Access permission failed!\n\n'
         + 'Unfortunately, the verification of your permission to access the '
         + 'contents of this file was not successful.'
         , olderSavings: "= older savings\n\n"
      }
      , de: {
         PWdialogAPI: 'Anmelden' 
         , PWdialogNEW: 'Bestätigen'
         , PWdialogENC: 'Entschlüsseln'
         , PWdialogDEC: 'Verschlüsseln'
         , hPasswordAPI: [ 'Eingabe Ihrer Zugangsdaten erforderlich (Login)!'
         , 'Für die Nutzung des Services muss ++Ihre Zugriffsberechtigung++ geprüft '
         + 'werden. Dazu dienen Ihre E-Mail-Adresse und eine ++hochverschlüsselte++ '
         + 'Version Ihres Passworts. Ein neues Passwort wird in zwei Schritten '
         + 'abgesichert bestätigt.'
         , 'Ihre E-Mail-Adresse ist in Ihrem Konto ++beim System++ fest '
         + '++hinterlegt++. Das Passwort muss bei der ++Registrierung++ oder im '
         + 'Falle des ++Vergessens++ bestätigt werden. Hierzu wird an die '
         + 'E-Mail-Adresse ein Kontrolllink versendet, der das Passwort und die '
         + 'Adresse über diesem zweiten Kontaktweg bestätigt (Zwei-Faktor-Authentifizierung).' ]
         , hPasswordNEW: [ 'Das neue Passwort bestätigen?'
         , 'Der Kontrolllink wurde angeklickt, der das Passwort und die '
         + 'E-Mail-Adresse über einen zweiten Kontaktweg bestätigt.;;\n'
         + 'Wenn das neue Passwort nicht selber angefordert wurde, kann die E-Mail '
         + 'ignoriert werden.'
         ]
         , hPasswordSTO: [ 'Verschlüsselte lokale Speicherung!'
         , 'Zuvor wurden mittels des Passworts sicherheitsrelevante Zugangsdaten '
         + '++personalisiert++ verschlüsselt. Auch zur Erleichterung der Eingabe '
         + 'muss nur das dafür verwendete Passwort eingeben werden. '
         + 'Die Verschlüsselung gilt für die folgende E-Mail-Adresse: '
         ]
         , hPasswordENC: [ 'Datei verschlüsselt speichern'
         , 'Die HTML-Datei kann mit allen Inhalten gegen Zugriff durch Dritte mittels '
         + 'Passwort durch Verschlüsselung geschützt werden. Es gibt ++keine '
         + 'Möglichkeit das Dateipasswort später zu ändern++ oder ein Ersatzpasswort '
         + 'anzufordern. Ohne dieses Passwort kann der Text nie mehr lesbar gemacht ' 
         + 'werden. Es sollte daher eine ++Merkhilfe++ eingetragen werden. '
         + 'Diese erscheint bei jeder Passwortabfrage.\n\n'
         + 'Es kann auch ++kein Dateipasswort++ eingeben werden. Wenn das '
         + 'Pseudo-Passwort: ++'+ conf.pseudoCrypt +'++;;\neingegeben wird, werden '
         + 'die Inhalte zwar verschlüsselt und komprimiert gespeichert, aber beim '
         + 'Laden automatisch entschlüsselt.' ]
         , hPasswordDEC: [ 'Dateipasswort zur Entschlüsselung erforderlich!'
         , 'Die HTML-Datei mit allen Inhalten wurde gegen Zugriff durch fremde Dritte '
         + 'mittels Verschlüsselung geschützt. Nur mit dem richtigen Passwortes ist '
         + 'es möglich, die verschlüsselten Dateiinhalte und -daten anzuzeigen!' ]
         , l0Password: [ 'Ihre E-Mail-Adresse', 'Ihre E-Mail-Adresse dient der '
         + '++Identifikation++ und als als ++Kontakt++ zur Passwortbestätigung. '
         + 'Der einmaliger Prüfcode wird an diese Adresse gesendet.' ]
         , l1Password: [ 'Passwort', 'Das Passwort ist ein Geheimnis. Es '
         + 'darf daher fremden ++Dritten++ nicht bekannt sein. Deshalb wird es auch '
         + 'durch viele Sicherheitsmassnahmen geschützt und bereits während '
         + 'der Eingabe ++höchstwertig verschlüsselt++.'
         , 'Geben Sie das Passwort einfach fortlaufend ein (beliebige Zeichen), '
         + 'der Cursor springt automatisch weiter. Füllen Sie die Felder '
         + 'getrennt aus, werden diese automatisch vereinigt. Bei Klick aufs '
         + '++Auge-Symbol++ kann das Passwort in Klarschrift bearbeitet werden. '
         + ( useLib.conf.puzzlePassword ? 'Besonders sicher: das innovative '
         + 'Puzzle-Passwort mit Klick auf das hintere Symbol.' : '' )]
         , lnewPW: [ 'Als __neues__ Passwort verwenden.'
         , 'Bei der ++Registrierung++ muss __immer__ ein neues Passwort vergeben '
         + 'werden. Auch wenn das Passwort ++vergessens++ wurde, muss der Haken '
         + 'gesetzt werden!;;\n'
         + '++Achtung++: Eine ++E-Mail++ mit einem ++Kontrolllink++ wird direkt '
         + 'versendet!! Bitte das entsprechende E-Mail-Postfach abfragen. Das alte '
         + 'Passwort bleibt erhalten, bis das neue hier bestätigt wurde.' ]    
         , l0Mnemonic: [ 'Passwortwiederholung', 'Bei der ersten Eingabe muss das '
         + 'Passwort wiederholt werden, da eine Fehleingabe den Inhalt unlesbar '
         + 'macht' ]    
         , l1Mnemonic: [ 'Merkhilfe für Passwort', 'Es kann ein beliebiger Text '
         + 'als Hinweis eingegeben werden. Dies kann gerade in der Projektarbeit '
         + 'variabel eingesetzt werden.' ]
         , confirmPersonal: [ "= Verschlüsselte lokale Speicherung (optional)\n\n"
         + "Wird diese Möglichkeit genutzt, muss ab dem nächsten ++Anmelden++ "
         + "(Login) ++nur++ noch das ++Verwaltungspasswort++ eingegeben werden, da "
         + "andere Felder u. a. die E-Mail-Adresse sowie eingetragene Werte für %% "
         + "etc. ++personalisiert++ in der HTML-Datei der APP verschlüsselt "
         + "++lokal gespeichert++ werden.\n\n"
         + "++Zusätzlich++ kann diese HTML-Datei zur ++einzigen++ erlaubten "
         + "++Anmeldemöglichkeit++ gemacht werden. Hierzu erhält die Datei eine Art "
         + "++Brandzeichen++ (personalisierter Transfer-Salt), welches mit "
         + "gespeichert wird. Der Aufruf der Anwendung ++muss dann aber __immer__++ "
         + "durch Öffnen ++dieser HTML-Datei++ erfolgen.\n\n"
         + "Soll die Anwendung personalisiert lokal gespeichert werden?"
         , "eigenes Brandzeichen erzeugen (Transfer-Salt)." ]
         , newStore: '++Achtung:++ Die zuvor mittels des Passworts für die E-Mail-'
         + 'Adresse: %% ++personalisiert++ verschlüsselten Werte müssen erneut '
         + 'eingegeben, diese Datei mit dem neuen Passwort verschlüsselt gespeichert '
         + 'und das neue Passwort anschließend wieder bestätigt werden.'
         , confirmNewident: '= Neue Identifikation anfordern?\n\n'
         + 'Zur Weiterbearbeitung darf die HTML-App mit einer anderen Identifikation '
         + 'und Verschlüsselung angelegt werden. Dazu sollte sie unter einem anderen '
         + 'Namen gesichert werden: %%'
         , confirmWiki: '= Die Leseberechtigung für diese Datei wurde erteilt!\n\n'
         + 'Es erfolgt die Entschlüsselung, was in bestimmten Fälle einen '
         + 'Augenblick dauern kann.'
         , errorWiki: '= Es liegt leider keine Leseberechtigung für diese Datei '
         + 'mit den übermittelten Zugangsdaten vor!'
         , passwordError: '= Zu E-Mail-Adresse und Passwort sind keine passenden '
         + 'Sicherheitskennungen in dieser Datei hinterlegt!'
         , errorDecrypt: '= Fehlerhafte Entschlüsselung\n\nEs wurden zwar Daten'
         + 'empfangen, diese konnten aber nicht entschlüsselt werden.'
         , errorDrag: '= Keine Passwortübergabe, da noch nicht eingegeben!'
         , noAuthorization: '= Zugriffsberechtigung nicht erteilt!\n\n'
         + 'Leider war die Überprüfung Ihrer Zugriffsberechtigung auf die Inhalte '
         + 'dieser Datei nicht erfolgreich.'
         , olderSavings: "= Vorherige Speicherungen\n\n"
      }
   }
   , $ = useLib.$
   , $LS = useLib.$LS
   , lang = useLib.conf ? useLib.conf.lang : 'en'
   , str2url64 = function( s, lenFix ) {
      s = btoa( s ).replace( /\+/g, '-' ).replace( /\//g, '_' ).replace( /=/g, '' );
      return !!lenFix ? ( s + '0000' ).slice( 0, lenFix ) : s;
   }
   , url642str = function( s ) {
      return atob( s.replace( /-/g, '+' ).replace( /_/g, '/' ));
   }
   , getTimeStamp = function() {
      return Math.floor(( new Date()).getTime() / 1000 );
   }
   , getIdentLen = function() {
      return conf.SHA3 === 'SHA3_256' ? 44 : 88;
   }
   , getRandStr = function( l ) {
   var
      c = "-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"
      ;
      return Array.apply( null, Array( l )).map(function() {
         return c[ parseInt( Math.random() * 64 )];
      }).join('');
   }
   , shuffleArray = function( a ) {
   // Randomize array element order in-place.
   // Using Durstenfeld shuffle algorithm.
      for ( var i = a.length - 1; i > 0; i-- ) {
      var
         j = Math.floor( Math.random() * ( i +1 ))
         , temp = a[ i ]
         ;
         a[ i ] = a[ j ];
         a[ j ] = temp;
      }
      return a;
   }
   , getSHA3 = function( str, l, type ) {
   //@ get fitting SHA3 type, if l is given a SHAKE is given
   type = !!!type ? conf.SHA3 : type == 256 ? 'SHA3_256' : 'SHA3_512';
   var
      sponge = useLib.SHA3.init( useLib.SHA3[
         !l ? type : type == 'SHA3_256' ? 'SHAKE128' : 'SHAKE256' 
      ])
      ;
      sponge.absorb( useLib.escapeUnicode( str ));
      return sponge.squeeze( l );
   }
   , str2hash = function( s ) {
      return str2url64( getSHA3( s ), getIdentLen());
   }
   , codeJsonOnePad = function( json, tranferAuth, type ) {
   //@ symmetric en- and decryption with secret key (transferSecret),
   //@ done like One-Time-Pad: encrypt and decrypt via XOR
   //@ REMARK: the secret key is combined with the encrypted password
   var
      onePad = getSHA3( tranferAuth, conf.onePadLength, type )
      , _js = ""
      ;
      for ( var j = 0; j < json.length; j += conf.onePadLength )
          _js += _xor( json.substr( j, conf.onePadLength ), onePad );
      return _js;

      function _xor( a, b ) {
      // int256array comes from getSHA3 based on shake output
      // REMARK: there is a problem with 0 it gets the utf-8 code: 65533...
         for (var i = 0, l = a.length, r = ''; i < l; i++ )
            r += String.fromCharCode(( a.charCodeAt( i ) ^ b[ i ]) & 0xff );
         return r;
      }
   }
   , getBase64Diff = function( s ) {
   //@ just take a part of authorization string. It's length is minimal 86 chars,
   //@ which are base64 coded. The first char defines the diffence between 0 and 63,
   //@ 23 chars are taken. That causes 6 * 23 = 138 bit security.
   //@ Calc on: "-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz"
   var
      d = s.charCodeAt( 0 )   // charcode and calc diffenence on ascii position
      ;
      d = d == 45 ? 0 : d < 58 ? d -47 : d < 91 ? d -54 : d == 95 ? 37 : d -59;
      return s.substr( d, 23 );
   }
   , setTransfer = function( pw ) {
   var
      trans = confSecret.transferSecret || mailStore.transferSecret
      || conf.transferBase
      ;
      if ( pw && trans ) {
         confTrans.ident = str2hash( conf.email + trans );
         confTrans.transfer = str2hash( pw + trans );
         confTrans.email = conf.email;
         if ( confSecret.originalPW )
            confTrans.transfer = confSecret.originalPW;
         $LS( 'uLsecureSession', confTrans, true );
         // this is done if password is given on different ways
         useLib.$CN.remove( $( useLib.cssNS.secDragg ), useLib.cssNS.locked );
      }
   }
   , setConf = function( js, pw ) {
      conf.email = js.email;
      mailStore.email = conf.email;
      js.password = js.password || pw;
      confSecret = getCSSStore( 'uLconfSecret', js.password );
      confTrans.password = js.password;
      setTransfer( js.password );
      if ( conf.originalPW && js.originalPW )   // from secure drop?
         confSecret.originalPW = js.originalPW;
   }
   , setCSSStore = function ( name, s, pw ) {
   // a style area is defined, where differentcontents may be stored as
   // CSS-::before-tags (same as in encrypted CSS transfers via JSONCSS). If
   // a password is given contents will be encypted and need the same
   // password for decryption 
   var
      e = _getStore()
      , r = '#' + name + ':before{content:"'
      ;
      if ( s === false ) // clear content
         s = '';
      else {
         s = JSON.stringify( s );
         s = r + ( !!!pw ? '//' + str2url64( s )
         : str2url64( codeJsonOnePad( s, pw )))
         + '";}\n';
      }
      r = new RegExp( r + '[^"]+";}\n' );
      e.innerHTML = r.test( e.innerHTML ) ? e.innerHTML.replace( r, s )
      : e.innerHTML + s;

      function _getStore() {
      var
         e = $( "uLsecureStore" )
         ;
         if ( !e ) {
            e = D.createElement( 'style' );
            e.id = "uLsecureStore";
            e.type = "text/css";
            e.charset = "utf-8";
            e.innerHTML = '\n';
            D.head.appendChild( e );
         }
         return e;
      }
   }
   , getCSSStore = function( name, pw ) {
   // returns the decrytpted content in secureStore of name
   var
      e = $( "uLsecureStore" )
      ;
      if ( e ) {
         e = e.innerHTML.match( new RegExp( '#' + name + ':before{content:"([^"]+)' ));
         e = e ? e[ 1 ] : false;
         e = !e ? {} : e.slice( 0, 2 ) == '//' ? url642str( e.slice( 2 ))
         : pw ? codeJsonOnePad( url642str( e ), pw ) : true;
         try { e = JSON.parse( e ) } catch (_) { e = {}; }
         return e;
      }
      return {};
   }
   , storePersonal = function( pw ) {
      setCSSStore( 'uLconfEmail', mailStore );
      if ( confSecret )
         setCSSStore( 'uLconfSecret', confSecret, pw );
      // update content of ownHTML
      useLib.ownHTML = useLib.ownHTML.replace(
         /<style\s+id="uLsecureStore"[\s\S]+?<\/style>\n/, ''
      ).replace( /(\n?<\/head>)/, $( "uLsecureStore" ).outerHTML + "$1" );
      storeSelf();
   }
   , storeSelf = function() {
   var
      fN = location.pathname
      , d = D.createElement( 'a' )
      ;
      fN = fN.slice( fN.lastIndexOf( '/' ) +1 );
      d.href = "data:text/plain;charset=utf-8," + encodeURIComponent(
         useLib.ownHTML
      );
      d.download = fN;
      D.body.appendChild( d );
      d.click();
      setTimeout( function() { D.body.removeChild( d ); }, 0); 
   }
   , encodeJson = function( path, verb, json, tHash=false ) {
   //@ ATTENTION: path: 'forgotPW' needs a special treatment! verb:
   //@  'put',json: 'newPassword', tHash: is normaly unknown! But it
   //@ might be send caused by click on eMail-link after forgotPW-call
   //@ from the useLibSecureAPI. If sended ones after at least one or
   //@ configured amount of cronjob control runs, it will be exchanged
   //@ by an generic one
   //@ REMARK: at some point the browser started to sort the key names of
   //@ objects... JSON.stringify could/would detroy the shuffel... It has
   //@ to be done by hand...
   tHash = tHash || confTrans.transfer;
   var
      _json = { uL_path: path        // extend json a bit before shuffle it
         , uL_verb: verb || 'GET'
         , uL_mail: conf.email
         , uL_timestamp: getTimeStamp()
         , uL_json: json || null
         , uL_codes: confSecret || {}
         , uL_auth: getBase64Diff( tHash )
         , uL_mist: ""
      }
      , k = shuffleArray( Object.keys( _json ))
      , salt = str2url64( getRandStr( 32 ), 44 )
      , pathJSON = '{'
      ;
      // create a shuffled json 
      for ( var i = 0; i < k.length; i++ )
         pathJSON += '"'+ k[ i ] +'":'+ JSON.stringify( _json[ k[ i ]]) +',';
      // prepare string and cut the ever same signs: {"uL_ and }
      json = useLib.escapeUnicode( pathJSON.slice( 0, -1 )) +'}';
      json = _getMist( json ).slice( 5, -1 );
      return str2url64( codeJsonOnePad( json, tHash + salt )) + salt;

      function _getMist( js ) {
      //@ REMARK: calc mist length based on multiples of transferLowSize
      var
         l = conf.transferLowSize - ( js.length - 6 ) % conf.transferLowSize
         //l = conf.transferLowSize - js.length + 6
         , p = js.indexOf( '"uL_mist":"' ) + 11
         ;
         return l > 0 ? js.slice( 0, p ) + getRandStr( l ) + js.slice( p )
         : js;
      }
   }
   , submitJSONCSS = function( path, cllbck, verb, json ) {
      askForPassword( path, _set, verb, json );

      function _set( _path ) {
         if ( _path = _path ? _path.replace( /\.php\?/, '.json.css?' ) : false )
            useLib.loadJSONCSS( _path, function( v ) {
               delete useLib.extern[ url ];
               if ( cllbck )
                  cllbck( v );
            }, 12000 );
      }
   }
   , confirm = function( msg, email, fn ) {
      msg = langConf[ lang ][ msg ].replace( /%%/, email || conf.email );
      useLib.confirm( msg, fn );
   }
   , mailStore = getCSSStore( 'uLconfEmail' )
   , confTrans = $LS( 'uLsecureSession', null, true ) || {}
   , confSecret = {}
   , isAsking = false   // for password question
   ;
   conf.email = confTrans.email || mailStore.email || conf.email || false;
   //@ read the CSSstore on loading and ask for possible security codes
   if ( confTrans.password )
      confSecret = getCSSStore( 'uLconfSecret', confTrans.password );

   D.addEventListener( "DOMContentLoaded", function() {
   var
      email = location.href.match( /(&|\?)email=(.+)$/ )
      , api = location.href.match( /(&|\?)code=([^&]+)/ )
      ;
      if ( !!api )
         setTimeout( function() {
            askForPassword( 'NEW', null, 'put', api[ 2 ], email[ 2 ]);
         }, 2000 );
      else if ( !conf.email && email )
         conf.email = email[ 2 ];
   }, false );

   function askForPassword( path, cllbck, verb, json, email=false ) {
   //@ several password types to differentiate: local for crypt and decrypt,
   //@ extern crypt and decrypt, global API NEW and known (forgot) 
   var
      hasStore = $( "uLsecureStore" )
      , intern = 'DEC,ENC'.indexOf( path ) > -1
      ;
      if ( isAsking )
         return;
      else if (( intern || path == conf.externalAuth ) && confTrans.filePassword )
         _callCllbck( true );
      else if (( !intern && path != 'NEW' ) && confTrans.password && confTrans.ident )
         _callCllbck( _getPath());
      else {
      var
         l = langConf[ lang ]
         , wikiHeads = []
         , org = conf.originalPW
         , pq = intern || path == conf.externalAuth
         ? conf.filePasswordQuality : conf.passwordQuality
         , dialog = [ 'span', {}].concat( useLib.getStruct.header(
            l[ 'hPassword'+ ( intern || path == 'NEW' ? path : hasStore ? 'STO'
            : 'API' )])
            , [ hasStore ? [ 'div', { style: "font-weight:bold;text-align:center;width" }
            , conf.email ] : null
            , [ 'form', { CN: useLib.cssNS.dialog, name: "askForPassword"
            , autocomplete: "off" }
               , [ 'fieldset', {}, intern || hasStore ? null
               : useLib.getStruct.input( 'uLemail', l.l0Password
                  , { type: "email", name: 'conf.email'
                     , value: email || conf.email || ''
                     , disabled: path == 'NEW' ? 'disabled' : null, required: true
                  }
               )
               , useLib.getStruct.input( 'uLpassword', l.l1Password
                  , { type: "password", name: "conf."+ ( intern ?'fileP' : 'p' )
                  + "assword", CN: "noTask", required: pq > 0, min: pq }
               )].concat(
                  intern ? [ path == 'DEC' ? null
                  : useLib.getStruct.input( 'uLRepeat', l.l0Mnemonic
                  , { type: "passwordrepeat", name: "pwRepeat", CN: "noTask", min: 0 })
                  , useLib.getStruct.input( 'uLmnemonic', l.l1Mnemonic, {
                  type: "textarea", name: "conf.localMnemonic", S: "height:4.5em;"
                  , rows: 3, value: confTrans.localMnemonic })]
                  : path != 'NEW' && path != conf.externalAuth
                  ? [ useLib.getStruct.radioCheck( 1, 0, l.lnewPW[ 0 ]
                  , '', 'conf.forgot', '', !conf.email, 0, 0, '' )
                  , [ 'div', { IH: useLib.WIKI.toHTML( l.lnewPW[ 1 ])}]
                  ] : null
               )
               , !intern && !hasStore && path != 'NEW' && conf.wikiEnsemble
               ? _getWiki( conf.wikiEnsemble ) : null, useLib.getStruct.buttonRow()
            ]]
         )
         ;
         isAsking = true;
         if ( path == 'ENC' )
            conf.originalPW = true;
         useLib.setModal( 'askForPassword', 'form', 'draggable sizable closable'
         , dialog, l[ 'PWdialog' + intern || path == 'NEW' ? path : 'API' ], '60em'
         , function( js ) {
            if ( js !== false ) {
               if ( hasStore ) {
                  if ( js.conf ) {
                     if ( !!js.conf.forgot ) {
                        useLib.DOM.removeNode( hasStore );
                        isAsking = false;
                        useLib.setMsg( 'hint', l.newStore.replace( /%%/, conf.email )
                        , 0, 6500 );
                        setTimeout( function() {
                           askForPassword( path, cllbck, verb, json );
                        }, 6500 );
                     }
                     else {
                        confTrans.password = js.password;
                        confSecret = getCSSStore( 'uLconfSecret', js.password );
                        setConf( js.conf, js.conf.password );
                        _callCllbck( _getPath());
                     }
                  }
               }
               else if ( intern ) {
                  if ( js.conf ) {
                     confTrans.oriFilePW = confSecret.originalPW;
                     confTrans.filePassword = js.conf.filePassword;
                     confTrans.localMnemonic = js.conf.localMnemonic;
                  }
                  else
                     confTrans.filePassword = "";
                  _callCllbck( true );
               }
               else if ( path == conf.externalAuth ) {
                  if ( js.conf ) {
                     confTrans.email = js.conf.email;
                     confTrans.filePassword = js.conf.password;
                     if ( !conf.hashAuth )
                        confTrans.oriFilePW = confSecret.originalPW;
                  }
                  _callCllbck( !!js.conf );
               }
               else if ( js.conf ) {
                  setConf( js.conf, js.conf.password );
                  if ( js.codes )
                     for ( var n in js.codes )
                        confSecret[ n ] = js.codes[ n ];
                  if ( !!js.conf.forgot ) {
                     if ( wikiHeads.length )
                        _confirmPersonal( js.conf.email );
                     else
                        _getIdentAPI();
                  }
                  else if ( path == 'NEW' ) {   // json == code == tempIdent
                     path = conf.identPath +'?uL='+ json + str2hash( confTrans.password
                     + json );
                     if ( /.css?/.test( conf.identPath ))
                        useLib.loadJSONCSS( path, _callCllbck );
                     else
                        useLib.security.submitCORS( path, _callCllbck, verb, {}, 'WIKI'
                        , true );
                  }
                  else
                     _callCllbck( _getPath());
               }
            }
            conf.originalPW = org;
            isAsking = false;
         });
         if ( intern && !hasStore )
            $( 'uLmnemonic' ).parentNode.style.width = '100%';
      }
      return false;
      
      function _getPath() {
         return conf.apiPath +'?uL='+ confTrans.ident + encodeJson( path, verb, json );
      }

      function _callCllbck( v ) {
         if ( path && useLib.extern[ path ])
            delete useLib.extern[ path ];
         if ( typeof cllbck == 'function' )
            cllbck( v );
      }

      function _getWiki( s ) {
      var
         wS = useLib.wikiSigns
         , a = []
         ;
         s = s.replace( new RegExp( '\n+'+ wS.inform, 'gmi' ), wS.inform ).split( '\n' );
         for ( var i = 0, attr, t, j=0; i < s.length; i++ ) {
            if ( s[ i ][ 0 ] == wS.header ) {
               if ( s[ i ][ 1 ] == wS.header ) {
                  code = ''; 
                  s[ i ] = __getHead( s[ i ], 2 );
                  attr = s[ i +1 ].split( wS.interface );
                  t = attr[ 0 ].slice( 1 ).trim();
                  attr = ! attr[ 1 ] ? {} : JSON.parse( '{"'+ attr[ 1 ].trim(
                  ).replace( /=/g, '":' ).replace( /"\s+/g, '","' ) +'}' );
                  attr.name = 'codes.'+ code;
                  attr.CN = "noTask";
                  attr.type = t;
                  a[ a.length -1 ].push( useLib.getStruct.input( 'uLcS'+ i, s[ i ]
                  , attr, '100%' ));
                  i++;
               }
               else {
                  s[ i ] = __getHead( s[ i ], 1 );
                  wikiHeads.push( s[ i ][ 0 ]);
                  a.push([ 'fieldset', {}, [ 'legend', {}, s[ i ][ 0 ]]
                  , __getInfo( s[ i ][ 1 ], 'inform' )
                  , __getInfo( s[ i ][ 2 ], 'explain' )]);
               }
            }
         }
         return a.length > 1 ? [ 'span', {}].concat( a ) : a[ 0 ];

         function __getHead( _s, c ) {
            _s = _s.trim().slice( c ).split( wS.inform );
            _s[ 0 ] = __getName( _s[ 0 ], c == 2 );
            if ( _s[ 1 ])
               _s[ 1 ] = __getName( _s[ 1 ].slice( 0, -wS.informEnd.length ));
            if ( _s[ 3 ]) {
               _s[ 2 ] = __getName( _s[ 3 ].slice( 0, -wS.explainEnd.length ));
               _s.length = 3; 
            }
            return _s;

         }
         function __getName( _s, CODE=false ) {
            _s = _s.split( wS.append );
            if ( CODE )
               code = _s.slice( -1 )[ 0 ].trim();
            return _s[ lang == 'en' ? 1 : 0 ].trim();
         }
         function __getInfo( _s, t ) {
            return _s ? [ 'span', { CN: useLib.cssNS[ t ], IH: _s }] : null;         
         }
      }

      function _getIdentAPI() {
      var
         tempIdent = getRandStr( getIdentLen())
         , _json = { ident: confTrans.ident, MAC: confTrans.transfer }
         ;
         if ( json )
            _json.uL_forward = { uL_path: path, uL_verb: verb, uL_json: json };
         cllbck( conf.identPath +'?uL='+ tempIdent + encodeJson( 'uLinitPW', verb, _json
         , str2hash( confTrans.password + tempIdent ))
         + '&em='+ btoa( conf.email ) +'&lg='+ lang );
      }

      function _confirmPersonal() {
      var
         msg = langConf[ lang ][ 'confirmPersonal' ].slice( 0 )
         ;
         msg[ 0 ] = useLib.WIKI.toHTML( msg[ 0 ].replace( /%%/, wikiHeads.length ? wikiHeads.join( ', ' ) : '' ))
         + '<div><input type="checkbox" id="_uL_cP_" onchange="useLib.security.'
         + 'conf.ownTransCode=!!this.checked;"><label style="padding-left:1.5em;" for="_uL_cP_">' + msg.pop()
         + '</label></div>';
         useLib.confirm( msg[ 0 ], function( v ) {
            if ( v === true ) {
               if ( conf.ownTransCode ) {
                  v = getRandStr( getIdentLen());
                  if ( !conf.encryptSalt )
                     mailStore.transferSecret = v;
                  else
                     confSecret.transferSecret = v;
               }
               storePersonal();
            }
            _getIdentAPI();
         });
      }
   }

   function getSecrets() {
      if ( !!confTrans.password || confTrans.filePassword )
         return btoa( 'uLSecConf={"email":"' + conf.email + _get( "password" )
         + _get( "originalPW" ) + _get( "filePassword" ) + _get( "localMnemonic" )
         +'"}' );
      useLib.setMsg( 'hint', langConf[ lang ][ 'errorDrag' ], 0, 6500 );

      function _get( t ) {
         return confTrans[ t ] ? '","'+ t +'":"'+ confTrans[ t ] : '';
      }
   }
   function setSecrets( s ) {
      s = atob( s );
      if ( s.slice( 0, 10 ) == 'uLSecConf=' ) {
         setConf( JSON.parse( s.slice( 10 )), false );
         return true;
      }
      return false;

   }

   useLib.security = {
      version: version
      , filePasswordQuality: conf.filePasswordQuality
      , minPW: conf.passwordQuality
      , getStoredEmail: function() { return conf.email || ''; }
      , getPasswordHash: function( pw ) {
         if ( conf.originalPW )
            confSecret.originalPW = pw;
         return str2hash( pw );
      }
      , apiPath: conf.apiPath
      , getRandStr: getRandStr
      , str2hash: str2hash
      , shuffleArray: shuffleArray
      , encodeJson: encodeJson
      , decodeJson: function( json, auth ) {
      var
         auth = !!auth ? auth : confSecret.transfer
         , error = { code: 401, msg: 'errorDecrypt' }
         ;
         try {
            json = JSON.parse( '{"uL_' + codeJsonOnePad( url642str(
               json.slice( 0, -44 )), auth + json.slice( -44 )) +'}' );
            if ( !!json.uL_auth && auth.indexOf( json.uL_auth ) > -1 )
               return json.uL_json;
            else
               return error;
         }
         catch (e) {
            return error;
         }
      }
      , submitJSONCSS: submitJSONCSS
      , cryptWiki: function( wiki, html, cllbck, pseudo=false ) {
      //@ en- and decryt given string all.wiki and hand it to callback function.
      //@ if conf.externalAuth ask for crypt hash an hand over all for integrity  
      //@ check via JSONCSS or iframe useLibCorsAPI.
      //@ The content is (de-)compressed via LZWplus base64 encodeing of the
      //@ encrytion it gets 137% bigger. Therefore it may encryted as ascii stream.
      //@ REMARK: HTML replaces Null and CR (\xfff0 and \n) automatically. They
      //@ have to replaced to correct this. 
      var
         p = wiki.indexOf( '{"useAuth":' )
         , auth = p > -1 ? JSON.parse( wiki.slice( p, wiki.indexOf( '"}', p ) +2 ))
         : false
         , uncrypted = pseudo || p == -1
         ;
         if ( confTrans.decrytkey ) {
            _cryptWiki( s, confTrans.decrytkey );
            return;
         }
         else if (( pseudo || !auth ) && !conf.externalAuth ) {
            auth = {
               useAuth: pseudo ? str2hash( conf.pseudoCrypt )
               : str2url64( getRandStr( getIdentLen()), getIdentLen())
               , uLMnemonic: confTrans.localMnemonic || ""
            };
         }
         else if ( auth.uLMnemonic )
            confTrans.localMnemonic = auth.uLMnemonic;
         if ( auth.useAuth == str2hash( conf.pseudoCrypt ))
            _cryptWiki( wiki, auth.useAuth, -1 );
         else
            askForPassword( conf.externalAuth || ( uncrypted ? 'ENC' : 'DEC' )
            , function( isPW ) {
               if ( isPW ) {
                  if ( confTrans.filePassword == "" )
                     cllbck( wiki );
                  else if ( confTrans.oriFilePW
                  && confTrans.oriFilePW == conf.pseudoCrypt ) {
                     auth.useAuth = str2hash( confTrans.oriFilePW );
                     _cryptWiki( wiki, auth.useAuth, -1 );
                  }
                  else if ( conf.externalAuth ) {
                     if ( auth && confTrans.newident )
                        confirm( 'confirmNewident', confTrans.newident
                        , function( v ) {
                           _externCryptWiki( v === true ? 'NEWIDENT'
                           : auth.useAuth
                           , function( _crypt, msg="" ) { _cryptWiki( wiki, _crypt
                           , true, msg ); });
                        });
                     else
                        _externCryptWiki( auth ? auth.useAuth : 'NEWIDENT'
                        , function( _crypt, msg="" ) { _cryptWiki( wiki, _crypt
                        , true, msg ); });
                  }
                  else {
                     auth.uLMnemonic = confTrans.localMnemonic;
                    _cryptWiki( wiki, auth.useAuth );
                  }
               }
               else
                  cllbck( -1 );
            });
         
         function _externCryptWiki( ident, cllbck ) {
         var
            path = conf.externalAuth +'?ident='+ ident +'&email='+ 
            ( !conf.hashAuth ? confTrans.email : str2hash( confTrans.email ))
            + '&password='+ ( !conf.hashAuth ? confTrans.oriFilePW
            : confTrans.filePassword )
            , verb = ident == 'NEWIDENT' ? 'PUT' : 'GET'
            ;
            if ( /\.css($|\?)/.test( path )) {
               if ( conf.encryptAuth )
                  submitJSONCSS( path, __getCryptWiki, verb, {});
               else
                  useLib.loadJSONCSS( path, __getCryptWiki, 6500 );
            }
            else
               useLib.security.submitCORS( path, __getCryptWiki, verb, {}, 'WIKI'
               , conf.encryptAuth );

            function __getCryptWiki( js ) {
            var
               r = js.code == 200
               ;
               if ( ident != "NEWIDENT" )
                  useLib.setMsg( r ? 'success' : 'error'
                  , langConf[ lang ][ r ? 'confirmWiki' : 'errorWiki' ], 0, 6500 );
               if ( js.msg )
                  useLib.alert( js.msg );
               if ( r ) {
                  js = JSON.parse( js.data );
                  if ( js.decrytkey ) {
                     if ( ident == "NEWIDENT" ) {
                        if ( js.msg )
                           useLib.alert( js.msg );
                        _externCryptWiki( js.decrytkey, cllbck );
                     }
                     else {
                        confTrans.writeright = js.readonly || -1;
                        confTrans.newident = js.newident || false;
                        confTrans.decrytkey = js.decrytkey;
                        $LS( 'uLsecureSession', confTrans, true );
                        cllbck( js.decrytkey, js.msg );
                     }
                  }
               }
            }
         }

         function _cryptWiki( s, crypt, EXTERN=false, msg="" ) {
            crypt = crypt.match(
               new RegExp( '[0-9A-Za-z-_]{'+ getIdentLen() +'}', 'g' )
            );
            if ( p > -1 )
               s = s.slice( s.indexOf( '"}', p ) +2 );
            if ( uncrypted ) {
               if ( !EXTERN || ( EXTERN != -1 && conf.savedateAuth )) {
                  __pushSavings();
                  s = JSON.stringify( conf.saveDates ) + s;
               }
               s = LZWplus.compress( s );
            }
            else
               s = conf.base64crypt ? atob( s )
               : s.replace( /\u240D/g, '\r' ).replace( /\u2400/g, '\0' );
            for ( var i = 0; i < crypt.length; i++ )
               s = codeJsonOnePad( s, ( EXTERN ? '' : confTrans.filePassword )
               + crypt[ i ]);
            if ( uncrypted && ( !confTrans.decrytkey || confTrans.writeright )) {
               cllbck( JSON.stringify( auth ) + ( conf.base64crypt ? btoa( s )
               : s.replace( /\r/g, '\u240D' ).replace( /\0/g, '\u2400' ))
               , confTrans.newident || false );
            }
            else {
               s = LZWplus.decompress( s );
               __tellSavings();
               cllbck( s );
            }
            function __pushSavings() {
               if ( !conf.saveDates )
                  conf.saveDates = [];
               if ( EXTERN && conf.saveDates[ 0 ][ 1 ] == conf.email )
                  conf.saveDates[ 0 ][ 0 ] = Date.now();
               else
                  conf.saveDates.unshift([ Date.now(), conf.email || false ]);
            }
            function __tellSavings() {
               if ( s.slice( 0, 2 ) == '[[' ) {
               var
                  d = s.slice( 0, s.indexOf( ']]' ) +2 )
                  ;
                  s = s.slice( d.length );
                  d = JSON.parse( d );
                  for ( var i = 1, dd = [ d[ 0 ]]; i < d.length -1; i++ ) {
                     if ( !!d[ i ][ 1 ] && d[ i ][ 1 ] != d[ i +1 ][ 1 ])
                        dd.push( d[ i ]);
                     else if ( d[ i ][ 0 ].slice( 0, 10 ) != d[ i +1 ][ 0 ].slice( 0, 10 ))
                        dd.push( d[ i ]);
                  }
                  for ( var i = 0, t = dd.length ? langConf[ lang ].olderSavings : ''
                  ; i < dd.length; i++ )
                     t += useLib.$D.toISO( dd[ i ][ 0 ]) +' '
                     + useLib.$D.toTime( dd[ i ][ 0 ] ) + ( dd[ i ][ 1 ]
                      ? ' by '+ dd[ i ][ 1 ] : '' ) +'\n\n';
                  setTimeout( function() { useLib.alert( msg +'\n\n'+ t ); }, 2000 );
                  conf.saveDates = dd;
               }
            }
         }
      }
      , dragsecure: function( e ) {
      var
         $E = useLib.$E
         ;
         e.draggable = true;
         e.dropable = true;
         $E( e, 'dragover', useLib.call.drop.ondragover );
         $E( e, 'dragenter', useLib.call.drop.ondragover );
         $E( e, 'dragstart', function _dragStart( ev ) {
            ev.dataTransfer.setData( "multipart/mixed", getSecrets());
            return true;
         });
         useLib.call.drop[ 'multipart/mixed' ] = setSecrets;
      }
      , hasCORSAPI: false
      , submitCORS: function( url, cllbck, verb, json, type, SHA3, timeout=4000 ) {
      // type := POST or ANALYSE or CRYPTWIKI
      // SHA3 := true or false
      // 'useLib/useLibSecureAPI.php?uL='
      var
         _json = {}
         ;
         type = type.toUpperCase();
         if ( type == 'EMAIL' )
            _tellPostAPI( JSON.stringify({
               type: type
               , url: url
               , post: 'uL='+ str2url64( useLib.escapeUnicode( JSON.stringify( json )))
               , timeout: timeout
            }));
         else if ( type == 'ANALYSE' ) {
            json = {
               type: verb == 'POST' ||  verb == 'PUT' ? 'SEND' : 'LOAD'
               , url: url
               , post: json
               , timeout: timeout
            };
            _addPreAuth();
            _tellPostAPI( JSON.stringify( json ));
         }
         else if ( type == 'WIKI' ) {
            url = url.split( '?' );
            for( var n in confSecret )
               url[ 1 ] += '&'+ n +'='+ confSecret[ n ];
            _json = {
               type: type
               , url: url[ 0 ]
               , post: url[ 1 ]
               , integrity: useLib.ownHTML.slice( 0, useLib.ownHTML.indexOf(
                  useLib.wikiSigns.engine +'customize' )).replace( 
                  /<style\s+id="uLsecureStore"[\s\S]+?<\/style>\n/, '' )
               , timeout: timeout
            };
            _addPreAuth();
            _tellPostAPI( JSON.stringify( _json ));
         }
         else { // type == 'POST' || 'GET'...
            for( var n in confSecret )
               _json[ n ] = confSecret[ n ];
            if ( SHA3 )
               askForPassword( url, _send, verb, _json );
            else {
               _json.ident = conf.email;
               _json.uL_path = url;
               _json.uL_verb = verb;
               _json.uL_json = json;
               _send( conf.apiPath +'?uL='+ btoa( JSON.stringify( _json )));
            }
         }

         function _send( p ) {
            p = p.split( '?' );
            p[ 0 ] = p[ 0 ].replace( /(\.json)?\.css/, '.php' );
            json = {
               type: type
               , url: p[ 0 ]
               , post: p[ 1 ] +'&lg='+ lang
               , timeout: timeout
            };
            _addPreAuth();
            _tellPostAPI( JSON.stringify( json ));
         }
         function _addPreAuth() {
            __set( 'redirectAUTH' );
            __set( 'redirectPW' );
            __set( 'originalPW' );

            function __set( n ) {
               if ( !!confSecret[ n ])
                  json[ n ] = confSecret[ n ];
            }
         }
         function _getPost( json ) {
            if ( SHA3 ) {
            var
               salt = str2url64( getRandStr( 32 ), 44 )
               ;
               json = { uL_content: json };  // create a framing key
               json = str2url64( codeJsonOnePad(
                  useLib.escapeUnicode( JSON.stringify( json )).slice( 5, -1 )
                  , confTrans.transfer + salt
               )) + salt;
            }
            else
               json = json.constructor === String ? json : JSON.stringify( json );
            return 'uL='+ json;
         }
         function _tellPostAPI( json ) {
         var
            e = $( conf.corsAPI )
            , url = useLib.conf.useLibPath + conf.corsAPI
            ;
if ( !!SHA3 )pAlert( url,json );
            if ( !e && !useLib.security.hasCORSAPI ) {
            var
               e = D.createElement( 'iframe' )
               ;
               e.id = conf.corsAPI;
               e.src = url;
               e.style = "position:absolute;width:0;height:0;border:none;";
               D.body.appendChild( e );
                e.onload = function() {
                  useLib.security.hasCORSAPI = true;
                  _tellPostAPI( json );
               };
               W.addEventListener( "message", __onMsg, false );
            }
            else {// its caused on crossorigin and security a bit tricky...
               e.contentWindow.postMessage( json
               , location.origin == 'null' || location.origin == 'file://'
               ? '*' : location.origin );}
            function __onMsg( ev ) {
               if ( ev.origin == location.origin
               || (( location.origin == 'null' || location.origin == 'file://' ) 
               && useLib.conf.useLibPath.indexOf( ev.origin ) > -1 )) {
                  if ( ev.data == 'active' )
                     useLib.security.hasCORSAPI = true;
                  else {
                  var
                     js = ev.data.slice( 0, 2 ) == "//"
                     ? JSON.parse( atob( ev.data.slice( 2 )).replace(/\n/g,'\\n'))
                     : useLib.security.decodeJson( ev.data )
                     ;
                     // if security defined message display it...
                     if ( js.msg ) {
                        if ( js.code == 401 ) {
                           delete confTrans.password;
                           delete conf.originalPW;
                        }
                        useLib.setMsg( /^20\d/.test( js.code ) ? 'success'
                        : 'error'
                        , langConf[ lang ][ js.msg ] || js.msg, 0, 4500 );
                        js.msg = false;
                     }
pAlert( JSON.stringify( js,null,'  '));
                     cllbck( js );
                  }
               }
               return true;
            }
         }
      }
   };
})( window, document );

(function( W, D ) {
   /*****
    *
    *    Start of the pussle password section
    *
    *    @ A alternative possibility for users to create a very secure password.
    *    @ Its based on the cognitive psycholgie knowlege about the skills of
    *    @ humans. Specially in recognition, filtering and reconstruction of 
    *    @ pictures. Its also a kind of gamification of a very disliked task.
    *    @ The technic creates passwords around 80 bits secure level, at least 
    *    @spread very wide and not to guess!
    * 
    *****/
   var u=useLib,$=u.$,$I=u.$I,$T=u.$T,$Q=u.$Q
   , shuffleArray = useLib.security.shuffleArray
   , languageDef = {
   //@ here may be customized different languages and the wording inside a
   //@ language all structures must have identical named elements
      en: {
         imgLoad: {
            info: [ "&&80% Selection of a photo as a template for the puzzle password&&"
            , "As an alternative to keyboard input, a puzzle password has a "
            + "++increased security++. It's your //secret// ++which photo++ you "
            + "chose.\n\n"
            , "People have the ability to remember ++pictures even in parts++ very well, "
            + "especially when events and habits are related. \n\n"
            + "For the puzzle password the photo becomes ++5 or more //Puzzle pieces//++ "
            + "selected and saved by you //secretly//. Then the input happens always in "
            + "the ever same steps:\n\n"
            + "-A. Picture completion: Five puzzle pieces are dragged in always the same "
            + "order on the correct position in //remembered// photo.\n\n"
            + "-B. enlargement: enlarge a //secret// puzzle piece and hide always the "
            + "the same five //secret// parts of it. It's like ticking a lottery ticket, "
            + "it's hard to guess the //right// ones!\n\n"
            + "-V. If you create several puzzle passwords, there is another //secret// in "
            + "front, because of the selection of the right puzzle image. This increases "
            + "the safety once again!\n\n"
            + "The security is set to ++several small++, easier to remember ++secrets++, "
            + "each of which is very difficult for others to guessed.\n\n"
            , "Of course, a puzzle password is only safe, if others can't guess! All parts "
            + "side by side and simple patterns will reduce safety!\n\n"
            + "* The photo should be best known only to you.\n\n"
            + "* The photo can also only be used to create a password and the original can "
               + "then be deleted directly.\n\n"
            + "* It should contain few continuous line structures, as otherwise the correct "
               + "positioning can be guessed by others if the selection is unfavourable.\n\n"
            ]
            , pwFile: [ "Photo selection", "Please select the ++wished photo++ or a puzzle "
               + "image stored before.\n\n"
               + "If you have already saved a puzzle password, you can display the ++same "
               + "puzzle password++ when changing your computer++ or your browser please "
               + "check box.\n\n"
               + "If necessary, please adjust the desired photo section by moving the photo "
               + "or using the lower slider (scrollbar)."
            ]
            , pwImage: [ "Load a previously saved puzzle image" ]
            , button: [ "Take over photo" ]
         }
         , imgRaster: {
            info: [ "Please select the desired security level"
               , "Sets the number of puzzle pieces and thus the security level of the "
               + "puzzle password."
            ]
            , parts: [ "Number of puzzle pieces"
               , "Depending on the grid and thus ++number of puzzle pieces++ it will also "
               + "be a little ++harder++ for you, but others have to guess from a much "
               + "larger selection! 5 * 7 pieces of the puzzle can be done ++without "
               + "practice++ well."
            ]
            , button: [ "Set security level" ]
         }
         , imgDefine: {
            info: [ "Please select %% puzzle pieces by clicking on the picture"
            , "The ++fixed sequence++ will be defined in the next step. So that the "
               + "positioning __not__ can be ++guessed by others++, but you can "
               + "recognize the parts well, please note:\n\n"
               + "* The parts shouldn't look ++too similar.\n\n"
               + "* There should be __no__ ++continuous++ structures visible.\n\n"
               + "* The parts should be striking for you, so that they can be "
               + "positioned by you in the //imagined// ++picture++."
            ]
            , button: [ "Take over puzzle pieces" ]
         }
         , imgParts: {
            info: [ "Please drag 5 pieces of the puzzle to the corresponding place (piece "
               + "A) in the then valid order."
            , "With ++click++ on the area, you can still show the ++original image++ in "
               + "the background. Later this will no longer be available. \n\n"
               + "It's also a little exercise for you to find the right position in each "
               + "case."
            ]
            , button: [ "Define sequence" ]
         }
         , imgDetail: {
            info: [ "Click the puzzle piece for the enlargement (piece B) and then five "
               + "pieces from this further puzzle!"
            , "Later, when logging in, __all__ these parts must be clicked again in the "
               + "++same order++.\n\n"
               + "The ++selection++ of the puzzle piece shows it enlarged ++in the "
               + "grid++.\n\n"
               + "++Clicks++ on the ++puzzle pieces++ in the grid fade them out and in.\n\n"
            ]
            , button: [ "Take over Enlargement" ]
         }
         , pwLoad: {
            info: [ "Please click on the desired puzzle picture!"
            , "For __every__ puzzle password there is an associated puzzle image. You have "
               + "several defined, which increases the ++security++. It is a secret "
               + "which ++picture++, as well as which ++grid++ you chose, because you "
               + "can mix different levels of security!\n\n"
            + "Please select the picture belonging to this ++password++ and then handle "
               + "the puzzle password with its reminder aid."
            ]
         }
         , pwParts: {
            info: [ "A. Please move five pieces of the puzzle to the correct position!"
            , "The order must be the same as when setting the puzzle password.\n\n"
               + "You should remember the corresponding positions of the original photo "
               + "of the creation."
            ]
            , button: [ "Take over positioning" ]
         }
         , pwDetail: {
            info: [ "B. Click the puzzle piece of the enlargement and the five "
               + "Pieces from the puzzle!"
            , "You should remember the selected missing or hidden parts of the creation."
            ]
            , button: [ "Confirm password" ]
         }
         , buttonBefore: [ "back" ]
         , savePassword: [ "Save the pieces in the puzzle image file."
            , "You can and should save //by hand// the puzzle image with the pieces of "
               + "the puzzle, so that you can load it on another computer or when using "
               + "another browser. Only then the puzzle pieces can be displayed there!\n\n"
               + "By selecting the puzzle graphic, e.g. right mouse button, you can "
               + "store, copy or even send it to yourself."
         ]
         , buttonSave: [ "store" ]
         , newPW: [ "Create new puzzle password and add it to puzzle image."
            , "You can create another puzzle password here. The old remains, because the "
               + "new puzzle picture is simply attached to the old one. You later select "
               + "the //correct// puzzle image in an additional process. This additional "
               + "secret further increases security." 
         ]
         , buttonNew: [ "New password" ]
         , msg: {
            missingPhoto: [ "Please select a photo first." ]
            , maxParts: [ "You have already selected %% parts!" ]
            , lessParts: [ "Please select %% more part(s)!" ]
            , missingDetail: [ "Please select the part for the enlargement!" ]
            , unsecurePattern: [ "Attention, the pattern you have just chosen may be "
               + "unsafe due to its regularity!"
            ]
            , unequalPasswords: [ "The two entries of the puzzle password are "
               + "unfortunately different!"
            ]
            , savePassword: [ "Puzzle image for saving", "[ Simply close this window after "
               + "saving, copying, sending etc. the image (e.g. right mouse button)! ]"
            ]
         }
      }
      , de: {
         imgLoad: {
            info: [ "&&80% Auswahl eines Fotos als Vorlage fürs Puzzle-Passwort&&"
            , "Ein Puzzle-Passwort hat alternativ zur Tasttatureingabe eine "
            + "++erhöhte Sicherheit++. Es ist Ihr //Geheimnis// ++welches Foto++ Sie gewählt "
            + "haben.\n\n"
            + "Menschen haben die Fähigkeit ++Bilder auch in Teilen++ sehr gut zu erinnern, "
            + "insbesondere wenn Ereignisse und Gewohnheiten damit verknüpft sind.\n\n"
            + "Für das Puzzle-Passworts werden aus dem Foto ++5 oder mehr //Puzzleteile//++ "
            + "von Ihnen //geheim// ausgewählt und gespeichert. Danach erfolgt die "
            + "Eingabe immer in exakt den gleichen Schritten:\n\n"
            + "-A. Bildergänzung: Fünf Puzzleteile in immer gleicher Reihenfolge auf die "
               + "richtige Position im //erinnerten// Foto ziehen.\n\n"
            + "-B. Vergrößerung: Ein //geheimes// dieser Puzzleteile vergrößern und immer die "
               + "selben fünf //geheimen// Teile davon ausblenden. Das ist wie beim Lottoschein "
               + "ankreuzen, da sind die //Richtigen// ja auch schwer zu erraten!\n\n"
            + "-V. Erzeugen Sie mehrere Puzzle-Passwörter, liegt vorgelagert noch ein weiteres "
               + "Geheimnis in der Auswahl des richtigen Puzzlebildes. Dies erhöht "
               + "die Sicherheit nochmals deutlich!\n\n"
            + "Die Sicherheit wird also auf ++mehrere kleine++, leichter zu merkende "
            + "++Geheimnisse++ aufgeteilt, von denen jedes für sich für andere sehr schwer zu "
            + "erraten ist.\n\n"
            , "Natürlich ist ein Puzzle-Passwort nur dann sicher, wenn Andere es "
            + "nicht erraten können! Alle Teile nebeneinander und einfache Muster "
            + "verringern die Sicherheit!\n\n"
            + "* Das Foto sollte am besten nur Ihnen gut bekannt sein.\n\n"
            + "* Das Foto kann auch nur zur Passworterstellung gemacht und das Original "
               + "dann wieder direkt gelöscht werden.\n\n"
            + "* Es sollte wenige durchgängige Linienstrukturen enthalten, da sonst "
               + "evtl. bei ungünstiger Auswahl die richtige Positionierung durch Andere "
               + "erraten werden kann"
            ]
            , pwFile: [ "Fotoauswahl", "Wählen Sie bitte das ++gewünschte Foto++ oder ein "
               + "gesichertes Puzzlebild an.\n\n"
               + "Haben Sie bereits ein Puzzle-Passwort gesichert, können Sie so bei "
               + "++Wechsel des Computers++ oder auch des Internet-Programms (Browsers) "
               + "das ++selbe Puzzle-Passwort++ anzeigen (Bitte Haken setzen!).\n\n"
               + "Stellen Sie bitte ggf. den gewünschten Fotoausschnitt durch Verschieben "
               + "des Fotos oder mittels des unteren Schiebers (Scrollbar) ein."
            ]
            , pwImage: [ "Ein bereits zuvor gesichertes Puzzlebild laden" ]
            , button: [ "Foto übernehmen" ]
         }
         , imgRaster: {
            info: [ "Bitte wählen Sie die gewünschte Sicherheitsstufe"
               , "Hiermit legen Sie die Anzahl der Puzzleteile und damit die "
               + "Sicherheitsstufe des Puzzle-Passworts fest."
            ]
            , parts: [ "Puzzleteileanzahl"
               , "Je nach Raster und damit ++Anzahl der Puzzleteile++ wird es zwar auch "
               + "für Sie etwas ++schwerer++, aber andere müssen aus einer viel größeren "
               + "Auswahl raten! 5 * 7 Puzzleteile kann man ++ohne Übung++ gut bewältigen." ]
            , button: [ "Sicherheitsstufe festlegen" ]
         }
         , imgDefine: {
            info: [ "Bitte wählen Sie %% Puzzleteile durch Anklicken im Bild an"
               , "Die ++feste Reihenfolge++ wird erst im nächsten Schritt festgelegt. "
               + "Damit die Positionierung __nicht__ durch Andere ++erraten++ werden kann, "
               + "Sie die Teile aber gut wieder erkennen, bitte beachten:\n\n"
               + "* Die Teile dürfen sich nicht ++zu ähnlich++ sehen.\n\n"
               + "* Es sollten __keine__ ++durchgängigen++ Strukturen erkennbar sein.\n\n"
               + "* Die Teile sollten für Sie markant sein, damit sie deren Lage im "
               +" //vorgestellten// ++Foto erinnern++ können.\n\n"
            ]
            , button: [ "Puzzleteile übernehmen" ]
         }
         , imgParts: {
            info: [ "Bitte ziehen Sie 5 Puzzleteile in der dann gültigen Reihenfolge "
               + "auf den zugehörigen Platz (Teil A)."
            , "Durch ++Klick++ auf die Fläche, können Sie hier noch das ++Originalbild++ im "
               + "Hintergrund einblenden. Später ist dieses nicht mehr vorhanden.\n\n"
               + "Auch eine kleine Übung für Sie, ob Sie die jeweils richtige Position "
               + "finden."
            ]
            , button: [ "Reihenfolge festlegen" ]
         }
         , imgDetail: {
            info: [ "Klicken Sie das Puzzleteil für die Vergrößerung (Teil B) und dann "
               + "fünf Teile aus diesem weiteren Puzzle an!"
            , "Später müssen dann beim Anmelden __alle__ diese Teile wieder in der "
               + "++gleichen Reihenfolge++ angeklickt werden.\n\n"
            + "Die ++Auswahl++ des Puzzleteiles blendet dieses vergrößert ++im Rasterfeld++ "
            + "ein.\n\n"
            + "++Klicks++ auf die ++Puzzleteile++ im Raster blenden diese ++aus und ein++.\n\n"
            ]
            , button: [ "Vergrößerung übernehmen" ]
         }
         , pwLoad: {
            info: [ "Klicken Sie bitte das gewünschte Puzzlebild an!"
            , "Für __jedes__ Puzzle-Passwort gibt es ein zugehöriges Puzzlebild. Sie haben "
               + "mehrere definiert, was die ++Sicherheit erhöht++. Es ist sowohl ein "
               + "++Geheimnis++ welches ++Bild++, als auch welches ++Raster++ Sie gewählt "
               + "haben, denn Sie können unterschiedliche Sicherheitsstufen mischen!\n\n"
            + "Wählen Sie bitte das zu diesem ++Passwort gehörige Bild++ aus und geben dann "
               + "mit dessen Erinnrungshilfe das Puzzle-Passwort ein."
            ]
         }
         , pwParts: {
            info: [ "A. Ziehen Sie bitte fünf Puzzleteile auf die richtige Position!"
            , "Die Reihenfolge muss die gleiche wie bei der Festlegung des Puzzle-Passwortes "
               + "sein.\n\n"
            + "Es sind die zugehörigen Positionen aus dem Originalfoto der Festlegung, "
               + "welche Sie erinnern sollen."
            ]
            , button: [ "Positionierung übernehmen" ]
         }
         , pwDetail: {
            info: [ "B. Klicken Sie das Puzzleteil der Vergrößerung und die fünf "
               + "Teile aus dem Puzzle an!"
            , "Es sind die angewählten fehlenden bzw. ausgeblendeten Teile der Festlegung, "
               + "welche Sie erinnern sollen."
            ]
            , button: [ "Passwort übernehmen" ]
         }
         , buttonBefore: [ "zurück" ]
         , savePassword: [ "Speichern der Teile in der Puzzlebild-Datei."
            , "Sie können und sollten das Bild der Puzzleteile speichern, damit Sie "
            + "dieses ggf. an einem anderen Computer oder beim "
            + "Einsatz eines anderen Internetprogramms (Browser) //von Hand// laden "
            + "können. Nur dann können Ihnen dort die Puzzleteile angezeigt werden!\n\n"
            + "Mit Anwahl der Puzzlegrafik z. B. rechte Maustaste, können Sie diese "
            + "speichern, kopieren oder auch an sich selber senden."
         ]
         , buttonSave: [ "sichern" ]
         , newPW: [ "Neues Puzzle-Passworts erzeugen und zu Puzzlebild hinzufügen."
            , "Sie können hier ein weiteres Puzzle-Passwort erzeugen. Das alte bleibt "
            + "dabei erhalten, weil das neue Puzzlebild einfach an das alte angehängt wird. "
            + "Sie wählen später in einem zusätzlichen Vorgang das //richtige// "
            + "Puzzlebild aus. Durch dieses zusätzliche Geheimnis wird die Sicherheit "
            + "nochmals erhöht." 
         ]
         , buttonNew: [ "Neues Passwort" ]
         , msg: {
            missingPhoto: [ "Bitte wählen Sie erst ein Foto." ]
            , maxParts: [ "Sie haben schon %% Teile ausgewählt!" ]
            , lessParts: [ "Bitte wählen Sie noch %% weitere(s) Teil(e) aus!" ]
            , missingDetail: [ "Bitte wählen Sie das Teil für die Vergrößerung aus!" ]
            , unsecurePattern: [ "Achtung, das von Ihnen gerade gewählte Anwahlmuster "
               + "ist aufgrund seiner Regelmäßigkeit möglicherweise unsicher!" ]
            , unequalPasswords: [ "Die beiden Eingaben des Puzzle-Passwortes sind "
               + "leider ungleich!" ]
            , savePassword: [ "Puzzlebild zur Speicherung"
               , "[ Dieses Fenster nach dem Speichern, Kopieren, Versenden "
               + "etc. des Bildes (z. B. rechte Maustaste) einfach schließen! ]" ]
         }
      }
   } // end of languageDef
   , oilPaintEffect = function( cv, intensity, radius ) {
   var
      w = cv.width
      , h = cv.height
      , ctx = cv.getContext( "2d" )
      , imgData = ctx.getImageData( 0, 0, w, h )
      , pixData = imgData.data.slice( 0 )    // fast copy the data array
      , pixelIntensityCount = []
      , xP = 0
      , yP = 0
      , curMax
      ;
      for ( var i = 0; i < pixData.length; i += 4 )
         // average of rgb = ( r + g + b ) / 3 multiplyed by intensity / 255
         pixData[ i + 3 ] = Math.round(
            intensity * ( pixData[ i ] + pixData[ i + 1 ] + pixData[ i + 2 ] ) / 765
         );
      for ( var i = 0; i < pixData.length; i += 4 ) {
         //@ push a filter square ( 2 * radius edge ) over each pixel
         pixelIntensityCount = [];
         for ( var x = xP - radius < 0 ? 0 : xP - radius       // check filter borders
            , xE = xP + radius > w ? w : xP + radius
            ; x < xE; x++
         ) {
            for ( var y = yP - radius < 0 ? 0 : yP - radius    // check filter borders
               , yE = yP + radius > h ? h : yP + radius
               ; y < yE; y++
            ) {
            var
               ii = ( y * w + x ) * 4
               , intensityVal = pixData[ ii + 3 ]
               ;
               if ( !pixelIntensityCount[ intensityVal ]) {
                  pixelIntensityCount[ intensityVal ] = {
                     val: 1
                     , r: pixData[ ii ]
                     , g: pixData[ ii + 1 ]
                     , b: pixData[ ii + 2 ]
                  }
               }
               else {
                  pixelIntensityCount[ intensityVal ].val++;
                  pixelIntensityCount[ intensityVal ].r += pixData[ ii ];
                  pixelIntensityCount[ intensityVal ].g += pixData[ ii + 1 ];
                  pixelIntensityCount[ intensityVal ].b += pixData[ ii + 2 ];
               }
            }
         }
         pixelIntensityCount.sort( function ( a, b ) {
            return b.val - a.val;
         });
         curMax = pixelIntensityCount[ 0 ].val;
         imgData.data[ i ] = ~~ ( pixelIntensityCount[ 0 ].r / curMax );
         imgData.data[ i + 1 ] = ~~ ( pixelIntensityCount[ 0 ].g / curMax );
         imgData.data[ i + 2 ] = ~~ ( pixelIntensityCount[ 0 ].b / curMax );
         imgData.data[ i + 3 ] = 255;
         if ( ++xP == w ) {      // count actual pixel position
            ++yP;
            xP = 0;
         } 
      }
      ctx.putImageData( imgData, 0, 0 );
      ctx.drawImage( cv, 0, 0 );
   }
   , initCanvas = function( id, w, h ) {
   var
      cv = id ? $( id ) : D.createElement( 'canvas' )
      ;
      cv.width = w;
      cv.height = h;
      return cv;
   }
   , getContent = function( name, next, before ) {
   var
      count = 0
      , header = function() {
         if ( name == 'imgDefine' )
            lang[ name ].info[ 0 ] = lang[ name ].info[ 0 ].replace( /%%|\d/, parts.c );
         return getStruct.header( lang[ name ].info );
      }
      , getRadioCheck = function( cB, label, checked, MC ) {
         return getStruct.radioCheck( cB, false, label, 'uL' + name + ( count++ )
            , 'uL' + name + '[]', 'uL' + name, checked, null, false
            , MC ? 'useLib.password.' + MC : MC
         );
      }
      , getButton = function( label, buttonText, action ) {
      //@ REMARK: the action is assigned to the hole fieldset, so its not nessesary to hit
      //@ the small button
      var
         c = getStruct.input( label + '1', lang[ label ], {})
         ;
         c.pop();
         c.push( getStruct.button( lang[ buttonText ], null ));
         return [ 'fieldset', { MC: action }, c ];
      }
      , getButtonRow = function() {
         return getStruct.buttonRow(
            lang[ name ].button
            , null //lang[ next ].button
            , before ? lang.buttonBefore : null
            , 'useLib.password.' + next + 'Set("next")'
            , 'useLib.password.' + name + 'Set("reset")'
            , useLib.password[ before + 'Set' ] ? 'useLib.password.' + before + 'Set("before")'
            : null
         );
      }
      , getCanvas = function( type, dropId=null ) {
      var
         wh = parts.c * parts.h + puzzleBorder + ( type == 'scroll' ? scrollBar : 0 )
         , id = type == 'top' ? 'uLtop' : 'uLmain'
         ;
         return [ 'div', { id: dropId
            , S: 'position:relative;margin:1.2em auto;border:'
            + scrollBorder + 'px solid black;width:' + wh + 'px;height:'
            + ( type == 'top' ? parts.h + puzzleBorder
            : ( wh + ( type == 'square' ? 0 : 2 * parts.h ))) + 'px;'
            }
            , [ 'div', { S: 'background:' + background + ';width:100%;height:100%;' 
               + 'overflow:' + ( type == 'scroll' ? 'scroll;' : 'hidden;' )
               //@ Apple had a very stupid idea and so scrollbars are not well supported.
               //@ There is a big usability problem that is not solvable here, but it
               //@ needs special treatment in some cases. To avoid browser specific code
               //@ and realize similar browser interaction direct image movement is added
               //+ ( isIpKit ? '-webkit-overflow-scrolling:touch;' : '' )
               , MM: type == 'scroll' ? 'useLib.password.scrollImg(event)' : null
               }
               ,  type == 'top' ? []
               : [ 'canvas', { id: id + 'Canvas'
                  , S: 'width:' + ( parts.c * parts.h + puzzleBorder ) + 'px;'
               }]
            ]
            , [ 'canvas', { id: id + 'Raster'
               , S: 'position:absolute;left:0;top:0;display:none;'
            }]
         ];
      }
      , getPuzzleImg = function( name, type ) {
      var
         wh = parts.h - puzzleBorder
         , rows = type == 'top' ? 1 : parts.c + ( type == 'square' ? 0 : 2 )
         , count = 0
         , divs = []
         , attr
         ;
         for ( var j = 0; j < rows; j++ ) {
            for ( var i = 0; i < parts.c; i++ ) {
               attr = { id: ( type == 'top' ? 'uLtop-' : 'uLmain-' ) + count++
                  , S: 'position:absolute;z-index:1000;'
                  + 'left:' + ( i * parts.h + puzzleBorder )
                  + 'px;top:' + ( j * parts.h + puzzleBorder )
                  + 'px;width:' + wh + 'px;height:' + wh + 'px;'
               };
               if ( type == 'top' ) {
                  attr.S += 'display:none;box-shadow: 6px 6px 6px #666666;';
                  attr.MD = 'useLib.password.down(event,\'' + name + '\',this.id);';
                  attr.MU = 'useLib.password.up(event,\'' + name + '\',this.id);';
                  divs.push( [ 'div', attr, [ 'canvas', { width: wh, height: wh }]]);
               }
               else {
                  attr.S += 'opacity:0;background:' + background
                  + ';box-shadow: inset 0 0 10px #333;';
                  attr.MC = 'useLib.password.click( event,\'' + name + '\', this.id );';
                  divs.push( [ 'div', attr ]);
               }
            }
         }
         return divs;
      }     
      , content = []
      , org = false
      ;
      if ( org ) {
         useLib.call.drop.image = org;
         org = false;
      }
      switch( name ) {
         case 'imgLoad': content = [
            [ 'fieldset', {}
               , getStruct.input( 'uLloadImg', lang[ name ].pwFile
                  , { name: name + 1, type: 'file', accept: 'image/\*'
                     , OC: 'useLib.password.loadImage( event )'
                  }
                  , '100%'
               )
               , getRadioCheck( true, lang[ name ].pwImage[ 0 ], false, null )
            ]
            , getCanvas( 'scroll', 'uLdropIt' )
         ];
         org = useLib.call.drop.image;
         useLib.call.drop.image = function( s ) {
         useLib.password.loadImage({}, s  );
            return true;
         };

         break;
         case 'imgRaster': 
            content = [[ 'fieldset', { CN: useLib.cssNS.inline }
            , [ 'legend', {}, lang[ name ].parts[ 0 ]]
            , [ 'span', { CN: useLib.cssNS.inform }, lang[ name ].parts[ 1 ]]
            , getRadioCheck( false, '35 (5 * 7)', parts.c == 5, 'parts(5)' )
            , getRadioCheck( false, '48 (6 * 8)', parts.c == 6, 'parts(6)' )
            , getRadioCheck( false, '63 (7 * 9)', parts.c == 7, 'parts(7)' )
            ], getCanvas( '' )];
         break;
         case 'imgDefine':
         case 'imgParts':
         case 'pwParts': content = [
            getCanvas( 'top' ).concat( getPuzzleImg( name, 'top' ))
            , getCanvas( '' ).concat( name == 'imgDefine'
               ? getPuzzleImg( name, 'main' ) : [])
         ];
         if ( name == 'pwParts' )
            content.push( getButton( 'newPW', 'buttonNew'
            , 'useLib.password.imgLoadSet("reset")' ));
         break;
         case 'imgDetail': 
         case 'pwDetail':
         content = [
            getCanvas( 'top' ).concat( getPuzzleImg( name, 'top' ))
            , getCanvas( 'square' ).concat( getPuzzleImg( name, 'square' ))
            , getButton( 'savePassword', 'buttonSave', 'useLib.password.saveImage()' )
         ];
         break;
         default: break;
      }
      ;
      return [ // could also be done via css, but... to prevent studid horizontal scrollbar
         [ 'div', { S: 'box-sizing:content-box;overflow-x:hidden;width:100%' }
         ].concat( header().concat( content ))
         , name == 'pwLoad'
         ? getButton( 'newPW', 'buttonNew', 'useLib.password.imgLoadSet("reset")' )
         : getButtonRow()
      ];
   }
   , setChoiseCanvas = function( cv, c, i, type ) {
   var
      wh = parts.h - puzzleBorder
      , e = $( 'uLtop-' + i )
      , _cv = e ? e.firstChild : initCanvas( false, wh, wh )
      ;
      c = $I( c );
      _cv.getContext( "2d" ).drawImage( cv
         , type == 'pw' ? c * wh : $I( c % parts.c ) * parts.h + puzzleBorder
         , type == 'pw' ? 0 : Math.floor( c / parts.c ) * parts.h + puzzleBorder
         , wh, wh
         , 0, 0, wh, wh
      );
      if ( type == 'back' )
         return _cv;
      else
         e.style.display = 'block';
   }
   , createPwCanvas = function( cv ) {
   var
      h = parts.h - puzzleBorder
      , _cv = initCanvas( false, parts.c * h, h )
      , ctx = _cv.getContext( "2d" )
      ;
      ctx.fillStyle = background;
      ctx.fillRect( 0, 0, _cv.width, _cv.height );
      pwData.imgDefine = shuffleArray( pwData.imgDefine );  
      for ( var i = 0; i < parts.c; i++ )
         ctx.drawImage(
            setChoiseCanvas( cv, pwData.imgDefine[ i ], i, 'back' ), i * h, 0
         );
      pwCanvas = _cv;
      createStoreCanvas( _cv );
   }
   , createStoreCanvas = function( cv ) {
   //@ appends a new pwCanvas to storeCanvas if already defined.
   //@ REMARK: resizeing a Canvas clears it to transparent black (see specification!)
   //@ well, it has to be redrawed and then appended
      if ( storeCanvas ) {
      var
         h = storeCanvas.height
         , x = storeCanvas.width - cv.width     // first pixels may stay empty
         , _cv = initCanvas( false, storeCanvas.width - ( x < 0 ? x : 0 ), h + cv.height )
         , ctx = _cv.getContext( "2d" )
         ;
         ctx.drawImage( storeCanvas, x < 0 ? -x : 0, 0, storeCanvas.width, h );
         ctx.drawImage( cv, 0, 0, cv.width, cv.height, x > 0 ? x : 0, h, cv.width, cv.height );
         storeCanvas = _cv;
      }
      else
         storeCanvas = cv;
   }
   , shuffleChoiseCanvas = function() {
   var
      i = parts.c
      , a = []
      ;
      while( i-- ) a.push( i );           // create array with integer values...
         a = shuffleArray( a );           // ... and shuffle it!
      for ( i = 0; i < parts.c; i++ ) {
         setChoiseCanvas( pwCanvas, a[ i ], i, 'pw' );
         a[ i ] = 'uLPot-' + a[ i ];      // give new shuffled id
         $( 'uLtop-' + i ).id = a[ i ];
      }
      storePos = false;
   }
   , setRasterCanvas = function( type ) {
   var
      w = parts.w + puzzleBorder
      , cv = initCanvas(( type == 'top' ? 'uLtop' : 'uLmain' ) + 'Raster', w
         , ( type == 'top' ? parts.h + puzzleBorder
         : ( w + ( type == 'square' ? 0 : 2 * parts.h )))
      )
      ;
      cv.style.display = 'block';
      if ( !rasterCanvas || rasterCanvas.width != w )
         rasterCanvas = _getRasterCanvas( w );
      cv.getContext( "2d" ).drawImage( rasterCanvas, 0, 0 );

      function _getRasterCanvas( w ) {
      var
         _cv = initCanvas( false, w, w + 2 * parts.h )
         , ctx = _cv.getContext( "2d" )
         , wh = parts.h - puzzleBorder
         ;
         ctx.fillStyle = 'white';
         ctx.globalAlpha = '.75';   // show it transparent
         ctx.fillRect( 0, 0, _cv.width, _cv.height );
         for ( var j = 0; j < parts.c + ( type == 'square' ? 0 : 2 ); j++ ) {
            for ( var i = 0; i < parts.c; i++ ) {
               ctx.clearRect(
                  puzzleBorder + i * parts.h
                  , puzzleBorder + j * parts.h
                  , wh, wh
               );
            }
         }
         return _cv;
      }
   }
   , scaleCanvas = function( scroll ) {
   //@ scales the image to fit in the selection canvas area. Becauses of possible raster
   //@ changes the dimensions of canvas and scroll container are corrected also.
   //@ EXLANATION: All images in the first step are fitted to the height of the canvas
   //@ (only scrollLeft then, without scaling by zoom). If the proportions of the image
   //@ samller than the canvas, this has to be corrected means scale is not allowed to
   //@ be less than 1.
   //@ REMARK: the scrollbar causes little problems with two appearing pixels. They are
   //@ cutted via margin-bottom:-2px
   var
      h = parts.h * ( parts.c + 2 ) + puzzleBorder
      , w = parts.w + puzzleBorder
      , scale = Math.max( 1., w / img.width / h * img.height )
      , _cv = initCanvas( false
         , $I( h / img.height * img.width * scale )
         , $I( h * scale )
      )
      , cv = initCanvas( 'uLmainCanvas', scroll ? _cv.width : w, scroll ? _cv.height : h )
      ;
      _cv.getContext( "2d" ).drawImage( img, 0, 0, _cv.width, _cv.height );
      if ( scroll )
         cv.getContext( "2d" ).drawImage( _cv, 0, 0 );
      else
         cv.getContext( "2d" ).drawImage( _cv, imgPos, 0, w, h, 0, 0, w, h );
      cv.style.width = cv.width  + 'px';
      cv.style.height = cv.height + 'px';
      cv.style.marginBottom = '-2px';  // can't explain, why -2px is nessesary!!
      // dimensions could be changed via raster change
      cv.parentNode.parentNode.style.width = ( w + ( scroll ? scrollBar : 0 )) + 'px';
      cv.parentNode.parentNode.style.height = ( h + ( scroll ? scrollBar : 0 )) + 'px';
   }
   , setParts = function( v, d ) {
   var
      h = puzzleWidth - (( v - 5 ) * 5 ) - ( !!d ? puzzleBorder : 0 )
      ;
      parts = { c: v, h: h, w: v * h };
      return parts;
   }
   , lang = languageDef[ useLib.conf.lang ]  // to choose right language
   , URL = W.URL || W.webkitURL
   , img = new Image()
   , imgPos = 0
   , scrollPos = 0
   , rasterCanvas = false
   , storeCanvas = false
   , pwCanvas = false
   , getStruct    // comes from password.getContent 
   , setContent   // ""
   , setMsg       // ""
   , setValue     // ""
   , parts = { c: 5, h: 50, w: 250 }
   , puzzleWidth = 50
   , puzzleBorder = 4
   , background = '#bbbbbb'
   , scrollBar
   , scrollBorder = 1
   , storePos
   , pwData = { imgDefineTop: [], imgDefine: []
      , imgPartsTop: [], imgParts:[], imgDetailTop: -1, imgDetail: []
      , pwPartsTop: [], pwParts:[], pwDetailTop: -1, pwDetail: []
   }
   , pwDataInit = function( type, c ) {
   //@ init pwDataOf Name with -1
      pwData[ type ] = [];
      while( pwData[ type ].length < ( c || 5 ))
         pwData[ type ].push( -1 );
   }
   , pwDataClear = function() {
      for ( var n in pwData )
         if ( n == 'imgDetailTop' || n == 'pwDetailTop' )
            pwData[ n ] = -1;
         else
            pwDataInit( n, n == 'imgDefineTop' ? parts.c : 5 );
   }
   , scrollContent = function( d ) {
      useLib.DOM.scrollIntoView( $Q( D, "form[data-ul=password]", 1 )
      , d == 1 ? 'bottom' : 'top' );
   }
   , checkParts = function( name ) {
   var
      i = pwData[ name ].filter( function( v ){ return v == -1 })
      ;
      if ( i.length > 0 ) {
         lang.msg.lessParts[ 0 ] = lang.msg.lessParts[ 0 ].replace( /%%|\d/, i.length );
         setMsg( lang.msg.lessParts );
      }
      else if ( _checkPattern( pwData[ name ]) > -1 )
         setMsg( lang.msg.unsecurePattern );
      return i.length == 0;

      function _checkPattern( p ) {
      var
         r = true
         , max = parts.c * ( parts.c + ( name.indexOf( 'Detail' ) > -1 ? 0 : 2 ))
         ;
         p = p.sort();
         r = _distance( p ) && _square( p ); // || to be supplemented?
         return r ? -1 : 1;

         function _distance( p ) {
            for ( var i = 1, t = p[ 1 ] - p[ 0 ], c = 1; i < p.length; i++ )
               if ( t == p[ i ] - p[ i - 1 ])
                  c++;
            return c < p.length;
         }
         function _square( p ) {
            for ( var i = 1
               , l = parts.c - 1
               , t0 = p[ 0 ] % parts.c
               , t1 = Math.floor( p[ 0 ] / parts.c )
               , t2 = p[ l ] % parts.c
               , t3 = Math.floor( p[ l ] / parts.c )
               , c = 0
               ; i < l; i++
            )
               if ( p[ i ] % parts.c == t0
               || p[ i ] % parts.c == t2 
               || Math.floor( p[ i ] / parts.c ) == t1
               || Math.floor( p[ i ] / parts.c ) == t3 )
                  c++;
            return c < parts.c - 2;
         }
      }
   }
   , checkComplete = function( pD, e ) {
   var
      i = pD.indexOf( -1 )
      ;
      if ( i < 0 ) {
         lang.msg.maxParts[ 0 ] = lang.msg.maxParts[ 0 ].replace( /%%|\d/, pD.length );
         setMsg( lang.msg.maxParts, e );
      }
      return i;
   }
   , setRaster = function( type ) {
      setRasterCanvas( 'top' );
      setRasterCanvas( type );
   }
   , setImagePW = function( type, pwImg ) {
      setParts( Math.round( pwImg.width / pwImg.height ));
      if ( storeCanvas && pwImg.height < puzzleWidth )
         createStoreCanvas( pwImg );
      else {
         storeCanvas = initCanvas( false, pwImg.width, pwImg.height );
         storeCanvas.getContext( "2d" ).drawImage( pwImg, 0, 0 );
      }
      if ( storeCanvas.height > puzzleWidth ) // pw, pw-5 pw-10
         return useLib.password.pwLoadSet( type );
      else {
         pwCanvas = storeCanvas;
         return useLib.password.pwPartsSet( type );
      }
   }
   ;
   useLib.password = {
   //@ some methods that are triggered by //global// button requests, so they are
   //@ added to a //global// struct useLib.password 
      loadImage: function( ev, src=false ) {
         img.onload = function() {
            img.width = img.naturalWidth;
            img.height = img.naturalHeight;
            scrollContent( 1 );
            if( $( 'uLimgLoad0' ).checked ) {
            var
               cv = initCanvas( 'uLmainCanvas', img.width, img.height )
               ;
               cv.getContext( "2d" ).drawImage( img, 0, 0, cv.width, cv.height );
            }
            else
               scaleCanvas( 1 );
         };
         img.src = src || URL.createObjectURL( ev.currentTarget.files[ 0 ]);
      }
      , saveImage: function() {
      var
         w = window.open( "about:blank" )
         ;
         w.document.write(
            "<title>" + lang.msg.savePassword[ 0 ] + "</title>"
            + "<img alt='" + lang.msg.savePassword[ 0 ]
            + "' src='" + storeCanvas.toDataURL( "image/png" )
            + "'/><p>" + lang.msg.savePassword[ 1 ] + '</p>'
         );
         return false;
      }
      , scrollImg: function( ev ) {
      //@ scrolls the loaded Image on pushing it via on mousemove
      //@ REMARK: at least this is done because of the catastrophic usability of IOS there
      //@ is no scrollbar shown at all! (even on use of -webkit-overflow-scrolling: touch;)
         if ( img.width && ev.buttons === 1 ) {
         var
            co = useLib.DOM.getCoord( ev )
            , d = scrollPos - co.x
            , e = ev.currentTarget
            ;
            if ( d > 10 || d < -10 )
               scrollPos = co.x;
            else {
               imgPos = Math.max( 0, Math.min( imgPos + d * 3, e.scrollWidth - e.clientWidth ));
               e.scrollLeft = imgPos;
               scrollPos += d;
            }
         }
      }
      , parts: function( v ) { setParts( v ); scaleCanvas( 0 ); setRasterCanvas(); }
      , click: function( ev, name, id ) {
      //@ all interactions on parts of mainCanvas
         id = $I( id.split( '-' )[ 1 ]);
         if ( name == 'imgDefine' || name == 'imgDetail' || name == 'pwDetail' ) {
         var
            i = pwData[ name ].indexOf( id )
            ;
            if ( i > -1 ) {
               if ( name != 'imgDefine' ) {
                  pwData[ name ][ i ] = -1;
                  ev.currentTarget.style.opacity = 0;
               }
            }
            else if (( i = checkComplete( pwData[ name ], ev.currentTarget )) > -1 ) {
               pwData[ name ][ i ] = id;
               ev.currentTarget.style.opacity = 1;
               if ( name == 'imgDefine' ) {
                  setChoiseCanvas( $( 'uLmainCanvas' ), id, i );
                  pwData[ name + 'Top' ][ i ] = id;
               }
            }
         }
      }
      , down: function( ev, name, id ) {
      //@ start dragging of parts of topCanvas
         if (( name == 'imgParts' || name == 'pwParts' )
         && ( pwData[ name + 'Top' ].indexOf( id ) > -1
         || checkComplete( pwData[ name + 'Top' ], ev.currentTarget ) > -1 )) {
         //@ start drag of image
         var
            e = $( 'uLmainRaster' )
            , r
            ;
            ev.preventDefault();
            if ( !storePos ) {
               r = useLib.DOM.getRect( e );
               storePos = {};
               storePos.top = r.y - useLib.DOM.getRect( $( 'uLtopRaster' )).y;
            }
            if ( !storePos[ id ])
               storePos[ id ] = ev.currentTarget.style.left;
            useLib.DOM.dragStart( ev, ev.currentTarget, 3
               , - parts.h + 15
               , 0
               , e.offsetWidth - 15
               , e.parentNode.offsetHeight + storePos.top - parts.h + 15
            );
         }
      }
      , up: function( ev, name, id ) {
      //@ click and end dragging of parts of topCanvas (even if dragged into mainCanvas area)
         if ( name == 'imgDefine' ) {
            id = $I( id.split( '-' )[ 1 ]);
            ev.currentTarget.style.display = 'none';
            $( 'uLmain-' + pwData.imgDefine[ id ]).style.opacity = 0;
            pwData.imgDefineTop[ id ] = -1;
            pwData.imgDefine[ id ] = -1;
         }
         else if ( name == 'imgDetail' || name == 'pwDetail' ) {
         var
            inactive = pwData[ name + 'Top' ] == -1
            , isactive = pwData[ name + 'Top' ] == id
            ;
            if ( !inactive && !isactive ) {  // other part is active
               inactive = true;
               $( pwData[ name + 'Top' ]).style.opacity = 1;
            }
            if ( inactive ) {
            var
               w = parts.h - puzzleBorder
               , wh = parts.h + puzzleBorder
               , cv = initCanvas( 'uLmainCanvas', wh, wh )
               ;
               cv.getContext( "2d" ).drawImage( pwCanvas
                  , $I( ev.currentTarget.id.split( '-' )[ 1 ]) * w, 0, w, w
                  , 0, 0, wh, wh
               );
            }
            pwData[ name + 'Top' ] = inactive ? id : -1;
            $( 'uLmainCanvas' ).style.display = inactive ? 'block' : 'none'; 
            ev.currentTarget.style.opacity = inactive ? 0.5 : 1;
         }
         else if ( name == 'imgParts' || name == 'pwParts' ) {
         // center image on best match of drop
         var
            s = ev.currentTarget.style
            , x = $I(( $I( s.left ) / parts.h ))
            , y = $I( Math.max( storePos.top, $I( s.top )) / parts.h ) - 1
            , p = ( '0' + String( y * parts.c + x )).slice( -2 )  // two digits
            , i = pwData[ name + 'Top' ].indexOf( id )   // already dragged? ...
            ;
            if ( i == -1 )
               i = pwData[ name + 'Top' ].indexOf( -1 ); // ... find free index
            if ( i > -1 ) {
               if ( pwData[ name ].indexOf( p ) == -1 ) {   // only if not occupied
                  s.left = ( x * parts.h + puzzleBorder ) + 'px';
                  s.top = ( y * parts.h + puzzleBorder + storePos.top ) + 'px';
                  pwData[ name + 'Top' ][ i ] = id;
                  pwData[ name ][ i ] = p;
               }
               else if ( $I( s.top ) > parts.h ) {
                  _back( id, i );
                  while ( pwData[ name + 'Top' ][ ++i ])    // put back all following
                     _back( pwData[ name + 'Top' ][ i ], i );
               }
            }

            function _back( _id, _i ) {
               if ( storePos[ _id ]) {
                  s = $( _id ).style;
                  s.left = storePos[ _id ];
                  s.top = puzzleBorder + 'px';
                  pwData[ name + 'Top' ][ _i ] = -1;
                  pwData[ name ][ _i ] = -1;
               }
            }
         }
      }
      , imgLoadSet: function( type ){
      var
         c = getContent( 'imgLoad', 'imgRaster' )
         ;
         if ( type == 'reset' )
            img = new Image();
         pwDataClear();
         if ( type == 'getIt' ) {
            setTimeout( _init, 10 );
            return c;
         }
         else {
            setContent( c );
            _init();
         }

         function _init() {
            useLib.call.drop.init( $('uLdropIt'));
            if ( img.width )
               scaleCanvas( 1 );
         }
      }
      , imgRasterSet: function( type ) {
      var
         cv = $( 'uLmainCanvas' )
         ;
         if ( !img.src ) {
            scrollContent( 1 );
            setMsg( lang.msg.missingPhoto );
         }
         else {
            if ( type == 'next' && $( 'uLimgLoad0' ).checked )
               setImagePW( 'reset', img );
            else {
               if ( type != 'before' ) {
                  setParts( 5 );
                  imgPos = cv.parentNode.scrollLeft;           
               }
               setContent( getContent( 'imgRaster', 'imgDefine', 'imgLoad' ));
               $( "uLimgRaster" + ( parts.c - 5 )).checked = true;
               scaleCanvas( 0 );
               setRasterCanvas( '' )
            }
         }
      }
      , imgDefineSet: function( type ) {
      var
         cv = $( 'uLmainCanvas' )
         , _cv = initCanvas( false, cv.width, cv.height )
         , intensity = 55
         , radius = 4
         ;
         _cv.getContext( "2d" ).drawImage( cv, 0, 0 );         // keep canvas
         if ( type == 'next' )
            oilPaintEffect( _cv, intensity, radius );
         setContent( getContent( 'imgDefine', 'imgParts', 'imgRaster' ));
         cv = initCanvas( 'uLmainCanvas', _cv.width, _cv.height );
         cv.getContext( "2d" ).drawImage( _cv, 0, 0 );         // recall canvas
         setRaster( '' );
         pwDataInit( 'imgDefineTop', parts.c );
         pwDataInit( 'imgDefine', parts.c );
      }
      , imgPartsSet: function( type ) {
         if ( type == 'before' || type == 'reset' || checkParts( 'imgDefine' )) {
         var
            cv = $( 'uLmainCanvas' )
            , _cv = initCanvas( false, cv.width, cv.height )
            ;
            _cv.getContext( "2d" ).drawImage( cv, 0, 0 );      // keep canvas
            if ( type == 'next' )
               createPwCanvas( cv );
            setContent( getContent( 'imgParts', 'imgDetail', 'imgDefine' ));
            shuffleChoiseCanvas();
            cv = initCanvas( 'uLmainCanvas', _cv.width, _cv.height );
            cv.getContext( "2d" ).drawImage( _cv, 0, 0 );      // recall canvas
            setRaster( '' );
            $('uLmainCanvas').style.opacity = 0;      // first don't show source photo ...
            $('uLmainRaster').onclick = function() {  // ... but on click in background
            var
               s = $('uLmainCanvas').style
               ;
               s.opacity = s.opacity == '0' ? '0.5' : '0';
            };
            pwDataInit( 'imgPartsTop' );
            storePos = false;
         }
      }
      , imgDetailSet: function( type ) {
         if ( type == 'before' || type == 'reset' || checkParts( 'imgParts' ) ) {
            setContent( getContent( 'imgDetail', 'pwParts', 'imgParts' ));
            setRaster( 'square' );
            shuffleChoiseCanvas();
            pwData.imgDetailTop = -1;
            pwDataInit( 'imgDetail' );
         }
      }
      , pwLoadSet: function( type ) { 
      //@ creates a list of all defined passwordpuzzles to select one
      var
         c = getContent( 'pwLoad', null, null )       // get header of form
         , btn = c.pop()
         , cList = shuffleArray( _analyseStoreCanvas())// get puzzle list and shuffle it
         ;
         for ( var i = 0, dim; i < cList.length; i++ ) {
            dim = 'width:' + cList[ i ].wC + 'px;height:' + cList[ i ].hC + 'px;';
            c.push([ 'div', { S: dim
               + 'margin:0.6em;border:' + scrollBorder + 'px solid black;' }
               , [ 'canvas', { id: 'pwLoad' + i, S: dim  }]
            ]);
         }
         c.push( btn );
         if ( type == 'getIt' ) {
            setTimeout( _init, 100 );
            return c;
         }
         else if ( type == 'before' || type == 'reset' || checkParts( 'imgDetail' )) {
            setContent( c );
            _init();
         }

         function _analyseStoreCanvas() {
         var
            w = storeCanvas.width
            , h = storeCanvas.height
            , data = storeCanvas.getContext( "2d" ).getImageData( 0, 0, w, h ).data
            , pw = [ setParts( 5, 1 ), setParts( 6, 1 ), setParts( 7, 1 )]
            , l = []
            ;
            for ( var y = 0, pos, i; y < h; y += pw[ i ].h ) {
               pos = ( y + 2 ) * w * 4;      // get first pixel in second row
               i = w > pw[ 1 ].w && __check0( pos, 0 ) ? 2
               : w > pw[ 0 ].w && __check0( pos, w - pw[ 0 ].w - 1 ) ? 1
               : 0;
               l.push({ y: y, w: pw[ i ].w, h: pw[ i ].h, c: pw[ i ].c
                  , wC: pw[ i ].w + puzzleBorder * ( pw[ i ].c + 1 )
                  , hC: pw[ i ].h + puzzleBorder * 2
                });
            }
            return l;

            function __check0( p, c ) {
               c *= 4;
               return c >= 0 && data.slice( p + c, p + c + 4 ).join( '' ) != '0000';
            }
         }

         function _init() {
            for ( var i = 0, l = cList, cv, x, y, h; i < l.length; i++ ) {
               x = storeCanvas.width - l[ i ].w;
               h = l[ i ].h;
               cv = initCanvas( 'pwLoad' + i, l[ i ].wC, l[ i ].hC );
               cv.onclick = ( function( x, y, w, h ) {
               // closure to init selected pwCanvas
                  return function(){
                     pwCanvas = initCanvas( false, w, h );
                     pwCanvas.getContext( "2d" ).drawImage(
                        storeCanvas, x, y, w, h, 0, 0, w, h
                     );
                     setParts( Math.round( w / h ));
                     useLib.password.pwPartsSet( 'reset' );
                  };
               })( x, l[ i ].y, l[ i ].w, h );
               for ( var j = 0; j < l[ i ].c; j++ )
               // split storeCanvas to puzzle parts on select canvas
                  cv.getContext( "2d" ).drawImage( storeCanvas
                     , x + j * h, l[ i ].y, h, h
                     , puzzleBorder + j * ( h + puzzleBorder ), puzzleBorder, h, h
                  );
            }
         }
      }
      , pwPartsSet: function( type ) {
      var
         c = getContent( 'pwParts', 'pwDetail', type == 'next' ?'imgDetail' : null )
         ;
         if ( type != 'next' )
            pwDataClear();
         if ( type == 'getIt' ) {
            setTimeout( _init, 10 );
            return [ c ];
         }
         else if ( type == 'next' && pwData.imgDetailTop == -1 )
            setMsg( lang.msg.missingDetail );
         else if ( type == 'before' || type == 'reset' || checkParts( 'imgDetail' )) {
            setContent( c );
            _init();
         }

         function _init() {
            setRaster( '' );
            shuffleChoiseCanvas();
            pwDataInit( 'pwPartsTop' );   
            storePos = false;
         }
      }
      , pwDetailSet: function( type ) {
         if ( type == 'reset' || checkParts( 'pwParts' ) ) {
            setContent( getContent( 'pwDetail', 'password', 'pwParts' ));
            setRaster( 'square' );
            shuffleChoiseCanvas();
            pwData.pwDetailTop = -1;
            pwDataInit( 'pwDetail' );
         }
      }
      , passwordSet: function( type ) {
      var
         r = false
         ;
         if ( pwData.pwDetailTop == -1 )
            setMsg( lang.msg.missingDetail );
         else if ( checkParts( 'pwDetail' )) {
         var
            pw = pwData.pwPartsTop.concat(
               pwData.pwParts, pwData.pwDetailTop, pwData.pwDetail
            ).join( ',' )
            ;
            if ( pwData.imgDetailTop != -1 && pwData.imgPartsTop.concat(
               pwData.imgParts, pwData.imgDetailTop, pwData.imgDetail
            ).join( ',' ) != pw ) {
               lang.msg.unequalPasswords[ 0 ].replace( /:.*/, '' )
               + ': ' + pw;
               setMsg( lang.msg.unequalPasswords );
            }
            else {
               setValue( storeCanvas.toDataURL( "image/png" ), pw );
               r = true;
            }
         }
         return r;
      }
      , getContent: function( _getStruct, _setContent, _setMsg, _setValue, _img ) {
      //@ this is to the interface to the internal useLib methods. The methods are allocated
      //@ to some global variables to be used in the functions of the graphical password
         getStruct = _getStruct;          // toCreate standard useLibStructs
         setContent = function( s ) {
            _setContent( s );             // setContent( useLibStruct )
            scrollContent( -1 );          // start on top!
         };
         setMsg = _setMsg;                // setMsg( msgStruct, elementOfMsg, type )
         setValue = _setValue;            // setValue( pwImg, pwData )
         scrollBar = useLib.DOM.getScrollbarWidth();
         if ( _img )                      // passwortImage from localStorage
            return setImagePW( 'getIt', _img );
         else {
            setParts( 5 );       
            return useLib.password.imgLoadSet( 'getIt' );
         }
      }
   };
})( window, document );

/**
   Streamable SHA-3 for javascript, with no lib/ext dependencies!

   Copyright © 2018  Desktopd Developers PHP script
   © 2019 transscripted by Dr. D. Fischer, Cologne (use-Optimierung)

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

   @license LGPL-3+
   @file
*/

/**
   SHA-3 (FIPS-202) for javascript (arrays of byte values)

   all operations are recreated for arrays of byte values. The given string is converted
   into it in the first step and the 'hash array of byte' is reconverted into a string
   by returning it. Methods on strings are translated into array operations and string
   appends have to be done as concat operations. According to byte length on bit
   operations the results allways have to be masked with &0xff. Stored between arrays
   are copied via a.slice(0). Constructions with list() are done in different steps, if
   nessesary with a temporary keep of a value between. 
   
   Mainly this translation from php is done to have two identic, high quality hashing
   methods on client and server to hash symmetric secrets. Therefore the client side
   does not need such a high performance as the serverside, at least it has.
   At least the performance is quite ok, because all keccak operations do not need
   conversions and native array methods could be used in many cases.

   Based on the php implementation php-sha3-streamable
   Reference: https://notabug.org/desktopd/php-sha3-streamable
   Based on the reference implementations, which are under CC-0
   Reference: http://keccak.noekeon.org/
   
   REMARK: it is nessesary to use a 'old' class definition because ES2015 classes
   are not supported by IE.
   For the same reason Uint8Array is not used (and on performance reasons)
   The 'translation' was done in two steps to make it traceable. First all as simular
   to the original as possible. Second (the javascript to use) eliminate some not
   nessesary variables, function calls and optimization of a few loops.
*/

(function( _root ) {
   function _SHA3 () {
      this.SHA3_224 = 1;
      this.SHA3_256 = 2;
      this.SHA3_384 = 3;
      this.SHA3_512 = 4;
      this.SHAKE128 = 5;
      this.SHAKE256 = 6;
   };
   
   _SHA3.prototype.init = function( type ) {
      var private = this.private;
      switch (type) {
         case this.SHA3_224: return private.__construct (1152, 448, 0x06, 28);   //144
         case this.SHA3_256: return private.__construct (1088, 512, 0x06, 32);   //136
         case this.SHA3_384: return private.__construct (832, 768, 0x06, 48);    //104
         case this.SHA3_512: return private.__construct (576, 1024, 0x06, 64);   //72
         case this.SHAKE128: return private.__construct (1344, 256, 0x1f);       //168
         case this.SHAKE256: return private.__construct (1088, 512, 0x1f);       //136
      }
      throw new Error ('Invalid operation type');
   };
   
   /**
      Feed input to SHA-3 "sponge"
   */
   _SHA3.prototype.absorb = function( data ) {
      var private = this.private
      , input
      ;
      if (PHASE_INPUT != private.phase) {
         throw new Error ('No more input accepted');
      }
      private.inputBuffer = private.inputBuffer.concat( str2array (data));
      while ( private.inputBuffer.length >= private.rateInBytes) {
         input = private.inputBuffer.slice( 0, private.rateInBytes );
         private.inputBuffer = private.inputBuffer.slice( private.rateInBytes );
         for ( var i = 0; i < private.rateInBytes; i++) {
            private.state[i] = (private.state[i] ^ input[i]) & 0xff;
         }
         private.state = keccakF1600Permute (private.state);
         private.blockSize = 0;
      }
      return this;
   };
   
   /**
      Get hash output
   */
   _SHA3.prototype.squeeze = function (length) {
      var private = this.private
      , outputLength = private.outputLength; // fixed length output
      length = length || null;
      if (length && 0 < outputLength && outputLength != length) {
         throw new Error ('Invalid length');
      }
      
      if (PHASE_INPUT == private.phase) {
         private.finalizeInput ();
      }
      
      if (PHASE_OUTPUT != private.phase) {
         throw new Error ('No more output allowed');
      }
      if (0 < outputLength) {
         private.phase = PHASE_DONE;
         return array2str( private.getOutputBytes (outputLength));
      }

      var blockLength = private.rateInBytes
      , output = private.outputBuffer.slice( 0, length )
      , neededLength = length - output.length
      , diff = neededLength % blockLength
      ;
      private.outputBuffer = private.outputBuffer.slice( length );
      if (diff) {
         readLength = ((neededLength - diff) / blockLength + 1)
            * blockLength;
      } else {
         readLength = neededLength;
      }
      
      var read = private.getOutputBytes (readLength);
      private.outputBuffer = private.outputBuffer.concat( read.slice( neededLength ));
      read = output.concat( read.slice( 0, neededLength ));
      // if shake keep int256array  
      return !!length ? read : array2str( read );
   };
   // internally used
   var PHASE_INIT = 1
   , PHASE_INPUT = 2
   , PHASE_OUTPUT = 3
   , PHASE_DONE = 4
   ;
   _SHA3.prototype.private = {
         __construct: function (rate, capacity, suffix, length) {
         if (1600 != (rate + capacity)) {
            throw new Error ('Invalid parameters');
         }
         if (0 != (rate % 8)) {
            throw new Error ('Invalid rate');
         }        
         this.state = (function( c, a ) {
            while(c--)
               a.push(0);
            return a;
         })( 200, [] );
         this.suffix = suffix;
         this.blockSize = 0;
         this.rateInBytes = rate / 8;
         this.outputLength = length || 0;
         this.phase = PHASE_INPUT;
         this.inputBuffer = [];
         this.outputBuffer = [];
         return new _SHA3();
      }
      , finalizeInput: function () {
         this.phase = PHASE_OUTPUT;
         if (0 < this.inputBuffer.length) {
            for ( var i = 0; i < this.inputBuffer.length; i++) {
               this.state[i] = (this.state[i] ^ this.inputBuffer[i]) & 0xff;
            }
            this.blockSize = this.inputBuffer.length;
         }     
         // Padding
         var rateInBytes = this.rateInBytes;
         this.state[this.blockSize] = (this.state[this.blockSize] ^ this.suffix) & 0xff;
         if ((this.suffix & 0x80) != 0
         && this.blockSize == (rateInBytes - 1)) {
            this.state = keccakF1600Permute (this.state);
         }
         this.state[rateInBytes - 1] = this.state[rateInBytes - 1] ^ 0x80;
         this.state = keccakF1600Permute (this.state);
      }
      , getOutputBytes: function (outputLength) {
         // Squeeze
         var output = []
         , blockSize
         ;
         while (0 < outputLength) {
            blockSize = Math.min (outputLength, this.rateInBytes);
            output = output.concat (this.state.slice (0, blockSize));
            outputLength -= blockSize;
            if (0 < outputLength) {
               this.state = keccakF1600Permute (this.state);
            }
         }
         return output;
      }
   };
   _root.SHA3 = new _SHA3();

   // translated methods
   var str2array = function( s ) {
      //for (var i=0, l=s.length, a=[]; i<l; a.push( s.charCodeAt (i++)));
      for (var i=0, l=s.length, a=[]; i<l; a[ i ] = s.charCodeAt( i++ ));
      return a;
   }
   , array2str = function( a ) {
      for (var i=0, l=a.length, s=''; i<l; s+=String.fromCharCode( a[i++]));
      return s;
   }
   , xor = function( a, b ) {
      for (var i=0, aa=[]; i<8; aa[i] = (a[i] ^ b[i++]) & 0xff);
      return aa;
   }
   , and = function( a, b ) {
      for (var i=0, aa=[]; i<8; aa[i] = (a[i] & b[i++]) & 0xff);
      return aa;
   }
   , not = function( a ) {
      for (var i=8, aa=[]; i>=0; i--)
         aa[i] = ~a[i] & 0xff;
      return aa;
   }
   , rotL64Low = function( n ) {
   //@ 64-bit bitwise left rotation (Little endian)
   return [].concat (
         (n[0] << 1) & 0xff ^ (n[7] >> 7)
         , (n[1] << 1) & 0xff ^ (n[0] >> 7)
         , (n[2] << 1) & 0xff ^ (n[1] >> 7)
         , (n[3] << 1) & 0xff ^ (n[2] >> 7)
         , (n[4] << 1) & 0xff ^ (n[3] >> 7)
         , (n[5] << 1) & 0xff ^ (n[4] >> 7)
         , (n[6] << 1) & 0xff ^ (n[5] >> 7)
         , (n[7] << 1) & 0xff ^ (n[6] >> 7)
      );
   }
   , rotL64 = function (n, offset) {
   //@ 64-bit bitwise left rotation (Little endian)
      var shift = offset % 8
      , octetShift = (offset - shift) / 8
      , overflow = 0x00
      ;
      n = n.slice (- octetShift).concat( n.slice( 0, - octetShift));
      for ( var i = 0; i < 8; i++) {
         a = n[i] << shift;
         n[i] = (0xff & a) | overflow;
         overflow = a >> 8;
      }
      n[0] = n[0] | overflow;
      return n;
   }
   ;
   function keccakF1600Permute (state) {
   //@ 1600-bit state version of Keccak's permutation
   var
      lanes = (function( s, c ) {
      //@ REMARK :: s is deleted, but on state used only, so its given back in a...
      var a = [];
         c = c || 1;
         while( s.length )
            a.push( s.splice( 0, c ));
         return a;
      })( state, 8 )
      , R = 1
      , values = [1,2,4,8,16,32,64,128]
      , x, y, idx
      ;
      for ( var round = 0, C, D, temp; round < 24; round++) {
         // θ step
         C = [];
         for ( x = 0; x < 5; x++) {
            // (x, 0) (x, 1) (x, 2) (x, 3) (x, 4)
            C[x] = xor( xor( xor( xor(lanes[x], lanes[x + 5]), lanes[x + 10])
               , lanes[x + 15]), lanes[x + 20]);
         }
         for ( x = 0; x < 5; x++) {
            D = xor( C[(x + 4) % 5], rotL64Low (C[(x + 1) % 5]));
            for ( y = 0, idx; y < 5; y++) {
               idx = x + 5 * y; // x, y
               lanes[idx] = xor( lanes[idx], D);
            }
         }
         C=D=null; 
         // ρ and π steps
         x = 1;
         y = 0;
         for ( var i = 0, current = lanes[1].slice(0), keep; i < 24; i++) {
            keep = x;
            x = y;
            y = (2 * keep + 3 * y) % 5;
            idx = x + 5 * y;
            keep = current.slice(0);
            current = lanes[idx].slice(0);
            lanes[idx] = rotL64 (keep, ((i + 1) * (i + 2) / 2) % 64);
         }
         temp = current = null;
         // χ step
         temp = [];
         for ( var y = 0; y < 5; y++) {
            for ( var x = 0; x < 5; x++) {
               temp[x] = lanes[x + 5 * y].slice(0);
            }
            for ( var x = 0; x < 5; x++) {
               lanes[x + 5 * y] = xor (temp[x]
                  , and (not (temp[(x + 1) % 5]), temp[(x + 2) % 5]));
            }
         }
         temp = null;      
         // ι step
         for ( var j = 0, offset, shift, octetShift; j < 7; j++) {
            R = ((R << 1) ^ ((R >> 7) * 0x71)) & 0xff;
            if (R & 2) {
               offset = (1 << j) - 1;
               shift = offset % 8;
               octetShift = (offset - shift) / 8;
               n = [0,0,0,0,0,0,0,0];
               n[octetShift] = values[shift];
               
               lanes[0] = xor (lanes[0], n);
            }
         }
      }
      return Array.prototype.concat.apply ([], lanes);
   }
})( useLib );

var LZWplus = (function () {
//@ TARGET: compress any UFT-8 Text to a ASCII-String and decompress it too.
//@ The ASCII-String may be encrypted afterward.
//@ The algorithm is based on LZW, but further developed. LZW is quite smart.
//@ I found some explanation here:
//@ https://archive.ph/20130107200800/http://oldwww.rasip.fer.hr/research/compress/algorithms/fund/lz/lz78.html#selection-511.11-513.6
//@ It creates a temporary dictionary list, starting with entries for all single
//@ chars. Each combination of letters gets a new entry. Stored as result is
//@ the number of the entry - single char or list of chars - followed by the last
//@ char of the new combination. This combination gets a new entry in the
//@ dictionary. LZW is optimized for ASCII text. It would be possible to escape
//@ all UTF-8, but this would massiv expand some texts...
//@
//@ It is unknown how long the dictionary list is. It could be on up to 4-byte char
//@ lenght. We have from 1 to 4 byte for the appended char and a unknown byte
//@ number to map the dictionary entry also from 1 to may be up to 8 byte.
//@ SOLUTION: 4 bit in front that tells how many bytes are used: the first 2 bit
//@ contain byte length of entry and the next 2 bit the utf-8 length. Zero means
//@ 1 Byte. There might be added a byte in front for this information, if it not
//@ fit into the first entry byte...
//@ 
//@ There is also a bottle neck on already compressed base64 data. They can't be
//@ compressed more and they are blown up by the base64 coding.
//@ SOLUTION: replace them first by an shorter and replace them back on
//@ decompressing. Compressed they are stored as not encoded string via atob in
//@ front of the compressed text. They are leaded with 2 byte for their amount
//@ and each block with for byte for its length. Those bytes are needed to
//@ separate the blocks on decompression first.
//@
//@ The LZWplus compressed data may be encrypted afterwards. The encryption may be
//@ tried via a brute force attac, therefore are criteria needed that detect a
//@ successful decryption. The LZWplus compressing obfuscates this massiv. In fact
//@ the most different base64 encoded mime type formats have a header which contains
//@ data to detect the format type very easy. At least a small ASCII string or some
//@ other constant data. They are the reason for the 'magic numbers' detecting a
//@ mime type. A attac could use those decoded magic numbers as creteria. Therefore
//@ even this header chars (maximum 40) are cutted and compressed also for
//@ obfuscate them together with the rest of the ASCII string. Bad fore the bad.
var
   int2c = String.fromCharCode
   , int2byte = function( v, fix=false ) {
   //@ get hex value in full length, split into bytes and change to ascii... 
   var
      h = ''
      ;
      v = v.toString( 16 );
      while ( v.length ) {      // ...from end
         h = int2c( '0x'+ ( '0'+ v ).slice( -2 )) + h;
         v = v.slice( 0, -2 );
      }
      return fix ? ( '\0\0\0'+ h ).slice( -fix ) : h;
   }
   , byte2int = function( s ) {
      for ( var i = 0, h = 0; i < s.length; i++ )
         h += ( '0'+ ( s.charCodeAt( i )).toString( 16 )).slice( -2 );
      return Number( '0x'+ h );
   }
   , utf2byte = function( c ) {
      return int2byte( c.charCodeAt( 0 ))
   }
   , byte2utf = function( s ) {
      return int2c( byte2int( s ));
   }
   , base64Char = int2byte( 0x20FF )   // not used UFT-8 char
   ;
   return {
      compress: function( s ) {
      //@ compress any UTF-8 string to LZWplus
      var
         dict = {}            // dictorary
         , entry = 1          // dictorary entry number
         , prefix = ''        // dictorary content on entry number
         , out = ''           // compressed string
         , base64 = []        // store of base64 parts
         ;
         // replace and decode base64 parts
         s = s.replace( /(base64,)([0-9a-zA-Z\/+]{30,}={0,2})/g
         , function( _, b, c ) {
         //@ REMARK: to obfuscate decrytions the header part of decoded base64 the
         //@ first 40 signs are still left as part of the ascii string
            c = atob( c );
            base64.push( int2byte( c.length -40, 4 ) + c.slice( 40 ));
            return b + c.slice( 0, 40 ) + base64Char + ( base64.length -1 )
            + base64Char;
         });
         for ( var i = 0, c; i < s.length; i++ ) {
            c = s.charAt( i );
            if ( dict.hasOwnProperty( prefix + c ))   // fastest test (4-8 times)
               prefix += c;
            else {
               dict[ prefix + c ] = entry++;
               out += _push( dict[ prefix ] || 0, c );
               prefix = '';
           }
         }
         if ( prefix.length )             // could be a rest on string end
            out += _push( dict[ prefix ] || 0, '' );
         return int2byte( base64.length, 2 ) + base64.join( '' ) + out;

         function _push( entry, c ) {
         // compress each pair auf int entry and utf-8 char to ascii string.
         // the first 4 bit contain byte length of entry and utf-8
         // REMARK: there might be added a byte for this information, if
         // it not fit into the first entry byte...
         var
            l = entry > 0x0fffff ? 4 : entry > 0x0fff ? 3 : entry > 0x0f ? 2 : 1
            ;
            c = utf2byte( c );
            entry = ( '\0'+ int2byte( entry )).slice( -l );
            l = ((( l -1 ) << 2 ) + ( c.length -1 )) << 4;
            entry = int2c( l | entry.charCodeAt( 0 )) + entry.slice( 1 );
            return entry + c;
         }
      }
      , decompress: function( s ) {
      // Decompress an LZWplus encrypted string
      var
         dict = [ '' ]                    // dictorary
         , l = byte2int( s.slice( 0, 2 )) // amount of base64 parts
         , stop = 9999999                 // prevent endless while on wrong s
         , base64 = []                    // store of base64 parts
         , out = ""                       // compressed string
         , c = l                          // char
         , entry                          // dictorary entry number
         ;
         // separate and remove base64 parts and encode them again
         s = s.slice( 2 );
         while ( c-- ) {
            entry = byte2int( s.slice( 0, 4 )) +4;
            base64.push( s.slice( 4, entry ));
            s = s.slice( entry );
         }
         while ( --stop > 0 ) {
            s = _pop();
            c = dict[ entry ] + c;
            dict.push( c );
            out += c;
            if ( s.length <= 0 )
               break;
         }
         // replace base64 parts
         if ( l > 0 )
            out = out.replace(
            //@ REMARK: reconstract first 40 signs of the header part of decoded
            //@ base64 and then encode again...
               new RegExp( '(base64,)(.{40})'+ base64Char +'(\\d+)'+ base64Char
               , 'gs' ), function( _, b, p40, c ) {
                  return b + btoa( p40 + base64[ parseInt( c )]);
               }
            );
         return out.replace(/\u0000+$/, '' );   // just in some cases...

         function _pop() {
         // analyse first 4 bit to get length of entry and utf-8 byte (each 0 to 3)
         // and convert them to entry int value and utf-8 char 
         var
            l = s.charCodeAt( 0 )
            , first = int2c( l & 0x0f )
            ;
            l >>= 4;
            c = ( l & 0x03 ) +1;
            entry = ( l >> 2 ) +1;
            l = entry + c;
            c = byte2utf( s.slice( entry, l ));
            entry = byte2int( first + s.slice( 1, entry ));
            return s.slice( l );
         }
      }
   };
})();