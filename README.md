# useLib

Die useLib bildet die Grundlage für software-ergonomisch optimierte Nutzungsoberflächen. Ihre Einbindung - auch in Altsysteme - bewirkt die Einhaltung der Vorgaben der Arbeitsstättenverordnung und diverser Regeln abgeleitet aus verschiedenen Accessibility-Ansätzen, kognitions-psychologischer Forschung, ergonomischer Standards zu Stellteilen und Anzeigen, Usability-Heuristiken und weiterer gesetzlicher Vorgaben.

Die Grundidee ist HTML5 gewissermaßen ergonomisch zu erweitern. Letztlich sind die abgebildeten Interaktionen und Techniken ein Vorschlag für HTML6.

useLibConf enthält alle Konfigurationparameter sowie die erweiterbaren Sprachdefinitionen, derzeit Deutsch und Englisch.

Zusammen mit mit den beiden Scripten uLwiki.js und uLsecurity.js kann die useLib einen wichtigen Beitrag zur Gewährleistung der Digitalen Souveränität auf höchstem Sicherheitsstandard leisten. Dies ist die Voraussetzung für dezentrale Arbeitssysteme mit massiver Minimierung des für den Betrieb notwendigen Energieeinsatzes und Wartungsaufwandes, also für echte Nachhaltigkeit.

Einsatzbeispiele finden sich in der useEditor-APP.de.html. Beliebige Design-Skins können mittels der useSkinner-APP.de.html interaktiv erzeugt werden. Beide Apps sowie die Beispiel-APPs wenden die useLib intensiv an. Dort findet sich auch ein Großteil der Dokumentation.